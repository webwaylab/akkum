<?
define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
$GLOBALS['APPLICATION']->RestartBuffer();
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");

global $USER;
if(!is_object($USER))
    $USER = new CUser;

$result = array();
$result['status'] = false;
$result['message'] = '';

$mode=$_REQUEST["action"];

/*if(check_bitrix_sessid())
{
    $result['status'] = false;
    $result['message'] = "Сессия не действительноа!";
}*/

switch($mode)
{
    case 'login':
        if(!$USER->IsAuthorized())
        {    
            $res = $USER->Login(htmlspecialcharsbx($_POST['email']), htmlspecialcharsbx($_POST['password']), $remember);
            if(empty($res['MESSAGE'])){
                $result['status'] = true;
            }else{
                $result['message'] = strip_tags($res['MESSAGE']);
            }
        }else{
            $result['message']="<font style='color:green'>Вы уже авторизованы!!!</font><br />";
            $result['status'] = true;
        }
    break;

    case 'register':
    
        if(!$USER->IsAuthorized())
        {
            $err = 0;
            $html = "";
            $name = htmlspecialcharsbx($_POST['name']);
            $email = trim($_POST['email']);
            $password = $password2 = trim($_POST['password']);
            $phone = phoneToNumber($_POST['phone']);

            if(strlen($name)==0)
            {
                $html.="Введите имя!<br />";
                $err++;
            }

            if(!preg_match("/^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-]+)*\.([a-zA-Z]{2,6})$/",$email))
            {
                $html.="Введите правильный email<br/> ";
                $err++;
            }

            if(!empty($email))
            {
                $rsUsers = CUser::GetList(($by="EMAIL"), ($order="desc"), Array("=EMAIL" =>$email));
                while($rsUsers->NavNext(true, "f_"))
                {
                    $html.="Введенный email уже есть на сайте!<br />";
                    $err++;
                }
            }

            if(strlen($password)<6 || strlen($password2)<6)
            {
                $html.="Длина пароля должна быть не менее 6 символов!<br />";
                $err++;
            }

            if($password!=$password2)
            {
                $html.="Пароли не совпадают!<br />";
                $err++;
            }
            
            if(!checkPhoneNumber($phone))
            {
                $html.="Введите номер телефона!<br />";
                $err++;
            }
            
            if($err==0)
            {
                global $USER;
                COption::SetOptionString("main","captcha_registration","N");

                $user = new CUser;
                $arFields = Array(
                    "NAME"                  => $name,
                	"LOGIN"             	=> $email,
                	"LID"               	=> SITE_ID,
                	"ACTIVE"            	=> "Y",
                	"PASSWORD"          	=> $password,
                	"CONFIRM_PASSWORD"  	=> $password,
                	"EMAIL"			=> $email,
                    "GROUP_ID"      => array(5),
                    "PERSONAL_PHONE" => $phone
                );
                $ID = $user->Add($arFields);

                $result['status'] = true;
                $result['message'] = "<font style='color:green'>На ваш email высланы регистрационные данные!!!</font><br />";
                $USER->Login($email, $password, 'Y');

                COption::SetOptionString("main", "captcha_registration", "Y");
            }else{
                $result['status'] = false;
                $result['message'] = $html;
            }
        }
    break;

    case 'recovery':
        if(!$USER->IsAuthorized())
        {
            $emailTo = trim(htmlspecialcharsbx($_POST['email']));
            $filter = Array("ACTIVE" => "Y", "=EMAIL"  => $emailTo);
            $user = CUser::GetList(($by="timestamp_x"), ($order="desc"), $filter)->Fetch();
            if(intval($user["ID"]) > 0 && !empty($emailTo))
            {
                $password = mb_substr(md5(uniqid(rand(),true)), 0, 8);

                $USER->Update($user['ID'], Array("PASSWORD" => $password, "CONFIRM_PASSWORD" => $password));
                $USER->SendUserInfo($user['ID'], SITE_ID, "Пароль изменён: ". $password);

                $rsSites = CSite::GetByID("s1");
                $arSite = $rsSites->Fetch();

                $arEventFields = array(
                    "SITE_NAME"           => $arSite['NAME'],
                    "EMAIL"               => $emailTo,
                    "MESSAGE"             => 'Пароль успешно восстановлен. Новый пароль: '. $password,
                    "USER_ID"             => $user['ID'],
                    "LOGIN"               => $user['LOGIN'],
                );
                CEvent::Send('USER_PASS_RECOVERY', $arSite, $arEventFields, "N");

                $result['message'] = "<font style='color:green'>Пароль успешно изменён. На ваш email придет сообщение с необходимыми данными.</font>";
                $result['status'] = true;
            }else{
                $result['message'] = 'Пользователь с такие e-mail адресом не найден.';
            }
        }
    break;

    case 'exit':
        if($USER->IsAuthorized())
        {
            $USER->logout();
            $result['status'] = true;
        }
    break;

    case "change_password":
        if($USER->IsAuthorized())
        {
            $errors=0;
            $html="";
            $rsUser = CUser::GetByID($USER->GetID());
            $arUser = $rsUser->Fetch();

            $salt = substr($arUser['PASSWORD'], 0, (strlen($arUser['PASSWORD']) - 32));
            $realPassword = substr($arUser['PASSWORD'], -32);
            $old_password = md5($salt.$_POST['old-password']);

            if($old_password!=$realPassword)
            {
                $html.="Старый пароль введен не правильно!<br />";
                $errors++;
            }

            $password=htmlspecialcharsbx($_POST['new-password']);
            $password2=htmlspecialcharsbx($_POST['new-password2']);

            if(strlen($password)<6 || strlen($password2)<6){
                $html.="Длина пароля должна быть не менее 6 символов!<br />";
                $errors++;
            }

            if($password!=$password2){
                $html.="Пароли не совпадают!<br />";
                $errors++;
            }

            if($errors==0)
            {
                $cuser = new CUser;
                $arFields = Array(
                    "PASSWORD" => $password,
                );
                $cuser->Update($USER->GetID(), $arFields);
                $result['status'] = true;
                $result['message'] = "<font style='color:green'>Пароль успешно изменен.</font>";
            }else{
                $result['status'] = false;
                $result['message'] = $html;
            }
        }
    break;
}

exit(json_encode($result));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>