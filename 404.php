<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Страница не найдена");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
?>
<div class="error-page">
    <h1>Страница не найдена</h1>
    <br/>
    <p>К сожалению, страница, которую Вы запросили, не была найдена (ошибка 404).<br/>
    <p>Вы можете перейти на <a href="/">главную страницу</a> или воспользоваться <a href="/catalog/battery/">каталогом товаров</a></p>
    <p>Если эта ошибка будет повторяться, обратитесь, пожалуйста, в <a href="/contacts/info/">службу поддержки</a>.</p>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>