<?
//include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Страница не найдена");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
?>

<h1>Страница не найдена</h1>
<br/>
К сожалению, страница, которую Вы запросили, не была найдена.<br/>
Вы можете перейти на <a href="/">главную страницу.</a> или воспользоваться <a href="/catalog/battery/">каталогом товаров</a>.<br/>
Если эта ошибка будет повторяться, обратитесь, пожалуйста, в <a href="/contacts/info/">службу поддержки</a>.<br/><br/>

<br/>
<br/>
<br/>

<?

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>