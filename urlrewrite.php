<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/catalog/masla_i_tehicheskie_jidkosti/([^\\/]+)((/\\?([^@]+))|/\$|\$)#",
		"RULE" => "BRAND_CODE=\$1",
		"ID" => "",
		"PATH" => "/catalog/masla_i_tehicheskie_jidkosti/masla_i_tehicheskie_jidkosti.php",
	),
	array(
		"CONDITION" => "#^/catalog/brand/([^\\/]+)((/\\?([^@]+))|/\$|\$)#",
		"RULE" => "BRAND_CODE=\$1",
		"ID" => "",
		"PATH" => "/catalog/brand/brand.php",
	),
	array(
		"CONDITION" => "#^/catalog/masla/([^\\/]+)((/\\?([^@]+))|/\$|\$)#",
		"RULE" => "BRAND_CODE=\$1",
		"ID" => "",
		"PATH" => "/catalog/masla/index.php",
	),
	array(
		"CONDITION" => "#^/bitrix/services/ymarket/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/bitrix/services/ymarket/index.php",
	),
	array(
		"CONDITION" => "#^/personal/order/#",
		"RULE" => "",
		"ID" => "bitrix:sale.personal.order",
		"PATH" => "/personal/order/index.php",
	),
	array(
		"CONDITION" => "#^/catalog/auto/#",
		"RULE" => "BRAND_CODE=\$1",
		"ID" => "",
		"PATH" => "/catalog/model_filter.php",
	),
	array(
		"CONDITION" => "#^/catalog/#",
		"RULE" => "",
		"ID" => "irbis:catalog",
		"PATH" => "/catalog/index.php",
	),
	array(
		"CONDITION" => "#^/catalog/#",
		"RULE" => "",
		"ID" => "irbis:catalog",
		"PATH" => "/catalog/noheader.php",
	),
	array(
		"CONDITION" => "#^/stati/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/stati/index.php",
	),
	array(
		"CONDITION" => "#^/store/#",
		"RULE" => "",
		"ID" => "bitrix:catalog.store",
		"PATH" => "/store/index.php",
	),
	array(
		"CONDITION" => "#^/news/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/news/index.php",
	),
);

?>