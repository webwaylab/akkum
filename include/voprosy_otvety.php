<div class="questions_answers_wrapper">
	<h1>Вопросы и ответы</h1>
	<div class="questions_answers_body">
		<div class="questions_answers_item">
			<div class="question black fs_18 reg">
				Надо ли заряжать наши АКБ после покупки?
			</div>
			<div class="answer">
				<table class="answer_table">
					<tr>
						<td class="answer_title">Ответ:</td>
						<td class="answer_body">
							Нет, наши аккумуляторы полностью заряжены и готовы к работе.
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="questions_answers_item">
			<div class="question black fs_18 reg">
				Можно ли проверить аккумулятор перед покупкой?
			</div>
			<div class="answer">
				<table class="answer_table">
					<tr>
						<td class="answer_title">Ответ:</td>
						<td class="answer_body">
							Разумеется, и в магазине и при доставке при Вас будет проведена проверка АКБ нагрузочной вилкой.
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="questions_answers_item">
			<div class="question black fs_18 reg">
				Чем европейский корпус отличается от азиатского и что такое "полярность"?
			</div>
			<div class="answer">
				<table class="answer_table">
					<tr>
						<td class="answer_title">Ответ:</td>
						<td class="answer_body">
							<div class="answer_body_text">
							Все просто: если высота клемм не превышает общую высоту аккумулятора, т.е. находятся на ступеньке или в углублении&nbsp;- это европейский корпус. Если клеммы возвышаются над крышкой&nbsp;- это азиатский корпус. Полянность&nbsp;- это расположение плюсовых и минусовых клемм в АКБ: плюс слева&nbsp;- прямая, плюс справа&nbsp;- обратная.
							</div>
							<img class="answer_body_img" src="/bitrix/templates/akk/q_a_akkums.png">
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="questions_answers_item">
			<div class="question black fs_18 reg">
				Почему наши цены настолько низкие?
			</div>
			<div class="answer">
				<table class="answer_table">
					<tr>
						<td class="answer_title">Ответ:</td>
						<td class="answer_body">
							Нет, мы не занимаемся восстановлением старых акумуляторов и уж тем более не продаем их под видом новых. Все дело в тщательном выборе поставщиков, оптимизации количества закупок, расходов на логистику и хранение, низкой арендной плате и осознании, что наше будущее зависит от наших покупателей.
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="questions_answers_item">
			<div class="question black fs_18 reg">
				Будет ли скидка, если сдаваемый аккумулятор немного меньшей или большей емкости?
			</div>
			<div class="answer">
				<table class="answer_table">
					<tr>
						<td class="answer_title">Ответ:</td>
						<td class="answer_body">
							Да, скидка как на сайте, если приобретаемый и сдаваемый АКБ "в одной весовой категории". В остальных случаях скидка согласно таблице:
							<table class="answer_body_table">
								<tr>
									<th>Емкость, А/ч</th>
									<th>Цена за 1 штуку, руб.</th>
								</tr>
								<tr>
									<td>35-42</td>
									<td>300</td>
								</tr>
								<tr>
									<td>43-54</td>
									<td>350</td>
								</tr>
								<tr>
									<td>55-65</td>
									<td>450</td>
								</tr>
								<tr>
									<td>66-87</td>
									<td>550</td>
								</tr>
								<tr>
									<td>88-119</td>
									<td>700</td>
								</tr>
								<tr>
									<td>120-150</td>
									<td>950</td>
								</tr>
								<tr>
									<td>170-210</td>
									<td>1400</td>
								</tr>
								<tr>
									<td>220-240</td>
									<td>1750</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="questions_answers_item">
			<div class="question black fs_18 reg">
				Чем отличается аккумулятор Delta DT от Delta DTM?
			</div>
			<div class="answer">
				<table class="answer_table">
					<tr>
						<td class="answer_title">Ответ:</td>
						<td class="answer_body">
							Серия DT ориентирована на применение в охранно-пожарных системах с обязательной регламентной ежегодной заменой аккумуляторов. Соответственно, их уровень качества закрывает именно эту упрощенную потребность. Аккумулятор Delta DTM имеет пятилетний срок рекомендованного использования.
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="questions_answers_item">
			<div class="question black fs_18 reg">
				Какой аккумулятор поставить на дизельный двигатель?
			</div>
			<div class="answer">
				<table class="answer_table">
					<tr>
						<td class="answer_title">Ответ:</td>
						<td class="answer_body">
							<p>
							Прежде всего, нужно учесть, что мощность аккумулятора для дизельного автомобиля должна быть больше, чем для бензинового, так как запуск при высокой степени сжатия требует больше оборотов двигателя.
							</p>
							<p>
							В остальном важно ориентироваться, чтобы ток заряда аккумулятора подходил генератору автомобиля. Например, если генератор рассчитан на максимально 70&nbsp;А/ч, более мощный аккумулятор не следует ставить: он просто не сможет полностью зарядиться.
							</p>
							<p>
							При выборе обязательно нужно учесть величину пускового тока, т.е. максимальной мощности на выходе, которую аккумулятор выдает за 10 секунд при -18<sup>0</sup>C. Этот момент важно учитывать, потому что дизельная смесь разогревается сложнее. Кроме того, машины на дизеле чаще всего оснащаются предпусковыми подогревателями, которые облегчают запуск. А для их нормального функционирования нужен мощный аккумулятор. Таким образом, величина пускового тока должна составлять от 450&nbsp;А и больше.
							</p>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="questions_answers_item">
			<div class="question black fs_18 reg">
				Как хранить аккумуляторы?
			</div>
			<div class="answer">
				<table class="answer_table">
					<tr>
						<td class="answer_title">Ответ:</td>
						<td class="answer_body">
							<p>Если автомобиль эксплуатируется только летом в зимний период рекомендуется снимать батарею с автомобиля. АКБ нужно полностью зарядить и периодически проверять заряд и при необходимости производит подзарядку.</p>
							<p>ВНИМАНИЕ ГЛУБОКИЙ РАЗРЯД убивает любую батарею.</p>
							<p>Если, это произошло, то необходимо зарядит аккумуляторную батарею от стационарного зарядного устройства током малой величины, но не позднее чем через 2-3 дня после глубокого разряда батареи.</p>
						</td>
					</tr>
				</table>
			</div>
		</div>
		
	</div>
</div>