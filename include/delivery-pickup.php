<div class="d_tc scheme_pickup val_mid">
    <a href="/process/delivery/" target="_blank" class="location_link">Схема проезда <br>для самовывоза</a>
</div>
<div class="d_tc scheme_pickup_content val_mid gray_font fs_12">
    Самовывоз осуществляется по адресу: г. Москва, 1-й Грайвороновский пр., 4.<br />
    Телефон: +7 (499) 650-52-09
</div>