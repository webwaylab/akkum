				<ul class="aside-nav-inner2">
					<li class="aside-nav-inner__item">
						<a href="/catalog/battery/auto-akb/" class="aside-nav__link">Легковые аккумуляторы</a>
					</li>
					<li class="aside-nav-inner__item">
						<a href="/catalog/battery/gelevye/" class="aside-nav__link">Гелевые автомобильные аккумуляторы</a>
					</li>
					<li class="aside-nav-inner__item">
						<a href="/catalog/battery/truck-akb/" class="aside-nav__link">Грузовые аккумуляторы</a>
					</li>
					<li class="aside-nav-inner__item">
						<a href="/catalog/battery/akb_dlya_ups/" class="aside-nav__link">Аккумуляторы для ИБП/UPS</a>
					</li>
					<li class="aside-nav-inner__item">
						<a href="/catalog/battery/moto-akb/" class="aside-nav__link">Мото аккумуляторы</a>
					</li>
					<li class="aside-nav-inner__item">
						<a href="/catalog/battery/li_ion/" class="aside-nav__link">Аккумуляторные батареи Li-ion</a>
					</li>
				</ul>
