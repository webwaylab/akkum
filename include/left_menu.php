<div class = "left_menu">

<div style="font-size: 14px; font-weight: normal;font-family: latobold;"><a href="/catalog/brand/">Аккумуляторы по маркам:</a></div>
<br>

<?php 
$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_MANUFACTURE");
$arFilter = Array("IBLOCK_ID"=> IB_CATALOG, "SECTION_CODE"=> SC_BATTERY, "INCLUDE_SUBSECTIONS"=>"Y", "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);

$modelArray = array();
//echo '<pre>';
while($ob = $res->GetNextElement()){
	$arFields = $ob->GetFields();
	if ($arFields['PROPERTY_MANUFACTURE_VALUE'] == '') continue;
	
	$name = mb_strtolower($arFields['PROPERTY_MANUFACTURE_VALUE'], 'UTF-8');
	
	if (empty($modelArray[$name])){
		$modelArray[$name] = 1;
	}
	else{
		$modelArray[$name]++;
	}
	
	
	//print_r($modelArray);
}
ksort($modelArray);
//echo '</pre>';
?>

<ul>
<?php foreach ($modelArray as $key => $val){ 
      $menulink = '/catalog/brand/'.str_replace(' ', '_', $key).'/';
      $pageurl = $_SERVER['REQUEST_URI'];
     // echo $menulink.$pageurl;
      if($pageurl == $menulink){ ?>
      <li><?php echo $key?> <span><?php echo $val;?> шт.</span></li>
<?php } else { ?>
	<li><a href = "/catalog/brand/<?php echo str_replace(' ', '_', $key)?>/"><?php echo $key?> <span><?php echo $val;?> шт.</span></a></li>
<?php }
}
?>
<?php  /*<!-- <li><a href = "https://www.kupit-akkumulyator.ru/catalog/battery/aktex/">Aktex</a></li>
	<li onMouseOver = "document.getElementById('sub_list_2').style.display = 'block'" onmouseout = "document.getElementById('sub_list_2').style.display = 'none'"><a href = "https://www.kupit-akkumulyator.ru/catalog/battery/bosch/">BOSCH</a>
		<ul id = "sub_list_2" style = "display: none;" onmouseout = "document.getElementById('sub_list_2').style.display = 'none'">
			<li><a href = "https://www.kupit-akkumulyator.ru/catalog/battery/bosch/s4_silver/">S4 Silver</a></li>
		</ul>
	</li>	
	<li><a href = "https://www.kupit-akkumulyator.ru/catalog/battery/bolk_moto/">Bolk moto</a></li>
	<li><a href = "https://www.kupit-akkumulyator.ru/catalog/battery/cobat/">Cobat</a></li>	
	<li><a href = "https://www.kupit-akkumulyator.ru/catalog/battery/mutlu/">Mutlu</a></li>	
	<li><a href = "https://www.kupit-akkumulyator.ru/catalog/battery/royal/">ROYAL</a></li>
	<li><a href = "https://www.kupit-akkumulyator.ru/catalog/battery/sebang/">SEBANG</a></li>
	<li><a href = "https://www.kupit-akkumulyator.ru/catalog/battery/solite/">Solite</a></li>
	<li><a href = "https://www.kupit-akkumulyator.ru/catalog/battery/titan/">Titan</a></li>		
	<li><a href = "https://www.kupit-akkumulyator.ru/catalog/battery/tudor/">Tudor</a></li>
	<li onMouseOver = "document.getElementById('sub_list_1').style.display = 'block'" onmouseout = "document.getElementById('sub_list_1').style.display = 'none'"><a href = "https://www.kupit-akkumulyator.ru/catalog/battery/varta/">VARTA</a>
		<ul id = "sub_list_1" style = "display: none;" onmouseout = "document.getElementById('sub_list_1').style.display = 'none'">
			<li><a href = "https://www.kupit-akkumulyator.ru/catalog/battery/varta/silver_dynamic/">Silver Dynamic</a></li>			
			<li><a href = "https://www.kupit-akkumulyator.ru/catalog/battery/varta/blue_dynamic/">Blue Dynamic</a></li>
		</ul>
	</li>
	<li><a href = "https://www.kupit-akkumulyator.ru/catalog/battery/vaiper/">VAIPER</a></li>	
	<li><a href = "https://www.kupit-akkumulyator.ru/catalog/battery/zver/">Зверь</a></li>	
	<li onMouseOver = "document.getElementById('sub_list_3').style.display = 'block';  " onmouseout = "document.getElementById('sub_list_3').style.display = 'none'"><a href = "https://www.kupit-akkumulyator.ru/catalog/battery/tyumen/">Тюмень</a>
		<ul id = "sub_list_3" style = "display: none;" onmouseout = "document.getElementById('sub_list_3').style.display = 'none'">
			<li><a href = "https://www.kupit-akkumulyator.ru/catalog/battery/tyumen/lider/">Premium</a></li>
		</ul>
	</li>
	 -->	*/?>
</ul>
</div>

<style>
	.left_menu a {
		text-decoration: underline;
		
	}
	
	.left_menu ul {
		margin-left: 15px;
	}
	
	.left_menu li {
		list-style-type: disc;
		color: #58b346;
		text-transform:capitalize;
		border-bottom:1px dashed #58b346;
	}
	.left_menu li a{
		text-decoration:none;
	}
	.left_menu li span{
		display:inline-block;
		float:right;
		text-transform: none;
	}
	
	
</style>