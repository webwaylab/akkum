<div class="contacts_info">
	<div class="clearfix">
		<div class="f_left">
			<h1 class="fs_24 black reg mb30 black_font">Контактная информация</h1>
			<div class="descript_art">
				<table width="100%" cellpadding="0" cellspacing="0">
					<tbody>
						<tr>
							<td><strong>Адрес:</strong></td>
							<td>119590, г. Москва, 1-й Грайвороновский пр., д. 4</td>
						</tr>
						<tr>
							<td><strong>Телефоны:</strong></td>
							<td>&nbsp; <a href="tel:+7 (499) 650-52-09" onclick="yaCounter22871155.reachGoal('tel'); ga('send', 'pageview', '/tel/');">+7 (499) 650-52-09</a>, <a href="tel:+7 (499) 650-55-55" onclick="yaCounter22871155.reachGoal('tel'); ga('send', 'pageview', '/tel/');">+7 (499) 650-55-55</a></td>
						</tr>
						<tr>
							<td><strong>E-mail:</strong></td>
							<td>
								<a href="mailto:info@kupit-akkumulyator.ru">info@kupit-akkumulyator.ru</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<span class="fs_24 black reg mb30 black_font">График работы магазина</span>
			<div class="descript_art">
				<table width="100%" cellpadding="0" cellspacing="0">
					<tbody>
						<tr>
							<td>
								<strong>Служба заказов</strong>
							</td>
							<td>
								Пн-Вс 10:00 - 21:00, без выходных
							</td>
						</tr>
						<tr>
							<td>
								<strong>Доставка</strong>
							</td>
							<td>
								Пн-Вс 13:00 - 21:00, без выходных
							</td>
						</tr>
						<tr>
							<td>
								<strong>Самовывоз</strong>
							</td>
							<td>
								<p>
									пн - пт 10:00 - 21:00,<br>
									сб - вс 10:00 - 19:00, без выходных
								</p>
							</td>
						</tr>
					</tbody>
				</table><br>
				<p>Оформление заказа на сайте — <span class="reg">КРУГЛОСУТОЧНО</span></p>
				<p>ООО "ЛОМ-АКБ"<br>ИНН/КПП 7722371656/772201001<br>ОГРН 1167746726070<br></p>
			</div>
		</div>
		<div class="f_right">
			<span class="fs_24 black reg mb30 black_font">СВИДЕТЕЛЬСТВО</span>
			<a class="gallery" href="/upload/medialibrary/7d1/7d1fdcf639eb2ad6ad090de02d8dd244.jpg"> <img alt="img004" src="/upload/medialibrary/7d1/7d1fdcf639eb2ad6ad090de02d8dd244.jpg" title="LOM_AKB.jpg"> </a>
		</div>
	</div>

	<div class="contact_map">
		<script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=aEKwqHJ7PZFFDWX78X5oib9YXMfGkEMg&width=100%&height=305&lang=ru_RU&sourceType=constructor"></script>
	</div>

	<span class="contact_btn">Оформление заказов на сайте! — КРУГЛОСУТОЧНО! </span>

	<div class="contact_gallery clearfix" id="wrap">
		<a class="gallery" href="/upload/1_610_0_0.jpg"><img width="283" alt="single_image" src="/bitrix/templates/akk/images/light_box1.jpg" height="188"></a> 
		<a class="gallery" href="/upload/2_610_0_0.jpg"><img width="283" alt="single_image" src="/bitrix/templates/akk/images/light_box2.jpg" height="188"></a> 
		<a class="gallery" href="/upload/3_610_0_0.jpg"><img width="283" alt="single_image" src="/bitrix/templates/akk/images/light_box3.jpg" height="188"></a> 
		<a class="gallery" href="/upload/4_610_0_0.jpg"><img width="283" alt="single_image" src="/bitrix/templates/akk/images/light_box4.jpg" height="188"></a> 
	</div>

	<br><br>

	<div class="article_video_box">
		<iframe width="100%" height="348" src="https://www.youtube.com/embed/7qnrZpLPRfg" frameborder="0" allowfullscreen></iframe>
	</div>
	<br>
	<br>

	<?$APPLICATION->IncludeComponent(
		"irbis:form-contact",
		".default",
		Array(
			)
			);?>
		</div>
		<div itemscope="" itemtype="https://schema.org/Organization" style="display: none;">
			<span itemprop="name">Купить аккумулятор</span>
			<div itemprop="address" itemscope="" itemtype="https://schema.org/PostalAddress">
				Main address: <span itemprop="streetAddress">1-й Грайвороновский пр., д. 4</span> <span itemprop="addressLocality">Москва</span>
				,
			</div>
			Tel:<span itemprop="telephone">+7 (499) 650-52-09</span>, Tel:<span itemprop="telephone">+7 (499) 650-55-55</span>, E-mail: <span itemprop="email"><a href="mailto:info@kupit-akkumulyator.ru">info@kupit-akkumulyator.ru</a></span>
		</div>
		<br>