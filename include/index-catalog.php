<?
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");
global $APPLICATION, $USER, $arrFilter, $catalogSectionRoot;

$catalogSectionRoot = 18;

$arParams = array(
    "IBLOCK_TYPE" => "catalog",
	"IBLOCK_ID" => "2",
	"TEMPLATE_THEME" => "green",
	"HIDE_NOT_AVAILABLE" => "N",
	"BASKET_URL" => "/personal/cart/",
	"ACTION_VARIABLE" => "action",
	"PRODUCT_ID_VARIABLE" => "id",
	"SECTION_ID_VARIABLE" => "SECTION_ID",
	"PRODUCT_QUANTITY_VARIABLE" => "quantity",
	"PRODUCT_PROPS_VARIABLE" => "prop",
	"SEF_MODE" => "Y",
	"SEF_FOLDER" => "/catalog/",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_FILTER" => "Y",
	"CACHE_GROUPS" => "N",
	"SET_TITLE" => "Y",
	"ADD_SECTION_CHAIN" => "Y",
	"ADD_ELEMENT_CHAIN" => "Y",
	"SET_STATUS_404" => "Y",
	"DETAIL_DISPLAY_NAME" => "N",
	"USE_ELEMENT_COUNTER" => "Y",
	"USE_FILTER" => "Y",
	"FILTER_NAME" => "",
	"FILTER_VIEW_MODE" => "VERTICAL",
);

$arResult["VARIABLES"]["SECTION_ID"] = $catalogSectionRoot;

$arFilter = array(
	"IBLOCK_ID" => $arParams["IBLOCK_ID"],
	"ACTIVE" => "Y",
	"GLOBAL_ACTIVE" => "Y",
);
if (0 < intval($arResult["VARIABLES"]["SECTION_ID"]))
{
	$arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
}
elseif ('' != $arResult["VARIABLES"]["SECTION_CODE"])
{
	$arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];
}

$obCache = new CPHPCache();
if ($obCache->InitCache(36000, serialize($arFilter), "/iblock/catalog"))
{
	$arCurSection = $obCache->GetVars();
}
elseif ($obCache->StartDataCache())
{
	$arCurSection = array();
	$dbRes = CIBlockSection::GetList(array(), $arFilter, false, array("ID", "NAME"));

	if(defined("BX_COMP_MANAGED_CACHE"))
	{
		global $CACHE_MANAGER;
		$CACHE_MANAGER->StartTagCache("/iblock/catalog");

		if ($arCurSection = $dbRes->Fetch())
		{
			$CACHE_MANAGER->RegisterTag("iblock_id_".$arParams["IBLOCK_ID"]);
		}
		$CACHE_MANAGER->EndTagCache();
	}
	else
	{
		if(!$arCurSection = $dbRes->Fetch())
			$arCurSection = array();
	}

	$obCache->EndDataCache($arCurSection);
}
if (!isset($arCurSection))
{
	$arCurSection = array();
}
?>
<?/*<div class="clearfix">
    <div class="grid_37 prefix_2">
        <div class="wrapper catalog_nav_wrap">
            <div class="catalog_nav f_left wrapper">
                <ul class="catalog_nav_list heavy">
                    <?
                    $currentRootSection = array();
                    $arFilter = array(
                        'IBLOCK_ID' => CATALOG_ID,
                        'DEPTH_LEVEL' => 1,
                        'GLOBAL_ACTIVE' => "Y"
                    );
                    $rsSect = CIBlockSection::GetList(array('sort' => 'asc'), $arFilter, false);
                    while ($arSection = $rsSect->GetNext())
                    {
                        if($arSection["ID"]==$catalogSectionRoot)
                        {
                            $currentRootSection = $arSection;
                            $class = "current";
                        }else{
                            $class = "";
                        }

                        ?>
                        <li class="catalog_nav_list_item <?=$class?>">
                            <a href="<?=$arSection["SECTION_PAGE_URL"]?>" class="catalog_nav_list_link"><?=$arSection["NAME"]?></a>
                        </li>
                        <?
                    }
                    ?>  
                </ul>
            </div>

            <div class="f_right catalog_search">
                <form class="search_form" method="GET" action="/search/">
                    <input type="text" name="q" class="search_form_input" value="Введите запрос" onBlur="if(this.value=='') this.value='Введите запрос'" onFocus="if(this.value =='Введите запрос' ) this.value=''">
                    <button class="search_form_btn"></button>
                </form>
            </div>

        </div>
    </div>
</div>
*/?>
<div class="clearfix">
	<noindex>
    <aside class="grid_8 resp_col7">
        <?$APPLICATION->IncludeComponent(
        	"irbis:model.filter",
        	"",
        	array(
        		"BRAND_HL" => 5,
                "MODEL_HL" => 3,
                "MODEL_ROW_HL" => 4,
                "FILTER_NAME" => "",
                "SELECT_FIELD" => ""
        	),
        	$component,
        	array("HIDE_ICONS" => "Y")
        );
        ?>


        <?/*$APPLICATION->IncludeComponent(
    		"bitrix:catalog.smart.filter",
    		"",
    		array(
    			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
    			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
    			"SECTION_ID" => $arCurSection['ID'],
    			"FILTER_NAME" => $arParams["FILTER_NAME"],
    			"PRICE_CODE" => $arParams["PRICE_CODE"],
    			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
    			"CACHE_TIME" => $arParams["CACHE_TIME"],
    			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
    			"SAVE_IN_SESSION" => "N",
    			"FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
    			"XML_EXPORT" => "Y",
    			"SECTION_TITLE" => "NAME",
    			"SECTION_DESCRIPTION" => "DESCRIPTION",
    			'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
    			"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
				"FILTER_ACTION" => "/catalog/battery/"
    		),
    		$component,
    		array('HIDE_ICONS' => 'Y')
    	);*/?>
		
		<?$APPLICATION->IncludeComponent(
                    "bitrix:catalog.section.list",
                    "filter_main",
                    array(
                        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                        "SECTION_ID" => $currentRootSection["ID"],
                        "SECTION_CODE" => $currentRootSection["CODE"],
                        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                        "CACHE_TIME" => $arParams["CACHE_TIME"],
                        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                        "COUNT_ELEMENTS" => "N",
                        "TOP_DEPTH" => "3",
                        "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
                        "VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
                        "SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
                        "HIDE_SECTION_NAME" => "N",
                        "ADD_SECTIONS_CHAIN" => "N"
                    ),
                    $component,
                    array("HIDE_ICONS" => "N")
                );?>

		<?//$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/banner-market.php"), false);?>

    	<?
            /*$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/left_menu.php"), false);*/
        ?>
    </aside> <!-- left col END !! -->

    </noindex> 




    <article class="grid_31 prefix_1 resp_col29">
		<?// if (!strstr($_SERVER['REQUEST_URI'], 'PAGEN_1=')) { ?> 

			<?$APPLICATION->IncludeComponent(
				"bitrix:catalog.section",
				"fl_auto",
					array(
						"IBLOCK_ID" => 13,
						"SECTION_ID" => 0,
						"SECTION_CODE" => "",
						"ELEMENT_SORT_FIELD" => "name",
						"ELEMENT_SORT_ORDER" => "asc",
						"PAGE_ELEMENT_COUNT" => "1000",
						"CACHE_TYPE" => $arParams["CACHE_TYPE"],
						"CACHE_TIME" => $arParams["CACHE_TIME"],
						"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
					),
					$component,
					array("HIDE_ICONS" => "N")
				);?>
						
		<?// } ?>
        <?
        if(isset($_GET["filter_form"]) && $_GET["filter_form"]=="model-form")
        {
            if(!empty($_GET["MODEL_ROW"]))
            {
                $addFilter = getModificationInformation($_GET["MODEL_ROW"]);
                $arrFilter = array_merge($arrFilter, $addFilter);
            }

            $arBrand = getElementByID($_GET["BRAND"]);
            $arModel = getElementByID($_GET["MODEL"]);

            $sectionName = "Купить аккумулятор для ".$arBrand["NAME"]." ".$arModel["NAME"];

        }else{
            if($APPLICATION->GetCurDir()=="/catalog/battery/" || isset($arrFilter["=PROPERTY_78"]) || isset($_GET["arrFilter_52_MIN"]) || isset($_GET["arrFilter_52_MAX"]))
            {
                $sectionName = "Аккумуляторы автомобильные";

                if(isset($arrFilter["=PROPERTY_78"]))
                {
                    $xmlValue = $arrFilter["=PROPERTY_78"][0];
                    $sectionName.= " ".getBrandAutoByXmlId($xmlValue);
                }

                if(intval($_GET["arrFilter_52_MIN"])>0 || intval($_GET["arrFilter_52_MAX"])>0)
                {
                    $sectionName.=" емкостью ";
                    if(intval($_GET["arrFilter_52_MIN"])>0)
                    {
                        $sectionName.= intval($_GET["arrFilter_52_MIN"]);
                    }

                    if(intval($_GET["arrFilter_52_MIN"])>0 && intval($_GET["arrFilter_52_MAX"])>0)
                    {
                        $sectionName.=" - ";
                    }

                    if(intval($_GET["arrFilter_52_MAX"])>0)
                    {
                        $sectionName.= intval($_GET["arrFilter_52_MAX"]);
                    }
                    $sectionName.= " А•ч";
                }

            }else{
                $sectionName = $currentRootSection["NAME"];
            }
        }
        ?>
        <?
        if($arParams["USE_COMPARE"]=="Y")
        {
        	?><?$APPLICATION->IncludeComponent(
        		"bitrix:catalog.compare.list",
        		"",
        		array(
        			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
        			"NAME" => $arParams["COMPARE_NAME"],
        			"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
        			"COMPARE_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["compare"],
        			"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
        			"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
        			'POSITION_FIXED' => isset($arParams['COMPARE_POSITION_FIXED']) ? $arParams['COMPARE_POSITION_FIXED'] : '',
        			'POSITION' => isset($arParams['COMPARE_POSITION']) ? $arParams['COMPARE_POSITION'] : ''
        		),
        		$component,
        		array("HIDE_ICONS" => "Y")
        	);?><?
        }

        if (isset($arParams['USE_COMMON_SETTINGS_BASKET_POPUP']) && $arParams['USE_COMMON_SETTINGS_BASKET_POPUP'] == 'Y')
        {
        	$basketAction = (isset($arParams['COMMON_ADD_TO_BASKET_ACTION']) ? $arParams['COMMON_ADD_TO_BASKET_ACTION'] : '');
        }
        else
        {
        	$basketAction = (isset($arParams['SECTION_ADD_TO_BASKET_ACTION']) ? $arParams['SECTION_ADD_TO_BASKET_ACTION'] : '');
        }
        $intSectionID = 0;
        ?>

		<?/*

		<div class="clearfix mb12 catalog_filter">
            <div class="h2 black fs_24 reg f_left"><?=$sectionName;?></div>
            <div class="f_right">

                <a href="#" id="show_categories_btn" class="catalogue_btn2 d_ib">
                    Категория: <span id="current_category_val">
                        <?if(count($arCurSection)>0):?><?=$arCurSection["NAME"]?><?else:?><?=$currentRootSection["NAME"]?><?endif;?>
                    </span> <span class="sprite-catalog_icon i"></span>
                </a>

                <?$APPLICATION->IncludeComponent(
                	"bitrix:catalog.section.list",
                	"tree",
                	array(
                		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                		"SECTION_ID" => $currentRootSection["ID"],
                		"SECTION_CODE" => $currentRootSection["CODE"],
                		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
                		"CACHE_TIME" => $arParams["CACHE_TIME"],
                		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                		"COUNT_ELEMENTS" => "N",
                		"TOP_DEPTH" => "3",
                		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
                		"VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
                		"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
                		"HIDE_SECTION_NAME" => "N",
                		"ADD_SECTIONS_CHAIN" => "N"
                	),
                	$component,
                	array("HIDE_ICONS" => "N")
                );?>

              </div>
        </div>

*/?>


		<?/*
		<noindex>
        <div class="filter_row clearfix grn_bg mb34">
            <div class="f_left">
                <div class="filter_label">Сортировать по:</div>
                <?
                $arSort = array(
                    "catalog_PRICE_1" => "цене",
                    "NAME" => "названию",
                    "PROPERTY_RATING" => "рейтингу",
                );
                foreach($arSort as $sortKey=>$sortTitle)
                {
                    if(isset($_GET["sortFields"]) && $_GET["sortFields"]==$sortKey)
                    {
                        $activeClass = "active";
                        if($_GET["sortOrder"]=="desc")
                        {
                            $sortOrder = "asc";
                            $arrowClass = "arrow_down";
                        }else{
                            $sortOrder = "desc";
                            $arrowClass = "arrow_up";
                        }
                    }else{
                        if($sortKey=="catalog_PRICE_1")
                        {
                            $activeClass = "active";
                            $sortOrder = "desc";
                            $arrowClass = "arrow_up";
                        }else{
                            $activeClass = "";
                            $sortOrder = "desc";
                            $arrowClass = "arrow_down";
                        }
                    }
                    $url = $APPLICATION->GetCurPageParam("sortFields=".$sortKey."&sortOrder=".$sortOrder, array("sortFields", "sortOrder"));
                    ?>
                    <div class="filter_item">
                        <a href="<?=$url?>" class="filter_link <?=$activeClass?> <?=$arrowClass?>"><?=$sortTitle?></a>
                    </div>
                    <?
                }
                if(isset($_GET["sortFields"]))
                {
                    $arParams["ELEMENT_SORT_FIELD"] = $_GET["sortFields"];
                    $arParams["ELEMENT_SORT_ORDER"] = $_GET["sortOrder"];
                }else{
                    $arParams["ELEMENT_SORT_FIELD"] = "catalog_PRICE_1";
                    $arParams["ELEMENT_SORT_ORDER"] = 'asc';
                }
                ?>
            </div>

            <div class="f_right">
                <?
                if(isset($_GET["SALELEADER"]) && !empty($_GET["SALELEADER"]))
                {
                    $arrFilter["PROPERTY_SALELEADER_VALUE"] = $_GET["SALELEADER"];
                    $urlHit = $APPLICATION->GetCurPageParam("", array("SALELEADER", "SPECIALOFFER"));
                }else{
                    $urlHit = $APPLICATION->GetCurPageParam("SALELEADER=да", array("SALELEADER", "SPECIALOFFER"));
                }

                if(isset($_GET["SPECIALOFFER"]) && !empty($_GET["SPECIALOFFER"]))
                {
                    $arrFilter["PROPERTY_SPECIALOFFER_VALUE"] = $_GET["SPECIALOFFER"];
                    $urlSale = $APPLICATION->GetCurPageParam("", array("SPECIALOFFER", "SALELEADER"));
                }else{
                    $urlSale = $APPLICATION->GetCurPageParam("SPECIALOFFER=да", array("SPECIALOFFER", "SALELEADER"));
                }
                ?>
                <div class="filter_item">
                    <a href="<?=$urlHit?>" class="filter_link hits <?if(!empty($_GET["SALELEADER"])):?>active<?endif;?>">хиты продаж</a>
                </div>
                <div class="filter_item">
                    <a href="<?=$urlSale?>" class="filter_link hits <?if(!empty($_GET["SPECIALOFFER"])):?>active<?endif;?>">акции</a>
                </div>
            </div>
        </div>
        </noindex>
        <?
        //CDev::pre($arrFilter);
        ?>


*/?>


		<?/*
        <?$APPLICATION->IncludeComponent(
        	"bitrix:catalog.section",
        	"index",
        	Array(
        		"IBLOCK_TYPE" => "catalog",
        		"IBLOCK_ID" => "2",
        		"SECTION_ID" => $catalogSectionRoot,
				'HEAD_SECTION'=> 'battery',
        		"SECTION_CODE" => "",
        		"SECTION_USER_FIELDS" => array("",""),
        		"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
        		"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
        		"ELEMENT_SORT_FIELD2" => "id",
        		"ELEMENT_SORT_ORDER2" => "desc",
        		"FILTER_NAME" => "arrFilter",
        		"INCLUDE_SUBSECTIONS" => "Y",
        		"SHOW_ALL_WO_SECTION" => "N",
        		"HIDE_NOT_AVAILABLE" => "N",
        		"PAGE_ELEMENT_COUNT" => "12",
        		"LINE_ELEMENT_COUNT" => "3",
        		"PROPERTY_CODE" => array("TYPE","POLARITY","CAPACITY","START_TOK","BRAND","MODEL","MODEL_ROW","TITLE","KEYWORDS","META_DESCRIPTION","SALELEADER","SPECIALOFFER","ARTNUMBER","BLOG_POST_ID","GARANTY","BLOG_COMMENTS_CNT","FORUM_MESSAGE_CNT","vote_count","rating","RECOMMEND","vote_sum","FORUM_TOPIC_ID","TOKEND","PRICE_CHANGE",""),
        		"OFFERS_LIMIT" => "5",
        		"TEMPLATE_THEME" => "green",
        		"PRODUCT_SUBSCRIPTION" => "N",
        		"SHOW_DISCOUNT_PERCENT" => "N",
        		"SHOW_OLD_PRICE" => "N",
        		"SHOW_CLOSE_POPUP" => "N",
        		"MESS_BTN_BUY" => "Купить",
        		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
        		"MESS_BTN_SUBSCRIBE" => "Подписаться",
        		"MESS_BTN_DETAIL" => "Подробнее",
        		"MESS_NOT_AVAILABLE" => "Нет в наличии",
                //"SECTION_URL" => "/catalog/#SECTION_CODE_PATH#/",
        		//"DETAIL_URL" => "/catalog/#SECTION_CODE_PATH#/#ELEMENT_CODE#/",
        		"SECTION_ID_VARIABLE" => "SECTION_ID",
        		"AJAX_MODE" => "N",
        		"AJAX_OPTION_JUMP" => "N",
        		"AJAX_OPTION_STYLE" => "Y",
        		"AJAX_OPTION_HISTORY" => "N",
        		"CACHE_TYPE" => "A",
        		"CACHE_TIME" => "36000000",
        		"CACHE_GROUPS" => "N",
        		"SET_TITLE" => "N",
        		"SET_BROWSER_TITLE" => "N",
        		"BROWSER_TITLE" => "-",
        		"SET_META_KEYWORDS" => "N",
        		"META_KEYWORDS" => "-",
        		"SET_META_DESCRIPTION" => "Y",
        		"META_DESCRIPTION" => "-",
        		"ADD_SECTIONS_CHAIN" => "N",
        		"SET_STATUS_404" => "N",
        		"CACHE_FILTER" => "N",
        		"ACTION_VARIABLE" => "action",
        		"PRODUCT_ID_VARIABLE" => "id",
        		"PRICE_CODE" => array("BASE"),
        		"USE_PRICE_COUNT" => "N",
        		"SHOW_PRICE_COUNT" => "1",
        		"PRICE_VAT_INCLUDE" => "Y",
        		"CONVERT_CURRENCY" => "N",
        		"BASKET_URL" => "/personal/cart/",
        		"USE_PRODUCT_QUANTITY" => "Y",
        		"ADD_PROPERTIES_TO_BASKET" => "N",
        		"PRODUCT_PROPS_VARIABLE" => "prop",
        		"PARTIAL_PRODUCT_PROPERTIES" => "N",
        		"PRODUCT_PROPERTIES" => array(),
        		"ADD_TO_BASKET_ACTION" => "ADD",
        		"DISPLAY_COMPARE" => "Y",
                "COMPARE_PATH" => "/catalog/compare/",
        		"PAGER_TEMPLATE" => "arrows",
        		"DISPLAY_TOP_PAGER" => "N",
        		"DISPLAY_BOTTOM_PAGER" => "Y",
        		"PAGER_TITLE" => "Товары",
        		"PAGER_SHOW_ALWAYS" => "N",
        		"PAGER_DESC_NUMBERING" => "N",
        		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        		"PAGER_SHOW_ALL" => "N",
        		"ADD_PICT_PROP" => "MORE_PHOTO",
        		"LABEL_PROP" => "",
        		"MESS_BTN_COMPARE" => "Сравнить",
        		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
        	)
        );?>

*/?>

				<?
                global $popularFilter;
                //$popularFilter['!PROPERTY_rating'] = false;
				//$popularFilter['PROPERTY_SALELEADER'] = 90;
				$popularFilter['!PROPERTY_POPULYARNYY_TOVAR_DOP_SVOYSTVA_SPRAVOCHNIKA_NOMENK'] = false;
				/*print_r("<pre>");
				print_r($popularFilter);
				print_r("</pre>");*/
                ?>
                <?$APPLICATION->IncludeComponent(
                	"bitrix:catalog.top",
                	"budget",
                	Array(
                		"IBLOCK_TYPE" => "catalog",
                		"IBLOCK_ID" => "2",
						'HEAD_SECTION' => "battery", 
                		"ELEMENT_SORT_FIELD" => "PROPERTY_rating",
                		"ELEMENT_SORT_ORDER" => "DESC",
                		"ELEMENT_SORT_FIELD2" => "id",
                		"ELEMENT_SORT_ORDER2" => "desc",
                		"FILTER_NAME" => "popularFilter",
                		"HIDE_NOT_AVAILABLE" => "N",
                		"ELEMENT_COUNT" => "4",
                		"LINE_ELEMENT_COUNT" => "4",
                		"PROPERTY_CODE" => array("BRAND", "MODEL", "MODEL_ROW", "TITLE", "KEYWORDS", "META_DESCRIPTION", "SALELEADER", "SPECIALOFFER", "ARTNUMBER", "BLOG_POST_ID", "GARANTY", "CAPACITY", "BLOG_COMMENTS_CNT", "FORUM_MESSAGE_CNT", "vote_count", "POLARITY", "START_TOK", "rating", "RECOMMEND", "vote_sum", "FORUM_TOPIC_ID", "TYPE", "TOKEND", "PRICE_CHANGE", "LENGTH", "WIDTH", "HEIGHT", ""),
                		"OFFERS_LIMIT" => "12",
                		"VIEW_MODE" => "SECTION",
                		"SHOW_DISCOUNT_PERCENT" => "N",
                		"SHOW_OLD_PRICE" => "N",
                		"SHOW_CLOSE_POPUP" => "N",
                		"MESS_BTN_BUY" => "Купить",
                		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
                		"MESS_BTN_DETAIL" => "Подробнее",
                		"MESS_NOT_AVAILABLE" => "Нет в наличии",
                		"SECTION_URL" => "",
                		"DETAIL_URL" => "",
                		"SECTION_ID_VARIABLE" => "SECTION_ID",
                		"CACHE_TYPE" => "A",
                		"CACHE_TIME" => "36000000",
                		"CACHE_GROUPS" => "N",
                		"CACHE_FILTER" => "Y",
                		"ACTION_VARIABLE" => "action",
                		"PRODUCT_ID_VARIABLE" => "id",
                		"PRICE_CODE" => array("BASE"),
                		"USE_PRICE_COUNT" => "N",
                		"SHOW_PRICE_COUNT" => "1",
                		"PRICE_VAT_INCLUDE" => "Y",
                		"CONVERT_CURRENCY" => "Y",
                		"BASKET_URL" => "/personal/cart/",
                		"USE_PRODUCT_QUANTITY" => "N",
                		"ADD_PROPERTIES_TO_BASKET" => "Y",
                		"PRODUCT_PROPS_VARIABLE" => "prop",
                		"PARTIAL_PRODUCT_PROPERTIES" => "N",
                		"PRODUCT_PROPERTIES" => array(),
                		"ADD_TO_BASKET_ACTION" => "ADD",
                		"DISPLAY_COMPARE" => "Y",
                		"TEMPLATE_THEME" => "site",
                		"ADD_PICT_PROP" => "-",
                		"LABEL_PROP" => "-",
                		"MESS_BTN_COMPARE" => "Сравнить",
                		"CURRENCY_ID" => "RUB",
                		"COMPARE_PATH" => "",
						"TITLE" => "Популярные товары",
						"LINK" => "/popular/"
                	)
                );?>
				
				<br><br>
				<?
                global $similarFilter;
				$similarFilter["=SECTION_ID"] = $catalogSectionRoot;
                $similarFilter["INCLUDE_SUBSECTIONS"] = "Y";
                ?>
                <?$APPLICATION->IncludeComponent(
                	"bitrix:catalog.top",
                	"budget",
                	Array(
                		"IBLOCK_TYPE" => "catalog",
                		"IBLOCK_ID" => "2",
						'HEAD_SECTION' => "battery", 
                		"ELEMENT_SORT_FIELD" => "sort",
                		"ELEMENT_SORT_ORDER" => "asc",
                		"ELEMENT_SORT_FIELD2" => "id",
                		"ELEMENT_SORT_ORDER2" => "desc",
                		"FILTER_NAME" => "similarFilter",
                		"HIDE_NOT_AVAILABLE" => "N",
                		"ELEMENT_COUNT" => "4",
                		"LINE_ELEMENT_COUNT" => "4",
                		"PROPERTY_CODE" => array("BRAND", "MODEL", "MODEL_ROW", "TITLE", "KEYWORDS", "META_DESCRIPTION", "SALELEADER", "SPECIALOFFER", "ARTNUMBER", "BLOG_POST_ID", "GARANTY", "CAPACITY", "BLOG_COMMENTS_CNT", "FORUM_MESSAGE_CNT", "vote_count", "POLARITY", "START_TOK", "rating", "RECOMMEND", "vote_sum", "FORUM_TOPIC_ID", "TYPE", "TOKEND", "PRICE_CHANGE", "LENGTH", "WIDTH", "HEIGHT", ""),
                		"OFFERS_LIMIT" => "12",
                		"VIEW_MODE" => "SECTION",
                		"SHOW_DISCOUNT_PERCENT" => "N",
                		"SHOW_OLD_PRICE" => "N",
                		"SHOW_CLOSE_POPUP" => "N",
                		"MESS_BTN_BUY" => "Купить",
                		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
                		"MESS_BTN_DETAIL" => "Подробнее",
                		"MESS_NOT_AVAILABLE" => "Нет в наличии",
                		"SECTION_URL" => "",
                		"DETAIL_URL" => "",
                		"SECTION_ID_VARIABLE" => "SECTION_ID",
                		"CACHE_TYPE" => "A",
                		"CACHE_TIME" => "36000000",
                		"CACHE_GROUPS" => "N",
                		"CACHE_FILTER" => "Y",
                		"ACTION_VARIABLE" => "action",
                		"PRODUCT_ID_VARIABLE" => "id",
                		"PRICE_CODE" => array("BASE"),
                		"USE_PRICE_COUNT" => "N",
                		"SHOW_PRICE_COUNT" => "1",
                		"PRICE_VAT_INCLUDE" => "Y",
                		"CONVERT_CURRENCY" => "Y",
                		"BASKET_URL" => "/personal/cart/",
                		"USE_PRODUCT_QUANTITY" => "N",
                		"ADD_PROPERTIES_TO_BASKET" => "Y",
                		"PRODUCT_PROPS_VARIABLE" => "prop",
                		"PARTIAL_PRODUCT_PROPERTIES" => "N",
                		"PRODUCT_PROPERTIES" => array(),
                		"ADD_TO_BASKET_ACTION" => "ADD",
                		"DISPLAY_COMPARE" => "Y",
                		"TEMPLATE_THEME" => "site",
                		"ADD_PICT_PROP" => "-",
                		"LABEL_PROP" => "-",
                		"MESS_BTN_COMPARE" => "Сравнить",
                		"CURRENCY_ID" => "RUB",
                		"COMPARE_PATH" => "",
						"TITLE" => "Бюджетные аккумуляторы",
						"LINK" => "/budget/"
                	)
                );?>

			
    </article>
	

</div>

<div class="section-description"></div>
	
<? if (!strstr($_SERVER['REQUEST_URI'], 'PAGEN_1=')) { ?>
	<div class="section-advantages clearfix">
		<h2>Преимущества сотрудничества с интернет-магазином «Купить аккумулятор»:</h2>
		<ul class="clearfix">
			<li>
				<span style="background-image:url(/bitrix/templates/akk/images/style_new_design/advantages-1.jpg);"></span>
				<h3>Денежная выгода</h3> 
				<p>Благодаря прямым контактам с дилерами и производителями все АКБ продаются без лишних торгово-закупочных, логистических, посреднических и прочих надбавок. Вы можете не только приобрести дешевый аккумулятор, но и дополнительно сэкономить за счет сдачи старой батареи, стоимость которой будет учтена как часть оплаты;</p> 
			</li>
			
			<li>
				<span style="background-image:url(/bitrix/templates/akk/images/style_new_design/advantages-4.jpg);"></span>
				<h3>Честность, открытость, прозрачность</h3>
				<p>Вы приобретаете аккумулятор дешево, причем его стоимость озвучивается сразу и полностью. Платные навязанные услуги, замаскированные надбавки, «мелкий шрифт» и прочие коммерческие уловки исключены; </p>
			</li>
			<li>
				<span style="background-image:url(/bitrix/templates/akk/images/style_new_design/advantages-3.jpg);"></span>
				<h3>Бесплатное консультирование</h3>
				<p>Специалисты расскажут об ассортименте АКБ, основных электротехнических и эксплуатационных параметрах, которые нужно учесть при выборе. Они же помогут определиться с покупкой аккумулятора для конкретного автомобиля; </p>
			</li>
			<li> 
				<h3>Гарантия</h3>
				<p>Длительная официальная гарантия (срок зависит от конкретного производителя)на каждый экземпляр.</p>
			</li>
		</ul>
	</div> 
<? } ?>

<div class="consultation_callback_form-wrap">
    <form class="consultation_callback_form">
       <h3>Консультация специалиста</h3>
        <div class="error"><p></p></div>
        <fieldset>
            <i class="icon-man"></i>
            <input type="text" name="name" class="txt_field2" value="" placeholder="Ваше имя">

            <input type="text" name="phone" class="txt_field2 phone_mask" value="" placeholder="Ваш телефон">

			<button type="submit" class="big_btn heavy btn-consultation">Проконсультироваться</button>
        </fieldset>
    </form>
	<p>Наш телефон: +7 (499) 650-55-55</p>
</div>