<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$requiredModules = array('iblock', 'catalog', 'sale');

foreach ($requiredModules as $requiredModule)
{
	if (!CModule::IncludeModule($requiredModule))
	{
		ShowError(GetMessage("F_NO_MODULE"));
		return 0;
	}
}

if($_REQUEST["AJAX_CALL"] == "Y" || $_REQUEST["is_ajax_post"] == "Y")
{
	$APPLICATION->RestartBuffer();
}

$arElementId = array();
$arSetParentWeight = array();
$DISCOUNT_PRICE_ALL = 0;
$arResult["MAX_DIMENSIONS"] = $arResult["ITEMS_DIMENSIONS"] = array();

CSaleBasket::UpdateBasketPrices(CSaleBasket::GetBasketUserID(), SITE_ID);

$arSelFields = array("ID", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "QUANTITY", "DELAY",
	"CAN_BUY", "PRICE", "WEIGHT", "NAME", "CURRENCY", "CATALOG_XML_ID", "VAT_RATE",
	"NOTES", "DISCOUNT_PRICE", "PRODUCT_PROVIDER_CLASS", "DIMENSIONS", "TYPE", "SET_PARENT_ID", "DETAIL_PAGE_URL"
);
$dbBasketItems = CSaleBasket::GetList(
		array("ID" => "ASC"),
		array(
				"FUSER_ID" => CSaleBasket::GetBasketUserID(),
				"LID" => SITE_ID,
				"ORDER_ID" => "NULL"
			),
		false,
		false,
		$arSelFields
	);
while ($arItem = $dbBasketItems->GetNext())
{
    if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y")
	{
		$arItem["PRICE"] = roundEx($arItem["PRICE"], SALE_VALUE_PRECISION);
		$arItem["QUANTITY"] = DoubleVal($arItem["QUANTITY"]);

		$arItem["WEIGHT"] = DoubleVal($arItem["WEIGHT"]);
		$arItem["VAT_RATE"] = DoubleVal($arItem["VAT_RATE"]);

		$arDim = unserialize($arItem["~DIMENSIONS"]);

		if(is_array($arDim))
		{
			$arItem["DIMENSIONS"] = $arDim;
			unset($arItem["~DIMENSIONS"]);

			$arResult["MAX_DIMENSIONS"] = CSaleDeliveryHelper::getMaxDimensions(
																	array(
																		$arDim["WIDTH"],
																		$arDim["HEIGHT"],
																		$arDim["LENGTH"]
																		),
																	$arResult["MAX_DIMENSIONS"]);

			$arResult["ITEMS_DIMENSIONS"][] = $arDim;
		}

		$arItem["PRICE_FORMATED"] = SaleFormatCurrency($arItem["PRICE"], $arItem["CURRENCY"]);
		$arItem["WEIGHT_FORMATED"] = roundEx(DoubleVal($arItem["WEIGHT"]/$arResult["WEIGHT_KOEF"]), SALE_WEIGHT_PRECISION)." ".$arResult["WEIGHT_UNIT"];

		if($arItem["DISCOUNT_PRICE"] > 0)
		{
			$arItem["DISCOUNT_PRICE_PERCENT"] = $arItem["DISCOUNT_PRICE"]*100 / ($arItem["DISCOUNT_PRICE"] + $arItem["PRICE"]);
			$arItem["DISCOUNT_PRICE_PERCENT_FORMATED"] = roundEx($arItem["DISCOUNT_PRICE_PERCENT"], 0)."%";
		}

		if (!CSaleBasketHelper::isSetItem($arItem))
		{
			$DISCOUNT_PRICE_ALL += $arItem["DISCOUNT_PRICE"] * $arItem["QUANTITY"];
			$arItem["DISCOUNT_PRICE"] = roundEx($arItem["DISCOUNT_PRICE"], SALE_VALUE_PRECISION);
			$arResult["ORDER_PRICE"] += $arItem["PRICE"] * $arItem["QUANTITY"];
		}

		if (!CSaleBasketHelper::isSetItem($arItem))
		{
			$arResult["ORDER_WEIGHT"] += $arItem["WEIGHT"] * $arItem["QUANTITY"];
		}

		$arResult["BASKET_ITEMS"][] = $arItem;
        $arElementId[] = $arItem["PRODUCT_ID"];
	}
    
    $arResult["PRICE_WITHOUT_DISCOUNT"] = SaleFormatCurrency($arResult["ORDER_PRICE"] + $DISCOUNT_PRICE_ALL, $allCurrency);
    $arResult["ORDER_WEIGHT_FORMATED"] = roundEx(DoubleVal($arResult["ORDER_WEIGHT"]/$arResult["WEIGHT_KOEF"]), SALE_WEIGHT_PRECISION)." ".$arResult["WEIGHT_UNIT"];
	$arResult["ORDER_PRICE_FORMATED"] = SaleFormatCurrency($arResult["ORDER_PRICE"], $arResult["BASE_LANG_CURRENCY"]);
	$arResult["VAT_SUM_FORMATED"] = SaleFormatCurrency($arResult["VAT_SUM"], $arResult["BASE_LANG_CURRENCY"]);
}





// get properties for iblock elements and their parents (if any)
$arCustomSelectFields = array();
$arSelect = array_merge(array("ID", "PREVIEW_PICTURE", "DETAIL_PICTURE", "PREVIEW_TEXT"), $arCustomSelectFields);
$arProductData = getProductProps($arElementId, $arSelect);

//print_r($arProductData);

foreach ($arResult["BASKET_ITEMS"] as &$arResultItem)
{
	$productId = $arResultItem["PRODUCT_ID"];
	if((int)$arProductData[$productId]["PREVIEW_PICTURE"] > 0)
		$arResultItem["PREVIEW_PICTURE"] = $arProductData[$productId]["PREVIEW_PICTURE"];
	if((int)$arProductData[$productId]["DETAIL_PICTURE"] > 0)
		$arResultItem["DETAIL_PICTURE"] = $arProductData[$productId]["DETAIL_PICTURE"];
	if($arProductData[$productId]["PREVIEW_TEXT"] != '')
		$arResultItem["PREVIEW_TEXT"] = $arProductData[$productId]["PREVIEW_TEXT"];

	foreach ($arProductData[$arResultItem["PRODUCT_ID"]] as $key => $value)
	{
		if (strpos($key, "PROPERTY_") !== false)
			$arResultItem[$key] = $value;
	}
    
	$arResultItem["PREVIEW_PICTURE_SRC"] = "";
	if (isset($arResultItem["PREVIEW_PICTURE"]) && intval($arResultItem["PREVIEW_PICTURE"]) > 0)
	{
		$arImage = CFile::GetFileArray($arResultItem["PREVIEW_PICTURE"]);
		if ($arImage)
		{
			$arFileTmp = CFile::ResizeImageGet(
				$arImage,
				array("width" => "110", "height" =>"110"),
				BX_RESIZE_IMAGE_PROPORTIONAL,
				true
			);

			$arResultItem["PREVIEW_PICTURE_SRC"] = $arFileTmp["src"];
		}
	}

	$arResultItem["DETAIL_PICTURE_SRC"] = "";
	if (isset($arResultItem["DETAIL_PICTURE"]) && intval($arResultItem["DETAIL_PICTURE"]) > 0)
	{
		$arImage = CFile::GetFileArray($arResultItem["DETAIL_PICTURE"]);
		if ($arImage)
		{
			$arFileTmp = CFile::ResizeImageGet(
				$arImage,
				array("width" => "110", "height" =>"110"),
				BX_RESIZE_IMAGE_PROPORTIONAL,
				true
			);

			$arResultItem["DETAIL_PICTURE_SRC"] = $arFileTmp["src"];
		}
	}
    
    if(empty($arResultItem["PREVIEW_PICTURE_SRC"]))
    {
        $arResultItem["PREVIEW_PICTURE_SRC"] = $arResultItem["DETAIL_PICTURE_SRC"];
    }
}
if (isset($arResultItem))
	unset($arResultItem);








$this->IncludeComponentTemplate();

if ($_REQUEST["AJAX_CALL"] == "Y" || $_REQUEST["is_ajax_post"] == "Y")
{
	die();
}
?>