<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="cart_items_box mb75">
    <?
    CDev::pre($arResult, true, false);
    foreach($arResult["BASKET_ITEMS"] as $arItem)
    {
        //CDev::pre($arItem, true, false);
        ?>
        <section class="cart_item">
            <div class="cart_item_cell1">
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="d_ib"><img src="<?=$arItem["PREVIEW_PICTURE_SRC"]?>" alt="<?=$arItem["NAME"]?>"></a>
            </div>
            <div class="cart_item_cell2">
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><h4 class="fs_18 bold mb5"><?=$arItem["NAME"]?></h4></a>
                <?=$arItem["PREVIEW_TEXT"]?>
            </div>
            <div class="cart_item_cell3">
                <div class="form_label">Выберите кол-во:</div>
                <div class="mb15">
                    <select class="styler">
                        <?
                        for($i=1; $i<100; $i++)
                        {
                            ?><option value="<?=$i?>" <?if($i==$arItem["QUANTITY"]):?>selected<?endif;?>><?=$i?></option><?
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="cart_item_cell4">
                <div class="product_item_price heavy mb20">14 900 <span class="rub">₽</span></div>
                <div class="p_rel">
                    <label class="checkbox_label fs_12 var2 abs_label">
                        <input type="checkbox" class="styler">
                        <span class="d_block wrapper">Сдача отработанного аккумулятора аналогичной ёмкости</span>
                    </label>
                </div>
            </div>
            <div class="cart_item_cell5">
                <a href="#" class="btn2 bold red_skin">удалить</a>
            </div>
        </section>
        <?
    }
    ?>

    <section class="total_cart_sum">
    общая сумма:
    <div class="product_item_price heavy"><?=number_format($arResult["ORDER_PRICE"], 0, "", " ")?> <span class="rub">₽</span></div>
    </section>
</div>