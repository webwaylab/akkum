<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CJSCore::Init(array('fx', 'popup', 'window', 'ajax'));
?>
<a name="order_form"></a>

<script type="text/javascript">
var BXFormPosting = false;
function submitForm(val)
{
	if (BXFormPosting === true)
		return true;

	BXFormPosting = true;
	if(val != 'Y')
		BX('confirmorder').value = 'N';

	var orderForm = BX('ORDER_FORM');
	BX.showWait();
	BX.ajax.submit(orderForm, ajaxResult);

	return true;
}

function ajaxResult(res)
{
	var orderForm = BX('ORDER_FORM');
	try
	{
		// if json came, it obviously a successfull order submit

		var json = JSON.parse(res);
		BX.closeWait();

		if (json.error)
		{
			BXFormPosting = false;
			return;
		}
		else if (json.redirect)
		{
			window.top.location.href = json.redirect;
		}
	}
	catch (e)
	{
		// json parse failed, so it is a simple chunk of html

		BXFormPosting = false;
		BX('order_form_content').innerHTML = res;
	}

	BX.closeWait();
	BX.onCustomEvent(orderForm, 'onAjaxSuccess');
}

function SetContact(profileId)
{
	BX("profile_change").value = "Y";
	submitForm();
}
</script>


<div class="clearfix">
    <div class="grid_37 prefix_2">
        <h1 class="black fs_24 reg mb50">Корзина</h1>
        <?
        if($_POST["is_ajax_post"] != "Y")
		{
			?><form action="<?=$APPLICATION->GetCurPage();?>" method="POST" name="ORDER_FORM" id="ORDER_FORM" enctype="multipart/form-data" class="create_order">
			<?=bitrix_sessid_post()?>
			<div id="order_form_content">
			<?
		}
		else
		{
			$APPLICATION->RestartBuffer();
		}
        
        if(!empty($arResult["ERROR"]) && $arResult["USER_VALS"]["FINAL_STEP"] == "Y")
		{
			foreach($arResult["ERROR"] as $v)
				echo ShowError($v);
			?>
			<script type="text/javascript">
				top.BX.scrollToNode(top.BX('ORDER_FORM'));
			</script>
			<?
		}
        
        include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/items.php");
        include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/auth.php");
		include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/delivery.php");
		include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/payment.php");
		include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/summary.php");
		?>

		<?if($_POST["is_ajax_post"] != "Y")
		{
			?>
				</div>
				<input type="hidden" name="confirmorder" id="confirmorder" value="Y">
				<input type="hidden" name="profile_change" id="profile_change" value="N">
				<input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">
				<input type="hidden" name="json" value="Y">
				<div class="bx_ordercart_order_pay_center"><a href="javascript:void();" onclick="submitForm('Y'); return false;" id="ORDER_CONFIRM_BUTTON" class="checkout"><?=GetMessage("SOA_TEMPL_BUTTON")?></a></div>
			</form>
			<?
			if($arParams["DELIVERY_NO_AJAX"] == "N")
			{
				?>
				<div style="display:none;"><?$APPLICATION->IncludeComponent("bitrix:sale.ajax.delivery.calculator", "", array(), null, array('HIDE_ICONS' => 'Y')); ?></div>
				<?
			}
		}
		else
		{
			?>
			<script type="text/javascript">
				top.BX('confirmorder').value = 'Y';
				top.BX('profile_change').value = 'N';
			</script>
			<?
			die();
		}
        ?>
        
          <h2 class="black fs_24 reg mb28">Оформление заказа</h2>

            <fieldset class="create_order_step">    
              <h3 class="fs_18 black reg mb15">Контактная информация</h3>
              
              <div class="clearfix">
                <div class="order_form_col f_left">
                  <div class="form_label">Ваше имя:</div>
                  <input type="text" value="Алена" class="txt_field">
                </div>
                <div class="order_form_col f_left">
                  <div class="form_label">Адрес эл. почты:</div>
                  <input type="text" value="alena@yandex.ru" class="txt_field">
                </div>
                <div class="order_form_col f_left">
                  <div class="form_label">Номер телефона:</div>
                  <input type="text" class="txt_field phone_mask">
                </div>
              </div>
            </fieldset>
            <fieldset class="create_order_step">
              <h3 class="fs_18 black reg mb15">Доставка</h3>
              
              <div class="form_row1">
                <div class="order_form_col">
                  <div class="form_label">Город:</div>
                  <select class="styler">
                    <option>Санкт-Петербург</option>
                    <option>Москва</option>
                    <option>Екатеринбург</option>
                  </select>
                </div>
              </div>

              <div class="form_row fs_12" id="delivery_row">
                <div class="form_label1">Способ доставки:</div>
                <label class="d_ib radio_wrap">
                  <input type="radio" class="styler" name="delivery">
                  <span class="radio_label">
                    Самовывоз
                    <span class="gray_font d_block">Бесплатно</span>
                  </span>
                </label>
                <label class="d_ib radio_wrap">
                  <input type="radio" class="styler" name="delivery"> 
                  <span class="radio_label">
                    Стандартная доставка
                    <span class="gray_font d_block">300 руб.</span>
                  </span>
                </label>
                <label class="d_ib radio_wrap">
                  <input type="radio" class="styler" name="delivery"> 
                  <span class="radio_label">
                    Доставка ко времени
                    <span class="gray_font d_block">600 руб.</span>
                  </span>
                </label>
                <label class="d_ib radio_wrap">
                  <input type="radio" class="styler" name="delivery"> 
                  <span class="radio_label">
                    Доставка в день заказа
                    <span class="gray_font d_block">800 руб.</span>
                  </span>
                </label>
                <label class="d_ib radio_wrap">
                  <input type="radio" class="styler" name="delivery"> 
                  <span class="radio_label">
                    Экспресс доставка
                    <span class="gray_font d_block">1 000 руб.</span>
                  </span>
                </label>

                <div id="delivery_details" class="delivery_details">
                  <!-- Delivery detail 1 -->
                  <div>
                    <div class="d_t">
                      <div class="d_tc scheme_pickup val_mid">
                        <a href="#" class="location_link">Схема проезда <br>для самовывоза</a>
                      </div>
                      <div class="d_tc scheme_pickup_content val_mid gray_font fs_12">
                        Условия доставки. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                      </div>
                    </div>
                  </div>
                  <!-- Delivery detail 1 END -->

                  <!-- Delivery detail 2 -->
                  <div class="wrapper">
                    <div class="order_form_col1 f_left">
                      <div class="form_label1">Адрес доставки:</div>
                      <textarea class="txt_field area_h113"></textarea>
                    </div>
                    <div class="wrapper">
                      <div class="form_label1">Дата доставки:</div>
                      <input type="text" class="txt_field order_form_col3 datepicker">
                    </div>
                  </div>
                  <!-- Delivery detail 2 END -->

                  <!-- Delivery detail 3 -->
                  <div class="wrapper">
                    <div class="order_form_col1 f_left">
                      <div class="form_label1">Адрес доставки:</div>
                      <textarea class="txt_field area_h113"></textarea>
                    </div>
                    <div class="wrapper fs_12">
                      <div class="mb20">
                        <div class="form_label1">Дата доставки:</div>
                        <input type="text" class="txt_field order_form_col3 datepicker">
                      </div>

                      <div class="form_label1">Время доставки:</div>
                      <label class="d_ib radio_wrap">
                        <input type="radio" class="styler" name="delivery_time"> 
                        <span class="radio_label">С 12:00 до 18:00</span>
                      </label>
                      <label class="d_ib radio_wrap">
                        <input type="radio" class="styler" name="delivery_time"> 
                        <span class="radio_label">С 18:00 до 22:00</span>
                      </label>

                    </div>
                  </div>
                  <!-- Delivery detail 3 END -->

                  <!-- Delivery detail 4 -->
                  <div class="wrapper">
                    <div class="order_form_col1 f_left">
                      <div class="form_label1">Адрес доставки:</div>
                      <textarea class="txt_field area_h113"></textarea>
                      <span class="grn_font heavy">Время доставки уточнит оператор</span>
                    </div>
                  </div>
                  <!-- Delivery detail 4 END -->

                  <!-- Delivery detail 5 -->
                  <div class="wrapper">
                    <div class="order_form_col1 f_left">
                      <div class="form_label1">Адрес доставки:</div>
                      <textarea class="txt_field area_h113"></textarea>
                      <span class="grn_font heavy">Время доставки уточнит оператор</span>
                    </div>
                  </div>
                  <!-- Delivery detail 5 END -->

                </div>

              </div>
            </fieldset>
            <fieldset class="create_order_step">
              <h3 class="fs_18 black reg mb15">Оплата</h3>
              
              <div class="form_row1 fs_12" id="payment_row">
                <div class="form_label1">Выберите способ оплаты:</div>
                <label class="d_ib radio_wrap">
                  <input type="radio" class="styler" name="payment"> <span class="radio_label">Наличными</span>
                </label>
                <label class="d_ib radio_wrap">
                  <input type="radio" class="styler" name="payment"> <span class="radio_label">Банковский перевод</span>
                </label>
              </div>

              <div id="payment_details" class="d_none">
                <div class="clearfix form_row2">
                  <div class="order_form_col f_left">
                    <div class="form_label1">Название компании:</div>
                    <input type="text" class="txt_field" value="ООО «Газпром»" onBlur="if(this.value=='') this.value='ООО «Газпром»'" onFocus="if(this.value =='ООО «Газпром»' ) this.value=''">
                  </div>
                  <div class="order_form_col f_left">
                    <div class="form_label1">ИНН:</div>
                    <input type="text" class="txt_field" value="23566347457" onBlur="if(this.value=='') this.value='23566347457'" onFocus="if(this.value =='23566347457' ) this.value=''">
                  </div>
                  <div class="order_form_col f_left">
                    <div class="form_label1">КПП:</div>
                    <input type="text" class="txt_field" value="23566347457" onBlur="if(this.value=='') this.value='23566347457'" onFocus="if(this.value =='23566347457' ) this.value=''">
                  </div>
                </div>
                
                <div class="clearfix form_row2">
                  <div class="order_form_col f_left">
                    <div class="form_label1">Расчетный счет:</div>
                     <input type="text" class="txt_field" value="ООО «Газпром»" onBlur="if(this.value=='') this.value='ООО «Газпром»'" onFocus="if(this.value =='ООО «Газпром»' ) this.value=''">
                  </div>
                  <div class="order_form_col f_left">
                    <div class="form_label1">БИК:</div>
                    <input type="text" class="txt_field" value="23566347457" onBlur="if(this.value=='') this.value='23566347457'" onFocus="if(this.value =='23566347457' ) this.value=''">
                  </div>
                  <div class="order_form_col f_left">
                    <div class="form_label1">Название банка:</div>
                    <input type="text" class="txt_field" value="23566347457" onBlur="if(this.value=='') this.value='23566347457'" onFocus="if(this.value =='23566347457' ) this.value=''">
                  </div>
                </div>
                
                <div class="clearfix">
                  <div class="order_form_col2">
                    <div class="form_label1">Юридический адрес:</div>
                    <textarea class="txt_field area_h113" onBlur="if(this.value=='') this.value='Санкт-Петербург, Невский проспект 55/4'" onFocus="if(this.value =='Санкт-Петербург, Невский проспект 55/4' ) this.value=''">Санкт-Петербург, Невский проспект 55/4</textarea>
                  </div>
                </div>
              </div>
            </fieldset>
            <fieldset class="create_order_step">
              <h3 class="fs_18 black reg mb17">Подтверждение</h3>
              
              <ul class="cart_total">
                <li class="cart_total_item">Ваше имя: Алена Торгуева</li>
                <li class="cart_total_item">Электронная почта: al12993@yandex.ru</li>
                <li class="cart_total_item">Телефон: +7 911 140 12 29</li>
                <li class="cart_total_item">Точное время доставки уточнит менеджер <br>при согласовании деталей заказа</li>
              </ul>

              <div class="cart_total_sum">
                <div class="fs_14">Общая сумма</div>
                <div class="product_item_price heavy">26 999 <span class="rub">₽</span></div>
              </div>

              <button class="btn2 grn_skin">Оформить заказ</button>
            </fieldset>
          </form>

        </div>  
      </div>