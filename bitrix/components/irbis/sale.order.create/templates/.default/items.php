<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="cart_items_box mb75">
    <?
    foreach($arResult["BASKET_ITEMS"] as $arItem)
    {
        //CDev::pre($arItem);
        $arItem["SUM"] = $arItem["PRICE"]*$arItem["QUANTITY"];
        ?>
        <section class="cart_item" id="item-<?=$arItem["ID"]?>">
            <div class="cart_item_cell1">
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="d_ib"><img src="<?=$arItem["PREVIEW_PICTURE_SRC"]?>" alt="<?=$arItem["NAME"]?>"/></a>
            </div>
            <div class="cart_item_cell2">
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><h4 class="fs_18 bold mb5"><?=$arItem["NAME"]?></h4></a>
                <?=$arItem["PREVIEW_TEXT"]?>
            </div>
            <div class="cart_item_cell3">
                <div class="form_label">Выберите кол-во:</div>
                <div class="mb15">
                    <select class="styler item-number" name="QUANTITY_<?=$arItem["ID"]?>" onchange='submitForm("Y");'>
                        <?
                        for($i=1; $i<100; $i++)
                        {
                            ?><option value="<?=$i?>" <?if($i==$arItem["QUANTITY"]):?>selected<?endif;?>><?=$i?></option><?
                        }
                        ?>
                    </select>
                </div>
                <?if($arItem["PROPERTY_INSTALL_VALUE"]=="да"):?>
                    <div class="p_rel">
                        <label class="checkbox_label fs_12 var2 abs_label">
                            <input type="checkbox" name="INSTALL_<?=$arItem["ID"]?>" class="styler" <?/*onchange='submitForm("Y");'*/?> <?if($arResult["INSTALL_".$arItem["ID"]]=="on"):?>checked<?endif;?>/>
                            <span class="d_block wrapper">Интересует установка<br />(от 300 руб.)</span>
                        </label>
                    </div>
                <?endif;?>
            </div>
            <div class="cart_item_cell4">
                <div class="product_item_price heavy mb20"><?=number_format($arItem["SUM"], 0, "", " ")?> <span class="rub">₽</span></div>
                <?if(intval($arItem["PROPERTY_PRICE_CHANGE_VALUE"])>0):?>
                    <div class="p_rel">
                        <label class="checkbox_label fs_12 var2 abs_label">
                            <input type="checkbox" name="CHANGED_PRICE_<?=$arItem["ID"]?>" class="styler" onchange='submitForm("Y");' <?if($arResult["CHANGED_PRICE_".$arItem["ID"]]=="on"):?>checked<?endif;?>/>
                            <span class="d_block wrapper">
																			<?if(strstr($arItem["DETAIL_PAGE_URL"], "/catalog/battery/")):?>
													Сдача отработанного аккумулятора аналогичной ёмкости
												<?else:?>
													Предъявляю купон на скидку
												<?endif;?>
</span>
                        </label>
                    </div>
                <?endif;?>
            </div>
            <div class="cart_item_cell5">
                <input type="hidden" name="DELETE_<?=$arItem["ID"]?>" value="N">
                <a href="#" class="btn2 bold red_skin item-delete">удалить</a>
            </div>
        </section>
        <?
    }
    ?>
    <div style="margin-top: -20px;">
        <span class="modal_btn" id="sale_review" data-modal="#sale">Дополнительная скидка 100 руб. за отзыв в Яндекс.Маркет!</span>
    </div>
    <section class="total_cart_sum">
    общая сумма:
    <div class="product_item_price heavy"><?=number_format($arResult["ORDER_PRICE"], 0, "", " ")?> <span class="rub">₽</span></div>
    </section>
</div>
