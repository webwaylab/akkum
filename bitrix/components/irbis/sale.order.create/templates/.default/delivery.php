<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<fieldset class="create_order_step">
    <h3 class="fs_18 black reg mb15">Доставка</h3>
    
    <div class="form_row1">
        <div class="order_form_col">
            <div class="form_label">Город:</div>
            <select class="styler" name="LOCATION">
                <?/*<option></option>*/?>
                <?
                //CDev::pre($arResult["LOCATION_LIST"], true, false);
                foreach($arResult["LOCATION_LIST"] as $arLocation)
                {
                    ?><option value="<?=$arLocation["ID"]?>" <?if($arLocation["ID"]==$arResult["LOCATION"]):?>selected<?endif;?>><?=$arLocation["CITY_NAME"]?></option><?
                }
                ?>
            </select>
        </div>
    </div>

    <div class="form_row fs_12" id="delivery_row">
        <div class="form_label1">Способ доставки:</div>
        <?
        foreach($arResult["DELIVERY_LIST"] as $arDelivery)
        {
            ?>
            <label class="d_ib radio_wrap">
                <input type="radio" class="styler" name="DELIVERY" value="<?=$arDelivery["ID"]?>" <?if($arDelivery["ID"]==$arResult["DELIVERY"]):?>checked<?endif;?>>
                <span class="radio_label">
                    <?=$arDelivery["NAME"]?>
                    <span class="gray_font d_block"><?=intval($arDelivery["PRICE"])==0 ? "Бесплатно" : number_format($arDelivery["PRICE"], 0, "", " ")." руб."?></span>
                </span>
            </label>
            <?
        }
        ?>
        
        <div id="delivery_details" class="delivery_details">
            <!-- Delivery detail 1 -->
            <?
            switch($arResult["DELIVERY"])
            {
                case "2":
                    ?>
                    <div>
                        <div class="d_t">
                            <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/delivery-pickup.php"), false);?>
                        </div>
                    </div>
                    <?
                break;
                case "1":
                    ?>
                    <div class="wrapper">
                        <div class="order_form_col1 f_left">
                            <div class="form_label1">Адрес доставки:</div>
                            <textarea name="ADDRESS" class="txt_field area_h113"><?=$arResult["ADDRESS"]?></textarea>
                        </div>
                        <div class="wrapper">
                            <div class="form_label1">Дата доставки:</div>
                            <input type="text" name="DATE_DELIVERY" class="txt_field order_form_col3 datepicker" value="<?=$arResult["DATE_DELIVERY"]?>">
                        </div>
                    </div>
                    <?
                break;
                case "3":
                    ?>
                    <div class="wrapper">
                        <div class="order_form_col1 f_left">
                            <div class="form_label1">Адрес доставки:</div>
                            <textarea name="ADDRESS" class="txt_field area_h113"><?=$arResult["ADDRESS"]?></textarea>
                        </div>
                        <div class="wrapper fs_12">
                            <div class="mb20">
                                <div class="form_label1">Дата доставки:</div>
                                <input type="text" name="DATE_DELIVERY" class="txt_field order_form_col3 datepicker" value="<?=$arResult["DATE_DELIVERY"]?>">
                            </div>
                            <div class="form_label1">Время доставки:</div>
                            <label class="d_ib radio_wrap">
                                <input type="radio" class="styler" name="DELIVERY_TIME" value="С 12:00 до 18:00" <?if("С 12:00 до 18:00"==$arResult["DELIVERY_TIME"]):?>checked<?endif;?>> 
                                <span class="radio_label">С 12:00 до 18:00</span>
                            </label>
                            <label class="d_ib radio_wrap">
                                <input type="radio" class="styler" name="DELIVERY_TIME" value="С 18:00 до 22:00" <?if("С 18:00 до 22:00"==$arResult["DELIVERY_TIME"]):?>checked<?endif;?>> 
                                <span class="radio_label">С 18:00 до 22:00</span>
                            </label>
                        </div>
                    </div>
                    <?
                break;
                case "4":
                case "5":
                    ?>
                    <div class="wrapper">
                        <div class="order_form_col1 f_left">
                            <div class="form_label1">Адрес доставки:</div>
                            <textarea name="ADDRESS" class="txt_field area_h113"><?=$arResult["ADDRESS"]?></textarea>
                            <span class="grn_font heavy">Время доставки уточнит оператор</span>
                        </div>
                    </div>
                    <?
                break;
            }
            ?>
        </div>
    
    </div>
</fieldset>