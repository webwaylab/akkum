<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<fieldset class="create_order_step">
    <h3 class="fs_18 black reg mb15">Оплата</h3>
    <div class="form_row1 fs_12" id="payment_row">
        <div class="form_label1">Выберите способ оплаты:</div>
        <?
        foreach($arResult["PAYMENT_LIST"] as $arPayment)
        {
            ?>
            <label class="d_ib radio_wrap">
                <input type="radio" class="styler" name="PAYMENT" value="<?=$arPayment["ID"]?>" <?if($arPayment["ID"]==$arResult["PAYMENT"]):?>checked<?endif;?>> <span class="radio_label"><?=$arPayment["NAME"]?></span>
            </label>
            <?
        }
        ?>
    </div>
          
    <div id="payment_details" class="d_none" <?if($arResult["PAYMENT"]==6):?>style="display:block"<?endif;?>>
        <div class="clearfix form_row2">
            <div class="order_form_col f_left">
                <div class="form_label1">Название компании:</div>
                <input type="text" name="ANKETA[FULL_NAME]" class="txt_field" value="<?=$arResult["ANKETA"]["FULL_NAME"]?>">
            </div>
            <div class="order_form_col f_left">
                <div class="form_label1">ИНН:</div>
                <input type="text" name="ANKETA[INN]" class="txt_field" value="<?=$arResult["ANKETA"]["INN"]?>" >
            </div>
            <div class="order_form_col f_left">
                <div class="form_label1">КПП:</div>
                <input type="text" name="ANKETA[KPP]" class="txt_field" value="<?=$arResult["ANKETA"]["KPP"]?>">
            </div>
        </div>
        
        <div class="clearfix form_row2">
            <div class="order_form_col f_left">
                <div class="form_label1">Расчетный счет:</div>
                <input type="text" name="ANKETA[RS]" class="txt_field" value="<?=$arResult["ANKETA"]["RS"]?>">
            </div>
            <div class="order_form_col f_left">
                <div class="form_label1">БИК:</div>
                <input type="text" name="ANKETA[BIK]" class="txt_field" value="<?=$arResult["ANKETA"]["BIK"]?>">
            </div>
            <div class="order_form_col f_left">
                <div class="form_label1">Название банка:</div>
                <input type="text" name="ANKETA[BANK_NAME]" class="txt_field" value="<?=$arResult["ANKETA"]["BANK_NAME"]?>">
            </div>
        </div>
        
        <div class="clearfix">
            <div class="order_form_col2">
                <div class="form_label1">Юридический адрес:</div>
                <textarea name="ANKETA[ADDRESS]" class="txt_field area_h113"><?=$arResult["ANKETA"]["ADDRESS"]?></textarea>
            </div>
        </div>
    </div>
</fieldset>