<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class SaleOrderCreateComponent extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
	{
	   global $APPLICATION;
	   return $arParams;
    }
    
    protected function checkRequiredModules()
	{
        if(! \Bitrix\Main\Loader::includeModule ('sale'))
		{
			ShowError(GetMessage('SALE_MODULE_NOT_INSTALL'));
			return;
		}
        
        if(! \Bitrix\Main\Loader::includeModule ('catalog'))
		{
			ShowError(GetMessage('CATALOG_MODULE_NOT_INSTALL'));
			return;
		}
        
        if(! \Bitrix\Main\Loader::includeModule ('iblock'))
		{
			ShowError(GetMessage('IBLOCK_MODULE_NOT_INSTALL'));
			return;
		}

		if(! \Bitrix\Main\Loader::includeModule("currency"))
		{
			ShowError(GetMessage("CURRENCY_MODULE_NOT_INSTALLED"));
			return;
		}
	}
    
    public function executeComponent()
	{
        global $USER;
        $this->checkRequiredModules();
        
        $this->arResult = array(
            "BASE_LANG_CURRENCY" => CSaleLang::GetLangCurrency(SITE_ID),
            "WEIGHT_UNIT" => htmlspecialcharsbx(COption::GetOptionString('sale', 'weight_unit', false, SITE_ID)),
            "WEIGHT_KOEF" => htmlspecialcharsbx(COption::GetOptionString('sale', 'weight_koef', 1, SITE_ID)),
            "LOCATION_LIST" => $this->getLocationList(),
            "PAYMENT_LAW_ID" => 6
        );
        
        //Список доставок и цена
        $this->getDeliveryList();
        
        //Оплата
        $this->paySystemList();
        
        if($_GET["logout"]=="yes" && !$USER->IsAuthorized())
        {
            unset($_SESSION["ORDER"]);
        }
        
        if($_POST["is_ajax_post"] != "Y")
        {
            $this->getItemsList();
            
            if(isset($_SESSION["ORDER"]) && is_array($_SESSION["ORDER"]))
            {
                $this->arResult+= $_SESSION["ORDER"];            
            }
            
            $this->arResult["PRICE_DELIVERY"] = $this->arResult["DELIVERY_LIST"][$this->arResult["DELIVERY"]]["PRICE"];
            $this->arResult["TOTAL_PRICE"] = $this->arResult["ORDER_PRICE"] + $this->arResult["PRICE_DELIVERY"];
        }
        
        if($USER->IsAuthorized())
        {
            $rsUser = CUser::GetByID($USER->GetID());
            $arUser = $rsUser->Fetch();
            
            $arUserProps = array("NAME", "EMAIL", "PERSONAL_PHONE");
            
            foreach($arUserProps as $code)
            {
                if(empty($this->arResult["USER"][$code]))
                {
                    $this->arResult["USER"][$code] = $_SESSION["ORDER"]["USER"][$code] = $arUser[$code];
                }
            }
            
            if(empty($this->arResult["ADDRESS"]))
                $this->arResult["ADDRESS"] = $_SESSION["ORDER"]["ADDRESS"] = $arUser["UF_ADDRESS"];
        }
        
        //Аякс
        $this->ajaxRequest();
        
        if($this->arResult["PAYMENT"]==$this->arResult["PAYMENT_LAW_ID"])
        {
            $this->arResult["TOTAL_PRICE"] = 1.03*$this->arResult["TOTAL_PRICE"];
        }
        
        $this->includeComponentTemplate();
	}
    
    public function getLocationList()
    {
        $arLocations = array();
        $array = array();
        $db_vars = CSaleLocation::GetList(
            array(
                    "SORT" => "ASC",
                    "COUNTRY_NAME_LANG" => "ASC",
                    "CITY_NAME_LANG" => "ASC"
                ),
            array("LID" => LANGUAGE_ID, "COUNTRY_ID"=>1, "!CITY_NAME"=>false),
            false,
            false,
            array()
        );
        while($vars = $db_vars->Fetch())
        {
            if($vars["CITY_NAME"]=="Москва" || $vars["CITY_NAME"]=="Санкт-Петербург")
            {
                $array[$vars["ID"]] = $vars;
                
            }else{
                $arLocations[$vars["ID"]] = $vars;
            }
            
        }
        
        $arLocations = $array + $arLocations;
        //$array = array_reverse($array);
        
        /*foreach($array as $first)
        {
            array_unshift($arLocations, $first);
        }*/
        
        return $arLocations;
    }
    
    public function getItemsList()
    {
        CModule::IncludeModule("sale");
        
        $arElementId = array();
        $arSetParentWeight = array();
        $DISCOUNT_PRICE_ALL = 0;
        $arResult["MAX_DIMENSIONS"] = $arResult["ITEMS_DIMENSIONS"] = array();
        $arSelFields = array("ID", "PRODUCT_ID", "QUANTITY", "DELAY",
        	"CAN_BUY", "PRICE", "WEIGHT", "NAME", "CURRENCY",  "DISCOUNT_PRICE", "DIMENSIONS", "DETAIL_PAGE_URL"
        );
        $dbBasketItems = CSaleBasket::GetList(
        		array("ID" => "ASC"),
        		array(
        				"FUSER_ID" => CSaleBasket::GetBasketUserID(),
        				"LID" => SITE_ID,
        				"ORDER_ID" => "NULL"
        			),
        		false,
        		false,
        		$arSelFields
        	);
        while ($arItem = $dbBasketItems->GetNext())
        {
            if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y")
        	{
        		$arItem["PRICE"] = roundEx($arItem["PRICE"], SALE_VALUE_PRECISION);
        		$arItem["QUANTITY"] = DoubleVal($arItem["QUANTITY"]);
        
        		$arItem["WEIGHT"] = DoubleVal($arItem["WEIGHT"]);
        		$arItem["VAT_RATE"] = DoubleVal($arItem["VAT_RATE"]);
        
        		$arDim = unserialize($arItem["~DIMENSIONS"]);
        
        		if(is_array($arDim))
        		{
        			$arItem["DIMENSIONS"] = $arDim;
        			unset($arItem["~DIMENSIONS"]);
        
        			$arResult["MAX_DIMENSIONS"] = CSaleDeliveryHelper::getMaxDimensions(
                                                                            array(
                                                                                    $arDim["WIDTH"],
                                                                                    $arDim["HEIGHT"],
                                                                                    $arDim["LENGTH"]
                                                                                    ),
                                                                            $arResult["MAX_DIMENSIONS"]);
        
        			$arResult["ITEMS_DIMENSIONS"][] = $arDim;
        		}
        
        		$arItem["PRICE_FORMATED"] = SaleFormatCurrency($arItem["PRICE"], $arItem["CURRENCY"]);
        		$arItem["WEIGHT_FORMATED"] = roundEx(DoubleVal($arItem["WEIGHT"]/$arResult["WEIGHT_KOEF"]), SALE_WEIGHT_PRECISION)." ".$arResult["WEIGHT_UNIT"];
        
        		if($arItem["DISCOUNT_PRICE"] > 0)
        		{
        			$arItem["DISCOUNT_PRICE_PERCENT"] = $arItem["DISCOUNT_PRICE"]*100 / ($arItem["DISCOUNT_PRICE"] + $arItem["PRICE"]);
        			$arItem["DISCOUNT_PRICE_PERCENT_FORMATED"] = roundEx($arItem["DISCOUNT_PRICE_PERCENT"], 0)."%";
        		}
        
        		if (!CSaleBasketHelper::isSetItem($arItem))
        		{
        			$DISCOUNT_PRICE_ALL += $arItem["DISCOUNT_PRICE"] * $arItem["QUANTITY"];
        			$arItem["DISCOUNT_PRICE"] = roundEx($arItem["DISCOUNT_PRICE"], SALE_VALUE_PRECISION);
        			$arResult["ORDER_PRICE"] += $arItem["PRICE"] * $arItem["QUANTITY"];
        		}
        
        		if (!CSaleBasketHelper::isSetItem($arItem))
        		{
        			$arResult["ORDER_WEIGHT"] += $arItem["WEIGHT"] * $arItem["QUANTITY"];
        		}
        
        		$arResult["BASKET_ITEMS"][] = $arItem;
                $arElementId[] = $arItem["PRODUCT_ID"];
        	}
            
            $arResult["PRICE_WITHOUT_DISCOUNT"] = SaleFormatCurrency($arResult["ORDER_PRICE"] + $DISCOUNT_PRICE_ALL, $allCurrency);
            $arResult["ORDER_WEIGHT_FORMATED"] = roundEx(DoubleVal($arResult["ORDER_WEIGHT"]/$arResult["WEIGHT_KOEF"]), SALE_WEIGHT_PRECISION)." ".$arResult["WEIGHT_UNIT"];
        	$arResult["ORDER_PRICE_FORMATED"] = SaleFormatCurrency($arResult["ORDER_PRICE"], $arResult["BASE_LANG_CURRENCY"]);
        	$arResult["VAT_SUM_FORMATED"] = SaleFormatCurrency($arResult["VAT_SUM"], $arResult["BASE_LANG_CURRENCY"]);
        }
        
        // get properties for iblock elements and their parents (if any)
        $arCustomSelectFields = array("PROPERTY_PRICE_CHANGE", "PROPERTY_INSTALL");
        $arSelect = array_merge(array("ID", "PREVIEW_PICTURE", "DETAIL_PICTURE", "PREVIEW_TEXT"), $arCustomSelectFields);
        $arProductData = getProductProps($arElementId, $arSelect);
        foreach ($arResult["BASKET_ITEMS"] as &$arResultItem)
        {
        	$productId = $arResultItem["PRODUCT_ID"];
        	if((int)$arProductData[$productId]["PREVIEW_PICTURE"] > 0)
        		$arResultItem["PREVIEW_PICTURE"] = $arProductData[$productId]["PREVIEW_PICTURE"];
        	if((int)$arProductData[$productId]["DETAIL_PICTURE"] > 0)
        		$arResultItem["DETAIL_PICTURE"] = $arProductData[$productId]["DETAIL_PICTURE"];
        	if($arProductData[$productId]["PREVIEW_TEXT"] != '')
        		$arResultItem["PREVIEW_TEXT"] = $arProductData[$productId]["PREVIEW_TEXT"];
        
        	foreach ($arProductData[$arResultItem["PRODUCT_ID"]] as $key => $value)
        	{
        		if (strpos($key, "PROPERTY_") !== false)
        			$arResultItem[$key] = $value;
        	}
            
        	$arResultItem["PREVIEW_PICTURE_SRC"] = "";
        	if (isset($arResultItem["PREVIEW_PICTURE"]) && intval($arResultItem["PREVIEW_PICTURE"]) > 0)
        	{
        		$arImage = CFile::GetFileArray($arResultItem["PREVIEW_PICTURE"]);
        		if ($arImage)
        		{
        			$arFileTmp = CFile::ResizeImageGet(
        				$arImage,
        				array("width" => "110", "height" =>"110"),
        				BX_RESIZE_IMAGE_PROPORTIONAL,
        				true
        			);
        
        			$arResultItem["PREVIEW_PICTURE_SRC"] = $arFileTmp["src"];
        		}
        	}
        
        	$arResultItem["DETAIL_PICTURE_SRC"] = "";
        	if (isset($arResultItem["DETAIL_PICTURE"]) && intval($arResultItem["DETAIL_PICTURE"]) > 0)
        	{
        		$arImage = CFile::GetFileArray($arResultItem["DETAIL_PICTURE"]);
        		if ($arImage)
        		{
        			$arFileTmp = CFile::ResizeImageGet(
        				$arImage,
        				array("width" => "110", "height" =>"110"),
        				BX_RESIZE_IMAGE_PROPORTIONAL,
        				true
        			);
        
        			$arResultItem["DETAIL_PICTURE_SRC"] = $arFileTmp["src"];
        		}
        	}
            
            if(empty($arResultItem["PREVIEW_PICTURE_SRC"]))
            {
                $arResultItem["PREVIEW_PICTURE_SRC"] = $arResultItem["DETAIL_PICTURE_SRC"];
            }
        }
        if (isset($arResultItem))
        	unset($arResultItem);
        
        $this->arResult["ORDER_PRICE"] = $arResult["ORDER_PRICE"];
        $this->arResult["BASKET_ITEMS"] = $arResult["BASKET_ITEMS"];
    }
    
    public function ajaxRequest()
    {
        global $USER;
        
        if($_SERVER["REQUEST_METHOD"] == "POST" && check_bitrix_sessid())
    	{
            unset($_POST["sessid"]);
            $_SESSION["ORDER"] = $_POST;    	   

            $this->arResult = array_merge($this->arResult, $_SESSION["ORDER"]);

            $this->checkUserFields();

            $this->recalculateBasket($_POST);
            
            $this->getItemsList();
            
            $this->arResult["TOTAL_PRICE"] = $this->arResult["ORDER_PRICE"];
            
            if(intval($_POST["LOCATION"])==0)
            {
                $this->arResult["ERRORS"]["LOCATION"] = "Выберите город доставки";
            }
            
            if(intval($_POST["DELIVERY"])==0)
            {
                $this->arResult["ERRORS"]["DELIVERY"] = "Выберите способ доставки";
                
            }
            else
            {
                
                $this->arResult["PRICE_DELIVERY"] = $this->arResult["DELIVERY_LIST"][$_POST["DELIVERY"]]["PRICE"];
                $this->arResult["TOTAL_PRICE"]+= $this->arResult["PRICE_DELIVERY"];
                
                switch($arResult["DELIVERY"])
                {
                    case "1":
                    
                        if(empty($_POST["ADDRESS"]))
                        {
                            $this->arResult["ERRORS"]["ADDRESS"] = "Заполните адрес доставки";
                        }
                        if(empty($_POST["DATE_DELIVERY"]))
                        {
                            $this->arResult["ERRORS"]["DATE_DELIVERY"] = "Заполните дату доставки";
                        }
                        
                    break;
                    case "3":
                    
                        if(empty($_POST["ADDRESS"]))
                        {
                            $this->arResult["ERRORS"]["ADDRESS"] = "Заполните адрес доставки";
                        }
                        if(empty($_POST["DATE_DELIVERY"]))
                        {
                            $this->arResult["ERRORS"]["DATE_DELIVERY"] = "Заполните дату доставки";
                        }
                        if(empty($_POST["DELIVERY_TIME"]))
                        {
                            $this->arResult["ERRORS"]["DELIVERY_TIME"] = "Заполните время доставки";
                        }

                    break;
                    case "4":
                    case "5":
                    
                        if(empty($_POST["ADDRESS"]))
                        {
                            $this->arResult["ERRORS"]["ADDRESS"] = "Заполните адрес доставки";
                        }
                        
                    break;
                }
            }
            
            if(intval($_POST["PAYMENT"])==0)
            {
                $this->arResult["ERRORS"]["PAYMENT"] = "Выберите способ оплаты";
            }
            
            if(intval($_POST["PAYMENT"])==$this->arResult["PAYMENT_LAW_ID"])
            {
                $this->checkBankAnketa();
            }
            
            if($_POST["create"]=="Y")
            {
                if(count($this->arResult["ERRORS"])>0)
                {
                    foreach($this->arResult["ERRORS"] as $error)
                    {
                        if(is_array($error))
                        {
                            $error = implode("<br/>", $error);
                        }
                        $this->arResult["ERROR_MESSAGE"].= $error.".<br />";
                    }
                }else{
                    $this->save();
                }
            }
    	}
    }
    
    public function checkUserFields()
    {
        global $USER;        
        $arFields = $_POST["USER"];
        $arFields["PERSONAL_PHONE"] = phoneToNumber($arFields["PERSONAL_PHONE"]);
        
        if($USER->IsAuthorized())
        {
            //$this->arResult["ERRORS"]["USER"]["AUTH"] = "Для оформления заказа нужно авторизоваться.";
        //}else{
            $user_id = $USER->GetID();
        }
        
        if(strlen($arFields["NAME"])<3)
        {
            $this->arResult["ERRORS"]["USER"]["NAME"] = "Введите свое имя";
        }
        
        if(!$this->isValidEmail($arFields["EMAIL"]))
        {
            $this->arResult["ERRORS"]["USER"]["EMAIL"] = "Введите корректный адрес эл. почты";
        }else{
            if($this->checkUserByEmail($arFields["EMAIL"], $user_id))
            {
                $this->arResult["ERRORS"]["USER"]["EMAIL"] = "Покупатель с указанным email-адресом уже зарегистрирован. Пожалуйста, <a href='#' class='modal_btn' data-modal='#authorization'>войдите</a> для оформления заказа";
            }
        }
        
        if(!$this->isValidPhone($arFields["PERSONAL_PHONE"]))
        {
            $this->arResult["ERRORS"]["USER"]["PERSONAL_PHONE"] = "Введите корректный номер телефона";
        }
        
        if(count($this->arResult["ERRORS"]["USER"])==0 && !$USER->IsAuthorized())
        {
            $this->userCreate();
        }
    }
    
    public function checkBankAnketa()
    {
        $arAnketa = $this->arResult["ANKETA"];
        $arRequire = array(
            "FULL_NAME", 
            "INN", 
            "KPP", 
            "RS", 
            "BIK", 
            "BANK_NAME",
            "ADDRESS"
        );
        
        foreach($arRequire as $code)
        {
            if(empty($arAnketa[$code]))
            {
                $this->arResult["ERRORS"][$code] = "Заполните банковские реквизиты";
                break;
            }
        }
    }
    
    public function isValidEmail($email)
    {
        return preg_match("/^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-]+)*\.([a-zA-Z]{2,6})$/", $email);
    }
    
    public function isValidPhone($phone)
    {
        return preg_match("/^[7]{1}[0-9]{10}$/", $phone);
    }
    
    public function checkUserByEmail($email, $user_id = false)
    {
        $filter = Array("=EMAIL" => $email);
        if(intval($user_id)>0)
        {
            $filter["!ID"] = $user_id;
        }
        $rsUsers = CUser::GetList(($by="personal_country"), ($order="desc"), $filter);
        if($rsUsers->NavNext(true, "f_"))
        {
            return true;
        }
        
        return false;
    }
    
   	public function recalculateBasket($arPost)
	{
        CModule::IncludeModule("sale");
        
		global $USER;
		$arRes = array();

		$arTmpItems = array();
        $this->arResult["ADDITIONAL_INFO"] = "";
        
        $dbItems = CSaleBasket::GetList(
    		array("ID" => "ASC"),
    		array(
    				"FUSER_ID" => CSaleBasket::GetBasketUserID(),
    				"LID" => SITE_ID,
    				"ORDER_ID" => "NULL"
    			),
    		false,
    		false,
    		array(
				"ID", "NAME", "MODULE", "PRODUCT_ID", "PRICE", "QUANTITY", "DELAY", "CAN_BUY"
			)
    	);
		while ($arItem = $dbItems->Fetch())
		{
			$arTmpItems[] = $arItem;
		}
        
        $this->arResult["NEW_ITEMS"] = $arPost;

		foreach ($arTmpItems as $arItem)
		{
			$isFloatQuantity = false;
            
            //Если поменяли количество
			if (!isset($arPost["QUANTITY_".$arItem["ID"]]) || floatval($arPost["QUANTITY_".$arItem["ID"]]) <= 0)
			{
				$quantityTmp = intval($arItem["QUANTITY"]);
			}
			else
			{
				$quantityTmp = intval($arPost["QUANTITY_".$arItem["ID"]]);
			}

			$deleteTmp = ($arPost["DELETE_".$arItem["ID"]] == "Y") ? "Y" : "N";
            
            //удаление
			if ($deleteTmp == "Y")
			{
				CSaleBasket::Delete($arItem["ID"]);
			}
			elseif ($arItem["CAN_BUY"] == "Y")
			{
				unset($arFields);
				$arFields = array();
				$arFields["QUANTITY"] = $quantityTmp;
                if($arPost["CHANGED_PRICE_".$arItem["ID"]]=="on")
                {
                    $price = $this->changedPrice($arItem["PRODUCT_ID"]);
                    
                    $arFields["DISCOUNT_PRICE"] = $price;
                    /*$arFields["PROPS"][] = array(
                        "NAME" => "Сдача отработанного аккумулятора аналогичной ёмкости", 
                        "CODE" => "CHANGE",
                        "VALUE" => "Да",
                        "SORT" => 100
                    ); */
                    $this->arResult["ADDITIONAL_INFO"].= "Для ".$arItem["NAME"].": Сдача отработанного аккумулятора аналогичной ёмкости\r\n";
                }else{
                    $ar_res = CPrice::GetBasePrice($arItem["PRODUCT_ID"]);
                    $price = $ar_res["PRICE"];
                    
                    unset($arFields["DISCOUNT_PRICE"]);
                    //$arFields["PROPS"] = array();
                }
                $arFields["PRICE"] = $price;

				//if (count($arFields) > 0 && ($arItem["QUANTITY"] != $arFields["QUANTITY"]) || $arItem["PRICE"]!=$price)
				CSaleBasket::Update($arItem["ID"], $arFields);
                    
                if($arPost["INSTALL_".$arItem["ID"]]=="on")
                {
                    $this->arResult["ADDITIONAL_INFO"].= "Для ".$arItem["NAME"].": Интересует установка\r\n";
                }
                
			}
		}

		return $arRes;
	}
    
    public function changedPrice($PRODUCT_ID)
    {
        CModule::IncludeModule("iblock");
        
        $db_props = CIBlockElement::GetProperty(CATALOG_ID, $PRODUCT_ID, array("sort" => "asc"), Array("CODE"=>"PRICE_CHANGE"));
        if($ar_props = $db_props->Fetch())
        {
            //return intval($ar_props["VALUE"]);
            return round(doubleval($ar_props["VALUE"]),2); //aquakosh (skype): Прайс вообще-то может быть с копейками, поэтому double и round.
        }
    }
    
    public function getDeliveryList()
    {
        $arResult = $this->arResult;
        
        $obCache = new CPHPCache; 
        $life_time = 60*60*24; //24 часа
        
        $cache_id = "locations";  
        //$obCache->CleanDir();
        
        // если кэш есть и он ещё не истек то
        if($obCache->InitCache($life_time, $cache_id, "/"))
        {
            // получаем закешированные переменные
            $vars = $obCache->GetVars();
            $arResult["DELIVERY_LIST"] = $vars["LOCATIONS"];
        }
        elseif ($obCache->StartDataCache()) // иначе запишем для того, чтобы следующий раз получить 
        {
            $arResult["DELIVERY_LIST"] = array();
            $dbDelivery = CSaleDelivery::GetList(
    			array("SORT"=>"ASC", "NAME"=>"ASC"),
    			array(
    				"LID" => SITE_ID,
    				"+<=WEIGHT_FROM" => $arResult["ORDER_WEIGHT"],
    				"+>=WEIGHT_TO" => $arResult["ORDER_WEIGHT"],
    				"+<=ORDER_PRICE_FROM" => $arResult["ORDER_PRICE"],
    				"+>=ORDER_PRICE_TO" => $arResult["ORDER_PRICE"],
    				"ACTIVE" => "Y",
    				"LOCATION" => $arResult["LOCATION"],
    			)
    		);
    		while ($arDelivery = $dbDelivery->Fetch())
    		{
                $arResult["DELIVERY_LIST"][$arDelivery["ID"]] = $arDelivery;
    		}
            
            $obCache->EndDataCache(array(
                "LOCATIONS"    => $arResult["DELIVERY_LIST"]
            ));
        }
        
        $this->arResult = $arResult;
                
        return $arResult;
    }
    
    public function paySystemList()
    {
        $arResult = $this->arResult;
        
        $pList = array();
        $arFilter = array("ACTIVE"=>"Y", "PERSON_TYPE_ID"=>1);
        
        $dbPaySystem = CSalePaySystem::GetList(
				array("SORT" => "ASC", "PSA_NAME" => "ASC"),
				$arFilter
		);

		while ($arPaySystem = $dbPaySystem->Fetch())
		{
			if( CSaleDelivery2PaySystem::isPaySystemApplicable($arPaySystem["ID"], $arResult["DELIVERY_ID"]))
			{

				if(!CSalePaySystemsHelper::checkPSCompability(
											$arPaySystem["PSA_ACTION_FILE"],
											$arOrder,
											$arResult["ORDER_PRICE"],
											$arResult["DELIVERY_PRICE"],
											$arResult["LOCATION"]
										))
				{
					continue;
				}

				if ($arPaySystem["PSA_LOGOTIP"] > 0)
					$arPaySystem["PSA_LOGOTIP"] = CFile::GetFileArray($arPaySystem["PSA_LOGOTIP"]);

				$arPaySystem["PSA_NAME"] = htmlspecialcharsEx($arPaySystem["PSA_NAME"]);
				$pList[$arPaySystem["ID"]] = $arPaySystem;
				$pList[$arPaySystem["ID"]]["PRICE"] = CSalePaySystemsHelper::getPSPrice(
																			$arPaySystem,
																			$arResult["ORDER_PRICE"],
																			$arResult["DELIVERY_PRICE"],
																			$arResult["LOCATION"]);
			}
		}
        
        $arResult["PAYMENT_LIST"] = $pList;
        
        $this->arResult = $arResult;
                
        return $arResult;
    }
    
    public function userCreate()
    {
        global $USER;
        
        $saveFields = array();
        $saveFields = Array(
            "NAME"              => $this->arResult["USER"]["NAME"],
            "EMAIL"             => $this->arResult["USER"]["EMAIL"],
            "LOGIN"             => $this->arResult["USER"]["EMAIL"],
            "PERSONAL_PHONE"    => $this->arResult["USER"]["PERSONAL_PHONE"],
            "LID"               => SITE_ID,
            "ACTIVE"            => "Y",
        );
        $def_group = COption::GetOptionString("main", "new_user_registration_def_group", "");
		if($def_group!="")
		{
			$GROUP_ID = explode(",", $def_group);
			$arPolicy = $USER->GetGroupPolicy($GROUP_ID);
            $saveFields["GROUP_ID"] = $GROUP_ID;
		}
		else
		{
			$arPolicy = $USER->GetGroupPolicy(array());
		}
        
        $password_min_length = intval($arPolicy["PASSWORD_LENGTH"]);
    
		if($password_min_length <= 0)
			$password_min_length = 6;
            
		$password_chars = array(
			"abcdefghijklnmopqrstuvwxyz",
			"ABCDEFGHIJKLNMOPQRSTUVWXYZ",
			"0123456789",
		);
        
        if($arPolicy["PASSWORD_PUNCTUATION"] === "Y")
		    $password_chars[] = ",.<>/?;:'\"[]{}\|`~!@#\$%^&*()-_+=";
            
		$saveFields["PASSWORD"] = $saveFields["CONFIRM_PASSWORD"] = randString($password_min_length+2, $password_chars);
    
        COption::SetOptionString("main","captcha_registration","N");
        $arAuthResult = $USER->Register(
            $saveFields["EMAIL"], 
            $saveFields["NAME"], 
            "", 
            $saveFields["PASSWORD"], 
            $saveFields["PASSWORD"],
            $saveFields["EMAIL"]
        );
        COption::SetOptionString("main","captcha_registration","Y");

        if ($arAuthResult != False && $arAuthResult["TYPE"] == "ERROR")
        {
            $arResult["ERRORS"]["REGISTER"] = GetMessage("STOF_ERROR_REG").((strlen($arAuthResult["MESSAGE"]) > 0) ? ": ".$arAuthResult["MESSAGE"] : "" );
            //$arResult["ERRORS"]["REGISTER"] = "Ошибка регистрации";
        }	
		else
		{
			if ($USER->IsAuthorized())
			{
			    $arEventFields = array(
                    "EMAIL" => $saveFields["EMAIL"],
                    "LOGIN" => $saveFields["EMAIL"],
                    "PASSWORD" => $saveFields["PASSWORD"]
                );
			    
                CEvent::Send("NEW_USER_CREATE", SITE_ID, $arEventFields);
                
                //Обновить телефон
                $сuser = new CUser;
                $сuser->Update($USER->GetID(), array("PERSONAL_PHONE" => $this->arResult["USER"]["PERSONAL_PHONE"]));
			}
			else
			{
				$arResult["OK_MESSAGE"][] = GetMessage("STOF_ERROR_REG_CONFIRM");
			}
		}    
        /*$user = new CUser;            
        $ID = $user->Add($saveFields);
        if (intval($ID) > 0)
        {
            
            //Отправить письмо пользователю о регистрации
            
            
            
            return $ID;
        }else{
            $this->arResult["ERRORS"]["REGISTRATION"] = $user->LAST_ERROR;
            $this->arResult["ERROR_MESSAGE"] = $user->LAST_ERROR;
        }*/
    }
    
    public function saveAnketa($ORDER_ID)
    {
        CModule::IncludeModule("iblock");
        
        $el = new CIBlockElement;

        $PROP = array();
        
        $arAnketa = $this->arResult["ANKETA"];
        $arRequire = array(
            "FULL_NAME", 
            "INN", 
            "KPP", 
            "RS", 
            "BIK", 
            "BANK_NAME",
            "ADDRESS"
        );
        
        foreach($arRequire as $code)
        {
            $PROP[$code] = $arAnketa[$code];
        }
        $PROP["ORDER_ID"] = $ORDER_ID;
        
        $arLoadProductArray = Array(
            "IBLOCK_SECTION_ID" => false,
            "IBLOCK_ID"      => 7,
            "PROPERTY_VALUES"=> $PROP,
            "NAME"           => "Анкета: ".$arAnketa["FULL_NAME"],
            "ACTIVE"         => "Y",
            //{aquakosh (skype): Идентификатор принадлежности инфоблока к конкретному заказу. Используется при выгрузке заказов в XML.
            "CODE"           => "order_#".strval($ORDER_ID)
            //}aquakosh
        );
        
        $PRODUCT_ID = $el->Add($arLoadProductArray);
    }
    
    public function save()
    {
        global $USER;
        
        if(empty($this->arResult["ERROR_MESSAGE"]))
        {
            //Оплата по безналичному расчёту для Юр. лиц (+3% к стоимости)
            if($this->arResult["PAYMENT"]==$this->arResult["PAYMENT_LAW_ID"])
            {
                $this->arResult["TOTAL_PRICE"] = 1.03 * $this->arResult["TOTAL_PRICE"];
            }
            
            $arOrder = array(
               "LID" => SITE_ID,
               "PERSON_TYPE_ID" => 1,
               "PAYED" => "N",
               "CANCELED" => "N",
               "STATUS_ID" => "N",
               "PRICE" => $this->arResult["TOTAL_PRICE"],
               "CURRENCY" => CSaleLang::GetLangCurrency(SITE_ID),
               "USER_ID" => $USER->GetID(),
               "PRICE_DELIVERY" => $this->arResult["PRICE_DELIVERY"],
               "PAY_SYSTEM_ID" => $this->arResult["PAYMENT"],
               "DELIVERY_ID" => $this->arResult["DELIVERY"],
               "USER_DESCRIPTION" => $this->arResult["COMMENT"],
               "ADDITIONAL_INFO" => $this->arResult["ADDITIONAL_INFO"]
            );
            
            $ORDER_ID = CSaleOrder::Add($arOrder);
            $ORDER_ID = IntVal($ORDER_ID);
            
            if ($ORDER_ID==0)
            {
                $this->arResult["ERROR_MESSAGE"] = "Ошибка оформления заказа";
            }
            else
            {    
                $fUserID = IntVal(CSaleBasket::GetBasketUserID(True));
                CSaleBasket::OrderBasket($ORDER_ID, $fUserID, SITE_ID);
                CSaleBasket::DeleteAll($fUserID);
            
                CSaleOrderPropsValue::Add(Array(
                    "ORDER_ID" => $ORDER_ID, 
                    "ORDER_PROPS_ID" => 11, 
                    "NAME" => "Город", 
                    "VALUE" => $this->arResult["LOCATION_LIST"][$this->arResult["LOCATION"]]["CITY_NAME"], 
                    "CODE" => "CITY"
                ));
            
                CSaleOrderPropsValue::Add(Array(
                    "ORDER_ID" => $ORDER_ID, 
                    "ORDER_PROPS_ID" => 6, 
                    "NAME" => "Местоположение", 
                    "VALUE" => $this->arResult["LOCATION"], 
                    "CODE" => "LOCATION"
                ));
                
                CSaleOrderPropsValue::Add(Array(
                    "ORDER_ID" => $ORDER_ID, 
                    "ORDER_PROPS_ID" => 7, 
                    "NAME" => "Адрес", 
                    "VALUE" => $this->arResult["ADDRESS"], 
                    "CODE" => "ADDRESS"
                ));
                
                CSaleOrderPropsValue::Add(Array(
                    "ORDER_ID" => $ORDER_ID, 
                    "ORDER_PROPS_ID" => 2, 
                    "NAME" => "Email", 
                    "VALUE" => $this->arResult["USER"]["EMAIL"], 
                    "CODE" => "EMAIL"
                ));
                
                CSaleOrderPropsValue::Add(Array(
                    "ORDER_ID" => $ORDER_ID, 
                    "ORDER_PROPS_ID" => 1, 
                    "NAME" => "Контактное лицо", 
                    "VALUE" => $this->arResult["USER"]["NAME"], 
                    "CODE" => "FIO"
                ));
                
                CSaleOrderPropsValue::Add(Array(
                    "ORDER_ID" => $ORDER_ID, 
                    "ORDER_PROPS_ID" => 3, 
                    "NAME" => "Телефон", 
                    "VALUE" => phoneToNumber($this->arResult["USER"]["PERSONAL_PHONE"]), 
                    "CODE" => "PHONE"
                ));
                
                CSaleOrderPropsValue::Add(Array(
                    "ORDER_ID" => $ORDER_ID, 
                    "ORDER_PROPS_ID" => 8, 
                    "NAME" => "Дата доставки", 
                    "VALUE" => $this->arResult["DATE_DELIVERY"], 
                    "CODE" => "DATE_DELIVERY"
                ));
                
                CSaleOrderPropsValue::Add(Array(
                    "ORDER_ID" => $ORDER_ID, 
                    "ORDER_PROPS_ID" => 9, 
                    "NAME" => "Время доставки", 
                    "VALUE" => $this->arResult["DELIVERY_TIME"], 
                    "CODE" => "DELIVERY_TIME"
                ));
                
                CSaleOrderPropsValue::Add(Array(
                    "ORDER_ID" => $ORDER_ID, 
                    "ORDER_PROPS_ID" => 10, 
                    "NAME" => "Марка авто", 
                    "VALUE" => $this->arResult["BRAND_AUTO"], 
                    "CODE" => "BRAND_AUTO"
                ));
                
                if($this->arResult["PAYMENT"]==$this->arResult["PAYMENT_LAW_ID"])
                {
                    $this->saveAnketa($ORDER_ID);
                }
                
                $this->sendMailTemplate($ORDER_ID);
                
                unset($this->arResult);
                unset($_SESSION["ORDER"]);
                $this->arResult["SUCCESS_MESSAGE"] = "Ваш <a href='/personal/order/detail/".$ORDER_ID."/'>заказ</a>  создан успешно!";
            }
        }
    }
    
    public function sendMailTemplate($ORDER_ID)
    {
        $this->getDeliveryList();
        $pList = $this->paySystemList($this->arResult);
        
        foreach($this->arResult["PAYMENT_LIST"] as $ptype)
        {
            if($this->arResult["PAYMENT"]==$ptype["ID"])
            {
                $pname = $ptype["NAME"];
                break;
            }
        }
        
        foreach ($this->arResult["DELIVERY_LIST"] as $delivery_id => $arDelivery)
		{
			if ($delivery_id !== 0 && intval($delivery_id) <= 0)
			{
				foreach ($arDelivery["PROFILES"] as $profile_id => $arProfile)
				{
				    if($this->arResult["DELIVERY"] ==$delivery_id.":".$profile_id)
                    {
                        $dname = $arProfile["TITLE"];
                        break;
                    }
				}
            }else{
                if($this->arResult["DELIVERY"] ==$delivery_id)
                {
                    $dname = $arDelivery["NAME"];
                    break;
                }
            }
        }
        
        $saleEmail = "info@kupit-akkumulyator.ru";
        $arEventFields = array(
            "EMAIL"                => $this->arResult["USER"]["EMAIL"],
            "ORDER_ID"             => $ORDER_ID,
            "SALE_EMAIL"           => $saleEmail,
            "ORDER_USER"           => $this->arResult["USER"]["NAME"],
            "ORDER_DATE"           => date("d.m.Y"),
            "PRICE"                => $this->arResult["TOTAL_PRICE"],
            "DELIVERY_NAME"        => $dname,
            "PAY_SYSTEM_NAME"      => $pname,
            "BCC"                  => "alie.emil@yandex.ru",//"hawkart@rambler.ru"//
        );
        
        foreach($this->arResult["BASKET_ITEMS"] as $arItem)
        {
            $arEventFields["ORDER_LIST"].= $arItem["NAME"]. " - ".$arItem["QUANTITY"]." : ".$arItem["PRICE"]." руб\r\n";
        }       
         
        CEvent::Send("SALE_NEW_ORDER", SITE_ID, $arEventFields);          
    }
    
}