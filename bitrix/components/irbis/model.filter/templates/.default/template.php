<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//dbg($arResult)
?>
<div id="model-filter-form-container" class=" border15 aside-block">
    <script src="<?=$templateFolder?>/ajax.js"></script>
    <form class="catalog_search_form bac_CCC" method="GET" id="model-filter-form" data-ajax-action="<?=$templateFolder?>/ajax.php">
        <?//bitrix_sessid_post()?>
        <input type="hidden" name="filter_form" value="model-form" />
        <div class="form_row form_row_btmline">
            <fieldset>
                <div class="bold dark_font mb7 name_filter"><a href="/catalog/auto/">Подбор по марке авто:</a></div>

                <div class="form_row">
                    <div class="form_label">Выберите марку:</div>
                    <select class="styler" name="BRAND">
                        <option></option>
                        <?
                        foreach($arResult["BRANDS"] as $arBrand)
                        {
                            ?><option
                                value="<?=$arBrand["ID"]?>"
                                <?if($arBrand["ID"]==$arResult["BRAND"]):?>selected<?endif;?>
                                data-code="<?=$arBrand["url_code"]?>">

                            <?=$arBrand["NAME"]?></option><?
                        }
                        ?>
                    </select>
                </div>

                <?if(!empty($arResult["BRAND"])):?>
                <div class="form_row">
                    <div class="form_label">Выберите модель:</div>
                    <select class="styler" name="MODEL">
                        <option></option>
                        <?
                        foreach($arResult["MODELS"] as $arModel)
                        {
                            ?><option value="<?=$arModel["ID"]?>" <?if($arModel["ID"]==$arResult["MODEL"]):?>selected<?endif;?> data-code="<?=$arModel["url_code"]?>" ><?=$arModel["NAME"]?></option><?
                        }
                        ?>
                    </select>
                </div>
                <?endif;?>

                <?if(!empty($arResult["MODEL"])):?>
                <div class="form_row">
                    <div class="form_label">Выберите модельный ряд:</div>
                    <select class="styler" name="MODEL_ROW">
                        <option></option>
                        <?
                        foreach($arResult["MODELS_ROWS"] as $arModelRow)
                        {
                            ?><option value="<?=$arModelRow["ID"]?>"
                                    <?if($arModelRow["ID"]==$arResult["MODEL_ROW"]):?>selected<?endif;?>
                                      data-code="<?=$arModelRow["url_code"]?>" ><?=$arModelRow["NAME"]?></option><?
                        }
                        ?>
                    </select>
                </div>

                    <?if (!$arParams['modelFilter']) {?>
                        <button class="btn grn_skin d_block">Подобрать</button>
                    <?}?>
                <?endif;?>
            </fieldset>

            <!--
            <?if (!$arParams['modelFilter']) {?>
                <div class="al_center"><a href="#" class="gray_link bold" onclick="clearFilter();">Сбросить фильтр</a></div>
            <?}?>
            -->
        </div>
        <div class="form_row">
            <fieldset>
                <div class="bold dark_font mb7 name_filter"><a href="/catalog/brand/">Аккумуляторы по маркам:</a></div>

                <div class="form_row">
                    <div class="form_label">Выберите марку:</div>
                    <select class="styler" name="AKK_BRAND">
                        <option></option>
						<?foreach($arResult["AKK_BRANDS"] as $arAkkBrand):?>
							<option value="<?=$arAkkBrand["NAME"]?>" data-code="<?=$arAkkBrand["url_code"]?>"><?=$arAkkBrand["NAME"]?></option>
                        <?endforeach?>
                    </select>
                </div>
            </fieldset>

            <!--
            <?if (!$arParams['modelFilter']) {?>
                <div class="al_center"><a href="#" class="gray_link bold" onclick="clearFilter();">Сбросить фильтр</a></div>
            <?}?>
            -->
        </div>
    </form>
</div>