<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<div id="model-filter-form-container">
    <script src="<?=$templateFolder?>/ajax.js"></script>
    <form class="catalog_search_form  mb40" method="GET" id="model-filter-form" data-ajax-action="<?=$templateFolder?>/ajax.php">  
        <?//bitrix_sessid_post()?>
        <input type="hidden" name="filter_form" value="model-form" />
        <div class="form_row">
            <fieldset class="mb18">
                <h3 class="bold dark_font mb7">Подбор по марке авто:</h3>
                
                <div class="form_row">
                    <div class="form_label">Выберите марку:</div>
                    <select class="styler" name="BRAND">
                        <option></option>
                        <?
                        foreach($arResult["BRANDS"] as $arBrand)
                        {
                            ?><option value="<?=$arBrand["UF_XML_ID"]?>" <?if($arBrand["UF_XML_ID"]==$arResult["BRAND"]):?>selected<?endif;?>><?=$arBrand["UF_NAME"]?></option><?
                        }
                        ?>
                    </select>
                </div>
                
                <?if(!empty($arResult["BRAND"])):?>
                <div class="form_row">
                    <div class="form_label">Выберите модель:</div>
                    <select class="styler" name="MODEL">
                        <option></option>
                        <?
                        foreach($arResult["MODELS"] as $arModel)
                        {
                            ?><option value="<?=$arModel["UF_XML_ID"]?>" <?if($arModel["UF_XML_ID"]==$arResult["MODEL"]):?>selected<?endif;?>><?=$arModel["UF_NAME"]?></option><?
                        }
                        ?>
                    </select>
                </div>
                <?endif;?>
                
                <?if(!empty($arResult["MODEL"])):?>
                <div class="form_row">
                    <div class="form_label">Выберите модельный ряд:</div>
                    <select class="styler" name="MODEL_ROW">
                        <option></option>
                        <?
                        foreach($arResult["MODELS_ROWS"] as $arModelRow)
                        {
                            ?><option value="<?=$arModelRow["UF_XML_ID"]?>" <?if($arModelRow["UF_XML_ID"]==$arResult["MODEL_ROW"]):?>selected<?endif;?>><?=$arModelRow["UF_NAME"]?></option><?
                        }
                        ?>
                    </select>
                </div>
                <?endif;?>
                
                <button class="btn grn_skin d_block">Подобрать</button>
            </fieldset>
            <div class="al_center"><a href="#" class="gray_link bold" onclick="clearFilter();">Сбросить фильтр</a></div>
        </div>
    </form>
</div>