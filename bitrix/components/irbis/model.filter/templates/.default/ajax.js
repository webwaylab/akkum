function setModelForm(object)
{
    var $form = $("#model-filter-form");
    var url = $form.data('ajax-action'),
    	params = {
    		action: '__action',
    		field: object.attr("name")
    	},
    	serialized = $form.serialize();
        
    params = $.param(params);
    params += "&"+serialized;
    
    $.ajax({
        type: "POST",
        url: url,
        data: params,
        success: function(result)
        {
            $("#model-filter-form-container").replaceWith(result);
            $('.styler').styler();
        }
    });

	return true;
}

function clearFilter()
{
    window.location.href = window.location.pathname;
    return false;
}

$(document).ready(function() {


    $("#model-filter-form select[name='BRAND']").on("change", function(e) {
        var code = $(this).find('option:selected').data('code');

        var r = window.location.pathname.match(/(\/catalog\/\w+?\/)/);

        window.location = '/catalog/auto/' + code + '/';
    });

    $("#model-filter-form select[name='MODEL']").on("change", function(e) {
        var code = $(this).find('option:selected').data('code');

        var r = window.location.pathname.match(/(\/catalog\/\w+?\/\w+?\/)/);

        window.location = r[0] + code + '/';
    });

    $("#model-filter-form select[name='MODEL_ROW']").on("change", function(e) {
        var code = $(this).find('option:selected').data('code');

        var r = window.location.pathname.match(/(\/catalog\/\w+?\/\w+?\/\w+?\/)/);

        window.location = r[0] + code + '/';
    });
	
	$("#model-filter-form select[name='AKK_BRAND']").on("change", function(e) {
        var code = $(this).find('option:selected').data('code');
        window.location = code;
    });
});