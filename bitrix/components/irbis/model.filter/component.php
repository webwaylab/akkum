<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$requiredModules = array('iblock');

foreach ($requiredModules as $requiredModule)
{
	if (!CModule::IncludeModule($requiredModule))
	{
		ShowError(GetMessage("F_NO_MODULE"));
		return 0;
	}
}

$arResult["BRANDS"] = array();
$arResult["MODELS"] = array();
$arResult["MODELS_ROWS"] = array();
$arResult["BRAND"] = $arParams['selectedBrand'] ? $arParams['selectedBrand']['ID'] : $_REQUEST["BRAND"];
$arResult["MODEL"] = $arParams['selectedModel'] ? $arParams['selectedModel']['ID'] : $_REQUEST["MODEL"];
$arResult["MODEL_ROW"] = $arParams['selectedModification'] ? $arParams['selectedModification']['ID'] : $_REQUEST["MODEL_ROW"];

$arSelect = Array("NAME", "ID", 'CODE');
$arFilter = Array("IBLOCK_ID"=>13, 'ACTIVE'=>"Y");
$res = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arFilter, false, false, $arSelect);
while($arItem = $res->GetNext())
{
    $arItem['url_code'] = $arItem['CODE'] ? $arItem['CODE'] : strtolower(preg_replace('#\s+|-#is', '_', CUtil::translit($arItem['NAME'], 'ru')));
    $arResult["BRANDS"][$arItem['ID']] = $arItem;
}

//MODELS
if(!empty($arResult["BRAND"]))
{
    $arSelect = Array("NAME", "ID");
    $arFilter = Array("IBLOCK_ID"=>14, 'ACTIVE'=>"Y", "=PROPERTY_BRAND_ID"=>intval($arResult["BRAND"]));
    $res = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arFilter, false, false, $arSelect);
    while($arBrand = $res->GetNext())
    {
        $arBrand['url_code'] = $arBrand['CODE'] ? $arBrand['CODE'] : strtolower(str_replace(['(', ')'], '', preg_replace('#\s+|-#is', '_', CUtil::translit($arBrand['NAME'], 'ru'))));
        $arResult["MODELS"][$arBrand["ID"]] = $arBrand;
    }
}

//MODELS ROWS
if(!empty($arResult["MODEL"]))
{
    $arSelect = Array("NAME", "ID", "PROPERTY_DATE_FROM", "PROPERTY_DATE_TO", 'CODE');
    $arFilter = Array("IBLOCK_ID"=>15, 'ACTIVE'=>"Y", "=PROPERTY_MODEL_ID"=>intval($arResult["MODEL"]));
    $res = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arFilter, false, false, $arSelect);
    while($arItem = $res->GetNext())
    {
        $name = $arItem['NAME'];// . ' ' . $arItem['PROPERTY_DATE_FROM_VALUE'] . '-' .  $arItem['PROPERTY_DATE_TO_VALUE'];
        $code = $arItem['CODE'] ? $arItem['CODE'] : strtolower(str_replace(['(', ')'], '', preg_replace('#\s+|-#is', '_', CUtil::translit($name, 'ru'))));
        $arItem['url_code'] = $code;

        $arResult["MODELS_ROWS"][$arItem["ID"]] = $arItem;
    }
}

//AKK BRANDS
$arSelect = Array("PROPERTY_MANUFACTURE");
$arFilter = Array("IBLOCK_ID"=> IB_CATALOG, "SECTION_CODE"=> SC_BATTERY, "INCLUDE_SUBSECTIONS"=>"Y", "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
$modelArray = array();
while($ob = $res->GetNextElement()){
	$arFields = $ob->GetFields();
	if ($arFields['PROPERTY_MANUFACTURE_VALUE'] == '') continue;
	$name = mb_strtolower($arFields['PROPERTY_MANUFACTURE_VALUE'], 'UTF-8');
	$menulink = '/catalog/brand/'.str_replace(' ', '_', $name).'/';
	$id = $arFields['PROPERTY_MANUFACTURE_VALUE_ID'];
	$modelArray[$name] = array(
		'NAME' => $arFields['PROPERTY_MANUFACTURE_VALUE'],
		'url_code' => $menulink
	);
}
ksort($modelArray);
$arResult["AKK_BRANDS"] = $modelArray;


$this->IncludeComponentTemplate();