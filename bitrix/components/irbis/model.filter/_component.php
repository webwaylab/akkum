<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$requiredModules = array('highloadblock', 'iblock');

foreach ($requiredModules as $requiredModule)
{
	if (!CModule::IncludeModule($requiredModule))
	{
		ShowError(GetMessage("F_NO_MODULE"));
		return 0;
	}
}

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

$arResult["BRANDS"] = array();
$arResult["MODELS"] = array();
$arResult["MODELS_ROWS"] = array();
$arResult["BRAND"] = $_REQUEST["BRAND"];
$arResult["MODEL"] = $_REQUEST["MODEL"];
$arResult["MODEL_ROW"] = $_REQUEST["MODEL_ROW"];

$brand_hl_id = $arParams['BRAND_HL'];
$model_hl_id = $arParams['MODEL_HL'];
$model_row_hl_id = $arParams['MODEL_ROW_HL'];

$hlblock = HL\HighloadBlockTable::getById($brand_hl_id)->fetch();
$entity = HL\HighloadBlockTable::compileEntity( $hlblock );
$entity_data_class = $entity->getDataClass();

//BRANDS
$arFilter = array(
    //"UF_ACTIVE" => "Y"
);
$rsData = $entity_data_class::getList(array(
	'filter' => $arFilter,
	'select' => array('*'),
	'limit' => false,
	'order' => array(
		'UF_NAME' => 'ASC'
	),
));
while($arBrand = $rsData->Fetch()) 
{
    $arResult["BRANDS"][$arBrand["UF_XML_ID"]] = $arBrand;
}

//MODELS
if(!empty($_REQUEST["BRAND"]) && isset($_REQUEST["BRAND"]))
{
    $hlblock = HL\HighloadBlockTable::getById($model_hl_id)->fetch();
    $entity = HL\HighloadBlockTable::compileEntity( $hlblock );
    $entity_data_class = $entity->getDataClass();
	$arFilter = array(
        "UF_BRAND_ID" => $arResult["BRANDS"][$_REQUEST["BRAND"]]["ID"]
    );
    $rsData = $entity_data_class::getList(array(
    	'filter' => $arFilter,
    	'select' => array('*'),
    	'limit' => false,
    	'order' => array(
    		'UF_NAME' => 'ASC'
    	),
    ));
    while($arBrand = $rsData->Fetch()) 
    {
        $arResult["MODELS"][$arBrand["UF_XML_ID"]] = $arBrand;
    }
}

//MODELS ROWS
if(!empty($_REQUEST["MODEL"]) && isset($_REQUEST["MODEL"]))
{
    $hlblock = HL\HighloadBlockTable::getById($model_row_hl_id)->fetch();
    $entity = HL\HighloadBlockTable::compileEntity( $hlblock );
    $entity_data_class = $entity->getDataClass();
	//$arFilter["=UF_MODEL_ID"] = intval($_REQUEST["MODEL"]);
    $arFilter = array(
        "UF_MODEL_ID" => $arResult["MODELS"][$_REQUEST["MODEL"]]["ID"]
    );
    $rsData = $entity_data_class::getList(array(
    	'filter' => $arFilter,
    	'select' => array('*'),
    	'limit' => false,
    	'order' => array(
    		'UF_NAME' => 'ASC'
    	),
    ));
    while($arBrand = $rsData->Fetch()) 
    {
        $arResult["MODELS_ROWS"][] = $arBrand;
    }
}
$this->IncludeComponentTemplate();