<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<script src="<?=$templateFolder."/ajax.js"?>"></script> 
<form action="<?=$APPLICATION->GetCurPage();?>" method="POST" enctype="multipart/form-data" class="opt-form">
    <?=bitrix_sessid_post()?>
    <div class="wrapper">
        <h3 class="fs_18 black reg mb15">Отправить сообщение</h3>
        <div class="mb_10">
            <label>Наименование организации:</label>
            <input type="text" class="txt_field" name="ORG" value="">
        </div>
        <div class="mb_10">
            <label>Ф.И.О:</label>
            <input type="text" class="txt_field" name="FIO" value="">
        </div>
        <div class="mb_10">
            <label>Город:</label>
            <input type="text" class="txt_field" name="CITY" value="">
        </div>
        <div class="mb_10">
            <label>Телефон:</label>
            <input type="text" class="txt_field" name="PHONE" value="">
        </div>
        <div class="mb_10">
            <label>E-mail:</label>
            <input type="text" class="txt_field" name="EMAIL" value="">
        </div>
        <div class="mb_10">
            <label>Примерная сумма закупок:</label>
            <input type="text" class="txt_field" name="PRICE" value="">
        </div>
        <div class="mb_10">
            <label>Примечания:</label>
            <textarea name="PREVIEW_TEXT" class="txt_field mb10"></textarea>
        </div>
        <div class="error bordered"></div>
        <button class="btn2 grn_skin">Отправить</button>
    </div>
</form>