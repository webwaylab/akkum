<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<span class="fs_22 black reg mb15 black_font">Отправить сообщение</span>
<script src="<?=$templateFolder."/ajax.js"?>"></script>
<form action="<?=$APPLICATION->GetCurPage();?>" method="POST" enctype="multipart/form-data" class="opt-form contact_form">
    <?=bitrix_sessid_post()?>
    <div class="wrapper">

        <div class="mb_10">
            <label>Имя*:</label>
            <input type="text" class="txt_field" name="NAME" value="">
        </div>
        <div class="mb_10">
            <label>Телефон*:</label>
            <input type="text" class="txt_field" name="PHONE" value="">
        </div>
        <div class="mb_10">
            <label>Сообщение*:</label>
            <textarea name="PREVIEW_TEXT" class="txt_field mb10"></textarea>
        </div>
        <div class="error bordered"></div>
        <button class="btn2 grn_skin">Отправить</button>
    </div>
</form>
