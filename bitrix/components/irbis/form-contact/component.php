<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$requiredModules = array('iblock');

foreach ($requiredModules as $requiredModule)
{
	if (!CModule::IncludeModule($requiredModule))
	{
		ShowError(GetMessage("F_NO_MODULE"));
		return 0;
	}
}

if($_REQUEST["AJAX_CALL"] == "Y" || $_REQUEST["is_ajax_post"] == "Y")
{
	$APPLICATION->RestartBuffer();
}

if($_SERVER["REQUEST_METHOD"] == "POST" && check_bitrix_sessid())
{
    $arResult["status"] = false;
    $errors = array();
    if(empty($_POST["NAME"]))
    {
        $errors[]= "Введите имя.";
    }
    if(empty($_POST["PHONE"]))
    {
        $errors[]= "Введите телефон.";
    }
    if(empty($_POST["PREVIEW_TEXT"]))
    {
        $errors[]= "Введите сообщение.";
    }
    
    if(count($errors)==0)
    {
        CModule::IncludeModule("iblock");
        $el = new CIBlockElement;
        
        $text = strip_tags($_POST["PREVIEW_TEXT"]);
        
        $PROP = array();
        unset($_POST["sessid"]);
        unset($_POST["PREVIEW_TEXT"]);
        $PROP = $_POST;
    
        $arLoadProductArray = Array(
            "IBLOCK_SECTION_ID" => false,
            "IBLOCK_ID"      => 12,
            "PROPERTY_VALUES"=> $PROP,
            "NAME"           => strip_tags($_POST["ORG"]),
            "ACTIVE"         => "Y",
            "PREVIEW_TEXT"  => $text
        );
        
        $PRODUCT_ID = $el->Add($arLoadProductArray);
        $arResult["status"] = true;
        $arResult["message"] = "<span class='success-msg'>Ваше сообщение отправлено!</span>";
    }else{
        $arResult["message"] = implode("<br/>", $errors);
    }
    
    echo json_encode($arResult); die(); 
}

$this->IncludeComponentTemplate();

if ($_REQUEST["AJAX_CALL"] == "Y" || $_REQUEST["is_ajax_post"] == "Y")
{
	die();
}
?>