<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

	$urt_temp = array(
'/catalog/accessories/areometry/areometr_universalnyy_steklyannyy_orion_ar_02/',
'/catalog/accessories/areometry/areometr_victor_v644/',
'/catalog/accessories/chargers/zaryadnoe_ustroystvo_orion_pw150_6a_12v/',
'/catalog/accessories/chargers/zaryadnoe_ustroystvo_kulon_100/',
'/catalog/accessories/chargers/zaryadnoe_ustroystvo_zu_75a_tambov/',
'/catalog/accessories/chargers/zaryadnoe_ustroystvo_zu_75a1_tambov/',
'/catalog/accessories/chargers/zaryadnoe_ustroystvo_orion_pw265_6a_12v/',
'/catalog/accessories/chargers/zaryadnoe_ustroystvo_kulon_707a_7a_strelochnyy_indikator/',
'/catalog/accessories/chargers/zaryadnoe_ustroystvo_orion_pw270_6a_12v/',
'/catalog/accessories/chargers/zaryadnoe_ustroystvo_zu_120_tambov/',
'/catalog/accessories/chargers/zaryadnoe_ustroystvo_zu_120m_tambov/',
'/catalog/accessories/chargers/zaryadnoe_ustroystvo_zu_120m_3_tambov/',
'/catalog/accessories/chargers/zaryadnoe_ustroystvo_12v_24v_230vt_380vt_16a_90_180ach_220v_bolk_master_cb_16a/',
'/catalog/accessories/chargers/zaryadnoe_ustroystvo_t_1021/',
'/catalog/accessories/chargers/zaryadnoe_ustroystvo_orion_pw325_15a_12v/',
'/catalog/accessories/chargers/zaryadnoe_ustroystvo_12v_24v_430vt_720vt_25a_140_300ach_220v_bolk_master_cb_25a/',
'/catalog/accessories/chargers/zaryadnoe_ustroystvo_kulon_715d_15a_zh_k_indikator/',
'/catalog/accessories/chargers/zaryadnoe_ustroystvo_12v_24v_510vt_850vt_30a_170_350ach_220v_bolk_master_cb_30a/',
'/catalog/accessories/chargers/zaryadnoe_ustroystvo_t_1001ar_avtomat_reversivnyy_tok/',
'/catalog/accessories/chargers/zaryadno_puskovoe_ustroystvo_sonar_209/',
'/catalog/accessories/chargers/zaryadno_puskovoe_ustroystvo_zpu_start_1/',
'/catalog/accessories/chargers/zaryadnoe_ustroystvo_12v_15a_220v_defort_dbc_15/',
'/catalog/accessories/chargers/stantsiya_energeticheskaya_c_kompressorom_i_fonarem_1100a_12v_i_220v_bolk_ps1100a_c/',
'/catalog/accessories/chargers/zaryadno_puskovoe_ustroystvo_t_1012a_avtomat_reversivnyy_tok/',
'/catalog/accessories/chargers/zaryadno_puskovoe_ustroystvo_zpu_12_24/',
'/catalog/accessories/avtokhimiya/zhir_dlya_klemm_akkumulyatora_0_05kg_lm_3140_7643/',
'/catalog/accessories/avtokhimiya/ochistitel_klemm_akkumulyatora_aerozol_141ml_gunk_btc6/',
'/catalog/accessories/elektrolit/dreko_elektrolit_kislotnyy_1l/',
'/catalog/accessories/krepezhi_i_kozhukhi/poddon_akkumulyatornoy_batarei/',
'/catalog/accessories/krepezhi_i_kozhukhi/krepezh_dlya_akkumulyatora_s_plankoy_komplekt_3_sht_/',
'/catalog/accessories/krepezhi_i_kozhukhi/termozashchita_dlya_akkumulyatora_190kh250kh180mm_neylonovaya_chernaya/',
'/catalog/accessories/nabor_avtomobilista/nabor_avtomobilista_provoda_prikurivaniya_zhilet_avariynyy_tros_perchatki_sumka_kachok_a210/',
'/catalog/accessories/nezamerzaika/nezamerzayushchaya_zhidkost_5l_30/',
	);



$this->setFrameMode(true);
$templateLibrary = array('popup');
$currencyList = '';
if (!empty($arResult['CURRENCIES']))
{
	$templateLibrary[] = 'currency';
	$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}
$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
	'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME'],
	'TEMPLATE_LIBRARY' => $templateLibrary,
	'CURRENCIES' => $currencyList
);
unset($currencyList, $templateLibrary);
$strMainID = $this->GetEditAreaId($arResult['ID']);
$arItemIDs = array(
	'ID' => $strMainID,
	'PICT' => $strMainID.'_pict',
	'DISCOUNT_PICT_ID' => $strMainID.'_dsc_pict',
	'STICKER_ID' => $strMainID.'_sticker',
	'BIG_SLIDER_ID' => $strMainID.'_big_slider',
	'BIG_IMG_CONT_ID' => $strMainID.'_bigimg_cont',
	'SLIDER_CONT_ID' => $strMainID.'_slider_cont',
	'SLIDER_LIST' => $strMainID.'_slider_list',
	'SLIDER_LEFT' => $strMainID.'_slider_left',
	'SLIDER_RIGHT' => $strMainID.'_slider_right',
	'OLD_PRICE' => $strMainID.'_old_price',
	'PRICE' => $strMainID.'_price',
	'DISCOUNT_PRICE' => $strMainID.'_price_discount',
	'SLIDER_CONT_OF_ID' => $strMainID.'_slider_cont_',
	'SLIDER_LIST_OF_ID' => $strMainID.'_slider_list_',
	'SLIDER_LEFT_OF_ID' => $strMainID.'_slider_left_',
	'SLIDER_RIGHT_OF_ID' => $strMainID.'_slider_right_',
	'QUANTITY' => $strMainID.'_quantity',
	'QUANTITY_DOWN' => $strMainID.'_quant_down',
	'QUANTITY_UP' => $strMainID.'_quant_up',
	'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
	'QUANTITY_LIMIT' => $strMainID.'_quant_limit',
	'BASIS_PRICE' => $strMainID.'_basis_price',
	'BUY_LINK' => $strMainID.'_buy_link',
	'ADD_BASKET_LINK' => $strMainID.'_add_basket_link',
	'BASKET_ACTIONS' => $strMainID.'_basket_actions',
	'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
	'COMPARE_LINK' => $strMainID.'_compare_link',
	'PROP' => $strMainID.'_prop_',
	'PROP_DIV' => $strMainID.'_skudiv',
	'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
	'OFFER_GROUP' => $strMainID.'_set_group_',
	'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
);
$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
$templateData['JS_OBJ'] = $strObName;

$strTitle = (
	isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"] != ''
	? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]
	: $arResult['NAME']
);
$strAlt = (
	isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"] != ''
	? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]
	: $arResult['NAME']
);

foreach($arResult['PROPERTIES'] as $prop_key=>$prop_item){
		$pos = mb_strpos($prop_item["NAME"], "(Домкраты)");
		if($pos > 0){
			$arResult['PROPERTIES'][$prop_key]["NAME"] = mb_substr($prop_item["NAME"], 0, $pos);
			$arResult['PROPERTIES'][$prop_key]["~NAME"] = $arResult['PROPERTIES'][$prop_key]["NAME"];
		}
}
foreach($arResult['DISPLAY_PROPERTIES'] as $prop_key=>$prop_item){
		$pos = mb_strpos($prop_item["NAME"], "(Домкраты)");
		if($pos > 0){
			$arResult['DISPLAY_PROPERTIES'][$prop_key]["NAME"] = mb_substr($prop_item["NAME"], 0, $pos);
			$arResult['DISPLAY_PROPERTIES'][$prop_key]["~NAME"] = $arResult['DISPLAY_PROPERTIES'][$prop_key]["NAME"];
		}
}

//CDev::pre($arResult['PROPERTIES']);

reset($arResult['MORE_PHOTO']);
$arFirstPhoto = current($arResult['MORE_PHOTO']);
?><!-- seo-tovar -->
<div class="product_item__card clearfix mb75" id="<? echo $arItemIDs['ID']; ?>">
    <div itemscope itemtype="http://schema.org/Product">
    <article class="grid_17 prefix_2 resp_col17">
        <div class="product_imgs"  id="product_img">
            <div class="product_label-box">
                <?if($arResult["PROPERTIES"]["SALELEADER"]["VALUE"]=="да"):?>
                    <span class="product_label product_label--hit"
                        title="<?=$arResult["PROPERTIES"]["SALELEADER"]["NAME"];?>"></span>
                <?endif;?>
                <?if($arResult["PROPERTIES"]["FREESHIPPING"]["VALUE"]=="да"):?>
                    <span class="product_label product_label--delivery"
                        title="<?=$arResult["PROPERTIES"]["FREESHIPPING"]["NAME"];?>"></span>
                <?endif;?>
                <?if($arResult["PROPERTIES"]["SPECIALOFFER"]["VALUE"]=="да"):?>
                    <span class="product_label product_label--present"
                        title="<?=$arResult["PROPERTIES"]["SPECIALOFFER"]["NAME"];?>"></span>
                <?endif;?>
            </div>
            <div class="big_product_img">
                <a href="#myModal" role="button" class="modal-img" data-toggle="modal">


					<img itemprop="image" src="<? echo $arFirstPhoto['SRC']; ?>" alt="<? echo $strAlt; ?>" id="<? echo $arItemIDs['PICT']; ?>" class="main_product_img">
				</a>
            </div>
            <div class="wrapper">
                <div class="product_thumbs resp_p0">
                    <div class="product_thumbs_inner">
                        <?
            		foreach ($arResult['MORE_PHOTO'] as &$arOnePhoto) {
                            ?>


							 <a href="<? echo $arOnePhoto['SRC']; ?>" class="product_thumb"><img src="<? echo $arOnePhoto['SRC']; ?>" alt="" class="product_thumb_img"></a>
                            <?
                        }
                        //{aquakosh (skype): Вывод дополнительных изображений товара в шаблон.
                        if(count($arResult["MORE_PHOTO_AQUAKOSH"]) > 0) {
                            foreach($arResult["MORE_PHOTO_AQUAKOSH"] as $arPHOTO) {
                                if (is_array($arPHOTO)) {
                                    ?>

									<a href="<?php echo $arPHOTO["BIG"]["SRC"]; ?>" class="product_thumb product_thumb_img"><img src="<? echo $arPHOTO["THUMB"]["SRC"]; ?>" alt=""></a>

                                    <?php
                                }
                            }
                        }
                        //}aquakosh
                        ?>
                    </div>
                </div>
            </div>

			<div style="" id="myModal" class="modal hide fade" id="myLargeModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content" style="overflow: hidden;">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <div class="product_imgs"  id="product_img_modal">
                                <div class="big_product_img">
                                  <img itemprop="image" id="<? echo $arItemIDs['PICT']; ?>" src="<? echo $arFirstPhoto['SRC']; ?>" alt="<? echo $strAlt; ?>" class="main_product_img_modal">

                                    <?
                                    //{ aquakosh: Надписи «Хит продаж», «Акция» и «Бесплатная доставка» на странице товара.
                                    $bottomIndex = 0; // Это индекс класса стиля CSS.
                                    // Хит продаж
                                    if ($arResult["PROPERTIES"]["SALELEADER"]["VALUE"]=="да") {
                                        ?>
                                        <span class="hit_label bottom_<?=strval($bottomIndex)?> bold">хит продаж</span>
                                        <?
                                        $bottomIndex++;
                                    }
                                    // Акция
                                    if ($arResult["PROPERTIES"]["SPECIALOFFER"]["VALUE"]=="да") {
                                        ?>
                                        <span class="hit_label bottom_<?=strval($bottomIndex)?> bold">акция</span>
                                        <?
                                        $bottomIndex++;
                                    }
                                    // Бесплатная доставка
                                    if ($arResult["PROPERTIES"]["FREESHIPPING"]["VALUE"]=="да") {
                                        ?>
                                        <span class="hit_label bottom_<?=strval($bottomIndex)?> bold">бесплатная доставка</span>
                                        <?
                                        $bottomIndex++;
                                    }
                                    //} aquakosh
                                    ?>

                                </div>
                                <div class="wrapper">
                                    <div class="product_thumbs_modal resp_p0">
                                        <div class="product_thumbs_inner_modal">
                                            <?
                                              foreach ($arResult['MORE_PHOTO'] as &$arOnePhoto) {
                                                ?>
                                                <a href="<? echo $arOnePhoto['SRC']; ?>" class="product_thumb_modal">
												<img src="<? echo $arOnePhoto['SRC']; ?>" alt=""></a>
                                                <?
                                            }
                                            //{aquakosh (skype): Вывод дополнительных изображений товара в шаблон.
                                            if(count($arResult["MORE_PHOTO_AQUAKOSH"]) > 0) {
                                                foreach($arResult["MORE_PHOTO_AQUAKOSH"] as $arPHOTO) {
                                                    if (is_array($arPHOTO)) {
                                                        ?>
                                                        <a href="<?php echo $arPHOTO["BIG"]["SRC"]; ?>" class="product_thumb_modal"><img src="<? echo $arPHOTO["THUMB"]["SRC"]; ?>" alt=""></a>
                                                        <?php
                                                    }
                                                }
                                            }
                                            //}aquakosh
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                </div>
                        </div>
                      </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                  </div>

        </div>
        <p><?=strip_tags($arResult["~DETAIL_TEXT"])?></p>

    </article>

		<article class="grid_18 prefix_2 resp_col17">
        <div itemprop="name"><h1><?=$arResult['NAME'];?></h1></div>
        <div class="clearfix mb10">
        <?if(!empty($arResult["PROPERTIES"]["PRICE_CHANGE"]["VALUE"]) && $arResult["PROPERTIES"]["PRICE_CHANGE"]["VALUE"]<$arResult['MIN_PRICE']['DISCOUNT_VALUE_VAT']){?>
        <span itemprop="offers" itemscope itemtype="http://schema.org/AggregateOffer">
        <?}else{?>
        <span itemprop="offers" itemscope itemtype="http://schema.org/Offer">
        <?}?>
			<div class="">
				<?if ($arParams['DISPLAY_COMPARE']):?>
                    <a href="javascript:void(0);" class="comprassion_link" id="<? echo $arItemIDs['COMPARE_LINK']; ?>">К сравнению</a>
                <?endif;?>
			</div>
			<?/*это надо только для аккумуляторов*/?>
			<?if ($arParams["ROOT_ITEM"] == "battery"):?>
				<div class="clearfix product_price_low">
					<p class="product_price">
						<span itemprop="lowPrice" class="semi_bold"><?=number_format($arResult["PROPERTIES"]["PRICE_CHANGE"]["VALUE"], 0, "", " ")?></span> <span class="rub">₽</span>
					</p>
					<span class="product_price_low__description">Цена при сдаче отработанного аккумулятора аналогичной ёмкости</span>

				</div>
			<?elseif($arResult["PROPERTIES"]["PRICE_CHANGE"]["VALUE"]):?>
				<div class="clearfix product_price_low">
					<p class="product_price">
						<span itemprop="lowPrice" class="semi_bold"><?=number_format($arResult["PROPERTIES"]["PRICE_CHANGE"]["VALUE"], 0, "", " ")?></span> <span class="rub">₽</span>
					</p>
					<span class="product_price_low__description">Цена по купону на скидку</span>

				</div>
			<?endif;?>

            <div class="f_left">
				<span class="product_price_label black" id="<? echo $arItemIDs['PRICE']; ?>">
					<?if(!empty($arResult["PROPERTIES"]["PRICE_CHANGE"]["VALUE"]) && $arResult["PROPERTIES"]["PRICE_CHANGE"]["VALUE"]<$arResult['MIN_PRICE']['DISCOUNT_VALUE_VAT']){?>
					<span itemprop="highPrice">
						<?}else{?>
						<span itemprop="price">
						<?}?><?=number_format($arResult['MIN_PRICE']['DISCOUNT_VALUE_VAT'], 0, "", " ")?></span>
						<span class="rub">₽</span>
					</span>


				</span>


            </div>
					<?// количество откроем для всего if(!empty($arResult["PROPERTIES"]["PRICE_CHANGE"]["VALUE"]) && $arResult["PROPERTIES"]["PRICE_CHANGE"]["VALUE"]<$arResult['MIN_PRICE']['DISCOUNT_VALUE_VAT']):?>
						<div class="product_col2 f_left quantity_items">
					<?
						$canBuy = $arResult['CAN_BUY'];
						$buyBtnMessage = ($arParams['MESS_BTN_BUY'] != '' ? $arParams['MESS_BTN_BUY'] : GetMessage('CT_BCE_CATALOG_BUY'));
						$addToBasketBtnMessage = ($arParams['MESS_BTN_ADD_TO_BASKET'] != '' ? $arParams['MESS_BTN_ADD_TO_BASKET'] : GetMessage('CT_BCE_CATALOG_ADD'));
						$notAvailableMessage = ($arParams['MESS_NOT_AVAILABLE'] != '' ? $arParams['MESS_NOT_AVAILABLE'] : GetMessageJS('CT_BCE_CATALOG_NOT_AVAILABLE'));
						$showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
						$showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);

						$showSubscribeBtn = false;
						$compareBtnMessage = ($arParams['MESS_BTN_COMPARE'] != '' ? $arParams['MESS_BTN_COMPARE'] : GetMessage('CT_BCE_CATALOG_COMPARE'));

						if ($arParams['USE_PRODUCT_QUANTITY'] == 'Y' && $canBuy)
						{
							?>

								<div class="form_label">Кол-во</div>
								<select class="styler" id="<? echo $arItemIDs['QUANTITY']; ?>">
									<?
									for($i=1; $i<20; $i++)
									{
										?><option value="<?=$i?>"><?=$i?></option><?
									}
									?>
								</select>

							<?
						}
						?>



						</div>
					<?//endif;?>
					<meta itemprop="priceCurrency" content="RUB" />
				</span>
        </div>

        <?if($canBuy):?>
            <div class="clearfix mb30">
                <div class="f_left">
                    <a href="javascript:void(0);" class="btn4 bold grn_skin buy_product" id="<? echo $arItemIDs['BUY_LINK']; ?>">Купить</a>
                    <div class="item_buttons vam">
		              <span class="item_buttons_counter_block" id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>" style="display: <? echo ($canBuy ? '' : 'none'); ?>;">
                        </span>
                    </div>
                </div>
                <div class="product_col1 f_left p_rel">
                    <a href="#" class="btn4 bold blue_skin buy_click_product toggle_btn" data-target=".phone_order_form" data-target-class="active">Купить в 1 клик</a>
                    <form class="tooltip_box phone_order_form one-click">
                        <input type="hidden" name="PRODUCT_ID" value="<?=$arResult["ID"]?>"/>
                        <span class="close_tooltip"></span>
                        <div class="error"></div>
                        <div class="tooltip_box_inner">
                            <h4 class="bold fs_14 mb5">Введите номер телефона:</h4>
                                <div class="mb-5">
                                    <input type="text" name="PERSONAL_PHONE" class="txt_field blue_field al_center phone_mask" placeholder="+7(___) ___-__-__">
                                </div>
                            <button class="btn blue_skin d_block">Купить</button>
                        </div>
                    </form>

                    <div class="tooltip_box order_success_message">
                        <span class="close_tooltip"></span>
                        <div class="tooltip_box_inner">
                            <p><img src="<?=SITE_TEMPLATE_PATH?>/images/done_form_icon.png" height="33" width="36" alt=""></p>
                            <h4 class="bold">Ваш заказ принят.</h4>
                            Ожидайте звонка. Спасибо!
                        </div>
                    </div>

                </div>
            </div>
        <?endif;?>

        <?
       	if (!empty($arResult['DISPLAY_PROPERTIES']))
        {
            ?>
			<?if($arParams["ROOT_ITEM"] == "battery"):?>
            <ul class="product_details" itemprop="description">
                <?
                foreach ($arResult['DISPLAY_PROPERTIES'] as &$arOneProp)
                {
                    ?>
					<?if(in_array($arOneProp['CODE'], array("ARTNUMBER", "TYPE", "CAPACITY", "POLARITY", "START_TOK", "GARANTY", "COUNTRY_MANUFACTURE", "BRAND", "MODEL", "MODEL_ROW")) && $arOneProp['DISPLAY_VALUE']):?>
                    <li class="product_detail_item">
                        <div class="product_detail_item_label f_left">
                            <span><? echo $arOneProp['NAME']; ?></span>
                        </div>
                        <div class="product_detail_item_value wrapper">
                            <?
                			echo (
                				is_array($arOneProp['DISPLAY_VALUE'])
                				? implode(' / ', $arOneProp['DISPLAY_VALUE'])
                				: $arOneProp['DISPLAY_VALUE']
                			); ?>
                			<?if($arOneProp['CODE']=="CAPACITY" && $arResult["FAST_LINK"]):?>
                				<span class="fast-link">&nbsp;<?=$arResult["FAST_LINK"]?></span>
                			<?endif;?>
                        </div>
                    </li>
					<?endif;?>
                    <?
          		}
            	unset($arOneProp);
                ?>

                <? if (!in_array($_SERVER['REQUEST_URI'], $urt_temp) && !strstr($_SERVER['REQUEST_URI'], '/catalog/accessories/provoda_prikurivaniya_akb/')
                && !strstr($_SERVER['REQUEST_URI'], '/catalog/accessories/nagruzochnye_vilki/')) { ?>
                <li class="product_detail_item">
                    <div class="product_detail_item_label f_left">
                        <span>Габариты, ДхШхВ</span>
                    </div>
                    <div class="product_detail_item_value wrapper">
                        <?=$arResult['PROPERTIES']["LENGTH"]['VALUE']?>×<?=$arResult['PROPERTIES']["WIDTH"]['VALUE']?>×<?=$arResult['PROPERTIES']["HEIGHT"]['VALUE']?>
                    </div>
                </li>
                <? } ?>
           	</ul>
			<?elseif($arParams["ROOT_ITEM"] == "masla"):?>
            <ul class="product_details" itemprop="description">
                <?
                foreach ($arResult['DISPLAY_PROPERTIES'] as &$arOneProp)
                {
                    ?>
					<?if(in_array($arOneProp['CODE'], array("ARTNUMBER", "OBEM_UPAKOVKI_L_MASLO", "KLASS_VYAZKOSTI_SAE_MASLO", "TIP_MASLO", "PROIZVODITEL_MASLO")) && $arOneProp['DISPLAY_VALUE']):?>
                    <li class="product_detail_item">
						<?if($arOneProp['CODE']=="OBEM_UPAKOVKI_L_MASLO"):?>
							<div class="product_detail_item_label f_left"><span>Объём: </span></div>
							<div class="product_detail_item_value wrapper">
								<?
								echo (
									is_array($arOneProp['DISPLAY_VALUE'])
									? implode(' / ', $arOneProp['DISPLAY_VALUE'])
									: $arOneProp['DISPLAY_VALUE']
								); ?>л.
							</div>
							<?continue;?>
						<?endif;?>
						<?if($arOneProp['CODE']=="KLASS_VYAZKOSTI_SAE_MASLO"):?>
							<div class="product_detail_item_label f_left"><span>Вязкость: </span></div>
							<div class="product_detail_item_value wrapper">
								<?
								echo (
									is_array($arOneProp['DISPLAY_VALUE'])
									? implode(' / ', $arOneProp['DISPLAY_VALUE'])
									: $arOneProp['DISPLAY_VALUE']
								); ?>
							</div>
							<?continue;?>
						<?endif;?>
						<?if($arOneProp['CODE']=="TIP_MASLO"):?>
							<div class="product_detail_item_label f_left"><span>Тип: </span></div>
							<div class="product_detail_item_value wrapper">
								<?
								echo (
									is_array($arOneProp['DISPLAY_VALUE'])
									? implode(' / ', $arOneProp['DISPLAY_VALUE'])
									: $arOneProp['DISPLAY_VALUE']
								); ?>
							</div>
							<?continue;?>
						<?endif;?>
						<?if($arOneProp['CODE']=="PROIZVODITEL_MASLO"):?>
							<div class="product_detail_item_label f_left"><span>Производитель: </span></div>
							<div class="product_detail_item_value wrapper">
								<?
								echo (
									is_array($arOneProp['DISPLAY_VALUE'])
									? implode(' / ', $arOneProp['DISPLAY_VALUE'])
									: $arOneProp['DISPLAY_VALUE']
								); ?>
							</div>
							<?continue;?>
						<?endif;?>

                        <div class="product_detail_item_label f_left">
                            <span><? echo $arOneProp['NAME']; ?></span>
                        </div>
                        <div class="product_detail_item_value wrapper">
                            <?
                			echo (
                				is_array($arOneProp['DISPLAY_VALUE'])
                				? implode(' / ', $arOneProp['DISPLAY_VALUE'])
                				: $arOneProp['DISPLAY_VALUE']
                			); ?>
                        </div>
                    </li>
					<?endif;?>
                    <?
          		}
            	unset($arOneProp);
			?>
           	</ul>
			<?elseif($arParams["ROOT_ITEM"] == "accessories"):?>
            <ul class="product_details" itemprop="description">
                <?
                foreach ($arResult['DISPLAY_PROPERTIES'] as &$arOneProp)
                {
                    ?>
					<?if(in_array($arOneProp['CODE'], array("ARTNUMBER", "MANUFACTURE_2", "GRUZOPODEMNOST_DOMKRATY", "VYSOTA_PODKHVATA_DOMKRATY", "VYSOTA_PODEMA_DOMKRATY")) && $arOneProp['DISPLAY_VALUE']):?>
                    <li class="product_detail_item">

                        <div class="product_detail_item_label f_left">
                            <span><? echo $arOneProp['NAME']; ?></span>
                        </div>
                        <div class="product_detail_item_value wrapper">
                            <?
                			echo (
                				is_array($arOneProp['DISPLAY_VALUE'])
                				? implode(' / ', $arOneProp['DISPLAY_VALUE'])
                				: $arOneProp['DISPLAY_VALUE']
                			); ?>
                        </div>
                    </li>
					<?endif;?>
                    <?
          		}
            	unset($arOneProp);
			?>
           	</ul>
			<?endif;?>
            <?
        }
        ?>

        <?/*$APPLICATION->IncludeComponent(
			"bitrix:iblock.vote",
			"stars",
			array(
				"IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
				"IBLOCK_ID" => $arParams['IBLOCK_ID'],
				"ELEMENT_ID" => $arResult['ID'],
				"ELEMENT_CODE" => "",
				"MAX_VOTE" => "5",
				"VOTE_NAMES" => array("1", "2", "3", "4", "5"),
				"SET_STATUS_404" => "N",
				"DISPLAY_AS_RATING" => $arParams['VOTE_DISPLAY_AS_RATING'],
				"CACHE_TYPE" => $arParams['CACHE_TYPE'],
				"CACHE_TIME" => $arParams['CACHE_TIME']
			),
			$component,
			array("HIDE_ICONS" => "Y")
		);*/?>

        <div class="info__block">
            <div class="info__block-metro">
                <p><span class="green">В наличии в магазине:</span>  <img width="19" height="14" style="margin-left: 5px;" title="" alt="metro" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAAOCAMAAAD6xte7AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABLFBMVEUAAADuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMisAAADIaoX1AAAAYnRSTlMAIXrD5fnmDJP6tmYzIzpisPiVG9PKMQoJJSrA1M6qCIV4dZYEndINBufwEg/tCwK49ylVjIEe8n6xvcelf8lkJ/4uVu85juLklP0r6Udfv1pgN7t7bt5YAY/fbXIibBPxEBma1EcAAAABYktHRACIBR1IAAAACXBIWXMAAAsSAAALEgHS3X78AAAA3ElEQVQY01WNZ0/CABRFb2mFihSUMoqigpNWpopV1CJLsQ5ciAPX/f8/wkeMHzjJS05ObvKACUpA1WaCakDBPyF9NjwXMaKx+YV46C+ZiWTKTItYmcWlrDlJ6eWVVeTyYmvr2Njc2hbTkwUUbGcHxZJTRqWqA0ZtF9gj91EnY8CBa+DwSLYNshZxyWPxk1N4TeCsRdKRYxvoeOj2gHPyQkKfvJSBAz8Fq0T/qk+73WLXguLj+ga35AB38kAl7/Fg4/FpqJHPo5fXyuiN1IbvVYwTH9N4n/hyOc33zy+QOTCxBzL3GQAAAABJRU5ErkJggg==" />
                <b>Текстильщики</b>, 1-й Грайворонский пр-д, д, 4</p>
            </div>
            <div class="info__block-left">
                <div class="info__block-place">
                    <p><span class="blue">Стандартная доставка:</span> <b>300 руб.</b> в пределах МКАД</p>
                    <p><span class="blue">Экспресс доставка:</span> <b>700 руб.</b> в пределах МКАД</p>
                    <p><span class="blue">За МКАД:</span> <b>+25 р/км</b></p>
                </div>
                <div class="info__block-choose">
                    <p><span class="green">Не можете определиться с выбором:</span></p>
                    <p>У нас большой ассортимент с гарантией лучшей цены. Наши квалифицированные специалисты Вам помогут! </p>
                </div>
            </div>
            <div class="info__block-right">
                <a href="#minimap" role="button" data-toggle="modal">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/mini-map.png" height="65" width="121" alt="map">
                </a>

                <div style="" id="minimap" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content" style="overflow: hidden;">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2247.22833190998!2d37.725333316232344!3d55.71978398054456!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x414ab53db1aba451%3A0xd92ea1997e4ae69c!2zMS3QuSDQk9GA0LDQudCy0L7RgNC-0L3QvtCy0YHQutC40Lkg0L_RgC3QtCwgNCwg0JzQvtGB0LrQstCwLCAxMDk1MTg!5e0!3m2!1suk!2sru!4v1489654153610" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                      </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                  </div>


                <p class="phone">+7 (499)<span> 650-55-55</span></p>
                <a data-modal="#callback_form" class="button big_callback_btn d_ib modal_btn" href="#">Заказать звонок </a>
            </div>

        </div>

    </article>


    </div>
</div>



<?

$emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
	if ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET'] && !$emptyProductProperties)
	{
?>
<div id="<? echo $arItemIDs['BASKET_PROP_DIV']; ?>" style="display: none;">
<?
		if (!empty($arResult['PRODUCT_PROPERTIES_FILL']))
		{
			foreach ($arResult['PRODUCT_PROPERTIES_FILL'] as $propID => $propInfo)
			{
?>
	<input type="hidden" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo htmlspecialcharsbx($propInfo['ID']); ?>">
<?
				if (isset($arResult['PRODUCT_PROPERTIES'][$propID]))
					unset($arResult['PRODUCT_PROPERTIES'][$propID]);
			}
		}
		$emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
		if (!$emptyProductProperties)
		{
?>
	<table>
<?
			foreach ($arResult['PRODUCT_PROPERTIES'] as $propID => $propInfo)
			{
?>
	<tr><td><? echo $arResult['PROPERTIES'][$propID]['NAME']; ?></td>
	<td>
<?
				if(
					'L' == $arResult['PROPERTIES'][$propID]['PROPERTY_TYPE']
					&& 'C' == $arResult['PROPERTIES'][$propID]['LIST_TYPE']
				)
				{
					foreach($propInfo['VALUES'] as $valueID => $value)
					{
						?><label><input type="radio" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo $valueID; ?>" <? echo ($valueID == $propInfo['SELECTED'] ? '"checked"' : ''); ?>><? echo $value; ?></label><br><?
					}
				}
				else
				{
					?><select name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]"><?
					foreach($propInfo['VALUES'] as $valueID => $value)
					{
						?><option value="<? echo $valueID; ?>" <? echo ($valueID == $propInfo['SELECTED'] ? '"selected"' : ''); ?>><? echo $value; ?></option><?
					}
					?></select><?
				}
?>
	</td></tr>
<?
			}
?>
	</table>
<?
		}
?>
</div>
<?
	}


	if ($arResult['MIN_PRICE']['DISCOUNT_VALUE'] != $arResult['MIN_PRICE']['VALUE'])
	{
		$arResult['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arResult['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'];
		$arResult['MIN_BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arResult['MIN_BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'];
	}
	$arJSParams = array(
		'CONFIG' => array(
			'USE_CATALOG' => $arResult['CATALOG'],
			'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
			'SHOW_PRICE' => (isset($arResult['MIN_PRICE']) && !empty($arResult['MIN_PRICE']) && is_array($arResult['MIN_PRICE'])),
			'SHOW_DISCOUNT_PERCENT' => ($arParams['SHOW_DISCOUNT_PERCENT'] == 'Y'),
			'SHOW_OLD_PRICE' => ($arParams['SHOW_OLD_PRICE'] == 'Y'),
			'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
			'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
			'SHOW_BASIS_PRICE' => ($arParams['SHOW_BASIS_PRICE'] == 'Y'),
			'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
			'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y')
		),
		'VISUAL' => array(
			'ID' => $arItemIDs['ID'],
		),
		'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
		'PRODUCT' => array(
			'ID' => $arResult['ID'],
			'PICT' => $arFirstPhoto,
			'NAME' => $arResult['~NAME'],
			'SUBSCRIPTION' => true,
			'PRICE' => $arResult['MIN_PRICE'],
			'BASIS_PRICE' => $arResult['MIN_BASIS_PRICE'],
			'SLIDER_COUNT' => $arResult['MORE_PHOTO_COUNT'],
			'SLIDER' => $arResult['MORE_PHOTO'],
			'CAN_BUY' => $arResult['CAN_BUY'],
			'CHECK_QUANTITY' => $arResult['CHECK_QUANTITY'],
			'QUANTITY_FLOAT' => is_double($arResult['CATALOG_MEASURE_RATIO']),
			'MAX_QUANTITY' => $arResult['CATALOG_QUANTITY'],
			'STEP_QUANTITY' => $arResult['CATALOG_MEASURE_RATIO'],
		),
		'BASKET' => array(
			'ADD_PROPS' => ($arParams['ADD_PROPERTIES_TO_BASKET'] == 'Y'),
			'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
			'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
			'EMPTY_PROPS' => $emptyProductProperties,
			'BASKET_URL' => $arParams['BASKET_URL'],
			'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
			'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
		)
	);
	if ($arParams['DISPLAY_COMPARE'])
	{
		$arJSParams['COMPARE'] = array(
			'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
			'COMPARE_PATH' => $arParams['COMPARE_PATH']
		);
	}
	unset($emptyProductProperties);
?>
<script type="text/javascript">
var <? echo $strObName; ?> = new JCCatalogElement(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
BX.message({
	ECONOMY_INFO_MESSAGE: '<? echo GetMessageJS('CT_BCE_CATALOG_ECONOMY_INFO'); ?>',
	BASIS_PRICE_MESSAGE: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_BASIS_PRICE') ?>',
	TITLE_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_TITLE_ERROR') ?>',
	TITLE_BASKET_PROPS: '<? echo GetMessageJS('CT_BCE_CATALOG_TITLE_BASKET_PROPS') ?>',
	BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
	BTN_SEND_PROPS: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_SEND_PROPS'); ?>',
	BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_BASKET_REDIRECT') ?>',
	BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE'); ?>',
	BTN_MESSAGE_CLOSE_POPUP: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE_POPUP'); ?>',
	TITLE_SUCCESSFUL: '<? echo GetMessageJS('CT_BCE_CATALOG_ADD_TO_BASKET_OK'); ?>',
	COMPARE_MESSAGE_OK: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_OK') ?>',
	COMPARE_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_UNKNOWN_ERROR') ?>',
	COMPARE_TITLE: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_TITLE') ?>',
	BTN_MESSAGE_COMPARE_REDIRECT: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT') ?>',
	SITE_ID: '<? echo SITE_ID; ?>'
});
</script>
