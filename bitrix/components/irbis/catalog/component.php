<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;

if (isset($arParams["USE_FILTER"]) && $arParams["USE_FILTER"]=="Y")
{
	$arParams["FILTER_NAME"] = trim($arParams["FILTER_NAME"]);
	if ($arParams["FILTER_NAME"] === '' || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
		$arParams["FILTER_NAME"] = "arrFilter";
}
else
	$arParams["FILTER_NAME"] = "";

$arDefaultUrlTemplates404 = array(
	"sections" => "",
	"section" => "#SECTION_ID#/",
	"element" => "#SECTION_ID#/#ELEMENT_ID#/",
	"compare" => "compare.php?action=COMPARE",
);

$arDefaultVariableAliases404 = array();

$arDefaultVariableAliases = array();

$arComponentVariables = array(
	"SECTION_ID",
	"SECTION_CODE",
	"ELEMENT_ID",
	"ELEMENT_CODE",
	"action",
);

$arResult = [];

if($arParams["SEF_MODE"] == "Y")
{
	$arVariables = array();

	$engine = new CComponentEngine($this);
	if (\Bitrix\Main\Loader::includeModule('iblock'))
	{
		$engine->addGreedyPart("#SECTION_CODE_PATH#");
		$engine->setResolveCallback(array("CIBlockFindTools", "resolveComponentEngine"));
	}
	$arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams["SEF_URL_TEMPLATES"]);
	$arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases404, $arParams["VARIABLE_ALIASES"]);
	
	$componentPage = $engine->guessComponentPath(
		$arParams["SEF_FOLDER"],
		$arUrlTemplates,
		$arVariables
	);

	if(!$componentPage && isset($_REQUEST["q"]))
		$componentPage = "search";

	$b404 = false;
	if(!$componentPage)
	{
		$componentPage = "sections";
		$b404 = true;
	}
    
    /**
     * Hawkart
    **/
    
    $curDir = $APPLICATION->GetCurDir();

    //��������� �� ���� ������ ��� ��� ������� (���� ���� ���������� ��������� ����� ������ � �������)
    if($arSection=getSectionByUrl($curDir))
    {
         $componentPage = "section";
         $arVariables["SECTION_ID"] = $arSection["ID"];
         $arVariables["SECTION_CODE"] = $arSection["CODE"];
         $arVariables["SECTION_CODE_PATH"] = $curDir;
         unset($arVariables["ELEMENT_CODE"]);
    }
	
	
    
    //���������� ���� ������� ����� ���������. ������ �� ���� �� ������.
    if($arElement=getElementByUrl($curDir))
    {
         $componentPage = "element";
         $arVariables["ELEMENT_ID"] = $arElement["ID"];
    }

    $arDopParams = "";
	if((isset($arVariables["ELEMENT_CODE"]) && $arVariables["ELEMENT_CODE"]) || (isset($arVariables["SECTION_CODE"]) && $arVariables["SECTION_CODE"]) || (isset($arVariables["SECTION_CODE_PATH"]) && $arVariables["SECTION_CODE_PATH"])){
		global $DB;
		$result = $DB->Query("SELECT e.ID, e.NAME, e.CODE, e.PREVIEW_TEXT FROM b_iblock_element e
			WHERE e.IBLOCK_ID = 17
				AND (e.CODE = '".trim($arVariables["SECTION_CODE"])."'
						OR e.CODE = '".trim($arVariables["ELEMENT_CODE"])."'
						OR e.CODE = '".trim($arVariables["SECTION_CODE_PATH"])."')
				AND e.ACTIVE = 'Y'");
			
		if($row = $result->Fetch())
		{
			if($row["CODE"])
			{
				$componentPage = "section";
		        $arVariables["SECTION_ID"] = "18";
		        $arVariables["SECTION_CODE"] = "battery";
		        $arVariables["SECTION_CODE_PATH"] = $curDir;
				unset($arVariables["ELEMENT_CODE"]);
				$arDopParams = $row;
				//$arVariables["SMART_FILTER_PATH"] = str_replace("+", " ", $row["PREVIEW_TEXT"]);
				$arParam = explode("&", $row["PREVIEW_TEXT"]);
				foreach($arParam as $val){
					$keys = explode("=", $val);
					$_GET[$keys[0]] = $keys[1];
				}	
			}
		}
	}

    /**
     * Hawkart end
    **/
    

	if($componentPage == "section")
	{
		if (isset($arVariables["SECTION_ID"]))
			$b404 |= (intval($arVariables["SECTION_ID"])."" !== $arVariables["SECTION_ID"]);
		else
			$b404 |= !isset($arVariables["SECTION_CODE"]);


	}
	
	
	
	if($b404 && $arParams["SET_STATUS_404"]==="Y")
	{
		$folder404 = str_replace("\\", "/", $arParams["SEF_FOLDER"]);
		if ($folder404 != "/")
			$folder404 = "/".trim($folder404, "/ \t\n\r\0\x0B")."/";
		if (substr($folder404, -1) == "/")
			$folder404 .= "index.php";

		if($folder404 != $APPLICATION->GetCurPage(true))
			CHTTP::SetStatus("404 Not Found");
	}

	CComponentEngine::InitComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);
	$arResult = $arResult + array(
		"FOLDER" => $arParams["SEF_FOLDER"],
		"URL_TEMPLATES" => $arUrlTemplates,
		"VARIABLES" => $arVariables,
		"ALIASES" => $arVariableAliases,
		"SEOTAG" => $arDopParams
	);
	
}
else
{
	$arVariables = array();

	$arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases, $arParams["VARIABLE_ALIASES"]);
	CComponentEngine::InitComponentVariables(false, $arComponentVariables, $arVariableAliases, $arVariables);

	$componentPage = "";

	$arCompareCommands = array(
		"COMPARE",
		"DELETE_FEATURE",
		"ADD_FEATURE",
		"DELETE_FROM_COMPARE_RESULT",
		"ADD_TO_COMPARE_RESULT",
		"COMPARE_BUY",
		"COMPARE_ADD2BASKET",
	);

	if(isset($arVariables["action"]) && in_array($arVariables["action"], $arCompareCommands))
		$componentPage = "compare";
	elseif(isset($arVariables["ELEMENT_ID"]) && intval($arVariables["ELEMENT_ID"]) > 0)
		$componentPage = "element";
	elseif(isset($arVariables["ELEMENT_CODE"]) && strlen($arVariables["ELEMENT_CODE"]) > 0)
		$componentPage = "element";
	elseif(isset($arVariables["SECTION_ID"]) && intval($arVariables["SECTION_ID"]) > 0)
		$componentPage = "section";
	elseif(isset($arVariables["SECTION_CODE"]) && strlen($arVariables["SECTION_CODE"]) > 0)
		$componentPage = "section";
	elseif(isset($_REQUEST["q"]))
		$componentPage = "search";
	else
		$componentPage = "sections";

	$arResult = array(
		"FOLDER" => "",
		"URL_TEMPLATES" => Array(
			"section" => htmlspecialcharsbx($APPLICATION->GetCurPage())."?".$arVariableAliases["SECTION_ID"]."=#SECTION_ID#",
			"element" => htmlspecialcharsbx($APPLICATION->GetCurPage())."?".$arVariableAliases["SECTION_ID"]."=#SECTION_ID#"."&".$arVariableAliases["ELEMENT_ID"]."=#ELEMENT_ID#",
			"compare" => htmlspecialcharsbx($APPLICATION->GetCurPage())."?".$arVariableAliases["action"]."=COMPARE",
		),
		"VARIABLES" => $arVariables,
		"ALIASES" => $arVariableAliases
	);
}

	/*print_r("<pre>");
	print_r($componentPage);
	print_r("</pre>");
	return;*/

$this->IncludeComponentTemplate($componentPage);
?>