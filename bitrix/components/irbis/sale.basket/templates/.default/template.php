<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CJSCore::Init(array('fx', 'popup', 'window', 'ajax'));
?>
<a name="order_form"></a>
<script src="<?=$templateFolder."/ajax.js"?>"></script>
<?
if($_POST["is_ajax_post"] != "Y")
{
	?><form action="<?=$APPLICATION->GetCurPage();?>" method="POST" name="ORDER_FORM" id="ORDER_FORM" enctype="multipart/form-data" class="create_order">
	<?=bitrix_sessid_post()?>
    <div id="order_form_content">
	<?
}
else
{    
	$APPLICATION->RestartBuffer();
}

if(!empty($arResult["ERROR"]) && $arResult["USER_VALS"]["FINAL_STEP"] == "Y")
{
	foreach($arResult["ERROR"] as $v)
		echo ShowError($v);
	?>
	<script type="text/javascript">
		top.BX.scrollToNode(top.BX('ORDER_FORM'));
	</script>
	<?
}

if(empty($arResult["BASKET_ITEMS"]))
{
    echo "<span style='color:red'>Корзина пуста</span>";
}else{
    include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/items.php");
}

if($_POST["is_ajax_post"] != "Y")
{
	?>
        </div>
		<input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">
		<input type="hidden" name="json" value="Y">
	</form>
	<?
}
else
{
	die();
}
?>