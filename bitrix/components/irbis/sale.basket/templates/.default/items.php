<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<? //{aquakosh (skype): Определяем, есть ли расхождения во флагах сдачи отработанных аккумуляторов - замена АКБ должна оформляться отдельным заказом.
$arFlagsChangeAKB = array();
foreach($arResult["BASKET_ITEMS"] as $arItem) {
    if (intval($arItem["PROPERTY_PRICE_CHANGE_VALUE"]) > 0) {
        // Вторая сумма задана - проверяем, установлен ли флаг замены АКБ.
        if($arResult["CHANGED_PRICE_".$arItem["ID"]]=="on") {
            // Замена АКБ.
            $arFlagsChangeAKB[] = "Y";
        } else {
            // Возможность замены АКБ есть, но НЕ выбрана.
            $arFlagsChangeAKB[] = "N";
        }
    } else {
        // Вторая сумма НЕ задана, следовательно эта позиция НЕ имеет возможности замены АКБ.
        $arFlagsChangeAKB[] = "N";
    }
}
$disableMakeOrder = false;
if (count(array_count_values($arFlagsChangeAKB)) > 1 ) {
    // В корзине разношёрстных значений флагов замены АКБ несколько - нельзя оформлять заказ.
    $disableMakeOrder = true;
}
//}aquakosh ?>

<div class="cart_items_box mb75">
    <?
    foreach($arResult["BASKET_ITEMS"] as $arItem)
    {
        $arItem["SUM"] = $arItem["PRICE"]*$arItem["QUANTITY"];
        ?>
        <section class="cart_item" id="item-<?=$arItem["ID"]?>">
            <div class="cart_item_cell1">
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="d_ib"><img src="<?=$arItem["PREVIEW_PICTURE_SRC"]?>" alt="<?=$arItem["NAME"]?>"/></a>
            </div>
            <div class="cart_item_cell2">
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><h4 class="fs_18 bold mb5"><?=$arItem["NAME"]?></h4></a>
                <?=$arItem["PREVIEW_TEXT"]?>
            </div>
            <div class="cart_item_cell3">
                <div class="form_label">Выберите кол-во:</div>
                <div class="mb15">
                    <select class="styler item-number" name="QUANTITY_<?=$arItem["ID"]?>" onchange='submitForm("Y");'>
                        <?
                        for($i=1; $i<100; $i++)
                        {
                            ?><option value="<?=$i?>" <?if($i==$arItem["QUANTITY"]):?>selected<?endif;?>><?=$i?></option><?
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="cart_item_cell4">
                <div class="product_item_price heavy mb20"><?=number_format($arItem["SUM"], 0, "", " ")?> <span class="ruble">руб.</span></div>
                <?if(intval($arItem["PROPERTY_PRICE_CHANGE_VALUE"])>0) {?>
                    <div class="p_rel">
                        <label class="checkbox_label fs_12 var2 abs_label">
                            <input type="checkbox" name="CHANGED_PRICE_<?=$arItem["ID"]?>" class="styler" onchange='submitForm("Y");' <?if($arResult["CHANGED_PRICE_".$arItem["ID"]]=="on"):?>checked<?endif;?>/>
                            <span class="d_block wrapper">
												<?if(strstr($arItem["DETAIL_PAGE_URL"], "/catalog/battery/")):?>
													Сдача отработанного аккумулятора аналогичной ёмкости
												<?else:?>
													Предъявляю купон на скидку
												<?endif;?>
							</span>
                        </label>
                    </div>
                <?}?>
            </div>
            <div class="cart_item_cell5">
                <input type="hidden" name="DELETE_<?=$arItem["ID"]?>" value="N">
                <a href="#" class="btn2 bold red_skin item-delete">удалить</a>
            </div>
        </section>
        <?
    }
    ?>
    <section class="total_cart_sum">
        общая сумма:
        <div class="product_item_price heavy"><?=number_format($arResult["ORDER_PRICE"], 0, "", " ")?> <span class="ruble">руб.</span></div>
    </section>
    
    <? //{aquakosh (skype): Определяем, есть ли расхождения во флагах сдачи отработанных аккумуляторов - замена АКБ должна оформляться отдельным заказом.
    if ($disableMakeOrder == true) {
        // Оформление заказа запрещено.
        ?>
        <section>
            <div class="warning">
                Внимание!<br />
                Вы НЕ можете оформить заказ, т.к. в корзине помимо обычных товаров, присутствует ещё и сдача отработанного аккумулятора аналогичной ёмкости!<br />
                Пожалуйста оформите сдачу аккумулятора ОТДЕЛЬНЫМ заказом.
            </div>
        </section>
        <a class="btn2 red_skin" href="">Оформление заказа недоступно</a>
        <?
    } else {
        // Оформление заказа разрешено.
        ?>
        <a class="btn2 grn_skin" href="/personal/order/make/">Перейти к оформлению заказа</a>
        <?
    }
    //}aquakosh ?>
    
</div>