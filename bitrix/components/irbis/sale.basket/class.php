<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class SaleBasketComponent extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
	{
	   global $APPLICATION;
	   return $arParams;
    }
    
    protected function checkRequiredModules()
	{
        if(! \Bitrix\Main\Loader::includeModule ('sale'))
		{
			ShowError(GetMessage('SALE_MODULE_NOT_INSTALL'));
			return;
		}
        
        if(! \Bitrix\Main\Loader::includeModule ('catalog'))
		{
			ShowError(GetMessage('CATALOG_MODULE_NOT_INSTALL'));
			return;
		}
        
        if(! \Bitrix\Main\Loader::includeModule ('iblock'))
		{
			ShowError(GetMessage('IBLOCK_MODULE_NOT_INSTALL'));
			return;
		}

		if(! \Bitrix\Main\Loader::includeModule("currency"))
		{
			ShowError(GetMessage("CURRENCY_MODULE_NOT_INSTALLED"));
			return;
		}
	}
    
    public function executeComponent()
	{
        global $USER;
        $this->checkRequiredModules();
        
        $this->arResult = array(
            "BASE_LANG_CURRENCY" => CSaleLang::GetLangCurrency(SITE_ID),
            "WEIGHT_UNIT" => htmlspecialcharsbx(COption::GetOptionString('sale', 'weight_unit', false, SITE_ID)),
            "WEIGHT_KOEF" => htmlspecialcharsbx(COption::GetOptionString('sale', 'weight_koef', 1, SITE_ID)),
        );
        
        $this->getItemsList();

        if(isset($_SESSION["ORDER"]) && is_array($_SESSION["ORDER"]))
        {
            $this->arResult+= $_SESSION["ORDER"]; 
        }

        //Аякс
        $this->ajaxRequest();
        
        $this->includeComponentTemplate();
	}
        
    public function getItemsList()
    {
        CModule::IncludeModule("sale");
        
        $arElementId = array();
        $arSetParentWeight = array();
        $DISCOUNT_PRICE_ALL = 0;
        $arResult["MAX_DIMENSIONS"] = $arResult["ITEMS_DIMENSIONS"] = array();
        $arSelFields = array("ID", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "QUANTITY", "DELAY",
        	"CAN_BUY", "PRICE", "WEIGHT", "NAME", "CURRENCY", "CATALOG_XML_ID", "VAT_RATE",
        	"NOTES", "DISCOUNT_PRICE", "PRODUCT_PROVIDER_CLASS", "DIMENSIONS", "TYPE", "SET_PARENT_ID", "DETAIL_PAGE_URL"
        );
        $dbBasketItems = CSaleBasket::GetList(
        		array("ID" => "ASC"),
        		array(
        				"FUSER_ID" => CSaleBasket::GetBasketUserID(),
        				"LID" => SITE_ID,
        				"ORDER_ID" => "NULL"
        			),
        		false,
        		false,
        		$arSelFields
        	);
        while ($arItem = $dbBasketItems->GetNext())
        {
            if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y")
        	{
        		$arItem["PRICE"] = roundEx($arItem["PRICE"], SALE_VALUE_PRECISION);
        		$arItem["QUANTITY"] = DoubleVal($arItem["QUANTITY"]);
        
        		$arItem["WEIGHT"] = DoubleVal($arItem["WEIGHT"]);
        		$arItem["VAT_RATE"] = DoubleVal($arItem["VAT_RATE"]);
        
        		$arDim = unserialize($arItem["~DIMENSIONS"]);
        
        		if(is_array($arDim))
        		{
        			$arItem["DIMENSIONS"] = $arDim;
        			unset($arItem["~DIMENSIONS"]);
        
        			$arResult["MAX_DIMENSIONS"] = CSaleDeliveryHelper::getMaxDimensions(
        																	array(
        																		$arDim["WIDTH"],
        																		$arDim["HEIGHT"],
        																		$arDim["LENGTH"]
        																		),
        																	$arResult["MAX_DIMENSIONS"]);
        
        			$arResult["ITEMS_DIMENSIONS"][] = $arDim;
        		}
        
        		$arItem["PRICE_FORMATED"] = SaleFormatCurrency($arItem["PRICE"], $arItem["CURRENCY"]);
        		$arItem["WEIGHT_FORMATED"] = roundEx(DoubleVal($arItem["WEIGHT"]/$arResult["WEIGHT_KOEF"]), SALE_WEIGHT_PRECISION)." ".$arResult["WEIGHT_UNIT"];
        
        		if($arItem["DISCOUNT_PRICE"] > 0)
        		{
        			$arItem["DISCOUNT_PRICE_PERCENT"] = $arItem["DISCOUNT_PRICE"]*100 / ($arItem["DISCOUNT_PRICE"] + $arItem["PRICE"]);
        			$arItem["DISCOUNT_PRICE_PERCENT_FORMATED"] = roundEx($arItem["DISCOUNT_PRICE_PERCENT"], 0)."%";
        		}
        
        		if (!CSaleBasketHelper::isSetItem($arItem))
        		{
        			$DISCOUNT_PRICE_ALL += $arItem["DISCOUNT_PRICE"] * $arItem["QUANTITY"];
        			$arItem["DISCOUNT_PRICE"] = roundEx($arItem["DISCOUNT_PRICE"], SALE_VALUE_PRECISION);
        			$arResult["ORDER_PRICE"] += $arItem["PRICE"] * $arItem["QUANTITY"];
        		}
        
        		if (!CSaleBasketHelper::isSetItem($arItem))
        		{
        			$arResult["ORDER_WEIGHT"] += $arItem["WEIGHT"] * $arItem["QUANTITY"];
        		}
        
        		$arResult["BASKET_ITEMS"][] = $arItem;
                $arElementId[] = $arItem["PRODUCT_ID"];
        	}
            
            $arResult["PRICE_WITHOUT_DISCOUNT"] = SaleFormatCurrency($arResult["ORDER_PRICE"] + $DISCOUNT_PRICE_ALL, $allCurrency);
            $arResult["ORDER_WEIGHT_FORMATED"] = roundEx(DoubleVal($arResult["ORDER_WEIGHT"]/$arResult["WEIGHT_KOEF"]), SALE_WEIGHT_PRECISION)." ".$arResult["WEIGHT_UNIT"];
        	$arResult["ORDER_PRICE_FORMATED"] = SaleFormatCurrency($arResult["ORDER_PRICE"], $arResult["BASE_LANG_CURRENCY"]);
        	$arResult["VAT_SUM_FORMATED"] = SaleFormatCurrency($arResult["VAT_SUM"], $arResult["BASE_LANG_CURRENCY"]);
        }
        
        // get properties for iblock elements and their parents (if any)
        $arCustomSelectFields = array("PROPERTY_PRICE_CHANGE");
        $arSelect = array_merge(array("ID", "PREVIEW_PICTURE", "DETAIL_PICTURE", "PREVIEW_TEXT"), $arCustomSelectFields);
        $arProductData = getProductProps($arElementId, $arSelect);
        foreach ($arResult["BASKET_ITEMS"] as &$arResultItem)
        {
        	$productId = $arResultItem["PRODUCT_ID"];
        	if((int)$arProductData[$productId]["PREVIEW_PICTURE"] > 0)
        		$arResultItem["PREVIEW_PICTURE"] = $arProductData[$productId]["PREVIEW_PICTURE"];
        	if((int)$arProductData[$productId]["DETAIL_PICTURE"] > 0)
        		$arResultItem["DETAIL_PICTURE"] = $arProductData[$productId]["DETAIL_PICTURE"];
        	if($arProductData[$productId]["PREVIEW_TEXT"] != '')
        		$arResultItem["PREVIEW_TEXT"] = $arProductData[$productId]["PREVIEW_TEXT"];
        
        	foreach ($arProductData[$arResultItem["PRODUCT_ID"]] as $key => $value)
        	{
        		if (strpos($key, "PROPERTY_") !== false)
        			$arResultItem[$key] = $value;
        	}
            
        	$arResultItem["PREVIEW_PICTURE_SRC"] = "";
        	if (isset($arResultItem["PREVIEW_PICTURE"]) && intval($arResultItem["PREVIEW_PICTURE"]) > 0)
        	{
        		$arImage = CFile::GetFileArray($arResultItem["PREVIEW_PICTURE"]);
        		if ($arImage)
        		{
        			$arFileTmp = CFile::ResizeImageGet(
        				$arImage,
        				array("width" => "110", "height" =>"110"),
        				BX_RESIZE_IMAGE_PROPORTIONAL,
        				true
        			);
        
        			$arResultItem["PREVIEW_PICTURE_SRC"] = $arFileTmp["src"];
        		}
        	}
        
        	$arResultItem["DETAIL_PICTURE_SRC"] = "";
        	if (isset($arResultItem["DETAIL_PICTURE"]) && intval($arResultItem["DETAIL_PICTURE"]) > 0)
        	{
        		$arImage = CFile::GetFileArray($arResultItem["DETAIL_PICTURE"]);
        		if ($arImage)
        		{
        			$arFileTmp = CFile::ResizeImageGet(
        				$arImage,
        				array("width" => "110", "height" =>"110"),
        				BX_RESIZE_IMAGE_PROPORTIONAL,
        				true
        			);
        
        			$arResultItem["DETAIL_PICTURE_SRC"] = $arFileTmp["src"];
        		}
        	}
            
            if(empty($arResultItem["PREVIEW_PICTURE_SRC"]))
            {
                $arResultItem["PREVIEW_PICTURE_SRC"] = $arResultItem["DETAIL_PICTURE_SRC"];
            }
        }
        if (isset($arResultItem))
        	unset($arResultItem);
        
        $this->arResult["ORDER_PRICE"] = $arResult["ORDER_PRICE"];
        $this->arResult["BASKET_ITEMS"] = $arResult["BASKET_ITEMS"];
    }
    
    public function ajaxRequest()
    {
        global $USER;
        
        if($_SERVER["REQUEST_METHOD"] == "POST" && check_bitrix_sessid())
    	{
            unset($_POST["sessid"]);
            $_SESSION["ORDER"] = $_POST;
            //{aquakosh (skype): Необходимо удалить из arResult старые данные по флагам замены АКБ, т.к. при снятии флага, в пришедших данных POST ничего нет, а merge массивов НЕ удаляет отсутствующие данные.
            foreach($this->arResult["BASKET_ITEMS"] as $arItem) {
                $this->arResult["CHANGED_PRICE_".$arItem["ID"]] = "off";
            }
            //}aquakosh
            $this->arResult = array_merge($this->arResult, $_POST);
            $this->recalculateBasket($_POST);
            $this->getItemsList();
    	}
    }
    
    public function recalculateBasket($arPost)
    {
        CModule::IncludeModule("sale");

        global $USER;
        $arRes = array();

        $arTmpItems = array();

        $dbItems = CSaleBasket::GetList(
            array("ID" => "ASC"),
            array(
                "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                "LID" => SITE_ID,
                "ORDER_ID" => "NULL"
            ),
            false,
            false,
            array(
                "ID", "NAME", "MODULE", "PRODUCT_ID", "PRICE", "QUANTITY", "DELAY", "CAN_BUY"
            )
    	);
	
        while ($arItem = $dbItems->Fetch()) {
            $arTmpItems[] = $arItem;
        }
        
        $this->arResult["NEW_ITEMS"] = $arPost;

        foreach ($arTmpItems as $arItem) {
            $isFloatQuantity = false;
            
            //Если поменяли количество
            if (!isset($arPost["QUANTITY_".$arItem["ID"]]) || floatval($arPost["QUANTITY_".$arItem["ID"]]) <= 0) {
                $quantityTmp = intval($arItem["QUANTITY"]);
            } else {
                $quantityTmp = intval($arPost["QUANTITY_".$arItem["ID"]]);
            }

            $deleteTmp = ($arPost["DELETE_".$arItem["ID"]] == "Y") ? "Y" : "N";
            
            //удаление
            if ($deleteTmp == "Y")
            {
                    CSaleBasket::Delete($arItem["ID"]);
            }
            elseif ($arItem["CAN_BUY"] == "Y")
            {
                unset($arFields);
                $arFields = array();
                $arFields["QUANTITY"] = $quantityTmp;
                if($arPost["CHANGED_PRICE_".$arItem["ID"]]=="on")
                {
                    $price = $this->changedPrice($arItem["PRODUCT_ID"]);
                    /*$arFields["PROPS"][] = array(
                        "NAME" => "Сдача отработанного аккумулятора аналогичной ёмкости", 
                        "CODE" => "CHANGE",
                        "VALUE" => "Да",
                        "SORT" => 100
                    );*/                
                } else {
                    $ar_res = CPrice::GetBasePrice($arItem["PRODUCT_ID"]);
                    $price = $ar_res["PRICE"];
                    //$arFields["PROPS"] = array();
                }
                $arFields["PRICE"] = $price;

                if (count($arFields) > 0 && ($arItem["QUANTITY"] != $arFields["QUANTITY"]) || $arItem["PRICE"]!=$price) {
                    CSaleBasket::Update($arItem["ID"], $arFields);
                }
            }
	}
        return $arRes;
    }
    
    public function changedPrice($PRODUCT_ID)
    {
        CModule::IncludeModule("iblock");
        
        $db_props = CIBlockElement::GetProperty(CATALOG_ID, $PRODUCT_ID, array("sort" => "asc"), Array("CODE"=>"PRICE_CHANGE"));
        if($ar_props = $db_props->Fetch())
        {
            //return $ar_props["VALUE"];
            return round(doubleval($ar_props["VALUE"]),2); //aquakosh (skype): Прайс вообще-то может быть с копейками, поэтому double и round.
        }
    }
}