$(document).ready(function() {
    $("body").on("submit", ".login-form", function(e){
    	e.preventDefault();
    	params=$(this).serialize()+"&action=login";
        $(".login-form").find(".error").hide().html("");
        $.ajax({
            type: 'POST',
            url: '/auth/ajax.php',
            data: params,
            dataType: 'json',
            success: function(result){
                if(result.status)
                    document.location.reload();
                else
                    $(".login-form").find(".error").show().html(result.message);
            }
        });
    });

    $("body").on("submit", ".register-form", function(e){
    	e.preventDefault();
    	params=$(this).serialize()+"&action=register";
        $(".register-form").find(".error").hide().html("");
        $.ajax({
            type: 'POST',
            url: '/auth/ajax.php',
            data: params,
            dataType: 'json',
            success: function(result){
                console.log(result);
                if(result.status){
                    $(".register-form").find(".error").show().html(result.message);
                    document.location.reload();
                }else{
                    $(".register-form").find(".error").show().html(result.message);
                }
            }
        });
    });

    $("body").on("submit", ".recovery-form", function(e){
    	e.preventDefault();
    	params=$(this).serialize()+"&action=recovery";
        $(".recovery-form").find(".error").hide().html("");
        $.ajax({
            type: 'POST',
            url: '/auth/ajax.php',
            data: params,
            dataType: 'json',
            success: function(result){
                if(result.status)
                    $(".recovery-form").find(".error").show().html(result.message);//location.reload();
                else
                    $(".recovery-form").find(".error").show().html(result.message);
            }
        });
    });

    $("body").on("submit", ".callback-form", function(e){
    	e.preventDefault();
    	params=$(this).serialize()+"&action=callback";
        $(".callback-form").find(".error").hide().html("");
        $.ajax({
            type: 'POST',
            url: '/actions.php',
            data: params,
            dataType: 'json',
            success: function(result)
            {
                if(result.status)
                {
                    $(".callback-form").find(".error").hide().html("");
                    $.arcticmodal('close');

                    var base_url = $('body #sitepath').attr('data-path');
                    $.ajax({
                        url:base_url+'/ajax/success-callback.php',
                        success: function (response) {
                            $('#success-callback').html(response);
                        }
                    });
                    $("#success-callback").arcticmodal();
                } else {
                    $(".callback-form").find(".error").show().html(result.message);
                }
            }
        });
    });

    $('body').on('submit', '.order-in-advance-form', function (e) {
        e.preventDefault();

        params = $(this).serialize() + '&action=order-in-advance';

        $('.order-in-advance-form').find('.error').hide().html('');

        $.ajax({
            type: 'POST',
            url: '/actions.php',
            data: params,
            dataType: 'json',
            success: function (result) {
                if (result.status) {
                    $('.order-in-advance-form').find('.error').hide().html('');
                    $.arcticmodal('close');

                    var base_url = $('body #sitepath').attr('data-path');
                    $.ajax({
                        url: base_url + '/ajax/success-callback.php',
                        success: function (response) {
                            $('#success-callback').html(response);
                        }
                    });

                    $('#success-callback').arcticmodal();
                } else {
                    $('.order-in-advance-form').find('.error').show().html(result.message);
                }
            }
        });
    });

    $("body").on("submit", ".one-click", function(e){
    	e.preventDefault();
        var self = $(this);
    	params = $(this).serialize()+"&action=one-click";
        self.find(".error").hide().html("");
        $.ajax({
            type: 'POST',
            url: '/actions.php',
            data: params,
            dataType: 'json',
            success: function(result){
                if(result.status)
                {
                    self.find(".error").hide().html("");
                    self.removeClass("active");
                    $.ajax({
                        mehtod: 'POST',
                        url: '/ajax/success-call-click.php',
                        success: function (data) {
                            self.parent().append(data);
                        }
                    });
                } else {
                    self.find(".error").show().html(result.message);
                }
            }
        });
    });


});
