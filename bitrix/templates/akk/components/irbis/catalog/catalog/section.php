<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

$this->setFrameMode(true);

CModule::IncludeModule("iblock");
global $APPLICATION, $USER, $arrFilter;

if(!$arResult["VARIABLES"]["SECTION_ID"]){
CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");
}

//Filter
if (!isset($arParams['FILTER_VIEW_MODE']) || (string)$arParams['FILTER_VIEW_MODE'] == '')
	$arParams['FILTER_VIEW_MODE'] = 'VERTICAL';
$arParams['USE_FILTER'] = (isset($arParams['USE_FILTER']) && $arParams['USE_FILTER'] == 'Y' ? 'Y' : 'N');
$verticalGrid = ('Y' == $arParams['USE_FILTER'] && $arParams["FILTER_VIEW_MODE"] == "VERTICAL");

if ($arParams['USE_FILTER'] == 'Y')
{

	$arFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ACTIVE" => "Y",
		"GLOBAL_ACTIVE" => "Y",
	);
	if (0 < intval($arResult["VARIABLES"]["SECTION_ID"]))
	{
		$arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
	}
	elseif ('' != $arResult["VARIABLES"]["SECTION_CODE"])
	{
		$arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];
	}

	$obCache = new CPHPCache();
	if ($obCache->InitCache(36000, serialize($arFilter), "/iblock/catalog"))
	{
		$arCurSection = $obCache->GetVars();
	}
	elseif ($obCache->StartDataCache())
	{
		$arCurSection = array();
		if (Loader::includeModule("iblock"))
		{
			$dbRes = CIBlockSection::GetList(array(), $arFilter, false, array("ID", "NAME", "CODE"));

			if(defined("BX_COMP_MANAGED_CACHE"))
			{
				global $CACHE_MANAGER;
				$CACHE_MANAGER->StartTagCache("/iblock/catalog");

				if ($arCurSection = $dbRes->Fetch())
				{
					$CACHE_MANAGER->RegisterTag("iblock_id_".$arParams["IBLOCK_ID"]);
				}
				$CACHE_MANAGER->EndTagCache();
			}
			else
			{
				if(!$arCurSection = $dbRes->Fetch())
					$arCurSection = array();
			}
		}
		$obCache->EndDataCache($arCurSection);
	}
	if (!isset($arCurSection))
	{
		$arCurSection = array();
	}
}
//hide filter
$showFilter = false;
$arCurrentPath = explode('/', $arResult['VARIABLES']['SECTION_CODE_PATH']);
foreach ($arParams['FILTER_SHOW_SECTIONS'] as $sectionName) {
    if (in_array($sectionName, $arCurrentPath)) {
        $showFilter = true;
        break;
    }
}
$head_section = $sectionName;
if(is_array($arResult["SEOTAG"])){
	$head_section = "battery";
    $showFilter = true;
    $sectionName = "battery";
}
?>
<?if(is_array($arResult["SEOTAG"])){?>
<style>
    .ui-tabs-panel.ui-widget-content.ui-corner-bottom {
        height: 0;
        padding: 0;
        margin-bottom: 5px;
        overflow: hidden;
        transition: all .3s;
    }
    .tabs__menu.ui-tabs.ui-widget.ui-widget-content.ui-corner-all:hover .ui-tabs-panel.ui-widget-content.ui-corner-bottom {
        height: auto;
        padding-top: 40px;
        overflow: visible;
        margin-bottom: 0px;
    }
</style>
<?}?>

<div class="clearfix">
	<?
		$page_uri = $_SERVER["REQUEST_URI"];
		/*print_r("<pre>");
		print_r($page_uri);
		print_r("</pre>");*/
	?>
    <? if($showFilter): ?>
        <aside class="grid_8 resp_col7">
			<?if(is_array($arResult["SEOTAG"])):?>
				<?$APPLICATION->IncludeComponent(
						"bitrix:catalog.smart.filter",
						"vertical_new",
						array(
							"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
							"IBLOCK_ID" => $arParams["IBLOCK_ID"],
							"SECTION_ID" => $currentRootSection["ID"],
							"FILTER_NAME" => "arrFilter",
							"PRICE_CODE" => $arParams["PRICE_CODE"],
							"CACHE_TYPE" => "A",
							"CACHE_TIME" => "36000000",
							"CACHE_NOTES" => "",
							"CACHE_GROUPS" => "N",
							"SAVE_IN_SESSION" => "N",
							"DOP_FILTER" => $sectionName,
							"SEOPAGE" => "Y",
							"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"]
						),
						$component,
						array('HIDE_ICONS' => 'Y')
					);?>
            <?elseif($sectionName == 'accessories'
					&& !strstr($page_uri, "/catalog/accessories/nabor_instrumentov/")
					&& !strstr($page_uri, "/catalog/accessories/zaryadnoe_ustrojstvo/")
					&& !strstr($page_uri, "/catalog/accessories/domkraty_podstavki_upory_pod_mashinu/")):?>
				<?$APPLICATION->IncludeComponent(
                    "bitrix:catalog.section.list",
                    "filter",
                    array(
                        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                        "SECTION_ID" => $currentRootSection["ID"],
                        "SECTION_CODE" => $currentRootSection["CODE"],
                        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                        "CACHE_TIME" => $arParams["CACHE_TIME"],
                        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                        "COUNT_ELEMENTS" => "N",
                        "TOP_DEPTH" => "3",
                        "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
                        "VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
                        "SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
                        "HIDE_SECTION_NAME" => "N",
                        "ADD_SECTIONS_CHAIN" => "N"
                    ),
                    $component,
                    array("HIDE_ICONS" => "N")
                );?>
            <?else:?>
			  <?if($sectionName != 'masla'
					&& !strstr($page_uri, "/catalog/accessories/")
					):?>
                <?$APPLICATION->IncludeComponent(
                	"irbis:model.filter",
                	"",
                	array(
                		"BRAND_HL" => 5,
                        "MODEL_HL" => 3,
                        "MODEL_ROW_HL" => 4,
                        "FILTER_NAME" => "",
                        "SELECT_FIELD" => ""
                	),
                	$component,
                	array("HIDE_ICONS" => "Y")
                );
                ?>
			  <?endif;?>
                <?
                if(isset($_GET["set_filter"]))
                {
                    if(!empty($_GET["CATALOG_LENGTH"]))
                    {
                        $arrFilter["<=CATALOG_LENGTH"] = $_GET["CATALOG_LENGTH"];
                    }
                }
                ?>



                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/banner-market.php"), false);?>



                <?

        		if(!empty($arResult['VARIABLES']['SECTION_ID']) && $arResult['VARIABLES']['SECTION_ID'] == 125){
        			$currentRootSection["ID"] = 125;
        		}

				if (strstr($page_uri, "/catalog/accessories/domkraty_podstavki_upory_pod_mashinu/")){
					$currentRootSection["ID"] = $arCurSection['ID'];
				}

				if (strstr($page_uri, "/catalog/accessories/nabor_instrumentov/")){
					$currentRootSection["ID"] = $arResult['VARIABLES']['SECTION_ID'];
				}

				if (strstr($page_uri, "/catalog/accessories/zaryadnoe_ustrojstvo/")){
					$currentRootSection["ID"] = 124; //$arResult['VARIABLES']['SECTION_ID'];
				}

				if(strstr($page_uri, "/catalog/accessories/nabor_instrumentov/")){
					$APPLICATION->IncludeComponent(
						"bitrix:catalog.section.list",
						"filter_instrument",
						array(
							"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
							"IBLOCK_ID" => $arParams["IBLOCK_ID"],
							"SECTION_ID" => $currentRootSection["ID"],
							"SECTION_CODE" => $currentRootSection["CODE"],
							"CACHE_TYPE" => $arParams["CACHE_TYPE"],
							"CACHE_TIME" => $arParams["CACHE_TIME"],
							"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
							"COUNT_ELEMENTS" => "N",
							"TOP_DEPTH" => "3",
							"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
							"VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
							"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
							"HIDE_SECTION_NAME" => "N",
							"ADD_SECTIONS_CHAIN" => "N"
						),
						$component,
						array("HIDE_ICONS" => "N")
					);

					$APPLICATION->IncludeComponent(
						"bitrix:catalog.smart.filter",
						"vertical_instrument",
						array(
							"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
							"IBLOCK_ID" => $arParams["IBLOCK_ID"],
							"SECTION_ID" => $currentRootSection["ID"],
							"FILTER_NAME" => "arrFilter",
							"PRICE_CODE" => $arParams["PRICE_CODE"],
							"CACHE_TYPE" => "A",
							"CACHE_TIME" => "36000000",
							"CACHE_NOTES" => "",
							"CACHE_GROUPS" => "N",
							"SAVE_IN_SESSION" => "N",
							"DOP_FILTER" => $sectionName,
							"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"]
						),
						$component,
						array('HIDE_ICONS' => 'Y')
					);
				}elseif(strstr($page_uri, "/catalog/accessories/zaryadnoe_ustrojstvo/")){

					$APPLICATION->IncludeComponent(
						"bitrix:catalog.section.list",
						"filter_zaryadnoe",
						array(
							"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
							"IBLOCK_ID" => $arParams["IBLOCK_ID"],
							"SECTION_ID" => $currentRootSection["ID"],
							"SECTION_CODE" => $currentRootSection["CODE"],
							"CACHE_TYPE" => $arParams["CACHE_TYPE"],
							"CACHE_TIME" => $arParams["CACHE_TIME"],
							"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
							"COUNT_ELEMENTS" => "N",
							"TOP_DEPTH" => "3",
							"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
							"VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
							"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
							"HIDE_SECTION_NAME" => "N",
							"ADD_SECTIONS_CHAIN" => "N"
						),
						$component,
						array("HIDE_ICONS" => "N")
					);

					$APPLICATION->IncludeComponent(
						"bitrix:catalog.smart.filter",
						"vertical_zaryadnoe",
						array(
							"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
							"IBLOCK_ID" => $arParams["IBLOCK_ID"],
							"SECTION_ID" => $currentRootSection["ID"],
							"FILTER_NAME" => "arrFilter",
							"PRICE_CODE" => $arParams["PRICE_CODE"],
							"CACHE_TYPE" => "A",
							"CACHE_TIME" => "36000000",
							"CACHE_NOTES" => "",
							"CACHE_GROUPS" => "N",
							"SAVE_IN_SESSION" => "N",
							"DOP_FILTER" => $sectionName,
							"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"]
						),
						$component,
						array('HIDE_ICONS' => 'Y')
					);
				}elseif(strstr($page_uri, "/catalog/accessories/domkraty_podstavki_upory_pod_mashinu/")){

					$APPLICATION->IncludeComponent(
						"bitrix:catalog.section.list",
						"filter_domkraty",
						array(
							"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
							"IBLOCK_ID" => $arParams["IBLOCK_ID"],
							"SECTION_ID" => $currentRootSection["ID"],
							"SECTION_CODE" => $currentRootSection["CODE"],
							"CACHE_TYPE" => $arParams["CACHE_TYPE"],
							"CACHE_TIME" => $arParams["CACHE_TIME"],
							"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
							"COUNT_ELEMENTS" => "N",
							"TOP_DEPTH" => "3",
							"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
							"VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
							"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
							"HIDE_SECTION_NAME" => "N",
							"ADD_SECTIONS_CHAIN" => "N"
						),
						$component,
						array("HIDE_ICONS" => "N")
					);

					$APPLICATION->IncludeComponent(
						"bitrix:catalog.smart.filter",
						"vertical_new",
						array(
							"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
							"IBLOCK_ID" => $arParams["IBLOCK_ID"],
							"SECTION_ID" => $currentRootSection["ID"],
							"FILTER_NAME" => "arrFilter",
							"PRICE_CODE" => $arParams["PRICE_CODE"],
							"CACHE_TYPE" => "A",
							"CACHE_TIME" => "36000000",
							"CACHE_NOTES" => "",
							"CACHE_GROUPS" => "N",
							"SAVE_IN_SESSION" => "N",
							"DOP_FILTER" => $sectionName,
							"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"]
						),
						$component,
						array('HIDE_ICONS' => 'Y')
					);
				}else{
					$APPLICATION->IncludeComponent(
						"bitrix:catalog.smart.filter",
						"vertical_new",
						array(
							"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
							"IBLOCK_ID" => $arParams["IBLOCK_ID"],
							"SECTION_ID" => $currentRootSection["ID"],
							"FILTER_NAME" => "arrFilter",
							"PRICE_CODE" => $arParams["PRICE_CODE"],
							"CACHE_TYPE" => "A",
							"CACHE_TIME" => "36000000",
							"CACHE_NOTES" => "",
							"CACHE_GROUPS" => "N",
							"SAVE_IN_SESSION" => "N",
							"DOP_FILTER" => $sectionName,
							"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"]
						),
						$component,
						array('HIDE_ICONS' => 'Y')
					);
				}
				?>

            <?endif;?>
        </aside> <!-- left col END !! -->
    <?endif;?>

    <article class="grid_31 prefix_1 resp_col29">
        <div class="hidden-block">
        <?
        if(isset($_GET["filter_form"]) && $_GET["filter_form"]=="model-form")
        {
            if(!empty($_GET["MODEL_ROW"]))
            {
                $addFilter = getModificationInformation($_GET["MODEL_ROW"]);
                $arrFilter = array_merge($arrFilter, $addFilter);
            }

            $arBrand = getElementByID($_GET["BRAND"]);
            $arModel = getElementByID($_GET["MODEL"]);

            $sectionName = "Купить аккумулятор для ".$arBrand["NAME"]." ".$arModel["NAME"];

        }else{
            if($APPLICATION->GetCurDir()=="/catalog/battery/" || isset($arrFilter["=PROPERTY_78"]) || isset($_GET["arrFilter_52_MIN"]) || isset($_GET["arrFilter_52_MAX"]))
            {
                $sectionName = "Аккумуляторы автомобильные";

                if(isset($arrFilter["=PROPERTY_78"]))
                {
                    $xmlValue = $arrFilter["=PROPERTY_78"][0];
                    $sectionName.= " ".getBrandAutoByXmlId($xmlValue);
                }

                if(intval($_GET["arrFilter_52_MIN"])>0 || intval($_GET["arrFilter_52_MAX"])>0)
                {
                    $sectionName.=" емкостью ";
                    if(intval($_GET["arrFilter_52_MIN"])>0)
                    {
                        $sectionName.= intval($_GET["arrFilter_52_MIN"]);
                    }

                    if(intval($_GET["arrFilter_52_MIN"])>0 && intval($_GET["arrFilter_52_MAX"])>0)
                    {
                        $sectionName.=" - ";
                    }

                    if(intval($_GET["arrFilter_52_MAX"])>0)
                    {
                        $sectionName.= intval($_GET["arrFilter_52_MAX"]);
                    }
                    $sectionName.= " А•ч";
                }

            }else{
                $sectionName = $currentRootSection["NAME"];
            }
        }
        ?>

        <?$APPLICATION->IncludeComponent(
        	"bitrix:catalog.section.list",
        	"",
        	array(
        		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
        		"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
        		"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
        		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
        		"CACHE_TIME" => $arParams["CACHE_TIME"],
        		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
        		"COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
        		"TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
        		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
        		"VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
        		"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
        		"HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
        		"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : '')
        	),
        	$component,
        	array("HIDE_ICONS" => "Y")
        );?>
        </div>
        <?
        if($arParams["USE_COMPARE"]=="Y")
        {
        	?><?$APPLICATION->IncludeComponent(
        		"bitrix:catalog.compare.list",
        		"",
        		array(
        			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
        			"NAME" => $arParams["COMPARE_NAME"],
        			"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
        			"COMPARE_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["compare"],
        			"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
        			"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
        			'POSITION_FIXED' => isset($arParams['COMPARE_POSITION_FIXED']) ? $arParams['COMPARE_POSITION_FIXED'] : '',
        			'POSITION' => isset($arParams['COMPARE_POSITION']) ? $arParams['COMPARE_POSITION'] : ''
        		),
        		$component,
        		array("HIDE_ICONS" => "Y")
        	);?><?
        }

        if (isset($arParams['USE_COMMON_SETTINGS_BASKET_POPUP']) && $arParams['USE_COMMON_SETTINGS_BASKET_POPUP'] == 'Y')
        {
        	$basketAction = (isset($arParams['COMMON_ADD_TO_BASKET_ACTION']) ? $arParams['COMMON_ADD_TO_BASKET_ACTION'] : '');
        }
        else
        {
        	$basketAction = (isset($arParams['SECTION_ADD_TO_BASKET_ACTION']) ? $arParams['SECTION_ADD_TO_BASKET_ACTION'] : '');
        }
        $intSectionID = 0;
        ?>

		<div class="h2 black fs_24 reg f_left"><?=$sectionName;?></div>

		<div class = "TOP_TEXT"></div>
		<?if($APPLICATION->GetCurDir()=="/catalog/accessories/"):?>
			<?$APPLICATION->IncludeComponent(
                	"bitrix:catalog.section.list",
                	"blocks",
                	array(
                		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        			     "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                		"SECTION_ID" => $currentRootSection["ID"],
                		"SECTION_CODE" => $currentRootSection["CODE"],
                		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
                		"CACHE_TIME" => $arParams["CACHE_TIME"],
                		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                		"COUNT_ELEMENTS" => "N",
                		"TOP_DEPTH" => "1",
                		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
                		"VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
                		"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
                		"HIDE_SECTION_NAME" => "N",
                		"ADD_SECTIONS_CHAIN" => "N",
						"SECTION_USER_FIELDS" => array('UF_*')
                	),
                	$component,
                	array("HIDE_ICONS" => "N")
                );?>
		<?else:?>
			<noindex>
                <div class="sort-wrapper">
                    <?php
                    Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/catalog-sort-block-show-ajax.js');

                    if ($_REQUEST['SHOW_AJAX_SORT'] == 'Y') {
                        $APPLICATION->RestartBuffer();
                        ?>
                        <div class="filter_row clearfix grn_bg mb34">
                            <div class="f_left">
                                <div class="filter_label">Сортировать по:</div>
                                <?
                                $arSort = array(
                                    "catalog_PRICE_1" => "цене",
                                //    "NAME" => "названию",
                                  //  "PROPERTY_POLARITY" => "полярности"
                                );
                                if (strpos($APPLICATION->GetCurDir(), 'battery') !== false){
                                    $arSort['PROPERTY_CAPACITY'] = "емкости";
                                    $arSort['PROPERTY_POLARITY'] = "полярности";
                                }
                                if (strpos($APPLICATION->GetCurDir(), 'domkraty_podstavki_upory_pod_mashinu') !== false){
                                    $arSort['DOMKRAT_SUBSECTION'] = "Тип домкрата";
                                }
                                foreach($arSort as $sortKey=>$sortTitle)
                                {
                                    if(isset($_GET["sortFields"]) && $_GET["sortFields"]==$sortKey)
                                    {
                                        $activeClass = "active";
                                        if($_GET["sortOrder"]=="desc")
                                        {
                                            $sortOrder = "asc";
                                            $arrowClass = "arrow_down";
                                        }else{
                                            $sortOrder = "desc";
                                            $arrowClass = "arrow_up";
                                        }
                                    }else{
                                        if($sortKey=="catalog_PRICE_1")
                                        {
                                            $activeClass = "active";
                                            $sortOrder = "desc";
                                            $arrowClass = "arrow_up";
                                        }else{
                                            $activeClass = "";
                                            $sortOrder = "desc";
                                            $arrowClass = "arrow_down";
                                        }
                                    }
                                    $url = $APPLICATION->GetCurPageParam("sortFields=".$sortKey."&sortOrder=".$sortOrder, array("sortFields", "sortOrder"));
                                    if($sortKey == 'PROPERTY_POLARITY'){
                                        $url = $APPLICATION->GetCurPageParam("sortFields=".$sortKey, array("sortFields", "sortOrder"));
                                        ?>
                                        <div class="filter_item drop">
                                            <a href="#" class="filter_link <?=$activeClass?> <?=$arrowClass?>">полярности</a>
                                            <div class="dropdown">
                                                <ul>
                                                    <li><span rel="<?=$url?>&sortOrder=desc">Прямая</span></li>
                                                    <li><span rel="<?=$url?>&sortOrder=asc">Обратная</span></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <?
                                    }elseif($sortKey == 'DOMKRAT_SUBSECTION'){
                                        ?>
                                        <div class="filter_item drop">
                                            <a href="#" class="filter_link <?=$arrowClass?>"><?=$sortTitle?></a>
                                            <div class="dropdown dropdown_large">
                                                <?$APPLICATION->IncludeComponent(
                                                    "bitrix:catalog.smart.filter",
                                                    "vertical_domkrat_type",
                                                    array(
                                                        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                                                        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                                                        "SECTION_ID" => $currentRootSection["ID"],
                                                        "FILTER_NAME" => "arrFilter",
                                                        "PRICE_CODE" => $arParams["PRICE_CODE"],
                                                        "CACHE_TYPE" => "A",
                                                        "CACHE_TIME" => "36000000",
                                                        "CACHE_NOTES" => "",
                                                        "CACHE_GROUPS" => "N",
                                                        "SAVE_IN_SESSION" => "N",
                                                        "DOP_FILTER" => $sectionName,
                                                        "TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"]
                                                    ),
                                                    $component,
                                                    array('HIDE_ICONS' => 'Y')
                                                );?>
                                                <!--<ul>
                                                    <li><a href="/catalog/battery/80-95/?sortFields=PROPERTY_POLARITY&sortOrder=desc">Прямая</a></li>
                                                </ul>-->
                                            </div>
                                        </div>
                                        <?
                                    }else{
                                        ?>
                                        <div class="filter_item">
                                            <span rel="<?=$url?>" class="filter_link <?=$activeClass?> <?=$arrowClass?>"><?=$sortTitle?></span>
                                        </div>
                                        <?
                                    }
                                }
                                ?>

                                <!--<div class="filter_item">
                                    <a href="#" class="filter_link">цене</a>
                                </div>
                                <div class="filter_item drop">
                                    <a href="#" class="filter_link arrow_down">названию</a>
                                    <div class="dropdown">
                                        <ul>
                                            <li><a href="#">Название 1</a></li>
                                            <li><a href="#">Название 2</a></li>
                                            <li><a href="#">Название 3</a></li>
                                            <li><a href="#">Название 4</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="filter_item drop">
                                    <a href="#" class="filter_link arrow_down">полярности</a>
                                    <div class="dropdown">
                                        <ul>
                                            <li><a href="#">Прямая</a></li>
                                            <li><a href="#">Обратная</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="filter_item">
                                    <a href="#" class="filter_link arrow_down">емкости</a>
                                </div>-->



                            </div>
                            <div class="f_right">
                                <?
                                if(isset($_GET["SALELEADER"]) && !empty($_GET["SALELEADER"]))
                                {
                                    $arrFilter["PROPERTY_SALELEADER_VALUE"] = $_GET["SALELEADER"];
                                    $urlHit = $APPLICATION->GetCurPageParam("", array("SALELEADER", "SPECIALOFFER"));
                                }else{
                                    $urlHit = $APPLICATION->GetCurPageParam("SALELEADER=да", array("SALELEADER", "SPECIALOFFER"));
                                }

                                if(isset($_GET["SPECIALOFFER"]) && !empty($_GET["SPECIALOFFER"]))
                                {
                                    $arrFilter["PROPERTY_SPECIALOFFER_VALUE"] = $_GET["SPECIALOFFER"];
                                    $urlSale = $APPLICATION->GetCurPageParam("", array("SPECIALOFFER", "SALELEADER"));
                                }else{
                                    $urlSale = $APPLICATION->GetCurPageParam("SPECIALOFFER=да", array("SPECIALOFFER", "SALELEADER"));
                                }
                                ?>
                                <div class="filter_item">
                                    <span rel="<?=$urlHit?>" class="filter_link hits sales <?if(!empty($_GET["SALELEADER"])):?>active<?endif;?>"></span>
                                </div>

                                <div class="filter_item">
                                    <span rel="<?=$urlSale?>" class="filter_link hits actions <?if(!empty($_GET["SPECIALOFFER"])):?>active<?endif;?>"></span>
                                </div>
                            </div>
                        </div>
                        <?php
                        die();
                    } ?>
                </div>
			</noindex>

			<?
        		if(!empty($arResult['VARIABLES']['SECTION_ID']) && intval($arResult['VARIABLES']['SECTION_CODE'])){
        			if($arResult['VARIABLES']['SECTION_ID'] == 413 || $arResult['VARIABLES']['SECTION_ID'] == 419){
	        			$arResult['VARIABLES']['SECTION_ID'] = 367;
        			}else{
        				$arResult['VARIABLES']['SECTION_ID'] = 453;
        			}
					$capasity = explode("-", $arResult['VARIABLES']['SECTION_CODE']);
					$arrFilter[">=PROPERTY_CAPACITY"] = $capasity[0];
					$arrFilter["<=PROPERTY_CAPACITY"] = $capasity[1];
        		}
				//CDev::pre($arResult);
				//CDev::pre($arParams);
				//CDev::pre($arrFilter);
				//CDev::pre($head_section);
			?>
			<?
			if(isset($_GET["sortFields"]))
			{
				$arParams["ELEMENT_SORT_FIELD"] = $_GET["sortFields"];
				$arParams["ELEMENT_SORT_ORDER"] = $_GET["sortOrder"];

					if(ToUpper($_GET["sortFields"]) != 'CATALOG_PRICE_1'){
						$arParams["ELEMENT_SORT_FIELD2"] = 'CATALOG_PRICE_1';
						$arParams["ELEMENT_SORT_ORDER2"] = 'asc';
					}

				$arParams["HIDE_NOT_AVAILABLE"] = 'Y';
			}
			?>
			<?$intSectionID = $APPLICATION->IncludeComponent(
				"pixelplus:catalog.section",
				"index",
				array(
					"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
					'HEAD_SECTION'=> $head_section,
					"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
					"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
					"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
					"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
					"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
					"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
					"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
					"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
					"INCLUDE_SUBSECTIONS" => "Y",
					"BASKET_URL" => $arParams["BASKET_URL"],
					"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
					"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
					"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
					"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
					"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
					"FILTER_NAME" => $arParams["FILTER_NAME"],
					"CACHE_TYPE" => $arParams["CACHE_TYPE"],
					"CACHE_TIME" => $arParams["CACHE_TIME"],
					"CACHE_FILTER" => $arParams["CACHE_FILTER"],
					"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
					"SET_TITLE" => $arParams["SET_TITLE"],
					"SET_STATUS_404" => $arParams["SET_STATUS_404"],
					"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
					"PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
					"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
					"PRICE_CODE" => $arParams["PRICE_CODE"],
					"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
					"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

					"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
					"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
					"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
					"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
					"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

					"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
					"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
					"PAGER_TITLE" => $arParams["PAGER_TITLE"],
					"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
					"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
					"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
					"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
					"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],

					"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
					"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
					"OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
					"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
					"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
					"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
					"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
					"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

					"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
					"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
					"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
					"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
					'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
					'CURRENCY_ID' => $arParams['CURRENCY_ID'],
					'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],

					'LABEL_PROP' => $arParams['LABEL_PROP'],
					'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
					'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],

					'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
					'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
					'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
					'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
					'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
					'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
					'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
					'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
					'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
					'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],

					'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
					"ADD_SECTIONS_CHAIN" => "N",
					'ADD_TO_BASKET_ACTION' => $basketAction,
					'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
					'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
"SHOW_ALL_WO_SECTION" => "Y",
					'SHOW_DEACTIVATED' => 'Y'
				),
				$component
			);?><?
			$GLOBALS['CATALOG_CURRENT_SECTION_ID'] = $intSectionID;
			unset($basketAction);
			//$APPLICATION->SetTitle($sectionName);
			?>
		<?endif?>
    </article>
</div>

<?
//сео теги

if(is_array($arResult["SEOTAG"])){
	$ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues(17, $arResult["SEOTAG"]["ID"]);

	$IPROPERTY = $ipropValues->getValues();

	$APPLICATION->SetPageProperty("title", $IPROPERTY["ELEMENT_META_TITLE"]);
	$APPLICATION->SetPageProperty("keywords", $IPROPERTY["ELEMENT_META_KEYWORDS"]);
	$APPLICATION->SetPageProperty("description", $IPROPERTY["ELEMENT_META_DESCRIPTION"]);
	$APPLICATION->SetTitle($IPROPERTY["ELEMENT_PAGE_TITLE"]);
}
?>
<?
/*if (ModuleManager::isModuleInstalled("sale"))
{
	$arRecomData = array();
	$recomCacheID = array('IBLOCK_ID' => $arParams['IBLOCK_ID']);
	$obCache = new CPHPCache();
	if ($obCache->InitCache(36000, serialize($recomCacheID), "/sale/bestsellers"))
	{
		$arRecomData = $obCache->GetVars();
	}
	elseif ($obCache->StartDataCache())
	{
		if (Loader::includeModule("catalog"))
		{
			$arSKU = CCatalogSKU::GetInfoByProductIBlock($arParams['IBLOCK_ID']);
			$arRecomData['OFFER_IBLOCK_ID'] = (!empty($arSKU) ? $arSKU['IBLOCK_ID'] : 0);
		}
		$obCache->EndDataCache($arRecomData);
	}
	if (!empty($arRecomData))
	{
		if (!isset($arParams['USE_SALE_BESTSELLERS']) || $arParams['USE_SALE_BESTSELLERS'] != 'N')
		{
			?><?$APPLICATION->IncludeComponent("bitrix:sale.bestsellers", "", array(
				"HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
				"PAGE_ELEMENT_COUNT" => "5",
				"SHOW_DISCOUNT_PERCENT" => $arParams['SHOW_DISCOUNT_PERCENT'],
				"PRODUCT_SUBSCRIPTION" => $arParams['PRODUCT_SUBSCRIPTION'],
				"SHOW_NAME" => "Y",
				"SHOW_IMAGE" => "Y",
				"MESS_BTN_BUY" => $arParams['MESS_BTN_BUY'],
				"MESS_BTN_DETAIL" => $arParams['MESS_BTN_DETAIL'],
				"MESS_NOT_AVAILABLE" => $arParams['MESS_NOT_AVAILABLE'],
				"MESS_BTN_SUBSCRIBE" => $arParams['MESS_BTN_SUBSCRIBE'],
				"LINE_ELEMENT_COUNT" => 5,
				"TEMPLATE_THEME" => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
				"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
				"CACHE_TYPE" => $arParams["CACHE_TYPE"],
				"CACHE_TIME" => $arParams["CACHE_TIME"],
				"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
				"BY" => array(
					0 => "AMOUNT",
				),
				"PERIOD" => array(
					0 => "15",
				),
				"FILTER" => array(
					0 => "CANCELED",
					1 => "ALLOW_DELIVERY",
					2 => "PAYED",
					3 => "DEDUCTED",
					4 => "N",
					5 => "P",
					6 => "F",
				),
				"FILTER_NAME" => $arParams["FILTER_NAME"],
				"ORDER_FILTER_NAME" => "arOrderFilter",
				"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
				"SHOW_OLD_PRICE" => $arParams['SHOW_OLD_PRICE'],
				"PRICE_CODE" => $arParams["PRICE_CODE"],
				"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
				"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
				"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
				"CURRENCY_ID" => $arParams["CURRENCY_ID"],
				"BASKET_URL" => $arParams["BASKET_URL"],
				"ACTION_VARIABLE" => (!empty($arParams["ACTION_VARIABLE"]) ? $arParams["ACTION_VARIABLE"] : "action")."_slb",
				"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
				"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
				"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
				"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
				"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
				"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
				"SHOW_PRODUCTS_".$arParams["IBLOCK_ID"] => "Y",
				"OFFER_TREE_PROPS_".$arRecomData['OFFER_IBLOCK_ID'] => $arParams["OFFER_TREE_PROPS"],
				"ADDITIONAL_PICT_PROP_".$arParams['IBLOCK_ID'] => $arParams['ADD_PICT_PROP'],
				"ADDITIONAL_PICT_PROP_".$arRecomData['OFFER_IBLOCK_ID'] => $arParams['OFFER_ADD_PICT_PROP']
			),
			$component,
			array("HIDE_ICONS" => "Y")
			);
		}
		if (!isset($arParams['USE_BIG_DATA']) || $arParams['USE_BIG_DATA'] != 'N')
		{
			?><?$APPLICATION->IncludeComponent("bitrix:catalog.bigdata.products", "", array(
				"LINE_ELEMENT_COUNT" => 5,
				"TEMPLATE_THEME" => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
				"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
				"BASKET_URL" => $arParams["BASKET_URL"],
				"ACTION_VARIABLE" => (!empty($arParams["ACTION_VARIABLE"]) ? $arParams["ACTION_VARIABLE"] : "action")."_cbdp",
				"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
				"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
				"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
				"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
				"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
				"SHOW_OLD_PRICE" => $arParams['SHOW_OLD_PRICE'],
				"SHOW_DISCOUNT_PERCENT" => $arParams['SHOW_DISCOUNT_PERCENT'],
				"PRICE_CODE" => $arParams["PRICE_CODE"],
				"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
				"PRODUCT_SUBSCRIPTION" => $arParams['PRODUCT_SUBSCRIPTION'],
				"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
				"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
				"SHOW_NAME" => "Y",
				"SHOW_IMAGE" => "Y",
				"MESS_BTN_BUY" => $arParams['MESS_BTN_BUY'],
				"MESS_BTN_DETAIL" => $arParams['MESS_BTN_DETAIL'],
				"MESS_BTN_SUBSCRIBE" => $arParams['MESS_BTN_SUBSCRIBE'],
				"MESS_NOT_AVAILABLE" => $arParams['MESS_NOT_AVAILABLE'],
				"PAGE_ELEMENT_COUNT" => 5,
				"SHOW_FROM_SECTION" => "Y",
				"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"DEPTH" => "2",
				"CACHE_TYPE" => $arParams["CACHE_TYPE"],
				"CACHE_TIME" => $arParams["CACHE_TIME"],
				"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
				"SHOW_PRODUCTS_".$arParams["IBLOCK_ID"] => "Y",
				"ADDITIONAL_PICT_PROP_".$arParams["IBLOCK_ID"] => $arParams['ADD_PICT_PROP'],
				"LABEL_PROP_".$arParams["IBLOCK_ID"] => "-",
				"HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
				"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
				"CURRENCY_ID" => $arParams["CURRENCY_ID"],
				"SECTION_ID" => $intSectionID,
				"SECTION_CODE" => "",
				"SECTION_ELEMENT_ID" => "",
				"SECTION_ELEMENT_CODE" => "",
				"PROPERTY_CODE_".$arParams["IBLOCK_ID"] => $arParams["LIST_PROPERTY_CODE"],
				"CART_PROPERTIES_".$arParams["IBLOCK_ID"] => $arParams["PRODUCT_PROPERTIES"],
				"RCM_TYPE" => (isset($arParams['BIG_DATA_RCM_TYPE']) ? $arParams['BIG_DATA_RCM_TYPE'] : ''),
				"OFFER_TREE_PROPS_".$arRecomData['OFFER_IBLOCK_ID'] => $arParams["OFFER_TREE_PROPS"],
				"ADDITIONAL_PICT_PROP_".$arRecomData['OFFER_IBLOCK_ID'] => $arParams['OFFER_ADD_PICT_PROP']
			),
			$component,
			array("HIDE_ICONS" => "Y")
			);
		}
	}
}*/
?>
