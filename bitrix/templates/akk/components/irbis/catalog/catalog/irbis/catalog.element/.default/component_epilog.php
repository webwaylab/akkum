<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/** @var $arResult */

/**
 * Так как продвижением сайта занимается конторка под названием "Demis Group", то в плане meta-тегов всё печально...
 * Поэтому нам, простым разработчикам, приходится делать вот такие костыли.
 *
 * Удачи, мой юный падаван
 */

// Передаём meta-теги из настроек Инфоблока в грёбаный /d-seo.php
global $aSEOData;
$aSEOData['title'] = $arResult['IPROPERTY_VALUES']['ELEMENT_META_TITLE'];
$aSEOData['descr'] = $arResult['IPROPERTY_VALUES']['ELEMENT_META_DESCRIPTION'];
$aSEOData['keywr'] = $arResult['IPROPERTY_VALUES']['ELEMENT_META_KEYWORDS'];

$pageInstance = Bitrix\Main\Page\Asset::getInstance();
$pageInstance->addCss(SITE_TEMPLATE_PATH . '/js/jquery.bxslider/jquery.bxslider.css');
$pageInstance->addJs(SITE_TEMPLATE_PATH . '/js/jquery.bxslider/jquery.bxslider.min.js');
?>
<div class="product_item__card">
    <article class="product_item__tabs">
        <div class="product_item__tabs_container">
            <div class="tabs-switch-wrap">
                <ul class="tabs-switch" data-tabs-group="PRODUCT_TABS">
                    <li class="tab-label" data-tabs-id="description"><span>Описание</span></li>
                    <li class="tab-label" data-tabs-id="reviews"><span>Отзывы</span></li>
                    <li class="tab-label active" data-tabs-id="availability"><span>Наличие</span></li>
                </ul>
            </div>
            <div class="tabs-wrap" data-tabs-content-group="PRODUCT_TABS">
                <div class="tab" data-tabs-content-id="description">
                    <?= $arResult['DETAIL_TEXT']; ?>
                </div>
                <div class="tab" data-tabs-content-id="reviews">
                    <?
                    global $comFilter;
                    $comFilter["=PROPERTY_PRODUCT_ID"] = $arResult['ID'];
                    ?>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "comments",
                        Array(
                            "IBLOCK_TYPE" => "services",
                            "IBLOCK_ID" => "6",
                            "NEWS_COUNT" => "20",
                            "SORT_BY1" => "ACTIVE_FROM",
                            "SORT_ORDER1" => "DESC",
                            "SORT_BY2" => "SORT",
                            "SORT_ORDER2" => "ASC",
                            "FILTER_NAME" => "comFilter",
                            "FIELD_CODE" => array("DATE_CREATE"),
                            "PROPERTY_CODE" => array("USER_ID", "PLUS", "MINUS", "PRODUCT_ID"),
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N",
                            "CACHE_TYPE" => "N",
                            "CACHE_TIME" => "36000000",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "Y",
                            "PREVIEW_TRUNCATE_LEN" => "",
                            "ACTIVE_DATE_FORMAT" => "j F Y",
                            "SET_TITLE" => "N",
                            "SET_BROWSER_TITLE" => "N",
                            "SET_META_KEYWORDS" => "N",
                            "SET_META_DESCRIPTION" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => "",
                            "INCLUDE_SUBSECTIONS" => "Y",
                            "DISPLAY_DATE" => "Y",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "Y",
                            "PAGER_TEMPLATE" => ".default",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "PAGER_TITLE" => "Новости",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "PRODUCT_ID" => $arResult['ID']
                        )
                    );?>
                </div>
                <div class="tab active" data-tabs-content-id="availability">
                    <div class="product_item__storage">
                        <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => "/include/product-detail-storage.php",
                            "EDIT_TEMPLATE" => ""
                            ),
                            false
                        );?>
                    </div>
                </div>
            </div>
        </div>
    </article>
</div>
