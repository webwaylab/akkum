<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $templateData */
/** @var @global CMain $APPLICATION */
use Bitrix\Main\Loader;
global $APPLICATION;
if (isset($templateData['TEMPLATE_THEME']))
{
	$APPLICATION->SetAdditionalCSS($templateData['TEMPLATE_THEME']);
}
if (isset($templateData['TEMPLATE_LIBRARY']) && !empty($templateData['TEMPLATE_LIBRARY']))
{
	$loadCurrency = false;
	if (!empty($templateData['CURRENCIES']))
		$loadCurrency = Loader::includeModule('currency');
	CJSCore::Init($templateData['TEMPLATE_LIBRARY']);
	if ($loadCurrency)
	{
	?>
	<script type="text/javascript">
		BX.Currency.setCurrencies(<? echo $templateData['CURRENCIES']; ?>);
	</script>
<?
	}
}

/** Магия meta-тегов, спасибо Demis Group */
global $aSEOData;

// Должно работать только на страницах "/catalog/battery/auto-akb/*БРЕНД*/"
if (preg_match('/^\/catalog\/battery\/auto-akb\/(.*)\/$/', $APPLICATION->GetCurDir())) {
    $brandName = $arResult['IPROPERTY_VALUES']['SECTION_PAGE_TITLE'];
    if (!empty($brandName)) {
        $aSEOData['title'] = 'Аккумуляторы ' . $brandName . ', купить автомобильные АКБ ' . $brandName . ' в Москве';
        $aSEOData['h1'] = $brandName . ' — автомобильные аккумуляторы';
        $aSEOData['descr'] = 'В нашем магазине вы можете купить аккумулятор бренда «' . $brandName . '» для автомобиля по выгодной цене. Документальное подтверждение качества, гарантия, бесплатное консультирование, доставка по Москве.';
    }
}

// Перебиваем title на постраничке (Заголовок раздела | Страница N)
// Мы не можем использовать 'title' или 'h1', так как они могут перезаписаться в d-seo.php,
// поэтому пришлось добавить 'title_override' (дальнейшую судьбу этой переменной смотри в d-seo.php)
if (isset($_REQUEST['PAGEN_1']) && $_REQUEST['PAGEN_1'] > 1) {
    $aSEOData['title_override'] = $arResult['IPROPERTY_VALUES']['SECTION_PAGE_TITLE'];
}

/*
// Атата, но без этого в некоторых разделах H1 становится "Page title" (дословно)
if (!isset($aSEOData['h1'])) {
    $aSEOData['h1'] = $arResult['IPROPERTY_VALUES']['SECTION_PAGE_TITLE'];
}
*/
if($_REQUEST['PAGEN_1'] > $arResult['TOTAL_PAGES_COUNT'])
{
	// Изменить тут протокол при переносе
	$APPLICATION->AddHeadString('<link rel="canonical" href="http://'. SITE_SERVER_NAME . $APPLICATION->GetCurPage() .'"/>');
}
?>