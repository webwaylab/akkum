<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @var string $strElementEdit */
/** @var string $strElementDelete */
/** @var array $arElementDeleteParams */
/** @var array $arSkuTemplate */
/** @var array $templateData */
global $APPLICATION;
?><div class="products clearfix product_page_similar <? echo $templateData['TEMPLATE_CLASS']; ?>">


                
<div class="similar-products">
    <ul class="slides">
		<?
		foreach ($arResult['ITEMS'] as $key => $arItem)
		{
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
			$strMainID = $this->GetEditAreaId($arItem['ID']);

			$arItemIDs = array(
				'ID' => $strMainID,
				'PICT' => $strMainID.'_pict',
				'SECOND_PICT' => $strMainID.'_secondpict',
				'MAIN_PROPS' => $strMainID.'_main_props',

				'QUANTITY' => $strMainID.'_quantity',
				'QUANTITY_DOWN' => $strMainID.'_quant_down',
				'QUANTITY_UP' => $strMainID.'_quant_up',
				'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
				'BUY_LINK' => $strMainID.'_buy_link',
				'BASKET_ACTIONS' => $strMainID.'_basket_actions',
				'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
				'SUBSCRIBE_LINK' => $strMainID.'_subscribe',
				'COMPARE_LINK' => $strMainID.'_compare_link',

				'PRICE' => $strMainID.'_price',
				'DSC_PERC' => $strMainID.'_dsc_perc',
				'SECOND_DSC_PERC' => $strMainID.'_second_dsc_perc',

				'PROP_DIV' => $strMainID.'_sku_tree',
				'PROP' => $strMainID.'_prop_',
				'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
				'BASKET_PROP_DIV' => $strMainID.'_basket_prop'
			);

			$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
			$productTitle = (
				isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])&& $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
				? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
				: $arItem['NAME']
			);
			$imgTitle = (
				isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
				? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
				: $arItem['NAME']
			);
			$changePrice = $arItem["PROPERTIES"]["PRICE_CHANGE"]["VALUE"];
			?>

			<li>
				<figure class="product_item maxheight<?if($arItem["PROPERTIES"]["SALELEADER"]["VALUE"]=="да" || !empty($changePrice)):?> sale<?endif;?>" id="<? echo $strMainID; ?>">
					<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
						<div class="product_label-box">
							<?if($arItem["PROPERTIES"]["SALELEADER"]["VALUE"]=="да"):?>
								<span class="product_label product_label--hit"
									title="<?=$arItem["PROPERTIES"]["SALELEADER"]["NAME"];?>"></span>
							<?endif;?>
							<?if($arItem["PROPERTIES"]["FREESHIPPING"]["VALUE"]=="да"):?>
								<span class="product_label product_label--delivery"
									title="<?=$arItem["PROPERTIES"]["FREESHIPPING"]["NAME"];?>"></span>
							<?endif;?>
							<?if($arItem["PROPERTIES"]["SPECIALOFFER"]["VALUE"]=="да"):?>
								<span class="product_label product_label--present"
									title="<?=$arItem["PROPERTIES"]["SPECIALOFFER"]["NAME"];?>"></span>
							<?endif;?>
						</div>
					
						<div class="product_item_img">
							<img src="<? echo $arItem['PREVIEW_PICTURE']['SRC']; ?>" alt="" id="<? echo $arItemIDs['SECOND_PICT']; ?>">
						</div>
					
						<figcaption class="product_item_description">
							<div class="product_item_content-box">
								<div class="product_item_content">
									 <?=$arItem["NAME"]?>
								</div>
							</div>
						
							<div class="product_item_action clearfix">
								<div class="product_item_price heavy  product_item_col product_item_col--left">
									<?
									
									if(!empty($changePrice))
									{
										?>
										<?=number_format($changePrice, 0, "", " ")?> <span class="ruble">руб.</span>
										<small>при обмене</small>
										<div class="old_price">
										<?
									}
									
									if (!empty($arItem['MIN_PRICE']))
									{
										if ('N' == $arParams['PRODUCT_DISPLAY_MODE'] && isset($arItem['OFFERS']) && !empty($arItem['OFFERS']))
										{
											echo GetMessage(
												'CT_BCS_TPL_MESS_PRICE_SIMPLE_MODE',
												array(
													'#PRICE#' => $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'],
													'#MEASURE#' => GetMessage(
														'CT_BCS_TPL_MESS_MEASURE_SIMPLE_MODE',
														array(
															'#VALUE#' => $arItem['MIN_PRICE']['CATALOG_MEASURE_RATIO'],
															'#UNIT#' => $arItem['MIN_PRICE']['CATALOG_MEASURE_NAME']
														)
													)
												)
											);
										}
										else
										{
											echo number_format($arItem['MIN_PRICE']['DISCOUNT_VALUE_VAT'], 0, "", " ");?> <span class="ruble">руб.</span><?
										}
									}
									
									if(!empty($changePrice))
									{
										?>
										</div>
										<?
									}
									?>
								</div>
								<div class="product_item_col product_item_col--right">
									<?
									if ($arItem['CAN_BUY'])
									{
										if ('Y' == $arParams['USE_PRODUCT_QUANTITY'])
										{
										?>
											<div class="bx_catalog_item_controls_blockone" style="display: none;"><div style="display: inline-block;position: relative;">
												<a id="<? echo $arItemIDs['QUANTITY_DOWN']; ?>" href="javascript:void(0)" class="bx_bt_button_type_2 bx_small" rel="nofollow">-</a>
												<input type="text" class="bx_col_input" id="<? echo $arItemIDs['QUANTITY']; ?>" name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>" value="<? echo $arItem['CATALOG_MEASURE_RATIO']; ?>">
												<a id="<? echo $arItemIDs['QUANTITY_UP']; ?>" href="javascript:void(0)" class="bx_bt_button_type_2 bx_small" rel="nofollow">+</a>
												<span id="<? echo $arItemIDs['QUANTITY_MEASURE']; ?>"><? echo $arItem['CATALOG_MEASURE_NAME']; ?></span>
											</div></div>
										<?
										}
										
										?>
										
										<a id="<? echo $arItemIDs['BUY_LINK']; ?>" class="product_btn product_btn--green action-block" href="javascript:void(0)" rel="nofollow">Купить</a>
										
										<div id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>" class="bx_catalog_item_controls_blocktwo">
											<a id="<? echo $arItemIDs['BUY_LINK']; ?>" class="bx_bt_button bx_medium" href="javascript:void(0)" rel="nofollow"></a>
										</div>
										
										<?
									}
									?>
								</div>
							</div>
							
							

										<?
							//определение родителя
							$root_item = "";
							if(strstr($arItem['DETAIL_PAGE_URL'], '/masla/'))
								$root_item = "masla";
							elseif(strstr($arItem['DETAIL_PAGE_URL'], '/battery/'))
								$root_item = "battery";
							elseif(strstr($arItem['DETAIL_PAGE_URL'], '/accessories/'))
								$root_item = "accessories";

							$nav = CIBlockSection::GetNavChain(false,$arItem['~IBLOCK_SECTION_ID']);
							$arSectionPath = $nav->GetNext();
							if(true || $arSectionPath['ID'] != 19){

							?>
							<div class="product_item_perfomance">
									<?if($root_item == "battery"):?>
										<?if($arItem['PROPERTIES']["CAPACITY"]['VALUE']):?>
											<div>Ёмкость: <?=$arItem['PROPERTIES']["CAPACITY"]['VALUE']?> А/ч</div>
										<?endif;?>
								  
										<?if($arItem['PROPERTIES']["START_TOK"]['VALUE']):?>
											<div>Пусковой ток: <?=$arItem['PROPERTIES']["START_TOK"]['VALUE']?> А</div>
										<?endif;?>
										
										<?if($arItem['PROPERTIES']["POLARITY"]['VALUE']):?>
											<div><?=$arItem['PROPERTIES']["POLARITY"]['NAME']?>: <?=$arItem['PROPERTIES']["POLARITY"]['VALUE']?></div>
										<?endif;?>
										
										<?if (!empty($arItem['PROPERTIES']["LENGTH"]['VALUE']) &&
												!empty($arItem['PROPERTIES']["WIDTH"]['VALUE']) &&
												!empty($arItem['PROPERTIES']["HEIGHT"]['VALUE'])):?>
											<div>Габариты: <?=$arItem['PROPERTIES']["LENGTH"]['VALUE']?>×<?=$arItem['PROPERTIES']["WIDTH"]['VALUE']?>×<?=$arItem['PROPERTIES']["HEIGHT"]['VALUE']?></div>
										<?endif;?>						
									<?endif;?>
									
									<?if($root_item == "accessories"):?>
										<?if($arItem['PROPERTIES']["ARTNUMBER"]['VALUE']):?>
											<div><?=$arItem['PROPERTIES']["ARTNUMBER"]['NAME']?>: <?=$arItem['PROPERTIES']["ARTNUMBER"]['VALUE']?></div>
										<?endif;?>
										
										<?if($arItem['PROPERTIES']["MANUFACTURE_2"]['VALUE']):?>
											<div><?=$arItem['PROPERTIES']["MANUFACTURE_2"]['NAME']?>: <?=$arItem['PROPERTIES']["MANUFACTURE_2"]['VALUE']?></div>
										<?endif;?>
									<?endif;?>
									
									<?if($root_item == "masla"):?>
										<?if($arItem['PROPERTIES']["ARTNUMBER"]['VALUE']):?>
											<div><?=$arItem['PROPERTIES']["ARTNUMBER"]['NAME']?>: <?=$arItem['PROPERTIES']["ARTNUMBER"]['VALUE']?></div>
										<?endif;?>
										
										<?if($arItem['PROPERTIES']["OBEM_UPAKOVKI_L_MASLO"]['VALUE']):?>
											<div>Объём: <?=$arItem['PROPERTIES']["OBEM_UPAKOVKI_L_MASLO"]['VALUE']?>л.</div>
										<?endif;?>
										
										<?if($arItem['PROPERTIES']["KLASS_VYAZKOSTI_SAE_MASLO"]['VALUE']):?>
											<div>Вязкость: <?=$arItem['PROPERTIES']["KLASS_VYAZKOSTI_SAE_MASLO"]['VALUE']?></div>
										<?endif;?>
										
										<?if($arItem['PROPERTIES']["TIP_MASLO"]['VALUE']):?>
											<div>Тип: <?=$arItem['PROPERTIES']["TIP_MASLO"]['VALUE']?></div>
										<?endif;?>
										
										<?if($arItem['PROPERTIES']["PROIZVODITEL_MASLO"]['VALUE']):?>
											<div>Производитель: <?=$arItem['PROPERTIES']["PROIZVODITEL_MASLO"]['VALUE']?></div>
										<?endif;?>
									<?endif;?>
							</div>
							
							<div class="product_btn-bottom-wrap">
								<a href="#" class="product_btn action-block toggle_btn" data-target=".phone_order_form_<?=$arItem["ID"]?>" data-target-class="active">Купить в 1 клик</a>

								<form class="tooltip_box product_tooltip_box phone_order_form_<?=$arItem["ID"]?> one-click">
									<input type="hidden" name="PRODUCT_ID" value="<?=$arItem["ID"]?>"/>
									<span class="close_tooltip"></span>
									<div class="error"></div>
									<div class="tooltip_box_inner">
										<h4 class="bold fs_14 mb5">Введите номер телефона:</h4>
											<div class="mb-5">
												<input type="text" name="PERSONAL_PHONE" class="txt_field al_center phone_mask" placeholder="+7(___) ___-__-__">
											</div>
										<button class="btn grn_skin d_block">Купить</button>
									</div>
								</form>

								<div class="tooltip_box product_tooltip_box order_success_message">
									<span class="close_tooltip"></span>
									<div class="tooltip_box_inner">
										<p><img src="<?=SITE_TEMPLATE_PATH?>/images/done_form_icon_green.png" height="33" width="36" alt=""></p>
										<h4 class="bold">Ваш заказ принят.</h4>
										Ожидайте звонка. Спасибо!
									</div>
								</div>
							 </div>
							<?
							}
							?>
						
						</figcaption>
					
					</a>
				</figure>


			<?
			$arJSParams = array(
				'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
				'SHOW_QUANTITY' => ($arParams['USE_PRODUCT_QUANTITY'] == 'Y'),
				'SHOW_ADD_BASKET_BTN' => false,
				'SHOW_BUY_BTN' => true,
				'SHOW_ABSENT' => true,
				'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
				'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
				'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
				'PRODUCT' => array(
					'ID' => $arItem['ID'],
					'NAME' => $productTitle,
					'PICT' => ('Y' == $arItem['SECOND_PICT'] ? $arItem['PREVIEW_PICTURE_SECOND'] : $arItem['PREVIEW_PICTURE']),
					'CAN_BUY' => $arItem["CAN_BUY"],
					'SUBSCRIPTION' => ('Y' == $arItem['CATALOG_SUBSCRIPTION']),
					'CHECK_QUANTITY' => $arItem['CHECK_QUANTITY'],
					'MAX_QUANTITY' => $arItem['CATALOG_QUANTITY'],
					'STEP_QUANTITY' => $arItem['CATALOG_MEASURE_RATIO'],
					'QUANTITY_FLOAT' => is_double($arItem['CATALOG_MEASURE_RATIO']),
					'SUBSCRIBE_URL' => $arItem['~SUBSCRIBE_URL'],
					'BASIS_PRICE' => $arItem['MIN_BASIS_PRICE']
				),
				'VISUAL' => array(
					'ID' => $arItemIDs['ID'],
					'PICT_ID' => ('Y' == $arItem['SECOND_PICT'] ? $arItemIDs['SECOND_PICT'] : $arItemIDs['PICT']),
					'QUANTITY_ID' => $arItemIDs['QUANTITY'],
					'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
					'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
					'PRICE_ID' => $arItemIDs['PRICE'],
					'BUY_ID' => $arItemIDs['BUY_LINK'],
					'BASKET_PROP_DIV' => $arItemIDs['BASKET_PROP_DIV'],
					'BASKET_ACTIONS_ID' => $arItemIDs['BASKET_ACTIONS'],
					'NOT_AVAILABLE_MESS' => $arItemIDs['NOT_AVAILABLE_MESS'],
					'COMPARE_LINK_ID' => $arItemIDs['COMPARE_LINK']
				),
				'BASKET' => array(
					'ADD_PROPS' => ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET']),
					'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
					'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
					'EMPTY_PROPS' => $emptyProductProperties,
					'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
					'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
				),
				'LAST_ELEMENT' => $arItem['LAST_ELEMENT']
			);
			if ($arParams['DISPLAY_COMPARE'])
			{
				$arJSParams['COMPARE'] = array(
					'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
					'COMPARE_PATH' => $arParams['COMPARE_PATH']
				);
			}
			?>
			
			<script type="text/javascript">
			var <? echo $strObName; ?> = new JCCatalogTopSection(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
			</script><?
			?>
			
			<div class="<? echo ($arItem['SECOND_PICT'] ? 'bx_catalog_item double' : 'bx_catalog_item'); ?>">
				<div class="bx_catalog_item_container" id="<? echo $strMainID; ?>">
					<div class="bx_catalog_item_price"><div id="<? echo $arItemIDs['PRICE']; ?>" class="bx_price"></div></div>
					<?
					$showSubscribeBtn = false;
					$compareBtnMessage = ($arParams['MESS_BTN_COMPARE'] != '' ? $arParams['MESS_BTN_COMPARE'] : GetMessage('CT_BCT_TPL_MESS_BTN_COMPARE'));
					if (!isset($arItem['OFFERS']) || empty($arItem['OFFERS']))
					{        
						$emptyProductProperties = empty($arItem['PRODUCT_PROPERTIES']);
						if ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET'] && !$emptyProductProperties)
						{
							?>
							<div id="<? echo $arItemIDs['BASKET_PROP_DIV']; ?>" style="display: none;">
								<?
								if (!empty($arItem['PRODUCT_PROPERTIES_FILL']))
								{
									foreach ($arItem['PRODUCT_PROPERTIES_FILL'] as $propID => $propInfo)
									{
										?>
										<input type="hidden" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo htmlspecialcharsbx($propInfo['ID']); ?>"><?
										if (isset($arItem['PRODUCT_PROPERTIES'][$propID]))
											unset($arItem['PRODUCT_PROPERTIES'][$propID]);
									}
								}
								$emptyProductProperties = empty($arItem['PRODUCT_PROPERTIES']);
								if (!$emptyProductProperties)
								{
									?>
									<table>
										<?
										foreach ($arItem['PRODUCT_PROPERTIES'] as $propID => $propInfo)
										{
											?>
											<tr><td><? echo $arItem['PROPERTIES'][$propID]['NAME']; ?></td>
												<td>
													<?
													if(
														'L' == $arItem['PROPERTIES'][$propID]['PROPERTY_TYPE']
														&& 'C' == $arItem['PROPERTIES'][$propID]['LIST_TYPE']
													)
													{
														foreach($propInfo['VALUES'] as $valueID => $value)
														{
															?><label><input type="radio" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo $valueID; ?>" <? echo ($valueID == $propInfo['SELECTED'] ? '"checked"' : ''); ?>><? echo $value; ?></label><br><?
														}
													}
													else
													{
														?><select name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]"><?
														foreach($propInfo['VALUES'] as $valueID => $value)
														{
															?><option value="<? echo $valueID; ?>" <? echo ($valueID == $propInfo['SELECTED'] ? '"selected"' : ''); ?>><? echo $value; ?></option><?
														}
														?></select><?
													}
													?>
												</td></tr>
										<?
										}
										?>
									</table>
								<?
								}
								?>
							</div>
						   <?
						}
						
					}
					?>
				</div>
			</div>
			</li>
			<?
		}
		?>
		</div>
    </ul>
</div>