<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$cartStyle = 'bx_cart_block';
$cartId = $cartStyle.$component->getNextNumber();
$arParams['cartId'] = $cartId;

if ($arParams['SHOW_PRODUCTS'] == 'Y')
	$cartStyle .= ' bx_cart_sidebar';

if ($arParams['POSITION_FIXED'] == 'Y')
{
	$cartStyle .= " bx_cart_fixed {$arParams['POSITION_HORIZONTAL']} {$arParams['POSITION_VERTICAL']}";
	if ($arParams['SHOW_PRODUCTS'] == 'Y')
		$cartStyle .= ' close';
}
?>

<script>
	var <?=$cartId?> = new BitrixSmallCart;
</script>

<a class="info_link" id="<?=$cartId?>" data-cart-value="<?=$arResult['NUM_PRODUCTS']?>" data-cart-seo-link="<?= str_replace('/', '_', $arParams['PATH_TO_BASKET']); ?>">
	<?
	$frame = $this->createFrame('bx_cart_block', false)->begin();
	
		require(realpath(dirname(__FILE__)).'/ajax_template.php');
	
	$frame->beginStub();
	
		require(realpath(dirname(__FILE__)).'/top_template.php');
	
	$frame->end();
	?>
</a>

<script>
	<?=$cartId?>.siteId       = '<?=SITE_ID?>';
	<?=$cartId?>.cartId       = '<?=$cartId?>';
	<?=$cartId?>.ajaxPath     = '<?=$componentPath?>/ajax.php';
	<?=$cartId?>.templateName = '<?=$templateName?>';
	<?=$cartId?>.arParams     =  <?=CUtil::PhpToJSObject ($arParams)?>;
	<?=$cartId?>.closeMessage = '<?=GetMessage('TSB1_COLLAPSE')?>';
	<?=$cartId?>.openMessage  = '<?=GetMessage('TSB1_EXPAND')?>';
	<?=$cartId?>.activate();

    $(function () {
        $('#<?= $cartId; ?>').prop('href', $('#<?= $cartId; ?>').data('cart-seo-link').replace(/_/g, '/'));
    });
</script>
