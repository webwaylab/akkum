$(function(){
    $("body").on("submit", "#comment-form", function(e){
    	e.preventDefault();
    	params=$(this).serialize()+"&action=addComment";
        var form = $(this);
        form.find(".error").hide().html("");
        $.ajax({
            type: 'POST',
            url: window.location.pathname+'?action=addComment',
            data: params,
            dataType: 'json',
            success: function(result){
                if(result.status)
                {
                    form.find("textarea").val("");
                    form.find(".error").show().html(result.message);
                    //document.location.reload();
                }   
                else
                    form.find(".error").show().html(result.message);
            }
        });
    });
});