<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
global $USER;

foreach($arResult["ITEMS"] as $arItem)
{
    ?>
    <div class="testimonial_item wrapper">
        <figure class="testimonial_item_img_box f_left">
            <div class="testimonial_item_img mb14">
                <?
                if($arItem["USER"]["PERSONAL_PHOTO"])
                {
                    ?><img src="<?=CFile::GetPath($arItem["USER"]["PERSONAL_PHOTO"])?>" height="104" width="104" alt=""><?
                }else{
                    echo "нет фото";
                }
                ?>
            </div>
            <figcaption class="bold al_center">
                <?if(!$USER->IsAuthorized()):?>
                    <?=$arItem["PROPERTIES"]["NAME"]["VALUE"]?>
                <?else:?>
                    <?=$arItem["USER"]["NAME"]?> <br> <?=$arItem["USER"]["LAST_NAME"]?>
                <?endif;?>
            </figcaption>
        </figure>
        <div class="wrapper">
            <h2 class="fs_18 bold mb12">Отзыв о модели: <?=$arResult["PRODUCT"]["NAME"]?> <time class="gray_font fs_13">| <?=$arItem["DISPLAY_ACTIVE_FROM"]?></time></h2>
            <div class="fs_13 mb20">
                <span class="bold mb10">Достоинства</span>
                <p><?=strip_tags($arItem["PROPERTIES"]["PLUS"]["VALUE"]["TEXT"])?></p>
            </div>
            <div class="fs_13">
                <span class="bold mb10">Недостатки</span>
                <p><?=strip_tags($arItem["PROPERTIES"]["MINUS"]["VALUE"]["TEXT"])?></p>
            </div>
        </div>
    </div>
    <?
}
?>

<form class="create_testimonial" autocomplete="off" action="<?=$GLOBALS["APPLICATION"]->GetCurDir()?>" METHOD="POST" id="comment-form">
    <?=bitrix_sessid_post()?>

    <figure class="testimonial_item_img_box f_left">
        <div class="testimonial_item_img mb14">
            <?
            if($arResult["CURRENT_USER"]["PERSONAL_PHOTO"])
            {
                ?><img src="<?=CFile::GetPath($arResult["CURRENT_USER"]["PERSONAL_PHOTO"])?>" height="104" width="104" alt=""><?
            }else{
                echo "нет фото";
            }
            ?>
        </div>
        <figcaption class="bold al_center">
            <?=$arResult["CURRENT_USER"]["NAME"]?> <br> <?=$arResult["CURRENT_USER"]["LAST_NAME"]?>
        </figcaption>
    </figure>
    <div class="wrapper">
        <div class="error"></div>
        <?if(!$USER->IsAuthorized()):?>
        <div class="mb_10">
            <input name="NAME" class="txt_field mb10" placeholder="Имя"></textarea>
        </div>
        <?endif;?>
        <div class="mb_10">
            <textarea name="PLUS" class="txt_area mb10" placeholder="Написать отзыв (достоинства)"></textarea>
        </div>
        <div class="mb_10">
            <textarea name="MINUS" class="txt_area mb10" placeholder="Написать отзыв (недостатки)"></textarea>
        </div>
        <button class="btn2 grn_skin">Отправить отзыв</button>
    </div>

</form>
