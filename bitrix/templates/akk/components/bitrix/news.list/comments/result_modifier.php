<?
CModule::IncludeModule("iblock");
global $USER;

$res = CIBlockElement::GetByID($arParams["PRODUCT_ID"]);
$arResult["PRODUCT"] = $res->GetNext();

foreach($arResult["ITEMS"] as &$arItem)
{
    $res = CUser::GetByID($arItem["PROPERTIES"]["USER_ID"]["VALUE"]);
    $arItem["USER"] = $res->GetNext();
}

if($USER->IsAuthorized())
{
    $res = CUser::GetByID($USER->GetID());
    $arResult["CURRENT_USER"] = $res->GetNext();
}
$arResult["IS_AUTH"] = $USER->IsAuthorized();

//AJAX
if($_REQUEST["action"]=="addComment") 
{
    $APPLICATION->RestartBuffer();
    if (!defined('PUBLIC_AJAX_MODE')) 
    {
        define('PUBLIC_AJAX_MODE', true);
    }
    
    CModule::IncludeModule("iblock");
    global $USER;
    
    $array = array();
    $msg = "";

    $res = CIBlockElement::GetByID($arParams["PRODUCT_ID"]);
    $arProduct = $res->GetNext();

    if(empty($_POST["PLUS"]) && empty($_POST["MINUS"]))
    {
        $msg = "Введите отзыв!";
    }
    
    if(!$USER->IsAuthorized() && empty($_POST["NAME"]))
    {
        $msg.= "<br />Введите свое имя!";
    }

    if(empty($msg))
    {
        $el = new CIBlockElement;

        $PROP = array();
        $PROP['USER_ID'] = $USER->GetID();
        $PROP["PRODUCT_ID"] = $arProduct["ID"];
        $PROP["PLUS"][0] = Array("VALUE" => Array ("TEXT" => strip_tags($_POST["PLUS"]), "TYPE" => "text"));
        $PROP["MINUS"][0] = Array("VALUE" => Array ("TEXT" => strip_tags($_POST["MINUS"]), "TYPE" => "text"));
        
        if(!$USER->IsAuthorized())
        {
            $PROP["NAME"] = htmlspecialcharsbx($_POST["NAME"]);
        }
        
        $arLoadProductArray = Array(
            "MODIFIED_BY"    => $USER->GetID(),
            "IBLOCK_SECTION_ID" => false,
            "IBLOCK_ID"      => 6,
            "PROPERTY_VALUES"=> $PROP,
            "NAME"           => $arProduct["NAME"],
            "ACTIVE"         => "N",
            "DATE_ACTIVE_FROM" => date("d.m.Y")
        );
        
        if($el->Add($arLoadProductArray))
        {
            $array["status"] = true;
            $array["message"] = "<font class='green'>После модерации ваш отзыв появится на сайте!</font>";
        }
    }else{
        $array["status"] = false;
        $array["message"] = $msg;
    }
    
    echo json_encode($array);
    
    require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');
    die();
}
?>

<script src="<?=SITE_TEMPLATE_PATH?>/ajaxupload.js"></script>
