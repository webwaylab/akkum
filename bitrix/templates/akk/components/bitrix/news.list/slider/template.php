<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (count($arResult["ITEMS"]) < 1)
	return;
?>
<section class="slider_box">
    <div class="container clearfix">
        <div class="grid_34 prefix_3">
        <div class="flexslider main_slider qq">
            <ul class="slides">
            <?
            foreach($arResult["ITEMS"] as $arItem)
            {
                if($arItem["PROPERTIES"]["TYPE"]["VALUE"]=="Видео")
                {
                    ?>
                    <li class="slider_item p_rel">
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/youtube_img.jpg" class="youtube_img p_rel" alt="">
                        <div class="youtube_play_box al_center">
                            <a href="#" class="d_ib modal_btn" data-modal="#youtube_modal">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/youtube_play_btn.png" alt="">
                            </a>
                        </div>
                        <div class="d_none">
                            <div id="youtube_modal" class="youtube_modal p_rel">
                                <div class="arcticmodal-close"></div>
                                <iframe width="1000" height="600" src="<?=$arItem["PROPERTIES"]["YOUTUBE"]["VALUE"]?>?controls=0" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </li>
                    <?
                }elseif($arItem["PROPERTIES"]["TYPE"]["VALUE"]=="Товар"){
                    ?>
                    <li class="slider_item">
                        <div class="slider_item_inner">
                            <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="" data-transition="top">
                            <div class="slide_banner" data-transition="right">
                                <?=htmlspecialchars_decode($arItem["PREVIEW_TEXT"])?>
                            </div>
                            <div class="slide_price black al_center" data-transition="top">
                                <div class="fs_36"><?=$arItem["PROPERTIES"]["PRICE"]["VALUE"]?></div>
                                <div class="fs_18">руб.</div>
                            </div>
                        </div>
                    </li>
                    <?
                }else{
					?>
                    <li class="slider_item">
                       
                            
						
							<?=htmlspecialchars_decode($arItem["PREVIEW_TEXT"])?>
						
                            
                       
                    </li>
                    <?
				}
            }
            ?>
            </ul>
        </div>
        </div>
    </div>
</section>