<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (empty($arResult["ITEMS"])) {
	return;
}
$arParams["BLOCK_LABEL"] = trim(strip_tags($arParams["BLOCK_LABEL"]));
?>

<span class="footer_title">
	<a href="<?=str_replace('#SITE_DIR#', '', $arResult["LIST_PAGE_URL"])?>">
		<?=$arParams["BLOCK_LABEL"]?>
	</a>
</span>
<ul class="footer_article-box">
<?foreach($arResult["ITEMS"] as $arItem):?>
    <li class="footer_article">
        <span class="footer_article-date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span>
        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="footer_article-link" title=""><?=$arItem["NAME"]?></a>
    </li>
<?endforeach;?>
</ul>
