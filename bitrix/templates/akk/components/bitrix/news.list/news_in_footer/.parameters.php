<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arTemplateParameters['BLOCK_LABEL'] = array(
	'PARENT' => 'BASE',
	'NAME' => GetMessage('BLOCK_LABEL_NAME'),
	'TYPE' => 'STRING',
	'DEFAULT' => GetMessage('BLOCK_LABEL_NAME_DEFAULT')
);