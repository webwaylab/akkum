<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = '<div class="clearfix"><div class="grid_38 resp_p0 resp_full_w"><ul class="breadcrumbs reg">';

$num_items = count($arResult);
$home = '/';
for($index = 0, $itemSize = $num_items; $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);

	if($arResult[$index]["LINK"] == '/catalog/')
		$strReturn .= '<li class="breadcrumbs_it d_ib asd1"><a href="'.$home.'"><span class="breadcrumbs_current">'.$title.'</span></a></li>';
	elseif($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
		$strReturn .= '<li class="breadcrumbs_it d_ib asd2"><a href="'.$arResult[$index]["LINK"].'"><span class="breadcrumbs_current">'.$title.'</span></a></li>';
	else
		$strReturn .= '<li class="breadcrumbs_it d_ib asd3"><span class="breadcrumbs_current">'.$title.'</span></li>';
}

$strReturn .= '</ul></div></div>';

$strReturn .= "<!--<div id = 'h1_bread'>".$arResult[2]["TITLE"].$arResult[3]["TITLE"]."</div>-->";
$strReturn .= "<!--<div id = 'title_bread'>".$arResult[1]["TITLE"]." | ".$arResult[2]["TITLE"]."| ".$arResult[3]["TITLE"]."</div>-->";

return $strReturn;
?>
