<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="border15 aside-block">
    <div class="bac_CCC">
    	<ul class="aside-nav">
			<?foreach($arResult['CATALOG_TREE']["accessories"]["ITEMS"]["domkraty_podstavki_upory_pod_mashinu"]["ITEMS"] as $domkrat_item):?>
				<li class="aside-nav__item">
					<a href="<?=$domkrat_item["SECTION_PAGE_URL"]?>" class="aside-nav__link"><?=$domkrat_item["NAME"]?></a>
				</li>
			<?endforeach;?>
	    </ul>
    </div>
</div>