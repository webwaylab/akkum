<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="border15 aside-block">
    <div class="bac_CCC">
        <div class="bold dark_font name_filter">
            <a class="td_underline" href="/catalog/accessories/"><?=GetMessage('HEADER')?></a>
        </div>
        <ul class="aside-nav">
		<? foreach($arResult["SECTIONS"] as $section): ?>
	        <li class="aside-nav__item <?=((count($arResult["CHILDS"][$section["ID"]])>0) ? "aside-nav__item_has_dropdown" : "")?>">
	        	<?if(count($arResult["CHILDS"][$section["ID"]])>0):?>
	        		<ul class="aside-nav-inner">
	        		<?foreach($arResult["CHILDS"][$section["ID"]] as $child): ?>
	        			<li class="aside-nav-inner__item">
	                        <a href="<?=$child["SECTION_PAGE_URL"]?>" class="aside-nav-inner__link"><?=$child["NAME"]?></a>
	                    </li>
	        		<?endforeach;?>
	        		</ul>
	        	<?endif;?>
	        	<a href="<?=$section["SECTION_PAGE_URL"]?>" class="aside-nav__link"><?=$section["NAME"]?></a>
		    </li>
		<?endforeach;?>
	    </ul>
    </div>
</div>