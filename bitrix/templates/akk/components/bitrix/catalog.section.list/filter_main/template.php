<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
    <div class="clearfix">
				<?$APPLICATION->IncludeComponent(
						"bitrix:catalog.smart.filter",
						"filter_main",
						array(
							"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
							"IBLOCK_ID" => $arParams["IBLOCK_ID"],
							"SECTION_ID" => "18",
							"FILTER_NAME" => "arrFilter",
							"PRICE_CODE" => $arParams["PRICE_CODE"],
							"CACHE_TYPE" => "A",
							"CACHE_TIME" => "36000000",
							"CACHE_NOTES" => "",
							"CACHE_GROUPS" => "N",
							"SAVE_IN_SESSION" => "N",
							"DOP_FILTER" => $sectionName,
							"SEOPAGE" => "Y",
							"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"]
						),
						$component,
						array('HIDE_ICONS' => 'Y')
					);?>
</div>

<div class="border15 aside-block">
    <div class="bac_CCC">
    	<div class="bold dark_font name_filter">
            <a style="text-decoration: underline" href="/catalog/"><?=GetMessage('HEADER')?></a>
        </div>
        <ul class="aside-nav">
			<li class="aside-nav__item">
				<a href="<?=$arResult['CATALOG_TREE']["battery"]["SECTION_PAGE_URL"]?>" class="aside-nav__link"><?=$arResult['CATALOG_TREE']["battery"]["NAME"]?></a>
				<?$APPLICATION->IncludeFile("/include/left_menu_h.php", Array(), Array("MODE"=>"html"));?>
			</li>
			<li class="aside-nav__item">
				<a href="<?=$arResult['CATALOG_TREE']["accessories"]["SECTION_PAGE_URL"]?>" class="aside-nav__link"><?=$arResult['CATALOG_TREE']["accessories"]["NAME"]?></a>
			</li>
			<li class="aside-nav__item">
				<a href="<?=$arResult['CATALOG_TREE']["masla"]["SECTION_PAGE_URL"]?>" class="aside-nav__link"><?=$arResult['CATALOG_TREE']["masla"]["NAME"]?></a>
			</li>
	    </ul>
		<br/>
		<?/*$APPLICATION->IncludeComponent(
			"bitrix:catalog.smart.filter",
			"vertical_akk_type",
			array(
				"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"SECTION_ID" => $currentRootSection["ID"],
				"FILTER_NAME" => "arrFilter",
				"PRICE_CODE" => $arParams["PRICE_CODE"],
				"CACHE_TYPE" => "N",
				"CACHE_TIME" => "36000000",
				"CACHE_NOTES" => "",
				"CACHE_GROUPS" => "N",
				"SAVE_IN_SESSION" => "N",
				"DOP_FILTER" => $sectionName,
				"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"]
			),
			$component,
			array('HIDE_ICONS' => 'Y')
		);*/?>

		<div class="battery_type">
			<div class="bold dark_font name_filter">
				<span>Подбираем по Емкости</span>
			</div>

		    <?$APPLICATION->IncludeComponent(
				"bitrix:menu",
				"type_akb",
				array(
					"ROOT_MENU_TYPE" => "emkostakb",
					"MENU_CACHE_TYPE" => "A",
					"MENU_CACHE_TIME" => "36000000",
					"MENU_CACHE_USE_GROUPS" => "Y",
					"MENU_CACHE_GET_VARS" => array(
					),
					"MAX_LEVEL" => "1",
					"ALLOW_MULTI_SELECT" => "N",
					"CHILD_MENU_TYPE" => "",
					"DELAY" => "N",
					"USE_EXT" => "N",
				),
				false
			);?>
		</div>		
		
		<div class="battery_type">
			<div class="bold dark_font name_filter">
				<span>Тип аккумулятора</span>
			</div>

		    <?$APPLICATION->IncludeComponent(
				"bitrix:menu",
				"type_akb",
				array(
					"ROOT_MENU_TYPE" => "typeakb",
					"MENU_CACHE_TYPE" => "A",
					"MENU_CACHE_TIME" => "36000000",
					"MENU_CACHE_USE_GROUPS" => "Y",
					"MENU_CACHE_GET_VARS" => array(
					),
					"MAX_LEVEL" => "1",
					"ALLOW_MULTI_SELECT" => "N",
					"CHILD_MENU_TYPE" => "",
					"DELAY" => "N",
					"USE_EXT" => "N",
				),
				false
			);?>
		</div>		
    </div>
</div>
