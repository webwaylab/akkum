<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CModule::IncludeModule("iblock");
global $APPLICATION;

$childs = array();
foreach($arResult["SECTIONS"] as $itemIdex => $arItem)
{
    if($arItem["DEPTH_LEVEL"]!=2)
    {
        $childs[$arItem["IBLOCK_SECTION_ID"]][] = $arItem;
        unset($arResult["SECTIONS"][$itemIdex]);
    }
}

$arResult["SECTIONS"]  = array_values($arResult["SECTIONS"]);
$arResult["CHILDS"] = $childs;
?>