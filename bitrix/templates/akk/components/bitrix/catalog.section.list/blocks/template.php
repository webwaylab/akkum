<?//CDev::pre($arResult);?>

<div class="products clearfix old_price_action">
	<?foreach($arResult['SECTIONS'] as $item):?>
	<div class="product_item-wrap sections">
	  <figure class="product_item sections">
		 <div class="wrap_inner_figure sections">
			<a href="<?=$item['SECTION_PAGE_URL']?>">
			   <div class="product_label-box">
			   </div>
			   <div class="product_item_img">
				<?if(empty($item['UF_ADD_IMAGE'])):?>
					<img src="<?=$item['PICTURE']['SRC']?>" alt="<?=$item['NAME']?>">
				<?else:?>
					<img src="<?=CFile::GetPath($item['UF_ADD_IMAGE'])?>" alt="<?=$item['NAME']?>">
				<?endif?>
			   </div>
			   <figcaption class="product_item_description">
				  <div class="product_item_content-box">
                                <div class="product_item_content is-truncated wb">
						<?=$item['NAME']?>
					 </div>
				  </div>
			   </figcaption>
			</a>
		 </div>
	  </figure>
	</div>
	<?endforeach?>
</div>