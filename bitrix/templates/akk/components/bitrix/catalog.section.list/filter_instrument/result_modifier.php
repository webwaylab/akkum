<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CModule::IncludeModule("iblock");
global $APPLICATION;

$childs = array();
foreach($arResult["SECTIONS"] as $itemIdex => $arItem)
{
    if($arItem["DEPTH_LEVEL"]!=2)
    {
        $childs[$arItem["IBLOCK_SECTION_ID"]][] = $arItem;
        unset($arResult["SECTIONS"][$itemIdex]);
    }
}

$arResult["SECTIONS"]  = array_values($arResult["SECTIONS"]);
$arResult["CHILDS"] = $childs;


$res = CIBlockSection::GetList(
	array('LEFT_MARGIN' => 'ASC'),
	array('IBLOCK_ID' => CATALOG_ID, 'ACTIVE' => 'Y', 'GLOBAL_ACTIVE' => 'Y'),
	false,
	array('ID', 'NAME', 'PICTURE', 'IBLOCK_SECTION_ID', 'SECTION_PAGE_URL')
);
while ($sec = $res->GetNext()){
	$sections[intval($sec['IBLOCK_SECTION_ID'])][$sec['ID']] = $sec;
}
if (!function_exists('buildTree')) {
	function buildTree($cats, $parent_id)
	{
		$res = array();
		if (is_array($cats) and isset($cats[$parent_id])) {
			foreach ($cats[$parent_id] as $cat) {
				$el = $cat;
				$el['ITEMS'] = buildTree($cats, $cat['ID']);
				$res[$cat['CODE']] = $el;
			}
		}
		return $res;
	}
}

foreach ($sections[0] as $sec){
	$secTree[$sec['CODE']] = $sec;
	$secTree[$sec['CODE']]['ITEMS'] = buildTree($sections, $sec['ID']);
}
if($_GET["debug"]){
	//CDev::pre($secTree["accessories"]["ITEMS"]["domkraty_podstavki_upory_pod_mashinu"]["ITEMS"]);
}
$arResult['CATALOG_TREE'] = $secTree;
?>