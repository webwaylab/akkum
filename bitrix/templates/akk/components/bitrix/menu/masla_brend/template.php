<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
?>

<?php foreach ($arResult["BRAND"] as $arBrand) { ?>
    <?php
    $brand = Cutil::translit($arBrand["PROPERTY_PROIZVODITEL_MASLO_VALUE"], "ru", array("change_case" => "L", "replace_space" => "_", "replace_other" => "-"));
    ?>
    <div class="brand-item">
        <a href="/catalog/masla/<?= $brand; ?>/">
            <div class="item-image">
                <div class="image-valign">
                    <img src="/bitrix/templates/akk/images/oils/oil_<?= $brand; ?>.png" alt="<?= $arBrand["PROPERTY_PROIZVODITEL_MASLO_VALUE"] ?>">
                </div>
            </div>
            <div class="brand-name"><?= $arBrand["PROPERTY_PROIZVODITEL_MASLO_VALUE"] ?></div>
        </a>
    </div>
<?php } ?>