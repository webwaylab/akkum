<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (empty($arResult))
	return;
?>

<ul class="footer_menu">
    <?foreach($arResult as $arItem):?>
    	<?if ($arItem["DEPTH_LEVEL"] == "1"):?>
        <li class="footer_menu-item">
            <a href="<?=$arItem["LINK"]?>" class="footer_menu-link" title=""><?=$arItem["TEXT"]?></a>
        </li>
      <?endif?>
    <?endforeach;?>
</ul>