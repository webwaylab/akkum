<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (empty($arResult))
	return;
?>

<ul class="aside-nav">
    <?foreach($arResult as $arItem):?>
    	<?if ($arItem["DEPTH_LEVEL"] == "1"):?>
		<li class="aside-nav__item">
			<a href="<?=$arItem["LINK"]?>" class="aside-nav__link"><?=$arItem["TEXT"]?></a>
		</li>
      <?endif?>
    <?endforeach;?>
</ul>
