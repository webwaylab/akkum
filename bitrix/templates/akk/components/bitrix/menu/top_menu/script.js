$(document).ready(function () {
    $('.tabs__menu-1 ul.tabs-new li').on('hover', function () {
        var tab_id = $(this).attr('data-tab');

        $('.tabs__menu-1 ul.tabs-new li').removeClass('current');
        $('.tabs__menu-1 .tab-contents').removeClass('current');

        $(this).addClass('current');
        $("#" + tab_id).addClass('current');
    });

    $('.tabs__menu-3 ul.tabs-new li').on('click', function () {
        var tab_id = $(this).attr('data-tab');

        $('.tabs__menu-3 ul.tabs-new li').removeClass('current');
        $('.tabs__menu-3 .tab-contents').removeClass('current');

        $(this).addClass('current');
        $("#" + tab_id).addClass('current');
    });

    $('.tabs__menu-3 ul.tabs-new li a').on('click', function (event) {
        event.preventDefault();
        /* Act on the event */
    });

    $("#tab_accessories .owl-carousel, #tab2 .owl-carousel").owlCarousel({
        autoPlay: false,
        navigation: true,
        items: 5,
        margin: 15,
        loop: true,
        itemsDesktop: [1199, 4],
        itemsDesktopSmall: false
    });
});