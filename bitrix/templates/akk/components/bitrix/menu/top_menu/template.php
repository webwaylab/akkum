<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);

if (empty($arResult)) {
    return;
}
//pa($arResult['EITEM']);
?>

<nav>

    <? if (strpos($APPLICATION->GetCurDir(), "/catalog/") !== false
        || strpos($APPLICATION->GetCurDir(), "/personal/cart/") !== false) { ?>

        <link rel="stylesheet" type="text/css" href="<?= $templateFolder ?>/style_append.css">
    <? } ?>

    <div class="container clearfix">
        <div class="grid_40">
            <div id="tabs__menu" class="tabs__menu">
                <ul class="clearfix">
                    <li class="tabs__menu-li"><a class="tabs__menu-link" href="#tabs__menu-1"
                                                 onclick="document.location.href='/catalog/battery/'">Каталог
                            аккумуляторов</a></li>
                    <li class="tabs__menu-li"><a class="tabs__menu-link" href="#tabs__menu-2"
                                                 onclick="document.location.href='/catalog/accessories/'">Аксессуары для
                            автомобиля </a></li>
                    <li class="tabs__menu-li"><a class="tabs__menu-link" href="#tabs__menu-3"
                                                 onclick="document.location.href='/catalog/masla/'">Масла и технические
                            жидкости</a></li>
                </ul>
                <div id="tabs__menu-1" class="tabs__menu-1">
                    <div class="tabs-container">
                        <ul class="tabs-new">
                            <li class="tab-links current" data-tab="tab1"><a href="/catalog/battery/auto-akb/">Автомобильные</a></li>
                            <li class="tab-links" data-tab="tab3"><a href="/catalog/battery/truck-akb/">Грузовые</a></li>
                            <li class="tab-links" data-tab="tab2"><a href="/catalog/battery/moto-akb/">Мото</a></li>
                            <li class="tab-links" data-tab="tab4"><a href="/catalog/battery/akb_dlya_ups/">ИБП (UPS)</a></li>
                            <li class="tab-links" data-tab="tab5"><a href="/catalog/auto/">Поиск по марке авто</a></li>
                            <li class="tab-links" data-tab="tab6"><a>Популярные</a></li>
                        </ul>

                        <div id="tab1" class="tab-contents current">

                            <? foreach ($arResult['CATALOG_TREE']["battery"]["ITEMS"]["auto-akb"]["ITEMS"] as $column): ?>
                                <ul>
                                    <? foreach ($column as $row): ?>
                                        <?
                                        $row["SECTION_PAGE_URL"] = str_replace("/battery/auto-akb/ADDBRAND_", "/brand/",
                                            $row["SECTION_PAGE_URL"]);
                                        ?>
                                        <li data-image="<?= $row["FILE"] ?>"><a
                                                    href="<?= $row["SECTION_PAGE_URL"] ?>"><?= $row["NAME"] ?></a></li>
                                    <? endforeach; ?>
                                </ul>
                            <? endforeach; ?>
                        </div>

                        <div id="tab2" class="tab-contents tabs-moto">
                            <div class="owl-carousel moto-slider">
                                <? foreach ($arResult['CATALOG_TREE']["battery"]["ITEMS"]["moto-akb"]["ITEMS"] as $column): ?>
                                    <? foreach ($column as $row): ?>
                                        <div class="slick_sl">
                                            <a href="<?= $row["SECTION_PAGE_URL"] ?>">
                                                <img src="<?= CFile::GetPath($row["PICTURE"]) ?>"
                                                     alt="<?= $row["NAME"] ?>">
                                            </a>
                                            <a href="<?= $row["SECTION_PAGE_URL"] ?>">
                                                <?= $row["NAME"] ?>
                                            </a>
                                        </div>
                                    <? endforeach; ?>
                                <? endforeach; ?>
                            </div>
                        </div>

                        <div id="tab3" class="tab-contents">
                            <? foreach ($arResult['CATALOG_TREE']["battery"]["ITEMS"]["truck-akb"]["ITEMS"] as $column): ?>
                                <ul>
                                    <? foreach ($column as $row): ?>
                                        <li data-image="<?= $row["FILE"] ?>"><a
                                                    href="<?= $row["SECTION_PAGE_URL"] ?>"><?= $row["NAME"] ?></a></li>
                                    <? endforeach; ?>
                                </ul>
                            <? endforeach; ?>
                        </div>

                        <div id="tab4" class="tab-contents">
                            <? foreach ($arResult['CATALOG_TREE']["battery"]["ITEMS"]["akb_dlya_ups"]["ITEMS"] as $column): ?>
                                <ul>
                                    <? foreach ($column as $row): ?>
                                        <li data-image="<?= $row["FILE"] ?>"><a
                                                    href="<?= $row["SECTION_PAGE_URL"] ?>"><?= $row["NAME"] ?></a></li>
                                    <? endforeach; ?>
                                </ul>
                            <? endforeach; ?>
                        </div>

                        <div id="tab5" class="tab-contents">
                            <?php
                            $GLOBALS['MANUFACTURER_MENU_FILTER'] = array(
                                '=PROPERTY_SHOW_IN_MANUFACTURER_MENU' => 155
                            );
                            ?>
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:catalog.section",
                                "fl_auto2",
                                array(
                                    "IBLOCK_ID" => 13,
                                    "SECTION_ID" => 0,
                                    "SECTION_CODE" => "",
                                    "ELEMENT_SORT_FIELD" => "name",
                                    "ELEMENT_SORT_ORDER" => "asc",
                                    "PAGE_ELEMENT_COUNT" => "1000",
                                    "FILTER_NAME" => "MANUFACTURER_MENU_FILTER",
                                    "PROPERTY_CODE" => array(
                                        "SHOW_IN_MANUFACTURER_MENU",
                                        ""
                                    )
                                    /*"CACHE_TYPE" => $arParams["CACHE_TYPE"],
                                    "CACHE_TIME" => $arParams["CACHE_TIME"],
                                    "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],*/
                                ),
                                $component,
                                array("HIDE_ICONS" => "N")
                            ); ?>

                        </div>

                        <div id="tab6" class="tab-contents">
                            <ul>
                                <li>
                                    <a href="<?= $arResult['CATALOG_TREE']["battery"]["ITEMS"]["auto-akb"]["SECTION_PAGE_URL"] ?>">
                                        Автомобильные аккумуляторы
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= $arResult['CATALOG_TREE']["battery"]["ITEMS"]["truck-akb"]["SECTION_PAGE_URL"] ?>">
                                        Грузовые аккумуляторы
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= $arResult['CATALOG_TREE']["battery"]["ITEMS"]["gelevye"]["SECTION_PAGE_URL"] ?>">
                                        <?= $arResult['CATALOG_TREE']["battery"]["ITEMS"]["gelevye"]["NAME"] ?>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= $arResult['CATALOG_TREE']["battery"]["ITEMS"]["moto-akb"]["SECTION_PAGE_URL"] ?>">
                                        Мото aккумуляторы
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="brand__tabs brand__tabs4">
                        <ul class="tabs">
                            <? foreach ($arResult['CATALOG_TREE']["battery"]["ITEMS"]["auto-akb-emkost"]["ITEMS"] as $arChild): ?>
                                <? if ($arChild["SECTION_PAGE_URL"] == $page_uri): ?>
                                    <li class="tab-link current" data-tab="tab-auto-akb">
                                        <a href="<?= $arChild["SECTION_PAGE_URL"] ?>"><?= $arChild["NAME"] ?></a>
                                    </li>
                                <? else: ?>
                                    <li class="tab-link" data-tab="tab-auto-akb">
                                        <a href="<?= $arChild["SECTION_PAGE_URL"] ?>"><?= $arChild["NAME"] ?></a>
                                    </li>
                                <? endif; ?>
                            <? endforeach; ?>
                            <? foreach ($arResult['CATALOG_TREE']["battery"]["ITEMS"]["truck-akb-emkost"]["ITEMS"] as $arChild): ?>
                                <? if ($arChild["SECTION_PAGE_URL"] == $page_uri): ?>
                                    <li class="tab-link current" data-tab="tab-truck-akb">
                                        <a href="<?= $arChild["SECTION_PAGE_URL"] ?>"><?= $arChild["NAME"] ?></a>
                                    </li>
                                <? else: ?>
                                    <li class="tab-link" data-tab="tab-truck-akb">
                                        <a href="<?= $arChild["SECTION_PAGE_URL"] ?>"><?= $arChild["NAME"] ?></a>
                                    </li>
                                <? endif; ?>
                            <? endforeach; ?>
                        </ul>
                    </div>


                </div>

                <div id="tabs__menu-2" class="tabs__menu-3">

                    <div id="tab_accessories" class="tab-contents current">
                        <div class="owl-carousel owl-theme">
                            <? $res_arr = array_chunk($arResult['CATALOG_TREE']['accessories']['ITEMS'], 2, true); ?>
                            <? foreach ($res_arr as $val): ?>
                                <div class="owl-wrap">
                                    <? foreach ($val as $sec): ?>
                                        <?
                                        $file = array();
                                        if ($sec['PICTURE']) {
                                            $file = CFile::ResizeImageGet($sec['PICTURE'], array('height' => 108), 1);
                                        }
                                        ?>
                                        <div class="accessory-item">
                                            <a href="<?= $sec['SECTION_PAGE_URL'] ?>">
                                                <div class="item-image">
                                                    <div class="image-valign">
                                                        <img src="<?= $file['src'] ?>" alt="">
                                                    </div>
                                                </div>
                                                <div class="accessory-name"><?= $sec['NAME'] ?></div>
                                            </a>
                                        </div>
                                    <? endforeach; ?>
                                </div>
                            <? endforeach; ?>
                        </div>
                    </div>
                </div>

                <div id="tabs__menu-3" class="tabs__menu-3">
                    <div class="tabs-container">
                        <ul class="tabs-new">
                            <li class="tab-links current" data-tab="tab7"><a>Виды масел</a></li>
                            <li class="tab-links" data-tab="tab8"><a>Бренды масел</a></li>
                        </ul>
                        <div id="tab7" class="tab-contents current">
                            <ul class="tabs__sub__menu">
                                <? foreach ($arResult['CATALOG_TREE']['masla']['ITEMS'] as $sec): ?>
                                    <?
                                    $file = array();
                                    if ($sec['PICTURE']) {
                                        $file = CFile::ResizeImageGet($sec['PICTURE'],
                                            array('width' => 278, 'height' => 165), 1);
                                    }
                                    ?>
                                    <li class="tabs__sub__menu__li">
                                        <a href="<?= $sec['SECTION_PAGE_URL'] ?>" class="tabs__sub__menu-li__link">
                                            <span class="tabs__sub__menu__li-img"
                                                  <? if ($file['src']){ ?>style="background-image:url(<?= $file['src'] ?>);"<? } ?>></span><?= $sec['NAME'] ?>
                                        </a>
                                    </li>
                                <? endforeach; ?>
                            </ul>
                        </div>
                        <div id="tab8" class="tab-contents">
                            <div class="owl-carousel owl-theme">
                                <?
                                $APPLICATION->IncludeComponent(
                                    "bitrix:menu",
                                    "masla_brend",
                                    array(
                                        "IBLOCK_ID" => CATALOG_ID,
                                        "MENU_CACHE_TYPE" => "A",
                                        "MENU_CACHE_TIME" => "36000000",
                                        "MENU_CACHE_USE_GROUPS" => "Y",
                                        "MENU_CACHE_GET_VARS" => array(),
                                        "MAX_LEVEL" => "1",
                                        "ALLOW_MULTI_SELECT" => "N",
                                        "DELAY" => "N",
                                        "USE_EXT" => "N",
                                    ),
                                    false
                                ); ?>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <?
            $url_no_view = array(
                '/catalog/battery/varta/',
                '/catalog/battery/zver/',
                '/catalog/battery/mutlu/',
                '/catalog/battery/titan/',
                '/catalog/battery/bosch/',
                '/catalog/battery/tyumen/',
                '/catalog/battery/bolk_moto/',
                '/catalog/battery/tudor/',
                '/catalog/battery/solite/',
                '/catalog/battery/vaiper/',
                '/catalog/battery/royal/',
                '/catalog/battery/sebang/',
                '/catalog/battery/cobat/',
                '/catalog/battery/aktex/',
                '/catalog/battery/afa/',
                '/catalog/avtomobilnye/',
            );
            ?>

            <? $page_uri = $_SERVER["REQUEST_URI"]; ?>

            <? if (strstr($page_uri, "/catalog/battery/") && strstr($page_uri, "/truck-akb/") && false): ?>
                <div class="brand__tabs brand__tabs1">
                    <ul class="tabs">
                        <? foreach ($arResult["CATALOG_TREE"]["battery"]["ITEMS"]["truck-akb"]["ITEMS"] as $key => $arChild): ?>
                            <? if (strstr($page_uri, $arChild["SECTION_PAGE_URL"])): ?>
                                <li class="tab-link current" data-tab="tab-<?= $key ?>">
                                    <?= $arChild["NAME"] ?>
                                </li>
                            <? else: ?>
                                <li class="tab-link" data-tab="tab-<?= $key ?>">
                                    <?= $arChild["NAME"] ?>
                                </li>
                            <? endif; ?>
                        <? endforeach; ?>
                    </ul>
                    <? foreach ($arResult["CATALOG_TREE"]["battery"]["ITEMS"]["truck-akb"]["ITEMS"] as $key => $arChild): ?>
                        <div id="tab-<?= $key ?>" class="tab-content <? if (strstr($page_uri,
                            $arChild["SECTION_PAGE_URL"])): ?>current<? endif; ?>">
                            <ul class="tabs_inner_list">
                                <? foreach ($arChild["ITEMS"] as $subkey => $subChild): ?>
                                    <? if (strstr($page_uri, $subChild["SECTION_PAGE_URL"])): ?>
                                        <li>
                                            <span class="active"><?= $subChild["NAME"] ?></span>
                                        </li>
                                    <? else: ?>
                                        <li>
                                            <a href="<?= $subChild["SECTION_PAGE_URL"] ?>"
                                               class=""><?= $subChild["NAME"] ?></a>
                                        </li>
                                    <? endif; ?>
                                <? endforeach ?>
                            </ul>
                        </div>
                    <? endforeach; ?>
                </div>
            <? endif; ?>

            <? if (strstr($page_uri, "/catalog/battery/") && strstr($page_uri, "/moto-akb/") && false): ?>
                <div class="brand__tabs brand__tabs2">
                    <ul class="tabs">
                        <? foreach ($arResult["CATALOG_TREE"]["battery"]["ITEMS"]["moto-akb"]["ITEMS"] as $key => $arChild): ?>
                            <? if (strstr($page_uri, $arChild["SECTION_PAGE_URL"])): ?>
                                <li class="tab-link current" data-tab="tab-<?= $key ?>">
                                    <?= $arChild["NAME"] ?>
                                </li>
                            <? else: ?>
                                <li class="tab-link" data-tab="tab-<?= $key ?>">
                                    <a href="<?= $arChild["SECTION_PAGE_URL"] ?>" class=""><?= $arChild["NAME"] ?></a>
                                </li>
                            <? endif; ?>
                        <? endforeach; ?>
                    </ul>
                </div>
            <? endif; ?>

            <? if (strstr($page_uri, "/catalog/battery/") && strstr($page_uri, "/gelevye/") && false): ?>
                <div class="brand__tabs brand__tabs3">
                    <ul class="tabs">
                        <? foreach ($arResult["CATALOG_TREE"]["battery"]["ITEMS"]["gelevye"]["ITEMS"] as $key => $arChild): ?>
                            <? if (strstr($page_uri, $arChild["SECTION_PAGE_URL"])): ?>
                                <li class="tab-link current" data-tab="tab-<?= $key ?>">
                                    <?= $arChild["NAME"] ?>
                                </li>
                            <? else: ?>
                                <li class="tab-link" data-tab="tab-<?= $key ?>">
                                    <a href="<?= $arChild["SECTION_PAGE_URL"] ?>" class=""><?= $arChild["NAME"] ?></a>
                                </li>
                            <? endif; ?>
                        <? endforeach; ?>
                    </ul>
                </div>
            <? endif; ?>

            <? if (strstr($page_uri, "/catalog/accessories/") && false): ?>
                <div class="brand__tabs brand__tabs5">
                    <ul class="tabs">
                        <? foreach ($arResult["CATALOG_TREE"]["accessories"]["ITEMS"] as $key => $arChild): ?>
                            <? if (strstr($page_uri, $arChild["SECTION_PAGE_URL"])): ?>
                                <li class="tab-link current" data-tab="tab-<?= $key ?>">
                                    <? if (count($arChild["ITEMS"])): ?>
                                        <a href="<?= $arChild["SECTION_PAGE_URL"] ?>"
                                           class=""><?= $arChild["NAME"] ?></a>
                                    <? else: ?>
                                        <a href="<?= $arChild["SECTION_PAGE_URL"] ?>"
                                           class=""><?= $arChild["NAME"] ?></a>
                                    <? endif; ?>
                                </li>
                            <? else: ?>
                                <li class="tab-link" data-tab="tab-<?= $key ?>">
                                    <? if (count($arChild["ITEMS"])): ?>
                                        <a href="<?= $arChild["SECTION_PAGE_URL"] ?>"
                                           class=""><?= $arChild["NAME"] ?></a>
                                    <? else: ?>
                                        <a href="<?= $arChild["SECTION_PAGE_URL"] ?>"
                                           class=""><?= $arChild["NAME"] ?></a>
                                    <? endif; ?>
                                </li>
                            <? endif; ?>
                        <? endforeach; ?>
                        <? foreach ($arResult["CATALOG_TREE"]["accessories"]["ITEMS"] as $key => $arChild): ?>
                            <? if (count($arChild["ITEMS"])): ?>
                                <div id="tab-<?= $key ?>" class="tab-content <? if (strstr($page_uri,
                                    $arChild["SECTION_PAGE_URL"])): ?>current<? endif; ?>">
                                    <ul class="tabs_inner_list">
                                        <? foreach ($arChild["ITEMS"] as $subkey => $subChild): ?>
                                            <? if (strstr($page_uri, $subChild["SECTION_PAGE_URL"])): ?>
                                                <li>
                                                    <span class="active"><?= $subChild["NAME"] ?></span>
                                                </li>
                                            <? else: ?>
                                                <li>
                                                    <a href="<?= $subChild["SECTION_PAGE_URL"] ?>"
                                                       class=""><?= $subChild["NAME"] ?></a>
                                                </li>
                                            <? endif; ?>
                                        <? endforeach ?>
                                    </ul>
                                </div>
                            <? endif; ?>
                        <? endforeach; ?>
                    </ul>
                </div>
            <? endif; ?>

        </div>
    </div>
</nav>
