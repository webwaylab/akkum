<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (empty($arResult))
	return;
//pa($arResult['CATALOG_TREE']['battery']);
?>

<nav>

	<script>
		$(document).ready(function(){

			$('.tabs__menu-1 ul.tabs-new li').click(function(){
				var tab_id = $(this).attr('data-tab');

				$('.tabs__menu-1 ul.tabs-new li').removeClass('current');
				$('.tabs__menu-1 .tab-contents').removeClass('current');

				$(this).addClass('current');
				$("#"+tab_id).addClass('current');
			});
			$('.tabs__menu-1 ul.tabs-new li a').on('click', function(event) {
				event.preventDefault();
				/* Act on the event */
			});

			$('.tabs__menu-3 ul.tabs-new li').click(function(){
				var tab_id = $(this).attr('data-tab');

				$('.tabs__menu-3 ul.tabs-new li').removeClass('current');
				$('.tabs__menu-3 .tab-contents').removeClass('current');

				$(this).addClass('current');
				$("#"+tab_id).addClass('current');
			});
			$('.tabs__menu-3 ul.tabs-new li a').on('click', function(event) {
				event.preventDefault();
				/* Act on the event */
			});

		});
	</script>
	<style>
		ul.tabs-new{
			margin: 0px;
			padding: 0px 15px;
			list-style: none;
			background: #fff;
			position: relative;
		}
		ul.tabs-new li {
			background: none;
			display: inline-block;
			cursor: pointer;
			color: #0f57b2;
			font-family: RobotoBold;
			font-size: 13px;
			line-height: 18px;
			text-transform: uppercase;
			position: relative;
			margin-right: 10px;
			padding: 0 15px;
		}
		ul.tabs-new li a:after {
			position: absolute;
			height: 1px;
			width: 100%;
			content: "";
			bottom: 10px;
			left: 0;
			border-bottom: 1px dashed;
		}
		ul.tabs-new li.current a:after {
			display: none;
		}
		ul.tabs-new li.current {
			background-color: #1ec247;
			border-radius: 5px;
			color: white;
			font-family: RobotoBold;
			font-size: 13px;
			font-weight: 700;
			line-height: 18px;
			text-transform: uppercase;
			position: relative;
		}
		ul.tabs-new li.current:after{
			width: 0;
			height: 0;
			border-style: solid;
			border-width: 8px 5px 0 5px;
			border-color: #1ec247 transparent transparent transparent;
			content: "";
			position: absolute;
			bottom: -7px;
			left: 0;
			display: block;
			right: 0;
			margin: 0 auto;
		}
		ul.tabs-new li a {
			padding: 10px 0px;
			display: block;
			position: relative;
			color: #0f57b2;
		}
		ul.tabs-new li.current a {
			color: #fff;
		}
		.tab-contents li {
			display: inline-block;
		}
		.tab-contents li a {
			color: #282826;
			font-family: RobotoBold;
			font-size: 14px;
			font-weight: 500;
			line-height: 30px;
			text-decoration: underline;
		}
		.tab-contents li ul li {
			display: inline-block;
			width: 100%;
			border-bottom: 1px dotted #fff;
		}
		.tab-contents{
			display: none;
			background: #ededed;
			padding: 15px;
		}
		.tabs__menu div.tabs-container {
			box-shadow: none;
			padding: 0;
		}
		.tab-contents.current{
			display: inherit;
		}
		.tab-contents ul li ul {
			display: inline-block;
			width: 100%;
			padding-left: 10px;
		}
		.tab-contents li {
			display: block;
			clear: both;
			margin-right: 5px;
			border-bottom: 1px dotted #d2d2d2;
		}
		.tab-contents li a {
			padding-left: 10px;
			position: relative;
		}
		.tab-contents li a:hover {
			text-decoration: none;
		}
		.tab-contents li a:after {
			content: "";
			position: absolute;
			left: 0px;
			top: 5px;
			background: url(<?SITE_TEMPLATE_PATH?>/bitrix/templates/akk/images/tab-arrow-green.png) no-repeat;
			width: 15px;
			height: 15px;
		}
		.tab-contents li ul li a:after {
			content: "";
			position: absolute;
			left: 0px;
			top: 5px;
			background: url(<?SITE_TEMPLATE_PATH?>/bitrix/templates/akk/images/tab-arrow-gray.png) no-repeat;
			width: 15px;
			height: 15px;
		}
		.tab-contents ul {
			display: inline-block;
			width: 14%;
		}


.tabs__menu-2 .tabs__sub__menu__li a .tabs__sub__menu__li-img { display: block; }
.tabs__menu-2 .tabs__sub__menu__li-name { display: block; }


		.tabs__menu-3 .tabs__sub__menu {
			padding: 0px 0 0 0;
			font: 18px/1 RobotoSlabBold,'Times New Roman',serif;
			display: inherit;
			width: auto;
		}
		.tabs__menu-3 .tabs__sub__menu__li {
			clear: none;
			display: block;
			float: left;
			margin: 0;
			width: 25%;
			text-align: center;
			box-sizing: border-box;
			padding: 0;
			border:none;
		}
		.tabs__menu-3 .tabs__sub__menu .tabs__sub__menu-li__link {
			display: block;
			padding: 0;
			line-height: inherit;
			font-size: inherit;
		}
		.tabs__menu-3 li a:after {
			display: none;
		}
		.tabs__menu div.tabs__menu-3 {
			background: #fff;
			height: 327px;
			padding-top: 20px;
			height: 307px;
		}
		.tabs__menu-3 ul.tabs-new li {
			margin-right: 0;
			padding: 0 15px;
			width: 47%;
			text-align: center;
		}
		.tabs__menu-3 ul.tabs-new {
			text-align: center;
		}
		.tabs__menu .tabs__menu-3 div {
			overflow: hidden;
			height: auto;
			margin-bottom: 0;
			padding-top: 0;
			border-radius: 0 5px 5px 5px;
			background-color: transparent;
			box-shadow: none;
		}
		.tabs__menu-3 .owl-carousel.owl-theme {
			margin-top: 20px;
		}
		.tabs__menu-3 .owl-buttons>div {
			width: 11px;
			height: 21px !important;
			position: absolute;
			top: 50%;
			margin-top: -10px;
			cursor: pointer;
			overflow: visible !important;
		}
		.tabs__menu-3 .owl-buttons .owl-prev {
			left: 0px;
			background-position: 0 -22px;
		}
		.tabs__menu-3 .owl-buttons .owl-next {
			right: 0px;
			background-position: 0 -22px;
		}
		#tab8 {
			padding: 0 50px;
		}
		.tabs__menu-3  .owl-item {
			text-align: center;
		}
		.tabs__menu-3 .owl-buttons {
			height: 3px !important;
		}
		.tabs__menu-3 .owl-item a {
			display: block;
			color: #282826;
			font-family: RobotoRegular;
			font-size: 14px;
			font-weight: 500;
			line-height: 30px;
			text-decoration: underline;
			margin-bottom: 15px;
		}
		.tabs__menu-3 .owl-item a:first-child {
			min-height: 128px;
			margin-top: 20px;
		}
		.tabs__menu-3 .tabs__sub__menu .tabs__sub__menu__li a .tabs__sub__menu__li-img {
			margin-top: 25px;
		}
	</style>

	<div class="container clearfix">
		<div class="grid_40">
			<div id="tabs__menu" class="tabs__menu">
				<ul class="clearfix">
					<li class="tabs__menu-li"><a class="tabs__menu-link" href="#tabs__menu-1" onclick="document.location.href='/catalog/battery/'">Каталог  аккумуляторов</a></li>
					<li class="tabs__menu-li"><a class="tabs__menu-link" href="#tabs__menu-2" onclick="document.location.href='/catalog/accessories/'">Аксессуары для автомобиля </a></li>
					<li class="tabs__menu-li"><a class="tabs__menu-link" href="#tabs__menu-3" onclick="document.location.href='/catalog/masla/'">Масла и технические жидкости</a></li>
				</ul>
				<div id="tabs__menu-1" class="tabs__menu-1">
					<? /*<ul class="tabs__sub__menu">
						<li class="tabs__sub__menu-li">
							<a href="<?=$arResult['CATALOG_TREE']['battery']['SECTION_PAGE_URL']?>" class="tabs__sub__menu-link"><span>Легковые</span><span>аккумуляторы</span></a>
							<ul class="tabs__sub-2_menu">
								<? foreach ($arResult['CATALOG_TREE']['battery']['ITEMS'] as $sec){
									if (in_array($sec['CODE'], array('gruzovye_akkumulyatory', 'moto_akkumulyatory', 'gelevye'))) {
										continue;
									}
									?>
									<li class="tabs__sub-2__menu-li"><a href="<?=$sec['SECTION_PAGE_URL']?>" class="tabs__sub-2__menu-link"><?=$sec['NAME']?></a></li>
								<? } ?>
							</ul>
						</li>
						<li class="tabs__sub__menu-li">
							<a href="<?=$arResult['CATALOG_TREE']['battery']['ITEMS']['gruzovye_akkumulyatory']['SECTION_PAGE_URL']?>" class="tabs__sub__menu-link"><span>Грузовые</span><span>аккумуляторы</span></a>
							<ul class="tabs__sub-2_menu">
								<? foreach ($arResult['CATALOG_TREE']['battery']['ITEMS']['gruzovye_akkumulyatory']['ITEMS'] as $sec){ ?>
									<li class="tabs__sub-2__menu-li"><a href="<?=$sec['SECTION_PAGE_URL']?>" class="tabs__sub-2__menu-link"><?=$sec['NAME']?></a></li>
								<? } ?>
							</ul>
						</li>
						<li class="tabs__sub__menu-li">
							<a href="<?=$arResult['CATALOG_TREE']['battery']['ITEMS']['moto_akkumulyatory']['SECTION_PAGE_URL']?>" class="tabs__sub__menu-link"><span>Мото</span><span>аккумуляторы</span></a>
							<ul class="tabs__sub-2_menu">
								<? foreach ($arResult['CATALOG_TREE']['battery']['ITEMS']['moto_akkumulyatory']['ITEMS'] as $sec){ ?>
									<li class="tabs__sub-2__menu-li"><a href="<?=$sec['SECTION_PAGE_URL']?>" class="tabs__sub-2__menu-link"><?=$sec['NAME']?></a></li>
								<? } ?>
							</ul>
						</li>
						<li class="tabs__sub__menu-li">
							<a href="<?=$arResult['CATALOG_TREE']['battery']['ITEMS']['gelevye']['SECTION_PAGE_URL']?>" class="tabs__sub__menu-link"><span>Гелевые</span><span>аккумуляторы</span></a>
							<ul class="tabs__sub-2_menu">
								<? foreach ($arResult['CATALOG_TREE']['battery']['ITEMS']['gelevye']['ITEMS'] as $sec){ ?>
									<li class="tabs__sub-2__menu-li"><a href="<?=$sec['SECTION_PAGE_URL']?>" class="tabs__sub-2__menu-link"><?=$sec['NAME']?></a></li>
								<? } ?>
							</ul>
						</li>
					</ul>*/?>
					<div class="tabs-container">
						<ul class="tabs-new">
							<li class="tab-links current" data-tab="tab1"><a href="javasript:void(0);">Автомобильные</a></li>
							<li class="tab-links" data-tab="tab2"><a href="javasript:void(0);">Мото</a></li>
							<li class="tab-links" data-tab="tab3"><a href="javasript:void(0);">Грузовые</a></li>
							<li class="tab-links" data-tab="tab4"><a href="javasript:void(0);">ИБП (UPS)</a></li>
							<li class="tab-links" data-tab="tab5"><a href="javasript:void(0);">Поиск по марке авто</a></li>
							<li class="tab-links" data-tab="tab6"><a href="javasript:void(0);">Дополнительные категории</a></li>
						</ul>

						<div id="tab1" class="tab-contents current">
							<ul>
								<li><a href="/catalog/brand/afa/">AFA</a></li>
								<li><a href="/catalog/brand/alphaline/">AlphaLINE</a></li>
								<li><a href="/catalog/brand/american/">American</a></li>
								<li><a href="/catalog/brand/atlas/">Atlas</a></li>
								<li><a href="/catalog/brand/autopower/">Autopower</a></li>
								<li><a href="/catalog/brand/black_horse/">Black Horse</a></li>
								<li>
									<a href="/catalog/brand/bosch/">Bosch</a>
								</li>
							</ul>
							<ul>
								<li>
									<ul>
										<li>
											<a href="/catalog/brand/bosch_s3/">Bosch S3</a>
										</li>
										<li>
											<a href="/catalog/brand/bosch_s4/">Bosch S4</a>
										</li>
										<li>
											<a href="/catalog/brand/bosch_s5/">Bosch S5</a>
										</li>
									</ul>
								</li>
								<li><a href="/catalog/brand/deka/">Deka</a></li>
								<li><a href="/catalog/brand/delkor/">Delkor</a></li>
								<li><a href="/catalog/brand/dominator/">Dominator</a></li>
								<li><a href="/catalog/brand/exide/">Exide</a></li>
							</ul>

							<ul>

								<li><a href="/catalog/brand/fb/">FB</a></li>
								<li><a href="/catalog/brand/fire_ball/">Fire Ball</a></li>
								<li><a href="/catalog/brand/forse/">Forse</a></li>
								<li><a href="/catalog/brand/giver/">Giver</a></li>
								<li><a href="/catalog/brand/hankook/">Hankook</a></li>
								<li><a href="/catalog/brand/hugel/">Hugel</a></li>
								<li><a href="/catalog/brand/medalist/">Medalist</a></li>
							</ul>

							<ul>

								<li><a href="/catalog/brand/mutlu/">Mutlu</a></li>
								<li><a href="/catalog/brand/norr/">Norr</a></li>
								<li><a href="/catalog/brand/sebang/">SEBANG</a></li>
								<li><a href="/catalog/brand/silverstar/">Silverstar</a></li>
								<li><a href="/catalog/brand/solite/">Solite</a></li>
								<li><a href="/catalog/brand/tab/">TAB</a></li>
								<li><a href="/catalog/brand/thor/">Thor</a></li>
							</ul>

							<ul>

								<li><a href="/catalog/brand/titan/">Titan</a></li>
								<li><a href="/catalog/brand/topla/">Topla</a></li>
								<li><a href="/catalog/brand/tudor/">Tudor</a></li>
								<li>
									<a href="/catalog/brand/tyumen/">Tyumen</a>
									<ul>
										<li><a href="/catalog/brand/tyumen_asia/">Tyumen Asia</a></li>
										<li><a href="/catalog/brand/tyumen_premium/">Tyumen Premium</a></li>
									</ul>
								</li>
								<li><a href="/catalog/brand/uno/">UNO</a></li>
							</ul>

							<ul>

								<li><a href="/catalog/brand/vaiper/">Vaiper</a></li>
								<li>
									<a href="/catalog/brand/varta/">Varta</a>
									<ul>
										<li><a href="/catalog/brand/varta_blue_dynamic/">Varta Blue Dynamic</a></li>
										<li><a href="/catalog/brand/varta_silver_dynamic/">Varta Silver Dynamic</a></li>
									</ul>
								</li>
								<li><a href="/catalog/brand/vektor_plus/">VEKTOR</a></li>
								<li><a href="/catalog/brand/z-power/">Z-Power</a></li>
								<li><a href="/catalog/brand/%D0%B0%D0%BA%D0%BE%D0%BC/">АКОМ</a></li>
							</ul>

							<ul>
								<li><a href="/catalog/brand/%D0%B7%D0%B2%D0%B5%D1%80%D1%8C/">Зверь</a></li>
							</ul>
						</div>

						<div id="tab2" class="tab-contents">
							<ul>
								<li><a href="/catalog/brand/red_energy/">Red Energy</a></li>
								<li><a href="/catalog/brand/yuasa/">YUASA</a></li>
								<li><a href="/catalog/battery/moto_akkumulyatory/forse_moto_agm_sukhozaryazhennyy/">Force</a></li>
								<li><a href="/catalog/battery/moto_akkumulyatory/delta_moto/">Delta</a></li>
								<li><a href="/catalog/battery/moto_akkumulyatory/varta_agm/">Varta</a></li>
								<li><a href="/catalog/battery/moto_akkumulyatory/exide_moto/">Exide</a></li>
								<li><a href="/catalog/battery/moto_akkumulyatory/fiamm_/">FIAMM</a></li>
							</ul>
							<ul>
								<li>
									<a href="/catalog/battery/moto_akkumulyatory/bolk_moto/">BOLK</a>
								</li>
							</ul>
						</div>

						<div id="tab3" class="tab-contents">
							<ul>
								<li><a href="/catalog/brand/black_horse/">Black Horse</a></li>
								<li><a href="/catalog/brand/giver/">Giver</a></li>
								<li><a href="/catalog/brand/vaiper/">Vaiper</a></li>
								<li><a href="/catalog/brand/z-power/">Z-Power</a></li>
								<li><a href="/catalog/battery/gruzovye_akkumulyatory/180_230_a_ch/tyumen_1/">Тюмень</a></li>
								<li><a href="/catalog/battery/gruzovye_akkumulyatory/125_145_a_ch/alphaline_3/">AlphaLINE</a></li>
								<li><a href="/catalog/battery/gruzovye_akkumulyatory/180_230_a_ch/autopower_5/">AUTOPOWER</a></li>





							</ul>
							<ul>

								<li><a href="/catalog/battery/gruzovye_akkumulyatory/125_145_a_ch/vektor_2/">VEKTOR</a></li>
								<li><a href="/catalog/battery/gruzovye_akkumulyatory/180_230_a_ch/contact_2/">Contact</a></li>
								<li><a href="/catalog/battery/gruzovye_akkumulyatory/180_230_a_ch/kainar/">KAINAR</a></li>
								<li><a href="/catalog/battery/gruzovye_akkumulyatory/125_145_a_ch/mutlu_3/">MUTLU</a></li>
								<li><a href="/catalog/battery/gruzovye_akkumulyatory/125_145_a_ch/varta_3/">VARTA</a></li>
								<li><a href="/catalog/battery/gruzovye_akkumulyatory/180_230_a_ch/hugel_action/">HUGEL</a></li>
								<li><a href="/catalog/battery/gruzovye_akkumulyatory/180_230_a_ch/topla/">TOPLA</a></li>



							</ul>
						</div>

						<div id="tab4" class="tab-contents">
							<ul>
								<li><a href="/catalog/brand/delta/">Delta</a></li>
								<li><a href="/catalog/brand/security_force/">Security Force</a></li>
								<li><a href="/catalog/brand/%D0%B2%D0%BE%D1%81%D1%82%D0%BE%D0%BA/">Восток</a></li>
								<li><a href="/catalog/brand/ventura/">Ventura</a></li>
							</ul>
						</div>

						<div id="tab5" class="tab-contents">
							<ul>
								<li><a href="/catalog/auto/audi/">Audi</a></li>
								<li><a href="/catalog/auto/bmw/">BMW</a></li>
								<li><a href="/catalog/auto/chevrolet/">Chevrolet</a></li>
								<li><a href="/catalog/auto/citroen/">Citroen</a></li>
								<li><a href="/catalog/auto/daewoo/">Daewoo</a></li>
								<li><a href="/catalog/auto/ford/">Ford</a></li>
								<li><a href="/catalog/auto/honda/">Honda</a></li>
							</ul>

							<ul>
								<li><a href="/catalog/auto/hyundai/">Hyundai</a></li>
								<li><a href="/catalog/auto/infiniti/">Infiniti</a></li>
								<li><a href="/catalog/auto/jeep/">Jeep</a></li>
								<li><a href="/catalog/auto/kia/">Kia</a></li>
								<li><a href="/catalog/auto/land_rover/">Land Rover</a></li>
								<li><a href="/catalog/auto/lexus/">Lexus</a></li>
								<li><a href="/catalog/auto/mazda/">Mazda</a></li>
							</ul>

							<ul>
								<li><a href="/catalog/auto/mercedes_benz/">Mercedes-Benz</a></li>
								<li><a href="/catalog/auto/mini/">Mini</a></li>
								<li><a href="/catalog/auto/mitsubishi/">Mitsubishi</a></li>
								<li><a href="/catalog/auto/nissan/">Nissan</a></li>
								<li><a href="/catalog/auto/opel/">Opel</a></li>
								<li><a href="/catalog/auto/peugeot/">Peugeot</a></li>
								<li><a href="/catalog/auto/renault/">Renault</a></li>
							</ul>

							<ul>
								<li><a href="/catalog/auto/skoda/">Skoda</a></li>
								<li><a href="/catalog/auto/subaru/">Subaru</a></li>
								<li><a href="/catalog/auto/suzuki/">Suzuki</a></li>
								<li><a href="/catalog/auto/toyota/">Toyota</a></li>
								<li><a href="/catalog/auto/volkswagen/">Volkswagen</a></li>
								<li><a href="/catalog/auto/volvo/">Volvo</a></li>
								<li><a href="/catalog/auto/vaz/">ВАЗ</a></li>
							</ul>

							<ul>
								<li><a href="/catalog/auto/oka/">ОКА</a></li>
								<li><a href="/catalog/auto/uaz/">УАЗ</a></li>
							</ul>

						</div>

						<div id="tab6" class="tab-contents">
							<ul>
								<li><a href="/catalog/battery/">Легковые</a></li>
								<li><a href="/catalog/battery/gruzovye_akkumulyatory/">Грузовые</a></li>
								<li>
									<a href="/catalog/battery/gelevye/">Гелевые</a>
									<ul>
										<li><a href="/catalog/battery/gelevye/auto/">для автомобиля</a></li>
										<li><a href="/catalog/battery/gelevye/akkumulyatory_dlya_kotlov/">для котлов</a></li>
									</ul>
								</li>
								<li><a href="/catalog/battery/gelevye/akb/">ИБП (UPS)</a></li>
								<li><a href="/catalog/battery/moto_akkumulyatory/">Для мотоцикла</a></li>
							</ul>
						</div>
					</div>
				</div>

				<div id="tabs__menu-2" class="tabs__menu-2">
					<div class="tabs__sub__menu-wrap tabs__sub__menu-wrap-1">
						<ul class="tabs__sub__menu tabs__sub__menu">
							<?
							$conNum = 1;
							$i = 0;
							$accCount = count($arResult['CATALOG_TREE']['accessories']['ITEMS']);
							$colCnt = ceil($accCount / 3);
							foreach ($arResult['CATALOG_TREE']['accessories']['ITEMS'] as $sec){
								$file = array();
								if ($sec['PICTURE']){
									$file = CFile::ResizeImageGet($sec['PICTURE'], array('width' => 70, 'height'  => 60), 1);
								}
								$i++;
							?>
							<li class="tabs__sub__menu__li">
								<a href="<?=$sec['SECTION_PAGE_URL']?>" class="tabs__sub__menu-li__link">
									<span class="tabs__sub__menu__li-img" <?if($file['src']){?>style="background-image:url(<?=$file['src']?>);"<?}?>></span>
									<span class="tabs__sub__menu__li-name"><?=$sec['NAME']?></span>
								</a>
							</li>
							<?
							if ($i % $colCnt == 0){
								$conNum++;
								?>
						</ul>
					</div>
					<div class="tabs__sub__menu-wrap tabs__sub__menu-wrap-<?=$conNum?>">
						<ul class="tabs__sub__menu">
							<?
						}
						?>
						<? } ?>
						</ul>
					</div>
				</div>

				<div id="tabs__menu-3" class="tabs__menu-3">

						<div class="tabs-container">
							<ul class="tabs-new">
								<li class="tab-links current" data-tab="tab7"><a href="javasript:void(0);">Виды масел</a></li>
								<li class="tab-links" data-tab="tab8"><a href="javasript:void(0);">Бренды масел</a></li>
							</ul>

							<div id="tab7" class="tab-contents current">
								<ul class="tabs__sub__menu">
									<? foreach ($arResult['CATALOG_TREE']['masla']['ITEMS'] as $sec){
										$file = array();
										if ($sec['PICTURE']){
											$file = CFile::ResizeImageGet($sec['PICTURE'], array('width' => 278, 'height'  => 165), 1);
										}
										?>
										<li class="tabs__sub__menu__li">
											<a href="<?=$sec['SECTION_PAGE_URL']?>" class="tabs__sub__menu-li__link">
												<span class="tabs__sub__menu__li-img" <?if($file['src']){?>style="background-image:url(<?=$file['src']?>);"<?}?>></span><?=$sec['NAME']?></a>
											</li>
											<? } ?>
										</ul>
									</div>

									<div id="tab8" class="tab-contents">
										<div class="owl-carousel owl-theme">

											<?$APPLICATION->IncludeComponent(
												"bitrix:catalog.smart.filter",
												"vertical_masla_brand",
												array(
													/*"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],*/
										"IBLOCK_ID" => CATALOG_ID,//$arParams["IBLOCK_ID"],
										/*"SECTION_ID" => $currentRootSection["ID"],*/
										"FILTER_NAME" => "arrFilter",
										/*"PRICE_CODE" => $arParams["PRICE_CODE"],*/
										"CACHE_TYPE" => "N",
										"CACHE_TIME" => "36000000",
										"CACHE_NOTES" => "",
										"CACHE_GROUPS" => "N",
										"SAVE_IN_SESSION" => "N",
										/*"DOP_FILTER" => $sectionName,
										"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"]*/
										),
												$component,
												array('HIDE_ICONS' => 'Y')
												);?>

								<!--<div>
									<a href="/catalog/masla/mobil/"><img src="/bitrix/templates/akk/images/oils/oil_mobil.png" alt="mobil"></a>
									<a href="/catalog/masla/mobil/">Mobil</a>
								</div>
								<div>
									<a href="/catalog/masla/shell/"><img src="/bitrix/templates/akk/images/oils/oil_shell.png" alt="shell"></a>
									<a href="/catalog/masla/shell/">Shell</a>
								</div>
								<div>
									<a href="/catalog/masla/castrol/"><img src="/bitrix/templates/akk/images/oils/oil_castrol.png" alt="castrol"></a>
									<a href="/catalog/masla/castrol/">Castrol</a>
								</div>
								<div>
									<a href="/catalog/masla/mercedes-benz/"><img src="/bitrix/templates/akk/images/oils/oil_mercedes-benz.png" alt="mercedes-benz"></a>
									<a href="/catalog/masla/mercedes-benz/">Mercedes-Benz </a>
								</div>
								<div>
									<a href="/catalog/masla/bmw/"><img src="/bitrix/templates/akk/images/oils/oil_bmw.png" alt="bmw"></a>
									<a href="/catalog/masla/bmw/">BMW</a>
								</div>-->
							</div>
						</div>

					</div>

				</div>
			</div>

			<?


			$url_no_view = array(
				'/catalog/battery/varta/',
				'/catalog/battery/zver/',
				'/catalog/battery/mutlu/',
				'/catalog/battery/titan/',
				'/catalog/battery/bosch/',
				'/catalog/battery/tyumen/',
				'/catalog/battery/bolk_moto/',
				'/catalog/battery/tudor/',
				'/catalog/battery/solite/',
				'/catalog/battery/vaiper/',
				'/catalog/battery/royal/',
				'/catalog/battery/sebang/',
				'/catalog/battery/cobat/',
				'/catalog/battery/aktex/',
				'/catalog/battery/afa/',
				'/catalog/avtomobilnye/',
				);
				?>
				<div id="sticky-anchor"></div>

				<?$page_uri = $_SERVER["REQUEST_URI"];?>

				<?if(strstr($page_uri, "/catalog/battery/") && strstr($page_uri, "/gruzovye_akkumulyatory/")):?>
				<div class="brand__tabs">
					<ul class="tabs">
						<?foreach($arResult["CATALOG_TREE"]["battery"]["ITEMS"]["gruzovye_akkumulyatory"]["ITEMS"] as $key=>$arChild):?>
						<?if(strstr($page_uri, $arChild["SECTION_PAGE_URL"])):?>
						<li class="tab-link current" data-tab="tab-<?=$key?>">
							<?=$arChild["NAME"]?>
						</li>
						<?else:?>
						<li class="tab-link" data-tab="tab-<?=$key?>">
							<?=$arChild["NAME"]?>
						</li>
						<?endif;?>
						<?endforeach;?>
					</ul>
					<?foreach($arResult["CATALOG_TREE"]["battery"]["ITEMS"]["gruzovye_akkumulyatory"]["ITEMS"] as $key=>$arChild):?>
					<div id="tab-<?=$key?>" class="tab-content <?if(strstr($page_uri, $arChild["SECTION_PAGE_URL"])):?>current<?endif;?>">
						<ul class="tabs_inner_list">
							<?foreach($arChild["ITEMS"] as $subkey=>$subChild):?>
							<?if(strstr($page_uri, $subChild["SECTION_PAGE_URL"])):?>
							<li>
								<span class="active"><?=$subChild["NAME"]?></span>
							</li>
							<?else:?>
							<li>
								<a href="<?=$subChild["SECTION_PAGE_URL"]?>" class=""><?=$subChild["NAME"]?></a>
							</li>
							<?endif;?>
							<?endforeach?>
						</ul>
					</div>
					<?endforeach;?>
				</div>
				<?endif;?>

				<?if(strstr($page_uri, "/catalog/battery/") && strstr($page_uri, "/moto_akkumulyatory/")):?>
				<div class="brand__tabs">
					<ul class="tabs">
						<?foreach($arResult["CATALOG_TREE"]["battery"]["ITEMS"]["moto_akkumulyatory"]["ITEMS"] as $key=>$arChild):?>
						<?if(strstr($page_uri, $arChild["SECTION_PAGE_URL"])):?>
						<li class="tab-link current" data-tab="tab-<?=$key?>">
							<?=$arChild["NAME"]?>
						</li>
						<?else:?>
						<li class="tab-link" data-tab="tab-<?=$key?>">
							<a href="<?=$arChild["SECTION_PAGE_URL"]?>" class=""><?=$arChild["NAME"]?></a>
						</li>
						<?endif;?>
						<?endforeach;?>
					</ul>
				</div>
				<?endif;?>

				<?if(strstr($page_uri, "/catalog/battery/") && strstr($page_uri, "/gelevye/")):?>
				<div class="brand__tabs">
					<ul class="tabs">
						<?foreach($arResult["CATALOG_TREE"]["battery"]["ITEMS"]["gelevye"]["ITEMS"] as $key=>$arChild):?>
						<?if(strstr($page_uri, $arChild["SECTION_PAGE_URL"])):?>
						<li class="tab-link current" data-tab="tab-<?=$key?>">
							<?=$arChild["NAME"]?>
						</li>
						<?else:?>
						<li class="tab-link" data-tab="tab-<?=$key?>">
							<a href="<?=$arChild["SECTION_PAGE_URL"]?>" class=""><?=$arChild["NAME"]?></a>
						</li>
						<?endif;?>
						<?endforeach;?>
					</ul>
				</div>
				<?endif;?>

				<?if(strstr($page_uri, "/") || (strstr($page_uri, "/catalog/battery/") && !strstr($page_uri, "/gruzovye_akkumulyatory/") && !strstr($page_uri, "/moto_akkumulyatory/") && !strstr($page_uri, "/gelevye/"))):?>
				<div class="brand__tabs">
					<ul class="tabs">
						<?$first=true;?>
						<?$first2=0;?>
						<?foreach($arResult['ITEMS'] as $itemIdex => $arItem):?>
						<?if($arItem["TEXT"]=="Каталог аккумуляторов"):?>
						<?if($arItem["HAS_CHILDREN"] && count($arItem["CHILDREN"])>0):?>
						<?foreach($arItem["CHILDREN"] as $key=>$arChild):?>
						<?if($arChild['NAME'] != 'Гелевые аккумуляторы' && $arChild['NAME'] != 'Мото аккумуляторы' && $arChild['NAME'] != 'Грузовые аккумуляторы'):?>
						<?if($arChild["SECTION_PAGE_URL"] == $page_uri):?>
						<li class="tab-link current" data-tab="tab-<?=$key?>">
							<?=$arChild["NAME"]?>
						</li>
						<?else:?>
						<li class="tab-link" data-tab="tab-<?=$key?>">
							<?=$arChild["NAME"]?>
						</li>
						<?endif;?>
						<?endif;?>
						<?endforeach;?>
						<?endif;?>
						<?endif;?>
						<?endforeach;?>
					</ul>

					<?foreach($arResult['ITEMS'] as $itemIdex => $arItem):?>
					<?if($arItem["TEXT"]=="Каталог аккумуляторов"):?>
					<?if($arItem["HAS_CHILDREN"] && count($arItem["CHILDREN"])>0):?>
					<?foreach($arItem["CHILDREN"] as $key=>$arChild){?>
					<?if($arChild['NAME'] != 'Гелевые аккумуляторы' && $arChild['NAME'] != 'Мото аккумуляторы' && $arChild['NAME'] != 'Грузовые аккумуляторы'):?>
					<div id="tab-<?=$key?>" class="tab-content <?if(strstr($page_uri, $arChild["SECTION_PAGE_URL"])):?>current<?endif;?>">
						<ul class="tabs_inner_list">
							<?if(count($arResult["CHILDREN"][$arChild["ID"]])>0):?>
							<?foreach($arResult["CHILDREN"][$arChild["ID"]] as $child):?>
							<?if($child["SECTION_PAGE_URL"] == $page_uri):?>
							<li>
								<span class="active"><?=$child["NAME"]?></span>
							</li>
							<?else:?>
							<li>
								<a href="<?=$child["SECTION_PAGE_URL"]?>" class=""><?=$child["NAME"]?></a>
							</li>
							<?endif;?>
							<?endforeach;?>
							<?endif;?>
						</ul>
					</div>
					<?endif;?>
					<?}?>
					<?endif;?>
					<?endif;?>
					<?endforeach;?>
				</div>
				<?endif;?>

				<?if(strstr($page_uri, "/catalog/accessories/")):?>
				<?
					/*print_r("<pre>");
					print_r($arResult["CATALOG_TREE"]["accessories"]["ITEMS"]);
					print_r("</pre>");*/
					?>
					<div class="brand__tabs">
						<ul class="tabs">
							<?foreach($arResult["CATALOG_TREE"]["accessories"]["ITEMS"] as $key=>$arChild):?>
							<?if(strstr($page_uri, $arChild["SECTION_PAGE_URL"])):?>
							<li class="tab-link current" data-tab="tab-<?=$key?>">
								<?if(count($arChild["ITEMS"])):?>
								<?=$arChild["NAME"]?>
								<?else:?>
								<a href="<?=$arChild["SECTION_PAGE_URL"]?>" class=""><?=$arChild["NAME"]?></a>
								<?endif;?>
							</li>
							<?else:?>
							<li class="tab-link" data-tab="tab-<?=$key?>">
								<?if(count($arChild["ITEMS"])):?>
								<?=$arChild["NAME"]?>
								<?else:?>
								<a href="<?=$arChild["SECTION_PAGE_URL"]?>" class=""><?=$arChild["NAME"]?></a>
								<?endif;?>
							</li>
							<?endif;?>
							<?endforeach;?>
							<?foreach($arResult["CATALOG_TREE"]["accessories"]["ITEMS"] as $key=>$arChild):?>
							<?if(count($arChild["ITEMS"])):?>
							<div id="tab-<?=$key?>" class="tab-content <?if(strstr($page_uri, $arChild["SECTION_PAGE_URL"])):?>current<?endif;?>">
								<ul class="tabs_inner_list">
									<?foreach($arChild["ITEMS"] as $subkey=>$subChild):?>
									<?if(strstr($page_uri, $subChild["SECTION_PAGE_URL"])):?>
									<li>
										<span class="active"><?=$subChild["NAME"]?></span>
									</li>
									<?else:?>
									<li>
										<a href="<?=$subChild["SECTION_PAGE_URL"]?>" class=""><?=$subChild["NAME"]?></a>
									</li>
									<?endif;?>
									<?endforeach?>
								</ul>
							</div>
							<?endif;?>
							<?endforeach;?>
						</ul>
					</div>
					<?endif;?>

				</div>
			</div>
		</nav>
