<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CModule::IncludeModule("iblock");
global $APPLICATION;

$childs = array();
$items = array();

foreach($arResult as $key => $arItem)
{
    $level2 = 0;
    $SectionID = 0;
    $items[$key] = $arItem;
    $items[$key]['HAS_CHILDREN'] = false;

    if($arItem["TEXT"]=="Каталог аккумуляторов")
    {
        $SectionID = 18;
    }

    if($arItem["TEXT"]=="Аксессуары для автомобиля")
    {
        $SectionID = 19;
    }


	if($arItem["TEXT"]=="Масла и технические жидкости")
    {
        $SectionID = 351;
    }


    if($SectionID>0)
    {
        $arSections = array();
        $rsParentSection = CIBlockSection::GetByID($SectionID);

        if ($arParentSection = $rsParentSection->GetNext())
        {
            if(strpos($APPLICATION->GetCurDir(), $arParentSection["SECTION_PAGE_URL"])!==false)
            {
                $items[$key]["SELECTED"] = "Y";
            }

            $arFilter = array(
                'IBLOCK_ID' => CATALOG_ID,
                '>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'],
                '<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'],
                '>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL'],
                '<DEPTH_LEVEL' => 4,
                'GLOBAL_ACTIVE' => "Y"
            );
            $rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'), $arFilter, false);
            while ($arSect = $rsSect->GetNext())
            {
                if($arSect["DEPTH_LEVEL"]==2)
                {
			$items[$key]['HAS_CHILDREN'] = true;
			$items[$key]['CHILDREN'][] = $arSect;
                    $level2++;
                }else{
                    $children[$arSect["IBLOCK_SECTION_ID"]][] = $arSect;
                }

            }
        }
    }
}

$arResult = array();

$arResult["ITEMS"] = $items;
$arResult['CHILDREN'] = $children;

$res = CIBlockSection::GetList(
	array('NAME' => 'ASC'),
	array('IBLOCK_ID' => CATALOG_ID, 'ACTIVE' => 'Y'/*, 'GLOBAL_ACTIVE' => 'Y'*/),
	false,
	array('ID', 'NAME', 'PICTURE', 'IBLOCK_SECTION_ID', 'SECTION_PAGE_URL')
);
while ($sec = $res->GetNext()){
	$sections[intval($sec['IBLOCK_SECTION_ID'])][$sec['ID']] = $sec;
}
if (!function_exists('buildTree')) {
	function buildTree($cats, $parent_id)
	{
		$res = array();
		if (is_array($cats) and isset($cats[$parent_id])) {
			foreach ($cats[$parent_id] as $cat) {
				$el = $cat;
				$el['ITEMS'] = buildTree($cats, $cat['ID']);
				$res[$cat['CODE']] = $el;
			}
		}
		return $res;
	}
}

foreach ($sections[0] as $sec){
	$secTree[$sec['CODE']] = $sec;
	$secTree[$sec['CODE']]['ITEMS'] = buildTree($sections, $sec['ID']);
}

$avtomobilnye_akkumulyatory = array();
$avt_item_emkost = array();
$column_num = 0;
$row_num = 0;
foreach($secTree["battery"]["ITEMS"]["auto-akb"]["ITEMS"] as $avt_item){
	if(intval($avt_item["NAME"])){
		$avt_item_emkost[intval($avt_item["NAME"])] = $avt_item;
		continue;
	}
	if($row_num == 7){
		$row_num  = 0;
		$column_num++;
	}
	$avtomobilnye_akkumulyatory[$column_num][$row_num] = $avt_item;
	$row_num++;
}
$secTree["battery"]["ITEMS"]["auto-akb"]["ITEMS"] = $avtomobilnye_akkumulyatory;
ksort($avt_item_emkost);
$secTree["battery"]["ITEMS"]["auto-akb-emkost"]["ITEMS"] = $avt_item_emkost;

$moto_akkumulyatory = array();
$column_num = 0;
$row_num = 0;
foreach($secTree["battery"]["ITEMS"]["moto-akb"]["ITEMS"] as $moto_item){
	if($row_num == 7){
		$row_num  = 0;
		$column_num++;
	}
	$moto_akkumulyatory[$column_num][$row_num] = $moto_item;
	$row_num++;
}
$secTree["battery"]["ITEMS"]["moto-akb"]["ITEMS"] = $moto_akkumulyatory;

$gruzovye_akkumulyatory = array();
$column_num = 0;
$row_num = 0;
$gruz_item_emkost = array();
foreach($secTree["battery"]["ITEMS"]["truck-akb"]["ITEMS"] as $gruz_item){
	if(intval($gruz_item["NAME"])){
		$gruz_item_emkost[] = $gruz_item;
		continue;
	}
	if($row_num == 7){
		$row_num  = 0;
		$column_num++;
	}
	$gruzovye_akkumulyatory[$column_num][$row_num] = $gruz_item;
	$row_num++;
}
$secTree["battery"]["ITEMS"]["truck-akb"]["ITEMS"] = $gruzovye_akkumulyatory;
$secTree["battery"]["ITEMS"]["truck-akb-emkost"]["ITEMS"] = $gruz_item_emkost;

$akb = array();
$column_num = 0;
$row_num = 0;
foreach($secTree["battery"]["ITEMS"]["akb_dlya_ups"]["ITEMS"] as $akb_item){
	if($row_num == 7){
		$row_num  = 0;
		$column_num++;
	}
	$akb[$column_num][$row_num] = $akb_item;
	$row_num++;
}
$secTree["battery"]["ITEMS"]["akb_dlya_ups"]["ITEMS"] = $akb;

$arResult['CATALOG_TREE'] = $secTree;
if($_GET["debug"]){
	CDev::pre($secTree);
}