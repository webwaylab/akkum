<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$currentRootSection["NAME"] = "";
?>

	<br><br><h1>Аккумуляторы</h1><br><br>

<div class="select__brands__auto">
  <h2 class="select__brands__auto-h2">Подбор по марке авто</h2>
<div id="tabs__brands" class="clearfix">
	<ul class="tabs__brands-nav clearfix">
		<li><a href="#tabs__brands-item1" title="легковые" class="tabs__brands-nav__li">легковые</a></li>
		<li><a href="#tabs__brands-item2" title="грузовые" class="tabs__brands-nav__li">грузовые</a></li>
		<li><a href="#tabs__brands-item3" title="автобусы" class="tabs__brands-nav__li">автобусы</a></li>
		<li><a href="#tabs__brands-item3" title="мотоциклы" class="tabs__brands-nav__li">мотоциклы</a></li>
	</ul>
	<div id="tabs__brands-item1" class="tabs__brands-content clearfix">
		<div class="tabs__brands-content-link spoiler-title closed"><span class="">Все марки</span></div>
		<div class="brand-list tabs__brands-content__brand-list  spoiler-announce">
            <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
				<?if($key==18):?>
					</div>
					<div class="brand-list tabs__brands-content__brand-list spoiler-body clearfix">
				<?endif;?>
    			<a href="catalog/auto/<?=Cutil::translit($arItem["NAME"], "ru", array("replace_space"=>"_","replace_other"=>"_"));?>/" class="item tabs__brands-content__brand-item">
        			<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="" data-pin-nopin="true">
        			<div class="name"><?=$arItem["NAME"]?></div> 
    			</a>
            <?endforeach;?>        
		</div>
	</div>
	<div id="tabs__brands-item2" class="tabs__brands-content clearfix">
		<div class="tabs__brands-content-link spoiler-title closed"><span class="">Все марки</span></div>
		<div class="brand-list tabs__brands-content__brand-list  spoiler-announce">
            <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
				<?if($key==18):?>
					</div>
					<div class="brand-list tabs__brands-content__brand-list spoiler-body clearfix">
				<?endif;?>
    			<a href="catalog/auto/<?=Cutil::translit($arItem["NAME"], "ru", array("replace_space"=>"_","replace_other"=>"_"));?>/" class="item tabs__brands-content__brand-item">
        			<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="" data-pin-nopin="true">
        			<div class="name"><?=$arItem["NAME"]?></div> 
    			</a>
            <?endforeach;?>        
		</div>
	</div>
	<div id="tabs__brands-item3" class="tabs__brands-content clearfix">   
		<div class="tabs__brands-content-link spoiler-title closed"><span class="">Все марки</span></div>
		<div class="brand-list tabs__brands-content__brand-list  spoiler-announce">
            <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
				<?if($key==18):?>
		</div>
		<div class="brand-list tabs__brands-content__brand-list spoiler-body clearfix">
				<?endif;?>
    			<a href="catalog/auto/<?=Cutil::translit($arItem["NAME"], "ru", array("replace_space"=>"_","replace_other"=>"_"));?>/" class="item tabs__brands-content__brand-item">
        			
					<span class="" style="background-image:url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>');" data-pin-nopin="true"></span>
					
        			<div class="name"><?=$arItem["NAME"]?></div> 
    			</a>
            <?endforeach;?>        
		</div>
	</div>
</div>
</div>

<div class="select__brands__auto select__brands__auto-akk">
  <h2 class="select__brands__auto-h2">Подбор по брендам</h2>
	<div class="tabs__brands-content clearfix">
		<div class="tabs__brands-content-link  spoiler-title-akk closed"><span class="">Все бренды</span></div>
		<div class="brand-list tabs__brands-content__brand-list  spoiler-announce">
			<? 
			$i = 0;
			foreach ($arResult['BRANDS'] as $brand) { ?>
				<?if($i == 18):?>
		</div>
		<div class="brand-list tabs__brands-content__brand-list spoiler-body-akk clearfix">
				<?endif;?>
				<?if(file_exists($_SERVER["DOCUMENT_ROOT"].$brand['FILE'])):?>
				<a href="<?=$brand['URL']?>" class="item tabs__brands-content__brand-item">
        			<span class="" style="background-image:url('<?=$brand['FILE']?>');" data-pin-nopin="true"></span>
        			<div class="name"><?=$brand["NAME"]?></div> 
    			</a>
				<?endif;?>
			<? $i++; 
			} ?>      
		</div>
	</div>
</div>

<?//p($arResult['BRANDS']);?>