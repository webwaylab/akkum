$(document).ready(function() {
	$("#menu_fl_auto .owl-carousel").owlCarousel({
		autoPlay: false,
		navigation:true,
		items : 7,
		margin: 15,
		loop:true,
		itemsDesktop : [1199,4],
		itemsDesktopSmall : false
	});
});  