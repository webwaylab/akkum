<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$listItems = array();
$column_num = 0;
$row_num = 0;
//CDev::pre(count($arResult['ITEMS']));

$arResult['ITEMS'][] = array(
    'NAME' => 'Все марки',
    'DETAIL_PAGE_URL' => '/catalog/auto/',
    'DETAIL_PAGE_URL_OVERRIDE' => true
);

foreach($arResult['ITEMS'] as $item){
	if($row_num == 7){
		$row_num  = 0;
		$column_num++;
	}
	$listItems[$column_num][$row_num] = $item;
	$row_num++;
}
$arResult['ITEMS'] = $listItems;