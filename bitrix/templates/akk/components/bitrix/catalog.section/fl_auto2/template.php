<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$currentRootSection["NAME"] = "";

// Override PAGEN_*
// Needed for PAGEN_1 in irbis:catalog component
global $NavNum;
$NavNum = 0;
?>

<div id="menu_fl_auto">
	<div class="owl-carousel owl-theme">
		<?foreach($arResult['ITEMS'] as $column):?>
			<div>
				<ul>
					<?foreach($column as $row):?>
						<li>
                            <?php if ($row['DETAIL_PAGE_URL_OVERRIDE'] == true) { ?>
                                <a href="<?= $row['DETAIL_PAGE_URL']; ?>">
                            <?php } else { ?>
                                <a href="/catalog/auto/<?=Cutil::translit($row["NAME"], "ru", array("replace_space"=>"_","replace_other"=>"_"));?>/">
                            <?php } ?>
								<?=$row["NAME"]?>
							</a>
						</li>
					<?endforeach;?>
				</ul>
			</div>
		<?endforeach;?>
	</div>
</div>
<?//CDev::pre($arResult['ITEMS']);?>