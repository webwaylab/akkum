<div class = "text-top"></div>

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
//var_dump($arResult['ITEMS']);
if (!empty($arResult['ITEMS']))
{
	$templateLibrary = array('popup');
	$currencyList = '';
	if (!empty($arResult['CURRENCIES']))
	{
		$templateLibrary[] = 'currency';
		$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
	}
	$templateData = array(
		'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
		'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME'],
		'TEMPLATE_LIBRARY' => $templateLibrary,
		'CURRENCIES' => $currencyList
	);
	unset($currencyList, $templateLibrary);

	$arSkuTemplate = array();
	if (!empty($arResult['SKU_PROPS']))
	{
		foreach ($arResult['SKU_PROPS'] as &$arProp)
		{
			$templateRow = '';
			if ('TEXT' == $arProp['SHOW_MODE'])
			{
				if (5 < $arProp['VALUES_COUNT'])
				{
					$strClass = 'bx_item_detail_size full';
					$strWidth = ($arProp['VALUES_COUNT']*20).'%';
					$strOneWidth = (100/$arProp['VALUES_COUNT']).'%';
					$strSlideStyle = '';
				}
				else
				{
					$strClass = 'bx_item_detail_size';
					$strWidth = '100%';
					$strOneWidth = '20%';
					$strSlideStyle = 'display: none;';
				}
				$templateRow .= '<div class="'.$strClass.'" id="#ITEM#_prop_'.$arProp['ID'].'_cont">'.
				'<span class="bx_item_section_name_gray">'.htmlspecialcharsex($arProp['NAME']).'</span>'.
				'<div class="bx_size_scroller_container"><div class="bx_size"><ul id="#ITEM#_prop_'.$arProp['ID'].'_list" style="width: '.$strWidth.';">';
				foreach ($arProp['VALUES'] as $arOneValue)
				{
					$arOneValue['NAME'] = htmlspecialcharsbx($arOneValue['NAME']);
					$templateRow .= '<li data-treevalue="'.$arProp['ID'].'_'.$arOneValue['ID'].'" data-onevalue="'.$arOneValue['ID'].'" style="width: '.$strOneWidth.';" title="'.$arOneValue['NAME'].'"><i></i><span class="cnt">'.$arOneValue['NAME'].'</span></li>';
				}
				$templateRow .= '</ul></div>'.
				'<div class="bx_slide_left" id="#ITEM#_prop_'.$arProp['ID'].'_left" data-treevalue="'.$arProp['ID'].'" style="'.$strSlideStyle.'"></div>'.
				'<div class="bx_slide_right" id="#ITEM#_prop_'.$arProp['ID'].'_right" data-treevalue="'.$arProp['ID'].'" style="'.$strSlideStyle.'"></div>'.
				'</div></div>';
			}
			elseif ('PICT' == $arProp['SHOW_MODE'])
			{
				if (5 < $arProp['VALUES_COUNT'])
				{
					$strClass = 'bx_item_detail_scu full';
					$strWidth = ($arProp['VALUES_COUNT']*20).'%';
					$strOneWidth = (100/$arProp['VALUES_COUNT']).'%';
					$strSlideStyle = '';
				}
				else
				{
					$strClass = 'bx_item_detail_scu';
					$strWidth = '100%';
					$strOneWidth = '20%';
					$strSlideStyle = 'display: none;';
				}
				$templateRow .= '<div class="'.$strClass.'" id="#ITEM#_prop_'.$arProp['ID'].'_cont">'.
				'<span class="bx_item_section_name_gray">'.htmlspecialcharsex($arProp['NAME']).'</span>'.
				'<div class="bx_scu_scroller_container"><div class="bx_scu"><ul id="#ITEM#_prop_'.$arProp['ID'].'_list" style="width: '.$strWidth.';">';
				foreach ($arProp['VALUES'] as $arOneValue)
				{
					$arOneValue['NAME'] = htmlspecialcharsbx($arOneValue['NAME']);
					$templateRow .= '<li data-treevalue="'.$arProp['ID'].'_'.$arOneValue['ID'].'" data-onevalue="'.$arOneValue['ID'].'" style="width: '.$strOneWidth.'; padding-top: '.$strOneWidth.';"><i title="'.$arOneValue['NAME'].'"></i>'.
					'<span class="cnt"><span class="cnt_item" style="background-image:url(\''.$arOneValue['PICT']['SRC'].'\');" title="'.$arOneValue['NAME'].'"></span></span></li>';
				}
				$templateRow .= '</ul></div>'.
				'<div class="bx_slide_left" id="#ITEM#_prop_'.$arProp['ID'].'_left" data-treevalue="'.$arProp['ID'].'" style="'.$strSlideStyle.'"></div>'.
				'<div class="bx_slide_right" id="#ITEM#_prop_'.$arProp['ID'].'_right" data-treevalue="'.$arProp['ID'].'" style="'.$strSlideStyle.'"></div>'.
				'</div></div>';
			}
			$arSkuTemplate[$arProp['CODE']] = $templateRow;
		}
		unset($templateRow, $arProp);
	}

	if ($arParams["DISPLAY_TOP_PAGER"])
	{
		?><? echo $arResult["NAV_STRING"]; ?><?
	}

	$strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
	$strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
	$arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));
?>


<?/*<div class="bx_catalog_list_home col<? echo $arParams['LINE_ELEMENT_COUNT']; ?> <? echo $templateData['TEMPLATE_CLASS']; ?>">*/?>

<div class="products clearfix">

	<?
	foreach ($arResult['ITEMS'] as $key => $arItem)
	{
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
		$strMainID = $this->GetEditAreaId($arItem['ID']);

    //CDev::pre($arItem);

	$arItemIDs = array(
		'ID' => $strMainID,
		'PICT' => $strMainID.'_pict',
		'SECOND_PICT' => $strMainID.'_secondpict',
		'STICKER_ID' => $strMainID.'_sticker',
		'SECOND_STICKER_ID' => $strMainID.'_secondsticker',
		'QUANTITY' => $strMainID.'_quantity',
		'QUANTITY_DOWN' => $strMainID.'_quant_down',
		'QUANTITY_UP' => $strMainID.'_quant_up',
		'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
		'BUY_LINK' => $strMainID.'_buy_link',
		'BASKET_ACTIONS' => $strMainID.'_basket_actions',
		'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
		'SUBSCRIBE_LINK' => $strMainID.'_subscribe',
		'COMPARE_LINK' => $strMainID.'_compare_link',

		'PRICE' => $strMainID.'_price',
		'DSC_PERC' => $strMainID.'_dsc_perc',
		'SECOND_DSC_PERC' => $strMainID.'_second_dsc_perc',
		'PROP_DIV' => $strMainID.'_sku_tree',
		'PROP' => $strMainID.'_prop_',
		'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
		'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
	);

	$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);

	$productTitle = (
		isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])&& $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
		? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
		: $arItem['NAME']
	);
	$imgTitle = (
		isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
		? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
		: $arItem['NAME']
	);

    $changePrice = $arItem["PROPERTIES"]["PRICE_CHANGE"]["VALUE"];
	?>

	<div class="product_item-wrap">
		<figure data-href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="product_item maxheight<?if($arItem["PROPERTIES"]["SALELEADER"]["VALUE"]=="да" || !empty($changePrice)):?> sale<?endif;?>" id="<? echo $strMainID; ?>">
			<div class="wrap_inner_figure">
				<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
					<div class="product_label-box">
						<?if($arItem["PROPERTIES"]["SALELEADER"]["VALUE"]=="да"):?>
							<span class="product_label product_label--hit"
								title="<?=$arItem["PROPERTIES"]["SALELEADER"]["NAME"];?>"></span>
						<?endif;?>
						<?if($arItem["PROPERTIES"]["FREESHIPPING"]["VALUE"]=="да"):?>
							<span class="product_label product_label--delivery"
								title="<?=$arItem["PROPERTIES"]["FREESHIPPING"]["NAME"];?>"></span>
						<?endif;?>
						<?if($arItem["PROPERTIES"]["SPECIALOFFER"]["VALUE"]=="да"):?>
							<span class="product_label product_label--present"
								title="<?=$arItem["PROPERTIES"]["SPECIALOFFER"]["NAME"];?>"></span>
						<?endif;?>
					</div>

					<div class="product_item_img">
						<?
						if ($arParams['DISPLAY_COMPARE'])
						{
						   ?>

						   <span id="<? echo $arItemIDs['COMPARE_LINK']; ?>" class="product_item_similar_link action-block">К сравнению</span>

						   <?
						}
						?>
						<img src="<? echo $arItem['PREVIEW_PICTURE']['SRC']; ?>" alt="" id="<? echo $arItemIDs['SECOND_PICT']; ?>">
					</div>

					<figcaption class="product_item_description">
						<div class="product_item_content-box">
							<div class="product_item_content">
								<?=$arItem['NAME']?>
							</div>

							<span class="product_item_tooltip">
								<?=$arItem['NAME']?>
							</span>
						</div>




						<div class="product_item_perfomance">
							<?if ($arItem['CAN_BUY']):?>
								<span class="can_bay can_buy_label">В наличии</span>
							<?else:?>
								<span class="not_can_bay can_buy_label">Нет в наличии</span>
							<?endif;?>

							<?if($arParams['HEAD_SECTION'] == "budget"):?>
								<?if($arItem['PROPERTIES']["CAPACITY"]['VALUE']):?>
									<div>Ёмкость: <?=$arItem['PROPERTIES']["CAPACITY"]['VALUE']?> А/ч</div>
								<?endif;?>

								<?if($arItem['PROPERTIES']["START_TOK"]['VALUE']):?>
									<div>Пусковой ток: <?=$arItem['PROPERTIES']["START_TOK"]['VALUE']?> А</div>
								<?endif;?>

								<?if($arItem['PROPERTIES']["POLARITY"]['VALUE']):?>
									<div><?=$arItem['PROPERTIES']["POLARITY"]['NAME']?>: <?=$arItem['PROPERTIES']["POLARITY"]['VALUE']?></div>
								<?endif;?>

								<?if (!empty($arItem['PROPERTIES']["LENGTH"]['VALUE']) &&
										!empty($arItem['PROPERTIES']["WIDTH"]['VALUE']) &&
										!empty($arItem['PROPERTIES']["HEIGHT"]['VALUE'])):?>
									<div>Габариты: <?=$arItem['PROPERTIES']["LENGTH"]['VALUE']?>×<?=$arItem['PROPERTIES']["WIDTH"]['VALUE']?>×<?=$arItem['PROPERTIES']["HEIGHT"]['VALUE']?></div>
								<?endif;?>
							<?endif;?>

							<?if($arParams['HEAD_SECTION'] == "accessories"):?>
								<?if($arItem['PROPERTIES']["ARTNUMBER"]['VALUE']):?>
									<div><?=$arItem['PROPERTIES']["ARTNUMBER"]['NAME']?>: <?=$arItem['PROPERTIES']["ARTNUMBER"]['VALUE']?></div>
								<?endif;?>

								<?if($arItem['PROPERTIES']["MANUFACTURE_2"]['VALUE']):?>
									<div><?=$arItem['PROPERTIES']["MANUFACTURE_2"]['NAME']?>: <?=$arItem['PROPERTIES']["MANUFACTURE_2"]['VALUE']?></div>
								<?endif;?>

								<?if($arItem['PROPERTIES']["GRUZOPODEMNOST_DOMKRATY"]['VALUE']):?>
									<div><?=$arItem['PROPERTIES']["GRUZOPODEMNOST_DOMKRATY"]['NAME']?>: <?=$arItem['PROPERTIES']["GRUZOPODEMNOST_DOMKRATY"]['VALUE']?></div>
								<?endif;?>

								<?if($arItem['PROPERTIES']["VYSOTA_PODKHVATA_DOMKRATY"]['VALUE']):?>
									<div><?=$arItem['PROPERTIES']["VYSOTA_PODKHVATA_DOMKRATY"]['NAME']?>: <?=$arItem['PROPERTIES']["VYSOTA_PODKHVATA_DOMKRATY"]['VALUE']?></div>
								<?endif;?>

								<?if($arItem['PROPERTIES']["VYSOTA_PODEMA_DOMKRATY"]['VALUE']):?>
									<div><?=$arItem['PROPERTIES']["VYSOTA_PODEMA_DOMKRATY"]['NAME']?>: <?=$arItem['PROPERTIES']["VYSOTA_PODEMA_DOMKRATY"]['VALUE']?></div>
								<?endif;?>
							<?endif;?>

							<?if($arParams['HEAD_SECTION'] == "masla"):?>
								<?if($arItem['PROPERTIES']["ARTNUMBER"]['VALUE']):?>
									<div><?=$arItem['PROPERTIES']["ARTNUMBER"]['NAME']?>: <?=$arItem['PROPERTIES']["ARTNUMBER"]['VALUE']?></div>
								<?endif;?>

								<?if($arItem['PROPERTIES']["OBEM_UPAKOVKI_L_MASLO"]['VALUE']):?>
									<div>Объём: <?=$arItem['PROPERTIES']["OBEM_UPAKOVKI_L_MASLO"]['VALUE']?>л.</div>
								<?endif;?>

								<?if($arItem['PROPERTIES']["KLASS_VYAZKOSTI_SAE_MASLO"]['VALUE']):?>
									<div>Вязкость: <?=$arItem['PROPERTIES']["KLASS_VYAZKOSTI_SAE_MASLO"]['VALUE']?></div>
								<?endif;?>

								<?if($arItem['PROPERTIES']["TIP_MASLO"]['VALUE']):?>
									<div>Тип: <?=$arItem['PROPERTIES']["TIP_MASLO"]['VALUE']?></div>
								<?endif;?>

								<?if($arItem['PROPERTIES']["PROIZVODITEL_MASLO"]['VALUE']):?>
									<div>Производитель: <?=$arItem['PROPERTIES']["PROIZVODITEL_MASLO"]['VALUE']?></div>
								<?endif;?>
							<?endif;?>
							<div>Артикул: <?=$arItem['PROPERTIES']["ARTNUMBER"]['VALUE']?></div>
						</div>
						<div class="product_item_action clearfix">
						<div class="product_item_price heavy  product_item_col product_item_col--left" id="<? echo $arItemIDs['PRICE']; ?>">
									<?

									if(!empty($changePrice))
									{
										?>
										<div class="change_price"><?/*new design  div*/?>
											<?=number_format($changePrice, 0, "", " ")?> <span class="ruble">руб.</span>
											<small>
																							<?if(strstr($arItem["DETAIL_PAGE_URL"], "/catalog/battery/")):?>
													при обмене
												<?else:?>
													по купону
												<?endif;?>
</small>

										</div>

										<div class="old_price">
												<?
											}

											if (!empty($arItem['MIN_PRICE']))
											{
												if ('N' == $arParams['PRODUCT_DISPLAY_MODE'] && isset($arItem['OFFERS']) && !empty($arItem['OFFERS']))
												{
													echo GetMessage(
														'CT_BCS_TPL_MESS_PRICE_SIMPLE_MODE',
														array(
															'#PRICE#' => $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'],
															'#MEASURE#' => GetMessage(
																'CT_BCS_TPL_MESS_MEASURE_SIMPLE_MODE',
																array(
																	'#VALUE#' => $arItem['MIN_PRICE']['CATALOG_MEASURE_RATIO'],
																	'#UNIT#' => $arItem['MIN_PRICE']['CATALOG_MEASURE_NAME']
																)
															)
														)
													);
												}
												else
												{

													echo number_format($arItem['MIN_PRICE']['DISCOUNT_VALUE_VAT'], 0, "", " ");?> <span class="ruble">руб.</span><?
												}
											}

											if(!empty($changePrice))
											{
												?>
										</div>
										<?
										}
										?>
						</div>



							<div class="product_item_col product_item_col--right">
								<?if ($arItem['CAN_BUY']):?>
									<input type="hidden" class="bx_col_input"
										id="<? echo $arItemIDs['QUANTITY']; ?>"
										name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>"
										value="<? echo $arItem['CATALOG_MEASURE_RATIO']; ?>">

									<span id="<? echo $arItemIDs['BUY_LINK']; ?>" class="product_btn product_btn--green action-block" rel="nofollow">Купить</span>
									<?/*new design*/?>

									<div id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>" class="bx_catalog_item_controls_blocktwo"></div>

								<?endif;?>
							</div>
						</div>
					</figcaption>
				</a>

			</div>

	        <div class="product_btn-bottom-wrap">
	            <span class="product_btn action-block toggle_btn" data-target=".phone_order_form_<?=$arItem["ID"]?>" data-target-class="active">Купить в 1 клик</span>

	            <div class="form_wrapper phone_order_form_<?=$arItem["ID"]?>" data-id="<?=$arItem["ID"]?>" data-call="phone_order_form_<?=$arItem["ID"]?>" ></div>
            </div>

	        <?
	       	$showSubscribeBtn = false;
	        	$compareBtnMessage = ($arParams['MESS_BTN_COMPARE'] != '' ? $arParams['MESS_BTN_COMPARE'] : GetMessage('CT_BCS_TPL_MESS_BTN_COMPARE'));
	        	if (!isset($arItem['OFFERS']) || empty($arItem['OFFERS']))
	        	{
	        		$emptyProductProperties = empty($arItem['PRODUCT_PROPERTIES']);
	        		if ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET'] && !$emptyProductProperties)
	        		{
	        ?>

			<div id="<? echo $arItemIDs['BASKET_PROP_DIV']; ?>" style="display: none;">
				<?
	        		if (!empty($arItem['PRODUCT_PROPERTIES_FILL']))
	        			{
	        				foreach ($arItem['PRODUCT_PROPERTIES_FILL'] as $propID => $propInfo)
	        				{
				?>

	        	<input type="hidden" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo htmlspecialcharsbx($propInfo['ID']); ?>">
					<?
	        			if (isset($arItem['PRODUCT_PROPERTIES'][$propID]))
	        				unset($arItem['PRODUCT_PROPERTIES'][$propID]);
							}
	        			}
	        			$emptyProductProperties = empty($arItem['PRODUCT_PROPERTIES']);
	        			if (!$emptyProductProperties)
	        			{
					?>

	        		<table>
						<?
	        				foreach ($arItem['PRODUCT_PROPERTIES'] as $propID => $propInfo)
	        					{
						?>

	        			<tr>
							<td><? echo $arItem['PROPERTIES'][$propID]['NAME']; ?></td>
	        				<td>
								<?
	        						if(
	        							'L' == $arItem['PROPERTIES'][$propID]['PROPERTY_TYPE']
	        							&& 'C' == $arItem['PROPERTIES'][$propID]['LIST_TYPE']
	        						)
	        						{
	        							foreach($propInfo['VALUES'] as $valueID => $value)
	        							{
	        					?>
								<label>
									<input type="radio" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo $valueID; ?>" <? echo ($valueID == $propInfo['SELECTED'] ? '"checked"' : ''); ?>>
									<? echo $value; ?>
								</label>
								<br>
									<?
										}
	        						}
	        						else
	        							{
	        						?>
								<select name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]">
									<?
	        							foreach($propInfo['VALUES'] as $valueID => $value)
	        							{
	        						?>
									<option value="<? echo $valueID; ?>" <? echo ($valueID == $propInfo['SELECTED'] ? 'selected' : ''); ?>>
										<? echo $value; ?>
									</option>
									<?
										}
									?>
								</select>
								<?
	        						}
								?>
	        				</td>
						</tr>
						<?
	        				}
						?>
	        		</table>
					<?
	        			}
					?>
	        </div>
	        <?
	        	}
	        }
	        ?>

    </figure>
</div>


<?
    $arJSParams = array(
			'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
			'SHOW_QUANTITY' => ($arParams['USE_PRODUCT_QUANTITY'] == 'Y'),
			'SHOW_ADD_BASKET_BTN' => false,
			'SHOW_BUY_BTN' => true,
			'SHOW_ABSENT' => true,
			'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
			'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
			'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
			'PRODUCT' => array(
				'ID' => $arItem['ID'],
				'NAME' => $productTitle,
				'PICT' => ('Y' == $arItem['SECOND_PICT'] ? $arItem['PREVIEW_PICTURE_SECOND'] : $arItem['PREVIEW_PICTURE']),
				'CAN_BUY' => $arItem["CAN_BUY"],
				'SUBSCRIPTION' => ('Y' == $arItem['CATALOG_SUBSCRIPTION']),
				'CHECK_QUANTITY' => $arItem['CHECK_QUANTITY'],
				'MAX_QUANTITY' => $arItem['CATALOG_QUANTITY'],
				'STEP_QUANTITY' => $arItem['CATALOG_MEASURE_RATIO'],
				'QUANTITY_FLOAT' => is_double($arItem['CATALOG_MEASURE_RATIO']),
				'SUBSCRIBE_URL' => $arItem['~SUBSCRIBE_URL'],
				'BASIS_PRICE' => $arItem['MIN_BASIS_PRICE']
			),
			'BASKET' => array(
				'ADD_PROPS' => ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET']),
				'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
				'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
				'EMPTY_PROPS' => $emptyProductProperties,
				'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
				'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
			),
			'VISUAL' => array(
				'ID' => $arItemIDs['ID'],
				'PICT_ID' => ('Y' == $arItem['SECOND_PICT'] ? $arItemIDs['SECOND_PICT'] : $arItemIDs['PICT']),
				'QUANTITY_ID' => $arItemIDs['QUANTITY'],
				'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
				'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
				'PRICE_ID' => $arItemIDs['PRICE'],
				'BUY_ID' => $arItemIDs['BUY_LINK'],
				'BASKET_PROP_DIV' => $arItemIDs['BASKET_PROP_DIV'],
				'BASKET_ACTIONS_ID' => $arItemIDs['BASKET_ACTIONS'],
				'NOT_AVAILABLE_MESS' => $arItemIDs['NOT_AVAILABLE_MESS'],
				'COMPARE_LINK_ID' => $arItemIDs['COMPARE_LINK']
			),
			'LAST_ELEMENT' => $arItem['LAST_ELEMENT']
		);
		if ($arParams['DISPLAY_COMPARE'])
		{
			$arJSParams['COMPARE'] = array(
				'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
				'COMPARE_PATH' => $arParams['COMPARE_PATH']
			);
		}
		unset($emptyProductProperties);
        ?>

		<script type="text/javascript">
        var <? echo $strObName; ?> = new JCCatalogSection(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
        </script>

		<?
		if ($key != 0 && ($key + 1) % 4 == 0){ ?>
			<div class="clearfix"></div>
		<? }
}
?>
    <script type="text/javascript">
    BX.message({
    	BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_BASKET_REDIRECT'); ?>',
    	BASKET_URL: '<? echo $arParams["BASKET_URL"]; ?>',
    	ADD_TO_BASKET_OK: '<? echo GetMessageJS('ADD_TO_BASKET_OK'); ?>',
    	TITLE_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_TITLE_ERROR') ?>',
    	TITLE_BASKET_PROPS: '<? echo GetMessageJS('CT_BCS_CATALOG_TITLE_BASKET_PROPS') ?>',
    	TITLE_SUCCESSFUL: '<? echo GetMessageJS('ADD_TO_BASKET_OK'); ?>',
    	BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
    	BTN_MESSAGE_SEND_PROPS: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_SEND_PROPS'); ?>',
    	BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE') ?>',
    	BTN_MESSAGE_CLOSE_POPUP: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE_POPUP'); ?>',
    	BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_BASKET_REDIRECT') ?>',
    	COMPARE_MESSAGE_OK: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_OK') ?>',
    	COMPARE_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_UNKNOWN_ERROR') ?>',
    	COMPARE_TITLE: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_TITLE') ?>',
    	BTN_MESSAGE_COMPARE_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT') ?>',
    	SITE_ID: '<? echo SITE_ID; ?>'
    });
    </script>
    <?
    	if ($arParams["DISPLAY_BOTTOM_PAGER"])
    	{
    		?><? echo $arResult["NAV_STRING"]; ?><?
    	}
    }
    ?>

</div>
<br>
<div class="section-description"></div>

<script src="/bitrix/templates/akk/js/jquery.dotdotdot.min.js"></script>
<script>
    $(function() {
        $(".product_item_content").dotdotdot();

        $(".product_item").on('click', function(e) {
        	if($(e.target).hasClass("action-block")) {
        		e.preventDefault();
        		return;
        	}
		});
    });
</script>
