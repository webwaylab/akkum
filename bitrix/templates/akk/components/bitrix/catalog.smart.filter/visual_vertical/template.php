<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/colors.css',
	'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME']
);
?>
<fieldset class="mb18 bx_filter <?=$templateData["TEMPLATE_CLASS"]?>">
    <h3 class="bold dark_font mb7">Подбор по параметрам:</h3>    
		<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="smartfilter">
			<?foreach($arResult["HIDDEN"] as $arItem):?>
			<input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?echo $arItem["CONTROL_ID"]?>" value="<?echo $arItem["HTML_VALUE"]?>" />
			<?endforeach;
			//prices
			foreach($arResult["ITEMS"] as $key=>$arItem)
			{
				$key = $arItem["ENCODED_ID"];
				if(isset($arItem["PRICE"])):
					if (!$arItem["VALUES"]["MIN"]["VALUE"] || !$arItem["VALUES"]["MAX"]["VALUE"] || $arItem["VALUES"]["MIN"]["VALUE"] == $arItem["VALUES"]["MAX"]["VALUE"])
						continue;
                    ?>
                    <div class="bx_filter_parameters_box active form_row" style="margin-bottom: 40px;">
                        <span class="bx_filter_container_modef"></span>
                        <span class="bx_filter_container_modef"></span>
                        <div class="form_label"><?=$arItem["NAME"]?>:</div>
                        <div class="slider_range_wrap mb14">
                            <?/*<div class="slider_range" data-start="<?=intval($arItem["VALUES"]["MIN"]["VALUE"])?>" data-end="<?=intval($arItem["VALUES"]["MAX"]["VALUE"])?>" data-max="<?=intval($arItem["VALUES"]["MAX"]["VALUE"])?>"></div>*/?>
                            <div class="val_mid">
                                <span class="d_ib val_mid">
                                    <input
										class="amount_start txt_field min-price"
										type="text"
										name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
										id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
														value="<?=intval($arItem["VALUES"]["MIN"]["HTML_VALUE"])!=intval($arItem["VALUES"]["MIN"]["VALUE"]) ? $arItem["VALUES"]["MIN"]["HTML_VALUE"] : ""?>"
	            										placeholder="<?=$arItem["VALUES"]["MIN"]["VALUE"]?>"
										size="5"
										onkeyup="smartFilter.keyup(this)"
                                        onchange="smartFilter.keyup(this)"
									/>
                                </span>
                                <span class="d_ib val_mid fs_12 amount_separator">до</span>
                                <span class="d_ib val_mid">
                               	    <input
										class="amount_end txt_field max-price"
										type="text"
										name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
										id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
														value="<?=intval($arItem["VALUES"]["MAX"]["HTML_VALUE"])!=intval($arItem["VALUES"]["MAX"]["VALUE"]) ? $arItem["VALUES"]["MAX"]["HTML_VALUE"] : ""?>"
	            										placeholder="<?=$arItem["VALUES"]["MAX"]["VALUE"]?>"
										size="5"
										onkeyup="smartFilter.keyup(this)"
                                        onchange="smartFilter.keyup(this)"
									/>
                                </span>
                            </div>
                        </div>
                        <div style="clear: both;"></div>
						<div class="bx_ui_slider_track" id="drag_track_<?=$key?>">
							<?
							$value1 = $arItem["VALUES"]["MIN"]["VALUE"];
							$value2 = $arItem["VALUES"]["MIN"]["VALUE"] + round(($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"])/4);
							$value3 = $arItem["VALUES"]["MIN"]["VALUE"] + round(($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"])/2);
							$value4 = $arItem["VALUES"]["MIN"]["VALUE"] + round((($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"])*3)/4);
							$value5 = $arItem["VALUES"]["MAX"]["VALUE"];
							?>
							<div class="bx_ui_slider_part p1"><span><?=intval($value1)?></span></div>
							<div class="bx_ui_slider_part p2"><span><?=$value2?></span></div>
							<div class="bx_ui_slider_part p3"><span><?=$value3?></span></div>
							<div class="bx_ui_slider_part p4"><span><?=$value4?></span></div>
							<div class="bx_ui_slider_part p5"><span><?=intval($value5)?></span></div>

							<div class="bx_ui_slider_pricebar_VD" style="left: 0;right: 0;" id="colorUnavailableActive_<?=$key?>"></div>
							<div class="bx_ui_slider_pricebar_VN" style="left: 0;right: 0;" id="colorAvailableInactive_<?=$key?>"></div>
							<div class="bx_ui_slider_pricebar_V"  style="left: 0;right: 0;" id="colorAvailableActive_<?=$key?>"></div>
							<div class="bx_ui_slider_range" 	id="drag_tracker_<?=$key?>"  style="left: 0;right: 0;">
								<a class="bx_ui_slider_handle left"  style="left:0;" href="javascript:void(0)" id="left_slider_<?=$key?>"></a>
								<a class="bx_ui_slider_handle right" style="right:0;" href="javascript:void(0)" id="right_slider_<?=$key?>"></a>
							</div>
						</div>
                    </div>
					<?
					$presicion = 2;
					if (Bitrix\Main\Loader::includeModule("currency"))
					{
						$res = CCurrencyLang::GetFormatDescription($arItem["VALUES"]["MIN"]["CURRENCY"]);
						$presicion = $res['DECIMALS'];
					}
					$arJsParams = array(
						"leftSlider" => 'left_slider_'.$key,
						"rightSlider" => 'right_slider_'.$key,
						"tracker" => "drag_tracker_".$key,
						"trackerWrap" => "drag_track_".$key,
						"minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
						"maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
						"minPrice" => intval($arItem["VALUES"]["MIN"]["VALUE"]),
						"maxPrice" => intval($arItem["VALUES"]["MAX"]["VALUE"]),
						"curMinPrice" => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
						"curMaxPrice" => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
						"fltMinPrice" => intval($arItem["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"] ,
						"fltMaxPrice" => intval($arItem["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"],
						"precision" => $presicion,
						"colorUnavailableActive" => 'colorUnavailableActive_'.$key,
						"colorAvailableActive" => 'colorAvailableActive_'.$key,
						"colorAvailableInactive" => 'colorAvailableInactive_'.$key,
					);
					?>
					<script type="text/javascript">
						BX.ready(function(){
							window['trackBar<?=$key?>'] = new BX.Iblock.SmartFilter(<?=CUtil::PhpToJSObject($arJsParams)?>);
						});
					</script>
				<?endif;
			}

            $arBoxes = array();
			//not prices
			foreach($arResult["ITEMS"] as $key=>$arItem)
			{
				if(
					empty($arItem["VALUES"])
					|| isset($arItem["PRICE"])
				)
					continue;

				/*if (
					$arItem["DISPLAY_TYPE"] == "A"
					&& (
						!$arItem["VALUES"]["MIN"]["VALUE"]
						|| !$arItem["VALUES"]["MAX"]["VALUE"]
						|| $arItem["VALUES"]["MIN"]["VALUE"] == $arItem["VALUES"]["MAX"]["VALUE"]
					)
				)
					continue;*/
				?>
				
						<?
						$arCur = current($arItem["VALUES"]);
						switch ($arItem["DISPLAY_TYPE"])
						{
							case "A"://NUMBERS_WITH_SLIDER
								?>
                                <div class="bx_filter_parameters_box active form_row" style="margin-bottom: 40px;">
                                    <span class="bx_filter_container_modef"></span>
                                    <span class="bx_filter_container_modef"></span>
                                    <div class="form_label"><?=$arItem["NAME"]?>:</div>
                                    <div class="slider_range_wrap mb14">
                                        <?/*<div class="slider_range" data-start="<?=intval($arItem["VALUES"]["MIN"]["VALUE"])?>" data-end="<?=intval($arItem["VALUES"]["MAX"]["VALUE"])?>" data-max="<?=intval($arItem["VALUES"]["MAX"]["VALUE"])?>"></div>*/?>
                                        <div class="val_mid">
                                            <span class="d_ib val_mid">
                                                <input
            										class="amount_start txt_field min-price"
            										type="text"
            										name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
            										id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
														value="<?=intval($arItem["VALUES"]["MIN"]["HTML_VALUE"])!=intval($arItem["VALUES"]["MIN"]["VALUE"]) ? $arItem["VALUES"]["MIN"]["HTML_VALUE"] : ""?>"
	            										placeholder="<?=$arItem["VALUES"]["MIN"]["VALUE"]?>"
            										size="5"
            										onkeyup="smartFilter.keyup(this)"
                                                    onchange="smartFilter.keyup(this)"
            									/>
                                            </span>
                                            <span class="d_ib val_mid fs_12 amount_separator">до</span>
                                            <span class="d_ib val_mid">
                                           	    <input
            										class="amount_end txt_field max-price"
            										type="text"
            										name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
            										id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
														value="<?=intval($arItem["VALUES"]["MAX"]["HTML_VALUE"])!=intval($arItem["VALUES"]["MAX"]["VALUE"]) ? $arItem["VALUES"]["MAX"]["HTML_VALUE"] : ""?>"
	            										placeholder="<?=$arItem["VALUES"]["MAX"]["VALUE"]?>"
            										size="5"
            										onkeyup="smartFilter.keyup(this)"
                                                    onchange="smartFilter.keyup(this)"
            									/>
                                            </span>
                                        </div>
                                    </div>
                                    <div style="clear: both;"></div>
    								<div class="bx_ui_slider_track" id="drag_track_<?=$key?>">
    									<?
    									$value1 = $arItem["VALUES"]["MIN"]["VALUE"];
    									$value2 = $arItem["VALUES"]["MIN"]["VALUE"] + round(($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"])/4);
    									$value3 = $arItem["VALUES"]["MIN"]["VALUE"] + round(($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"])/2);
    									$value4 = $arItem["VALUES"]["MIN"]["VALUE"] + round((($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"])*3)/4);
    									$value5 = $arItem["VALUES"]["MAX"]["VALUE"];
    									?>
    									<div class="bx_ui_slider_part p1"><span><?=$value1?></span></div>
    									<div class="bx_ui_slider_part p2"><span><?=$value2?></span></div>
    									<div class="bx_ui_slider_part p3"><span><?=$value3?></span></div>
    									<div class="bx_ui_slider_part p4"><span><?=$value4?></span></div>
    									<div class="bx_ui_slider_part p5"><span><?=$value5?></span></div>
    
    									<div class="bx_ui_slider_pricebar_VD" style="left: 0;right: 0;" id="colorUnavailableActive_<?=$key?>"></div>
    									<div class="bx_ui_slider_pricebar_VN" style="left: 0;right: 0;" id="colorAvailableInactive_<?=$key?>"></div>
    									<div class="bx_ui_slider_pricebar_V"  style="left: 0;right: 0;" id="colorAvailableActive_<?=$key?>"></div>
    									<div class="bx_ui_slider_range" 	id="drag_tracker_<?=$key?>"  style="left: 0;right: 0;">
    										<a class="bx_ui_slider_handle left"  style="left:0;" href="javascript:void(0)" id="left_slider_<?=$key?>"></a>
    										<a class="bx_ui_slider_handle right" style="right:0;" href="javascript:void(0)" id="right_slider_<?=$key?>"></a>
    									</div>
    								</div>
                                </div>
								<?
								$arJsParams = array(
									"leftSlider" => 'left_slider_'.$key,
									"rightSlider" => 'right_slider_'.$key,
									"tracker" => "drag_tracker_".$key,
									"trackerWrap" => "drag_track_".$key,
									"minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
									"maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
									"minPrice" => $arItem["VALUES"]["MIN"]["VALUE"],
									"maxPrice" => $arItem["VALUES"]["MAX"]["VALUE"],
									"curMinPrice" => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
									"curMaxPrice" => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
									"fltMinPrice" => intval($arItem["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"] ,
									"fltMaxPrice" => intval($arItem["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"],
									"precision" => 0,
									"colorUnavailableActive" => 'colorUnavailableActive_'.$key,
									"colorAvailableActive" => 'colorAvailableActive_'.$key,
									"colorAvailableInactive" => 'colorAvailableInactive_'.$key,
								);
								?>
								<script type="text/javascript">
									BX.ready(function(){
										window['trackBar<?=$key?>'] = new BX.Iblock.SmartFilter(<?=CUtil::PhpToJSObject($arJsParams)?>);
									});
								</script>
								<?
								break;
							case "B"://NUMBERS
                            
                                $arBoxes[] = $arItem;
								/*?>
								<div class="bx_filter_parameters_box_container_block"><div class="bx_filter_input_container">
									<input
										class="min-price"
										type="text"
										name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
										id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
										value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
										size="5"
										onkeyup="smartFilter.keyup(this)"
										/>
								</div></div>
								<div class="bx_filter_parameters_box_container_block"><div class="bx_filter_input_container">
									<input
										class="max-price"
										type="text"
										name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
										id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
										value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
										size="5"
										onkeyup="smartFilter.keyup(this)"
										/>
								</div></div>
								<?*/
								break;
							case "G"://CHECKBOXES_WITH_PICTURES
								?>
								<?foreach ($arItem["VALUES"] as $val => $ar):?>
									<input
										style="display: none"
										type="checkbox"
										name="<?=$ar["CONTROL_NAME"]?>"
										id="<?=$ar["CONTROL_ID"]?>"
										value="<?=$ar["HTML_VALUE"]?>"
										<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
									/>
									<?
									$class = "";
									if ($ar["CHECKED"])
										$class.= " active";
									if ($ar["DISABLED"])
										$class.= " disabled";
									?>
									<label for="<?=$ar["CONTROL_ID"]?>" data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx_filter_param_label dib<?=$class?>" onclick="smartFilter.keyup(BX('<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')); BX.toggleClass(this, 'active');">
										<span class="bx_filter_param_btn bx_color_sl">
											<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
											<span class="bx_filter_btn_color_icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
											<?endif?>
										</span>
									</label>
								<?endforeach?>
								<?
								break;
							case "H"://CHECKBOXES_WITH_PICTURES_AND_LABELS
								?>
								<?foreach ($arItem["VALUES"] as $val => $ar):?>
									<input
										style="display: none"
										type="checkbox"
										name="<?=$ar["CONTROL_NAME"]?>"
										id="<?=$ar["CONTROL_ID"]?>"
										value="<?=$ar["HTML_VALUE"]?>"
										<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
									/>
									<?
									$class = "";
									if ($ar["CHECKED"])
										$class.= " active";
									if ($ar["DISABLED"])
										$class.= " disabled";
									?>
									<label for="<?=$ar["CONTROL_ID"]?>" data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx_filter_param_label<?=$class?>" onclick="smartFilter.keyup(BX('<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')); BX.toggleClass(this, 'active');">
										<span class="bx_filter_param_btn bx_color_sl">
											<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
												<span class="bx_filter_btn_color_icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
											<?endif?>
										</span>
										<span class="bx_filter_param_text">
											<?=$ar["VALUE"]?>
										</span>
									</label>
								<?endforeach?>
								<?
								break;
							case "P"://DROPDOWN
								$checkedItemExist = false;
                                //CDev::pre($arItem);
        						?>
                                
                                <div class="bx_filter_parameters_box <?if ($arItem["DISPLAY_EXPANDED"]== "Y"):?>active<?endif?>">
                					<span class="bx_filter_container_modef"></span>
                					<div class="bx_filter_block">
                						<div class="bx_filter_parameters_box_container">
                            <div class="bx_filter_select_container">
                            
                                <?/*<div class="bx_filter_select_text" data-role="currentOption">
									<?
									foreach ($arItem["VALUES"] as $val => $ar)
									{
										if ($ar["CHECKED"])
										{
											echo $ar["VALUE"];
											$checkedItemExist = true;
										}
									}
									if (!$checkedItemExist)
									{
										echo GetMessage("CT_BCSF_FILTER_ALL");
									}
									?>
								</div>*/?>
                            
                            
                                <div class="form_row1">
                                    <div class="form_label"><?=$arItem["NAME"]?>:</div>
                                    
                                    
                                    <input
    									style="display: none"
    									type="radio"
    									name="<?=$arCur["CONTROL_NAME_ALT"]?>"
    									id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
    									value=""
    								/>
    								<?foreach ($arItem["VALUES"] as $val => $ar):?>
    									<input
    										style="display: none"
    										type="radio"
    										name="<?=$ar["CONTROL_NAME_ALT"]?>"
    										id="<?=$ar["CONTROL_ID"]?>"
    										value="<? echo $ar["HTML_VALUE_ALT"] ?>"
    										<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
    									/>
    								<?endforeach?>
                                    
                                    <select class="styler" onchange="$('#'+$(this).find(':selected').attr('for')).attr('checked', true); smartFilter.selectDropDownItem(this, $(this).find(':selected').data('control'))">
                                        <option></option>
                                        <?
        								foreach ($arItem["VALUES"] as $val => $ar):
        									$class = "";
        									if ($ar["CHECKED"])
        										$class.= " selected";
        									if ($ar["DISABLED"])
        										$class.= " disabled";
                                            ?>
                                                <option for="<?=$ar["CONTROL_ID"]?>" <?=$class?> data-role="label_<?=$ar["CONTROL_ID"]?>" data-control="<?=CUtil::JSEscape($ar["CONTROL_ID"])?>"><?=$ar["VALUE"]?></option>
        								<?endforeach?>
                                    </select>                                    
                                </div>
                                
                                </div>
                                
                                
                                                </div>
                						<div class="clb"></div>
                					</div>
                				</div>
								<?
								break;
							case "R"://DROPDOWN_WITH_PICTURES_AND_LABELS
								?>
                                <div class="bx_filter_parameters_box <?if ($arItem["DISPLAY_EXPANDED"]== "Y"):?>active<?endif?>">
                					<span class="bx_filter_container_modef"></span>
                					<div class="bx_filter_block">
                						<div class="bx_filter_parameters_box_container">
                                
                                
								<div class="bx_filter_select_container">
									<div class="bx_filter_select_block" onclick="smartFilter.showDropDownPopup(this, '<?=CUtil::JSEscape($key)?>')">
										
                                        <div class="bx_filter_select_text" data-role="currentOption">
											<?
											$checkedItemExist = false;
											foreach ($arItem["VALUES"] as $val => $ar):
												if ($ar["CHECKED"])
												{
												?>
													<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
														<span class="bx_filter_btn_color_icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
													<?endif?>
													<span class="bx_filter_param_text">
														<?=$ar["VALUE"]?>
													</span>
												<?
													$checkedItemExist = true;
												}
											endforeach;
											if (!$checkedItemExist)
											{
												?><span class="bx_filter_btn_color_icon all"></span> <?
												echo GetMessage("CT_BCSF_FILTER_ALL");
											}
											?>
										</div>
										<div class="bx_filter_select_arrow"></div>
										<input
											style="display: none"
											type="radio"
											name="<?=$arCur["CONTROL_NAME_ALT"]?>"
											id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
											value=""
										/>
										<?foreach ($arItem["VALUES"] as $val => $ar):?>
											<input
												style="display: none"
												type="radio"
												name="<?=$ar["CONTROL_NAME_ALT"]?>"
												id="<?=$ar["CONTROL_ID"]?>"
												value="<?=$ar["HTML_VALUE_ALT"]?>"
												<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
											/>
										<?endforeach?>
										<div class="bx_filter_select_popup" data-role="dropdownContent" style="display: none">
											<ul>
												<li style="border-bottom: 1px solid #e5e5e5;padding-bottom: 5px;margin-bottom: 5px;">
													<label for="<?="all_".$arCur["CONTROL_ID"]?>" class="bx_filter_param_label" data-role="label_<?="all_".$arCur["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape("all_".$arCur["CONTROL_ID"])?>')">
														<span class="bx_filter_btn_color_icon all"></span>
														<? echo GetMessage("CT_BCSF_FILTER_ALL"); ?>
													</label>
												</li>
											<?
											foreach ($arItem["VALUES"] as $val => $ar):
												$class = "";
												if ($ar["CHECKED"])
													$class.= " selected";
												if ($ar["DISABLED"])
													$class.= " disabled";
											?>
												<li>
													<label for="<?=$ar["CONTROL_ID"]?>" data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx_filter_param_label<?=$class?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')">
														<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
															<span class="bx_filter_btn_color_icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
														<?endif?>
														<span class="bx_filter_param_text">
															<?=$ar["VALUE"]?>
														</span>
													</label>
												</li>
											<?endforeach?>
											</ul>
										</div>
									</div>
								</div>
                                
                                </div>
            						<div class="clb"></div>
            					</div>
            				</div>
                                
								<?
								break;
							case "K"://RADIO_BUTTONS
								?>
								<label class="bx_filter_param_label" for="<? echo "all_".$arCur["CONTROL_ID"] ?>">
									<span class="bx_filter_input_checkbox">
										<input
											type="radio"
											value=""
											name="<? echo $arCur["CONTROL_NAME_ALT"] ?>"
											id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
											onclick="smartFilter.click(this)"
										/>
										<span class="bx_filter_param_text"><? echo GetMessage("CT_BCSF_FILTER_ALL"); ?></span>
									</span>
								</label>
								<?foreach($arItem["VALUES"] as $val => $ar):?>
									<label data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx_filter_param_label" for="<? echo $ar["CONTROL_ID"] ?>">
										<span class="bx_filter_input_checkbox <? echo $ar["DISABLED"] ? 'disabled': '' ?>">
											<input
												type="radio"
												value="<? echo $ar["HTML_VALUE_ALT"] ?>"
												name="<? echo $ar["CONTROL_NAME_ALT"] ?>"
												id="<? echo $ar["CONTROL_ID"] ?>"
												<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
												onclick="smartFilter.click(this)"
											/>
											<span class="bx_filter_param_text"><? echo $ar["VALUE"]; ?></span>
										</span>
									</label>
								<?endforeach;?>
								<?
								break;
							default://CHECKBOXES
								?>
                                <div class="bx_filter_parameters_box <?if ($arItem["DISPLAY_EXPANDED"]== "Y"):?>active<?endif?>">
                					<span class="bx_filter_container_modef"></span>
                					<div class="bx_filter_block">
                						<div class="bx_filter_parameters_box_container">
                                <div class="mb32">
                                    <div class="form_label2"><?=$arItem["NAME"]?>:</div>
        						    <?foreach($arItem["VALUES"] as $val => $ar):?>
                                    <label data-role="label_<?=$ar["CONTROL_ID"]?>" class="checkbox_label fs_12      <? echo $ar["DISABLED"] ? 'disabled': '' ?>"  for="<? echo $ar["CONTROL_ID"] ?>">
                                        <input
        									type="checkbox"
        									value="<? echo $ar["HTML_VALUE"] ?>"
        									name="<? echo $ar["CONTROL_NAME"] ?>"
        									id="<? echo $ar["CONTROL_ID"] ?>"
        									<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
        									onchange="smartFilter.click(this)"
                                            class="styler"
        						        />
                                        <span><? echo $ar["VALUE"]; ?></span>
                                    </label>
        						    <?endforeach;?>
                                </div>
                                
                                    </div>
            						<div class="clb"></div>
            					</div>
            				</div>
						<?
						}
						?>
						
			<?
			}
			?>
            <div class="mb23">
                <div class="form_label1">Габариты Д-Ш-В:</div>
                <div class="val_mid">
                    <?
                    foreach($arBoxes as $key=>$arItem)
        			{
                        ?>
                        <span class="d_ib val_mid">
                            <input
								class="min-price"
								type="hidden"
								name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
								id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
								value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
								size="5"
								onkeyup="smartFilter.keyup(this)"
								/>
                            <input
    							class="txt_field w_60 max-price"
    							type="text"
    							name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
    							id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
    							value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
    							size="5"
    							onkeyup="$('#<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>').val($(this).val()); smartFilter.keyup($('#<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>')); smartFilter.keyup(this)"
    							/>
                        </span>
                        <?
                        if($key<count($arBoxes)-1):?>
                            <span class="d_ib val_mid fs_12">X</span>
                        <?endif;
        			}
                    ?>
                </div>
            </div>
			<div class="clb"></div>
			<div class="bx_filter_button_box active mb18">
				<div class="bx_filter_block">
					<div class="bx_filter_parameters_box_container">
						<input class="btn grn_skin d_block" type="submit" id="set_filter" name="set_filter" value="Применить фильтр" />
						<input class="bx_filter_search_reset" type="submit" id="del_filter" name="del_filter" value="<?=GetMessage("CT_BCSF_DEL_FILTER")?>" style="display: none;"/>

						<div class="bx_filter_popup_result <?=$arParams["POPUP_POSITION"]?>" id="modef" <?if(!isset($arResult["ELEMENT_COUNT"])) echo 'style="display:none"';?> style="display: inline-block;">
							<?/*echo GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<span id="modef_num">'.intval($arResult["ELEMENT_COUNT"]).'</span>'));*/?>
							<a href="<?echo $arResult["FILTER_URL"]?>">Товаров: </a> <span id="modef_num"><?=intval($arResult["ELEMENT_COUNT"])?></span>
                            <span class="arrow"></span>
						</div>
					</div>
				</div>
			</div>
            
            <div class="al_center">
                <a href="#" onclick="$('#del_filter').click(); return false;" class="gray_link bold">Сбросить фильтр</a>
            </div>
            
		</form>
</fieldset>
<script>
	var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>');
</script>