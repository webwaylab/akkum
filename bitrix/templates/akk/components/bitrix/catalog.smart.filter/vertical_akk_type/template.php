<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);


$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/colors.css',
	'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME']
);


$propsOrder = Array("SORT"=>"ASC", "VALUE"=>"ASC");
$propsFilter = array("IBLOCK_ID" => CATALOG_ID, "CODE" => "TYPE");
$rsAkkumTypes = CIBlockProperty::GetList($propsOrder, $propsFilter);
$arAkkumTypes = $rsAkkumTypes->Fetch();

$filterAkkumTypes = $arResult["ITEMS"][$arAkkumTypes["ID"]];

$propsOrder2 = Array("SORT"=>"ASC", "VALUE"=>"ASC");
$propsFilter2 = array("IBLOCK_ID" => CATALOG_ID, "CODE" => "TIP_GIBRIDNYE_AKKUMULYATORY_DOP_SVOYSTVA_SPRAVOCHN");
$rsAkkumTypes2 = CIBlockProperty::GetList($propsOrder2, $propsFilter2);
$arAkkumTypes2 = $rsAkkumTypes2->Fetch();

$filterAkkumTypes2 = $arResult["ITEMS"][$arAkkumTypes2["ID"]];

$propsOrder3 = Array("SORT"=>"ASC", "VALUE"=>"ASC");
$propsFilter3 = array("IBLOCK_ID" => CATALOG_ID, "CODE" => "TIP_AKKUMULYATORA_DOP_SVOYSTVA_SPRAVOCHNIKA_NOMENK");
$rsAkkumTypes3 = CIBlockProperty::GetList($propsOrder3, $propsFilter3);
$arAkkumTypes3 = $rsAkkumTypes3->Fetch();

$filterAkkumTypes3 = $arResult["ITEMS"][$arAkkumTypes3["ID"]];

//CDev::pre($filterAkkumTypes2);
//CDev::pre($filterAkkumTypes3);
?>

<div class="battery_type">
	<div class="bold dark_font name_filter">
	    <span>Тип аккумулятора</span>
	</div>
	<ul class="aside-nav">
		<?foreach($filterAkkumTypes["VALUES"] as $filterAkkumType):?>
			<li class="aside-nav__item">
				<a href="/catalog/battery/?<?=$filterAkkumType['CONTROL_NAME_ALT']?>=<?=$filterAkkumType['HTML_VALUE_ALT']?>&set_filter=Применить+фильтр" class="aside-nav__link"><?=$filterAkkumType['VALUE']?></a>
			</li>
		<?endforeach;?>
		<?foreach($filterAkkumTypes2["VALUES"] as $filterAkkumType):?>
			<li class="aside-nav__item">
				<a href="/catalog/battery/?<?=$filterAkkumType['CONTROL_NAME_ALT']?>=<?=$filterAkkumType['HTML_VALUE_ALT']?>&set_filter=Применить+фильтр" class="aside-nav__link">Гибридные</a>
			</li>
			<?break;?>
		<?endforeach;?>
		<?foreach($filterAkkumTypes3["VALUES"] as $filterAkkumType):?>
			<li class="aside-nav__item">
				<a href="/catalog/battery/?<?=$filterAkkumType['CONTROL_NAME_ALT']?>=<?=$filterAkkumType['HTML_VALUE_ALT']?>&set_filter=Применить+фильтр" class="aside-nav__link"><?=$filterAkkumType['VALUE']?></a>
			</li>
		<?endforeach;?>
			<li class="aside-nav__item">
				<a href="/budget/" class="aside-nav__link">Бюджетные</a>
			</li>
	</ul>
</div>	