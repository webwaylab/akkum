<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);


$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/colors.css',
	'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME']
);


$propsOrder = Array("SORT"=>"ASC", "VALUE"=>"ASC");
$propsFilter = array("IBLOCK_ID" => CATALOG_ID, "CODE" => "PROIZVODITEL_MASLO");
$rsMasloBrands = CIBlockProperty::GetList($propsOrder, $propsFilter);
$arMasloBrands = $rsMasloBrands->Fetch();

$filterMasloBrands = $arResult["ITEMS"][$arMasloBrands["ID"]];

/*CDev::pre($arResult["ITEMS"]);
CDev::pre($arMasloBrands);*/

?>

<?foreach($filterMasloBrands["VALUES"] as $filterMasloBrand):?>
	<div>
		<a href="/catalog/masla/?<?=$filterMasloBrand['CONTROL_NAME_ALT']?>=<?=$filterMasloBrand['HTML_VALUE_ALT']?>&set_filter=Применить+фильтр">
			<img src="/bitrix/templates/akk/images/oils/oil_<?=strtolower($filterMasloBrand['VALUE'])?>.png" alt="<?=$filterMasloBrand['VALUE']?>">
		</a>
		<a href="/catalog/masla/?<?=$filterMasloBrand['CONTROL_NAME_ALT']?>=<?=$filterMasloBrand['HTML_VALUE_ALT']?>&set_filter=Применить+фильтр"><?=$filterMasloBrand['VALUE']?></a>
	</div>
<?endforeach;?>
