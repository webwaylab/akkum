<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


if (isset($arParams["TEMPLATE_THEME"]) && !empty($arParams["TEMPLATE_THEME"]))
{
	$arAvailableThemes = array();
	$dir = trim(preg_replace("'[\\\\/]+'", "/", dirname(__FILE__)."/themes/"));
	if (is_dir($dir) && $directory = opendir($dir))
	{
		while (($file = readdir($directory)) !== false)
		{
			if ($file != "." && $file != ".." && is_dir($dir.$file))
				$arAvailableThemes[] = $file;
		}
		closedir($directory);
	}

	if ($arParams["TEMPLATE_THEME"] == "site")
	{
		$solution = COption::GetOptionString("main", "wizard_solution", "", SITE_ID);
		if ($solution == "eshop")
		{
			$theme = COption::GetOptionString("main", "wizard_eshop_adapt_theme_id", "blue", SITE_ID);
			$arParams["TEMPLATE_THEME"] = (in_array($theme, $arAvailableThemes)) ? $theme : "blue";
		}
	}
	else
	{
		$arParams["TEMPLATE_THEME"] = (in_array($arParams["TEMPLATE_THEME"], $arAvailableThemes)) ? $arParams["TEMPLATE_THEME"] : "blue";
	}
}
else
{
	$arParams["TEMPLATE_THEME"] = "blue";
}

$arParams["FILTER_VIEW_MODE"] = (isset($arParams["FILTER_VIEW_MODE"]) && $arParams["FILTER_VIEW_MODE"] == "horizontal") ? "horizontal" : "vertical";
$arParams["POPUP_POSITION"] = (isset($arParams["POPUP_POSITION"]) && in_array($arParams["POPUP_POSITION"], array("left", "right"))) ? $arParams["POPUP_POSITION"] : "left";

//---------------------
CModule::IncludeModule("iblock");
$res = CIBlockSection::GetByID($arParams["SECTION_ID"]);
$arSection = $res->GetNext();

if(!empty($arSection["SECTION_PAGE_URL"]))
    $arResult["FORM_ACTION"] = $arSection["SECTION_PAGE_URL"];


/*
$find_code = 'MANUFACTURE'; //Искомое свойство

global $_GET; //собираем свойства GET и сверяем что заполненно в нем из нашего $filterArr;
$get = $_GET;
$find = '';
$selectArray = array();


$i = 0;
$p = 0;

foreach($arResult['ITEMS'] as $key => $arItem){
	if (count($arItem['VALUES']) > 0){
		foreach ($arItem['VALUES'] as $k => $item){	
			$n = (isset($item['CONTROL_NAME_ALT'])) ? $item['CONTROL_NAME_ALT'] : $item['CONTROL_NAME'];

			if (array_key_exists($n, $get) && $find_code != $arItem['CODE'] && $get[$n] != ''){
				$i++;
			}
			elseif(array_key_exists($n, $get) && $find_code == $arItem['CODE']){
				$p++;
				$find = $get[$n];
				$selectArray[$item['HTML_VALUE_ALT']] = $item['VALUE'];
			}
			
			if(array_key_exists($n, $get) && $arItem['CODE'] == 'BASE' && intval($get[$n]) == intval($item['VALUE'])){
				$i--;
			}
		}
	}
}

if ($i == 0 && $p > 0){
	//Если в фильтре выбрана только марка, редиректим
	$name = $selectArray[$find];
	LocalRedirect('/catalog/brand/'.$name.'/');
}
*/

