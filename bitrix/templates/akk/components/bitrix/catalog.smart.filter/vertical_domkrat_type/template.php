<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);


$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/colors.css',
	'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME']
);


$propsOrder = Array("SORT"=>"ASC", "VALUE"=>"ASC");
$propsFilter = array("IBLOCK_ID" => CATALOG_ID, "CODE" => "TIP_DOMKRATA_DOMKRATY");
$rsDomkratTypes = CIBlockProperty::GetList($propsOrder, $propsFilter);
$arDomkratTypes = $rsDomkratTypes->Fetch();

$filterDomkratTypes = $arResult["ITEMS"][$arDomkratTypes["ID"]];

//CDev::pre($filterDomkratTypes);
?>
	<ul>
		<?foreach($filterDomkratTypes["VALUES"] as $filterDomkratType):?>
			<li class="aside-nav__item">
				<a href="/catalog/accessories/domkraty_podstavki_upory_pod_mashinu/?<?=$filterDomkratType['CONTROL_NAME_ALT']?>=<?=$filterDomkratType['HTML_VALUE_ALT']?>&set_filter=Применить+фильтр"><?=$filterDomkratType['VALUE']?></a>
			</li>
		<?endforeach;?>
	</ul>