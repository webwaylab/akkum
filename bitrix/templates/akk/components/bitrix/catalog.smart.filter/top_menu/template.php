<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */


//echo '<pre>'; print_r($arResult['BRANDS']); echo "</pre>";
//CDev::pre($arResult['BRANDS']);
?>

<?if ($arParams['AKK_TYPE_CODE'] == 'moto'):?>
	<?//CDev::pre($arResult['BRANDS']);?>
	<div class="owl-carousel moto-slider">
		<?foreach($arResult['BRANDS'] as $column):?>
		
			<?foreach($column as $row):?>
					<div class="slick_sl">

						<a href="<?=$row["URL"]?>">
							<img src="<?=$row["FILE"]?>" alt="<?=$arResult['filterAkkTypes'][$row["PROPERTY_TYPE_ENUM_ID"]]["CONTROL_NAME_ALT"]?>">
						</a>
						<a href="<?=$row["URL"]?>">
							<?=$row["NAME"]?>
						</a>
					</div>

			<?endforeach;?>
		
		<?endforeach;?>
	</div>
<? else:?>

	<?foreach($arResult['BRANDS'] as $column):?>
		<ul>
			<?foreach($column as $row):?>
				<li data-image="<?=$row["FILE"]?>">
					<a href="<?=$row["URL"]?>">
						<?=$row["NAME"]?>
					</a>
				</li>
			<?endforeach;?>
		</ul>
	<?endforeach;?>

<? endif;?>
