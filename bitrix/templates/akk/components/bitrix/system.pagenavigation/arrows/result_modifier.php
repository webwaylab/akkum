<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/** @var $arResult */

// Cut query string from 'BRAND_CODE' to '&' symbol
$startPos = mb_strpos($arResult['NavQueryString'], 'BRAND_CODE', 0, 'UTF-8');
if ($startPos !== false) {
    $endPos = mb_strpos($arResult['NavQueryString'], '%3F', $startPos, 'UTF-8');

    if ($endPos === false) {
        $endPos = mb_strpos($arResult['NavQueryString'], '%2F', $startPos, 'UTF-8');
    }

    $removeString = mb_substr($arResult['NavQueryString'], 0, $endPos, 'UTF-8');
    $arResult['NavQueryString'] = str_replace($arResult['NavQueryString'], '', $arResult['NavQueryString']);
}