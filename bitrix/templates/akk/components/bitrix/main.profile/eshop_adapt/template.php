<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
//CDev::pre($arResult["arUser"]);
?>

<div class="user_profile_info clearfix">
    <div class="user_profile_img f_left">
        <input type="file" class="user_profile_upload_img">
        <div class="user_profile_img_label" onclick="$('input[name=myfile]').click(); return false;">Загрузить фото</div>
        <?if(intval($arResult["arUser"]["PERSONAL_PHOTO"])>0):?>
            <img src="<?=CFile::GetPath($arResult["arUser"]["PERSONAL_PHOTO"])?>" class="logo-img" width="128" height="128">
        <?else:?>
            <img src="<?=SITE_TEMPLATE_PATH?>/images/def_profile_img.jpg" alt="" class="logo-img">
        <?endif;?>
    </div>
    <div class="wrapper">
        
        <?=ShowError($arResult["strProfileError"]);?>
        <?
        if ($arResult['DATA_SAVED'] == 'Y')
        	echo ShowNote(GetMessage('PROFILE_DATA_SAVED'));
        ?>
    
        <h2 class="fs_18 mb10"><span class="bold d_block"><?=$arResult["arUser"]["LAST_NAME"]?></span> <?=$arResult["arUser"]["NAME"]?> <?=$arResult["arUser"]["SECOND_NAME"]?></h2>
        
        <form class="profile_info_form" method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>?" enctype="multipart/form-data">
            <?=$arResult["BX_SESSION_CHECK"]?>
            <input type="hidden" name="lang" value="<?=LANG?>" />
            <input type="hidden" name="LOGIN" value=<?=$arResult["arUser"]["LOGIN"]?> />
            <input type="hidden" name="ID" value="<?=$arResult["ID"]?>" />
            
            <fieldset class='mb20'>
            
                <div class="form_row2 clearfix">
                    <div class="profile_info_form_col1 f_left">
                        <div class="form_label3">Ваше фамилия:</div>
                        <input type="text" name="LAST_NAME" class="txt_field" value="<?=$arResult["arUser"]["LAST_NAME"]?>">
                    </div>
                    <div class="profile_info_form_col2 f_left">
                        <div class="form_label3">Ваше имя:</div>
                        <input type="text" name="NAME" class="txt_field" value="<?=$arResult["arUser"]["NAME"]?>">
                    </div>
                </div>
            
                <div class="form_row2 clearfix">
                    <div class="profile_info_form_col1 f_left">
                        <div class="form_label3">Ваше отчество:</div>
                        <input type="text" name="SECOND_NAME" class="txt_field" value="<?=$arResult["arUser"]["SECOND_NAME"]?>">
                    </div>
                    <div class="profile_info_form_col2 f_left">
                        <div class="form_label3">Дата рождения:</div>
                        <input type="text" name="PERSONAL_BIRTHDAY" class="txt_field datepicker" value="<?=$arResult["arUser"]["PERSONAL_BIRTHDAY"]?>">
                    </div>
                </div>
                
                <div class="form_row2">
                    <div class="form_label3">Адрес доставки:</div>
                    <textarea name="UF_ADDRESS" class="txt_field area_h113"><?=$arResult["arUser"]["UF_ADDRESS"]?></textarea>
                </div>
                
                <div class="form_row2 clearfix">
                    <div class="profile_info_form_col1 f_left">
                        <div class="form_label3">Эл почта:</div>
                        <input type="text" name="EMAIL" class="txt_field" value="<?=$arResult["arUser"]["EMAIL"]?>">
                    </div>
                    <div class="profile_info_form_col2 f_left">
                        <div class="form_label3">Телефон:</div>
                        <input type="text" name="PERSONAL_PHONE" class="txt_field phone_mask" value="<?=$arResult["arUser"]["PERSONAL_PHONE"]?>">
                    </div>
                </div>
            </fieldset>
            
            <!--<button class="btn3 grn_skin">Сохранить изменения</button>-->
            
            <input name="save" value="<?=GetMessage("MAIN_SAVE")?>" class="btn3 grn_skin" type="submit">
            
        </form>
        
    </div>
</div>

<?/*
<div class="bx_profile bx_<?=$arResult["THEME"]?>">
	<form method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>?" enctype="multipart/form-data">
		<?=$arResult["BX_SESSION_CHECK"]?>
		<input type="hidden" name="lang" value="<?=LANG?>" />
		<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
		<input type="hidden" name="LOGIN" value=<?=$arResult["arUser"]["LOGIN"]?> />
		<input type="hidden" name="EMAIL" value=<?=$arResult["arUser"]["EMAIL"]?> />

		<h2><?=GetMessage("LEGEND_PROFILE")?></h2>
		<strong><?=GetMessage('NAME')?></strong><br/>
		<input type="text" name="NAME" maxlength="50" value="<?=$arResult["arUser"]["NAME"]?>" /><br><br>

		<strong><?=GetMessage('LAST_NAME')?></strong><br/>
		<input type="text" name="LAST_NAME" maxlength="50" value="<?=$arResult["arUser"]["LAST_NAME"]?>" /><br><br>

		<strong><?=GetMessage('SECOND_NAME')?></strong><br/>
		<input type="text" name="SECOND_NAME" maxlength="50"  value="<?=$arResult["arUser"]["SECOND_NAME"]?>" /><br><br>

		<h2><?=GetMessage("MAIN_PSWD")?></h2>
		<strong><?=GetMessage('NEW_PASSWORD_REQ')?></strong><br/>
		<input type="password" name="NEW_PASSWORD" maxlength="50" value="" autocomplete="off" /> <br><br>

		<strong><?=GetMessage('NEW_PASSWORD_CONFIRM')?></strong><br/>
		<input type="password" name="NEW_PASSWORD_CONFIRM" maxlength="50" value="" autocomplete="off" /> <br><br>

		<?if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):?>
			<h2><?=strlen(trim($arParams["USER_PROPERTY_NAME"])) > 0 ? $arParams["USER_PROPERTY_NAME"] : GetMessage("USER_TYPE_EDIT_TAB")?></h2>
			<?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):?>
				<strong><?=$arUserField["EDIT_FORM_LABEL"]?><?if ($arUserField["MANDATORY"]=="Y"):?><span class="starrequired">*</span><?endif;?></strong><br/>
				<?$APPLICATION->IncludeComponent(
					"bitrix:system.field.edit",
					$arUserField["USER_TYPE"]["USER_TYPE_ID"],
					array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField), null, array("HIDE_ICONS"=>"Y")
				);?>
				<br/>
			<?endforeach;?>
		<?endif;?>

		<input name="save" value="<?=GetMessage("MAIN_SAVE")?>" class="bx_bt_button bx_big shadow" type="submit">
	</form>
</div>
<br>*/?>
<?
/*
if($arResult["SOCSERV_ENABLED"])
{
	$APPLICATION->IncludeComponent("bitrix:socserv.auth.split", ".default", array(
			"SHOW_PROFILES" => "Y",
			"ALLOW_DELETE" => "Y"
		),
		false
	);
}*/
?>