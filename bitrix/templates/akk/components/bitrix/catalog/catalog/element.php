<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

//CDev::pre($arResult);

$this->setFrameMode(true);
if (isset($arParams['USE_COMMON_SETTINGS_BASKET_POPUP']) && $arParams['USE_COMMON_SETTINGS_BASKET_POPUP'] == 'Y')
{
	$basketAction = (isset($arParams['COMMON_ADD_TO_BASKET_ACTION']) ? array($arParams['COMMON_ADD_TO_BASKET_ACTION']) : array());
}
else
{
	$basketAction = (isset($arParams['DETAIL_ADD_TO_BASKET_ACTION']) ? $arParams['DETAIL_ADD_TO_BASKET_ACTION'] : array());
}
?><?$ElementID = $APPLICATION->IncludeComponent(
	"bitrix:catalog.element",
	"",
	array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
		"META_KEYWORDS" => $arParams["DETAIL_META_KEYWORDS"],
		"META_DESCRIPTION" => $arParams["DETAIL_META_DESCRIPTION"],
		"BROWSER_TITLE" => $arParams["DETAIL_BROWSER_TITLE"],
		"BASKET_URL" => $arParams["BASKET_URL"],
		"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
		"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
		"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
		"CHECK_SECTION_ID_VARIABLE" => (isset($arParams["DETAIL_CHECK_SECTION_ID_VARIABLE"]) ? $arParams["DETAIL_CHECK_SECTION_ID_VARIABLE"] : ''),
		"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
		"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"SET_TITLE" => $arParams["SET_TITLE"],
		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
		"PRICE_CODE" => $arParams["PRICE_CODE"],
		"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
		"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
		"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
		"PRICE_VAT_SHOW_VALUE" => $arParams["PRICE_VAT_SHOW_VALUE"],
		"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
		"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
		"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
		"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
		"LINK_IBLOCK_TYPE" => $arParams["LINK_IBLOCK_TYPE"],
		"LINK_IBLOCK_ID" => $arParams["LINK_IBLOCK_ID"],
		"LINK_PROPERTY_SID" => $arParams["LINK_PROPERTY_SID"],
		"LINK_ELEMENTS_URL" => $arParams["LINK_ELEMENTS_URL"],

		"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
		"OFFERS_FIELD_CODE" => $arParams["DETAIL_OFFERS_FIELD_CODE"],
		"OFFERS_PROPERTY_CODE" => $arParams["DETAIL_OFFERS_PROPERTY_CODE"],
		"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
		"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
		"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
		"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],

		"ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
		"ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
		"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
		"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
		'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
		'CURRENCY_ID' => $arParams['CURRENCY_ID'],
		'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
		'USE_ELEMENT_COUNTER' => $arParams['USE_ELEMENT_COUNTER'],

		'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
		'LABEL_PROP' => $arParams['LABEL_PROP'],
		'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
		'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
		'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
		'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
		'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
		'SHOW_MAX_QUANTITY' => $arParams['DETAIL_SHOW_MAX_QUANTITY'],
		'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
		'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
		'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
		'MESS_BTN_COMPARE' => $arParams['MESS_BTN_COMPARE'],
		'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],
		'USE_VOTE_RATING' => $arParams['DETAIL_USE_VOTE_RATING'],
		'VOTE_DISPLAY_AS_RATING' => (isset($arParams['DETAIL_VOTE_DISPLAY_AS_RATING']) ? $arParams['DETAIL_VOTE_DISPLAY_AS_RATING'] : ''),
		'USE_COMMENTS' => $arParams['DETAIL_USE_COMMENTS'],
		'BLOG_USE' => (isset($arParams['DETAIL_BLOG_USE']) ? $arParams['DETAIL_BLOG_USE'] : ''),
		'BLOG_URL' => (isset($arParams['DETAIL_BLOG_URL']) ? $arParams['DETAIL_BLOG_URL'] : ''),
		'BLOG_EMAIL_NOTIFY' => (isset($arParams['DETAIL_BLOG_EMAIL_NOTIFY']) ? $arParams['DETAIL_BLOG_EMAIL_NOTIFY'] : ''),
		'VK_USE' => (isset($arParams['DETAIL_VK_USE']) ? $arParams['DETAIL_VK_USE'] : ''),
		'VK_API_ID' => (isset($arParams['DETAIL_VK_API_ID']) ? $arParams['DETAIL_VK_API_ID'] : 'API_ID'),
		'FB_USE' => (isset($arParams['DETAIL_FB_USE']) ? $arParams['DETAIL_FB_USE'] : ''),
		'FB_APP_ID' => (isset($arParams['DETAIL_FB_APP_ID']) ? $arParams['DETAIL_FB_APP_ID'] : ''),
		'BRAND_USE' => (isset($arParams['DETAIL_BRAND_USE']) ? $arParams['DETAIL_BRAND_USE'] : 'N'),
		'BRAND_PROP_CODE' => (isset($arParams['DETAIL_BRAND_PROP_CODE']) ? $arParams['DETAIL_BRAND_PROP_CODE'] : ''),
		'DISPLAY_NAME' => (isset($arParams['DETAIL_DISPLAY_NAME']) ? $arParams['DETAIL_DISPLAY_NAME'] : ''),
		'ADD_DETAIL_TO_SLIDER' => (isset($arParams['DETAIL_ADD_DETAIL_TO_SLIDER']) ? $arParams['DETAIL_ADD_DETAIL_TO_SLIDER'] : ''),
		'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
		"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : ''),
		"ADD_ELEMENT_CHAIN" => (isset($arParams["ADD_ELEMENT_CHAIN"]) ? $arParams["ADD_ELEMENT_CHAIN"] : ''),
		"DISPLAY_PREVIEW_TEXT_MODE" => (isset($arParams['DETAIL_DISPLAY_PREVIEW_TEXT_MODE']) ? $arParams['DETAIL_DISPLAY_PREVIEW_TEXT_MODE'] : ''),
		"DETAIL_PICTURE_MODE" => (isset($arParams['DETAIL_DETAIL_PICTURE_MODE']) ? $arParams['DETAIL_DETAIL_PICTURE_MODE'] : ''),
		'ADD_TO_BASKET_ACTION' => $basketAction,
		'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
		'DISPLAY_COMPARE' => (isset($arParams['USE_COMPARE']) ? $arParams['USE_COMPARE'] : ''),
		'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
		'SHOW_BASIS_PRICE' => (isset($arParams['DETAIL_SHOW_BASIS_PRICE']) ? $arParams['DETAIL_SHOW_BASIS_PRICE'] : 'Y')
	),
	$component
);?><?
$GLOBALS["CATALOG_CURRENT_ELEMENT_ID"] = $ElementID;
unset($basketAction);
if ($ElementID > 0)
{
    ?>
    <article class="product_info_box clearfix">
        <div class="grid_38 prefix_2 resp_full_w">
            <div class="product_page_tabs">
                <ul class="product_page_tabs_nav clearfix">
                    <li class="product_page_tab_item"><a href="#similar">Похожие товары</a></li>
                    <li class="product_page_tab_item"><a href="#testimonials">Отзывы</a></li>
                </ul>
                <div id="similar">
                <?
                
                CModule::IncludeModule("iblock");
                $res = CIBlockElement::GetByID($ElementID);
                $arElement = $res->GetNext();

                global $similarFilter;
                $similarFilter["=SECTION_ID"] = $arElement["IBLOCK_SECTION_ID"];
                $similarFilter["INCLUDE_SUBSECTIONS"] = "Y";
                $similarFilter["!ID"] = $ElementID;
                ?>
                <?$APPLICATION->IncludeComponent(
                	"bitrix:catalog.top",
                	"similar",
                	Array(
                		"IBLOCK_TYPE" => "catalog",
                		"IBLOCK_ID" => "2",
                		"ELEMENT_SORT_FIELD" => "id",
                		"ELEMENT_SORT_ORDER" => "desc",
                		"ELEMENT_SORT_FIELD2" => "rand",
                		"ELEMENT_SORT_ORDER2" => "desc",
                		"FILTER_NAME" => "similarFilter",
                		"HIDE_NOT_AVAILABLE" => "N",
                		"ELEMENT_COUNT" => "9",
                		"LINE_ELEMENT_COUNT" => "3",
                		"PROPERTY_CODE" => array("BRAND", "MODEL", "MODEL_ROW", "TITLE", "KEYWORDS", "META_DESCRIPTION", "SALELEADER", "SPECIALOFFER", "ARTNUMBER", "BLOG_POST_ID", "GARANTY", "CAPACITY", "BLOG_COMMENTS_CNT", "FORUM_MESSAGE_CNT", "vote_count", "POLARITY", "START_TOK", "rating", "RECOMMEND", "vote_sum", "FORUM_TOPIC_ID", "TYPE", "TOKEND", "PRICE_CHANGE", ""),
                		"OFFERS_LIMIT" => "12",
                		"VIEW_MODE" => "SECTION",
                		"SHOW_DISCOUNT_PERCENT" => "N",
                		"SHOW_OLD_PRICE" => "N",
                		"SHOW_CLOSE_POPUP" => "N",
                		"MESS_BTN_BUY" => "Купить",
                		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
                		"MESS_BTN_DETAIL" => "Подробнее",
                		"MESS_NOT_AVAILABLE" => "Нет в наличии",
                		"SECTION_URL" => "",
                		"DETAIL_URL" => "",
                		"SECTION_ID_VARIABLE" => "SECTION_ID",
                		"CACHE_TYPE" => "N",
                		"CACHE_TIME" => "36000000",
                		"CACHE_GROUPS" => "Y",
                		"CACHE_FILTER" => "N",
                		"ACTION_VARIABLE" => "action",
                		"PRODUCT_ID_VARIABLE" => "id",
                		"PRICE_CODE" => array("BASE"),
                		"USE_PRICE_COUNT" => "N",
                		"SHOW_PRICE_COUNT" => "1",
                		"PRICE_VAT_INCLUDE" => "Y",
                		"CONVERT_CURRENCY" => "Y",
                		"BASKET_URL" => "/personal/cart/",
                		"USE_PRODUCT_QUANTITY" => "N",
                		"ADD_PROPERTIES_TO_BASKET" => "Y",
                		"PRODUCT_PROPS_VARIABLE" => "prop",
                		"PARTIAL_PRODUCT_PROPERTIES" => "N",
                		"PRODUCT_PROPERTIES" => array(),
                		"ADD_TO_BASKET_ACTION" => "ADD",
                		"DISPLAY_COMPARE" => "Y",
                		"TEMPLATE_THEME" => "site",
                		"ADD_PICT_PROP" => "-",
                		"LABEL_PROP" => "-",
                		"MESS_BTN_COMPARE" => "Сравнить",
                		"CURRENCY_ID" => "RUB",
                		"COMPARE_PATH" => ""
                	)
                );?>
            </div>
        
            <div id="testimonials">
                <?
                global $comFilter;
                $comFilter["=PROPERTY_PRODUCT_ID"] = $ElementID;
                ?>
                <?$APPLICATION->IncludeComponent(
                	"bitrix:news.list",
                	"comments",
                	Array(
                		"IBLOCK_TYPE" => "services",
                		"IBLOCK_ID" => "6",
                		"NEWS_COUNT" => "20",
                		"SORT_BY1" => "ACTIVE_FROM",
                		"SORT_ORDER1" => "DESC",
                		"SORT_BY2" => "SORT",
                		"SORT_ORDER2" => "ASC",
                		"FILTER_NAME" => "comFilter",
                		"FIELD_CODE" => array("DATE_CREATE"),
                		"PROPERTY_CODE" => array("USER_ID", "PLUS", "MINUS", "PRODUCT_ID"),
                		"CHECK_DATES" => "Y",
                		"DETAIL_URL" => "",
                		"AJAX_MODE" => "N",
                		"AJAX_OPTION_JUMP" => "N",
                		"AJAX_OPTION_STYLE" => "Y",
                		"AJAX_OPTION_HISTORY" => "N",
                		"CACHE_TYPE" => "N",
                		"CACHE_TIME" => "36000000",
                		"CACHE_FILTER" => "N",
                		"CACHE_GROUPS" => "Y",
                		"PREVIEW_TRUNCATE_LEN" => "",
                		"ACTIVE_DATE_FORMAT" => "j F Y",
                		"SET_TITLE" => "N",
                		"SET_BROWSER_TITLE" => "N",
                		"SET_META_KEYWORDS" => "N",
                		"SET_META_DESCRIPTION" => "N",
                		"SET_STATUS_404" => "N",
                		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                		"ADD_SECTIONS_CHAIN" => "N",
                		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
                		"PARENT_SECTION" => "",
                		"PARENT_SECTION_CODE" => "",
                		"INCLUDE_SUBSECTIONS" => "Y",
                		"DISPLAY_DATE" => "Y",
                		"DISPLAY_NAME" => "Y",
                		"DISPLAY_PICTURE" => "Y",
                		"DISPLAY_PREVIEW_TEXT" => "Y",
                		"PAGER_TEMPLATE" => ".default",
                		"DISPLAY_TOP_PAGER" => "N",
                		"DISPLAY_BOTTOM_PAGER" => "N",
                		"PAGER_TITLE" => "Новости",
                		"PAGER_SHOW_ALWAYS" => "N",
                		"PAGER_DESC_NUMBERING" => "N",
                		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                		"PAGER_SHOW_ALL" => "N",
                        "PRODUCT_ID" => $ElementID
                	)
                );?>
                </div>
                
            </div>
        </div>
    </article>
    <?
}
?>