<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="clearfix">
    <div class="grid_24 prefix_2 resp_p0">
        <?$ElementID = $APPLICATION->IncludeComponent(
        	"bitrix:news.detail",
        	"",
        	Array(
        		"DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
        		"DISPLAY_NAME" => $arParams["DISPLAY_NAME"],
        		"DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
        		"DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
        		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
        		"FIELD_CODE" => $arParams["DETAIL_FIELD_CODE"],
        		"PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
        		"META_KEYWORDS" => $arParams["META_KEYWORDS"],
        		"META_DESCRIPTION" => $arParams["META_DESCRIPTION"],
        		"BROWSER_TITLE" => $arParams["BROWSER_TITLE"],
        		"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
        		"SET_TITLE" => $arParams["SET_TITLE"],
        		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
        		"INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
        		"ADD_SECTIONS_CHAIN" => $arParams["ADD_SECTIONS_CHAIN"],
        		"ACTIVE_DATE_FORMAT" => $arParams["DETAIL_ACTIVE_DATE_FORMAT"],
        		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
        		"CACHE_TIME" => $arParams["CACHE_TIME"],
        		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
        		"USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
        		"GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
        		"DISPLAY_TOP_PAGER" => $arParams["DETAIL_DISPLAY_TOP_PAGER"],
        		"DISPLAY_BOTTOM_PAGER" => $arParams["DETAIL_DISPLAY_BOTTOM_PAGER"],
        		"PAGER_TITLE" => $arParams["DETAIL_PAGER_TITLE"],
        		"PAGER_SHOW_ALWAYS" => "N",
        		"PAGER_TEMPLATE" => $arParams["DETAIL_PAGER_TEMPLATE"],
        		"PAGER_SHOW_ALL" => $arParams["DETAIL_PAGER_SHOW_ALL"],
        		"CHECK_DATES" => $arParams["CHECK_DATES"],
        
        		"ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
        		"ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
        		"IBLOCK_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
        	),
        	$component
        );?>

        <?if($arParams["USE_RATING"]=="Y" && $ElementID):?>
        <?$APPLICATION->IncludeComponent(
        	"bitrix:iblock.vote",
        	"",
        	Array(
        		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
        		"ELEMENT_ID" => $ElementID,
        		"MAX_VOTE" => $arParams["MAX_VOTE"],
        		"VOTE_NAMES" => $arParams["VOTE_NAMES"],
        		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
        		"CACHE_TIME" => $arParams["CACHE_TIME"],
        	),
        	$component
        );?>
        <?endif?>
        <?if($arParams["USE_CATEGORIES"]=="Y" && $ElementID):
        	global $arCategoryFilter;
        	$obCache = new CPHPCache;
        	$strCacheID = $componentPath.LANG.$arParams["IBLOCK_ID"].$ElementID.$arParams["CATEGORY_CODE"];
        	if($arParams["CACHE_TYPE"] == "N" || $arParams["CACHE_TYPE"] == "A" && COption::GetOptionString("main", "component_cache_on", "Y") == "N")
        		$CACHE_TIME = 0;
        	else
        		$CACHE_TIME = $arParams["CACHE_TIME"];
        	if($obCache->StartDataCache($CACHE_TIME, $strCacheID, $componentPath))
        	{
        		$rsProperties = CIBlockElement::GetProperty($arParams["IBLOCK_ID"], $ElementID, "sort", "asc", array("ACTIVE"=>"Y","CODE"=>$arParams["CATEGORY_CODE"]));
        		$arCategoryFilter = array();
        		while($arProperty = $rsProperties->Fetch())
        		{
        			if(is_array($arProperty["VALUE"]) && count($arProperty["VALUE"])>0)
        			{
        				foreach($arProperty["VALUE"] as $value)
        					$arCategoryFilter[$value]=true;
        			}
        			elseif(!is_array($arProperty["VALUE"]) && strlen($arProperty["VALUE"])>0)
        				$arCategoryFilter[$arProperty["VALUE"]]=true;
        		}
        		$obCache->EndDataCache($arCategoryFilter);
        	}
        	else
        	{
        		$arCategoryFilter = $obCache->GetVars();
        	}
        	if(count($arCategoryFilter)>0):
        		$arCategoryFilter = array(
        			"PROPERTY_".$arParams["CATEGORY_CODE"] => array_keys($arCategoryFilter),
        			"!"."ID" => $ElementID,
        		);
        		?>
        		<hr /><h3><?=GetMessage("CATEGORIES")?></h3>
        		<?foreach($arParams["CATEGORY_IBLOCK"] as $iblock_id):?>
        			<?$APPLICATION->IncludeComponent(
        				"bitrix:news.list",
        				$arParams["CATEGORY_THEME_".$iblock_id],
        				Array(
        					"IBLOCK_ID" => $iblock_id,
        					"NEWS_COUNT" => $arParams["CATEGORY_ITEMS_COUNT"],
        					"SET_TITLE" => "N",
        					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        					"CACHE_TYPE" => $arParams["CACHE_TYPE"],
        					"CACHE_TIME" => $arParams["CACHE_TIME"],
        					"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
        					"FILTER_NAME" => "arCategoryFilter",
        					"CACHE_FILTER" => "Y",
        					"DISPLAY_TOP_PAGER" => "N",
        					"DISPLAY_BOTTOM_PAGER" => "N",
        				),
        				$component
        			);?>
        		<?endforeach?>
        	<?endif?>
        <?endif?>
        <?if($arParams["USE_REVIEW"]=="Y" && IsModuleInstalled("forum") && $ElementID):?>
        <hr />
        <?$APPLICATION->IncludeComponent(
        	"bitrix:forum.topic.reviews",
        	"",
        	Array(
        		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
        		"CACHE_TIME" => $arParams["CACHE_TIME"],
        		"MESSAGES_PER_PAGE" => $arParams["MESSAGES_PER_PAGE"],
        		"USE_CAPTCHA" => $arParams["USE_CAPTCHA"],
        		"PATH_TO_SMILE" => $arParams["PATH_TO_SMILE"],
        		"FORUM_ID" => $arParams["FORUM_ID"],
        		"URL_TEMPLATES_READ" => $arParams["URL_TEMPLATES_READ"],
        		"SHOW_LINK_TO_FORUM" => $arParams["SHOW_LINK_TO_FORUM"],
        		"DATE_TIME_FORMAT" => $arParams["DETAIL_ACTIVE_DATE_FORMAT"],
        		"ELEMENT_ID" => $ElementID,
        		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
        		"URL_TEMPLATES_DETAIL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"]
        	),
        	$component
        );?>
        <?endif?>
    </div>  
    <!--<aside class="grid_8 prefix_4 resp_col28p">
        <h3 class="fs_24 black reg mb25">Мы в соц. сетях</h3>
        <div class="vk_box">
            <script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>
            <div id="vk_groups"></div>
            <script type="text/javascript">
            VK.Widgets.Group("vk_groups", {mode: 0, width: "220", height: "321", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 83340791);
            </script>
        </div>
        <?
        global $arrFilterNews;
        $arrFilterNews["!ID"] = $ElementID;
        ?>
        <?$APPLICATION->IncludeComponent(
        	"bitrix:news.list",
        	"articles",
        	Array(
        		"IBLOCK_TYPE" => "news",
        		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
        		"NEWS_COUNT" => "3",
        		"SORT_BY1" => "ACTIVE_FROM",
        		"SORT_ORDER1" => "DESC",
        		"SORT_BY2" => "SORT",
        		"SORT_ORDER2" => "ASC",
        		"FILTER_NAME" => "arrFilterNews",
        		"FIELD_CODE" => array("", ""),
        		"PROPERTY_CODE" => array('LABEL'),
        		"CHECK_DATES" => "Y",
        		"DETAIL_URL" => "",
        		"AJAX_MODE" => "N",
        		"AJAX_OPTION_JUMP" => "N",
        		"AJAX_OPTION_STYLE" => "Y",
        		"AJAX_OPTION_HISTORY" => "N",
        		"CACHE_TYPE" => "A",
        		"CACHE_TIME" => "36000000",
        		"CACHE_FILTER" => "N",
        		"CACHE_GROUPS" => "Y",
        		"PREVIEW_TRUNCATE_LEN" => "100",
        		"ACTIVE_DATE_FORMAT" => "j F Y",
        		"SET_TITLE" => "N",
        		"SET_BROWSER_TITLE" => "N",
        		"SET_META_KEYWORDS" => "N",
        		"SET_META_DESCRIPTION" => "N",
        		"SET_STATUS_404" => "N",
        		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        		"ADD_SECTIONS_CHAIN" => "N",
        		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
        		"PARENT_SECTION" => "",
        		"PARENT_SECTION_CODE" => "",
        		"INCLUDE_SUBSECTIONS" => "Y",
        		"DISPLAY_DATE" => "Y",
        		"DISPLAY_NAME" => "Y",
        		"DISPLAY_PICTURE" => "Y",
        		"DISPLAY_PREVIEW_TEXT" => "Y",
        		"PAGER_TEMPLATE" => ".default",
        		"DISPLAY_TOP_PAGER" => "N",
        		"DISPLAY_BOTTOM_PAGER" => "N",
        		"PAGER_TITLE" => $arParams["IBLOCK_ID"]==5 ? "Новости" : "Статьи",
        		"PAGER_SHOW_ALWAYS" => "N",
        		"PAGER_DESC_NUMBERING" => "N",
        		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        		"PAGER_SHOW_ALL" => "N",
                "NEWS_TITLE" => "Статьи",
                "LABEL_CSS" => "blue_label"
        	)
        );?>
    </aside>  -->
</div>