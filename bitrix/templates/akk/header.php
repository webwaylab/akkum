<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

global $USER, $APPLICATION;
CUtil::InitJSCore();
CJSCore::Init(array("fx", "ajax"));

$pageInstance = Bitrix\Main\Page\Asset::getInstance();

$curPage = $APPLICATION->GetCurPage(true);
?><!DOCTYPE html>
<html lang="<?=LANGUAGE_ID?>">
<head>
    <?
    $seo_url = array(
        '/catalog/accessories/zaryadnoe_ustrojstvo/',
        '/catalog/accessories/nabor_instrumentov_dlya_avto/',
        '/catalog/avtomobilnye/',
        '/catalog/battery/zver/',
        '/catalog/battery/varta/',
        '/catalog/battery/varta/blue_dynamic/',
        '/catalog/battery/varta/silver_dynamic/',
        '/catalog/accessories/nagruzochnye_vilki/',
        '/catalog/battery/gelevye/akb/',
        '/catalog/battery/gelevye/',
        '/catalog/battery/bosch/',
        '/catalog/battery/bosch/s4_silver/',
        '/catalog/battery/mutlu/',
        '/',
        '/catalog/battery/moto_akkumulyatory/',
        '/catalog/battery/tudor/',
        '/catalog/auto/',
        '/catalog/battery/afa/',
        '/catalog/battery/solite/',
        '/catalog/battery/titan/',
        '/catalog/battery/aktex/',
        '/catalog/battery/vaiper/',
        '/catalog/battery/royal/',
        '/catalog/battery/cobat/',
        '/catalog/battery/tyumen/',
        '/catalog/battery/tyumen/lider/',
        '/catalog/battery/bolk_moto/',
        '/catalog/battery/sebang/',
    );

    if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/d-robots.php')) {
    include_once ($_SERVER['DOCUMENT_ROOT'] . '/d-robots.php');
    $dRobots = dRobots::fromFile();
    $noindex = $dRobots->checkUrl($_SERVER['REQUEST_URI']) ? '<meta name="googlebot" content="noindex">' . PHP_EOL : '';
    } else $noindex = '';

    //Вывод в шаблоне
    echo $noindex;

 if (preg_match('#^/\?PAGEN_1=#',$_SERVER['REQUEST_URI'])){
  echo '<meta name="robots" content="noindex, follow"/>';
 }

$url=SITE_TEMPLATE_PATH; // example http url ##

$url = str_replace('http://', 'https://', $url );
define(SITE_TEMPLATE_PATH,$url);


 $dir = $APPLICATION->GetCurDir();
    /*if($dir='/' && empty($noindex)){
        if(isset($_GET['PAGEN_1'])){
            echo '<meta name="robots" content="none"/>';
        }
    }*/
?>

    <?/*<meta http-equiv="Content-Type" content="text/html; charset=<?=LANG_CHARSET;    //Установка кодировки сайта // Это выводится через $APPLICATION->ShowHead() ?> " /> */?>

    <?/* if (in_array($_SERVER['REQUEST_URI'], $seo_url)) { ?>
        <? $APPLICATION->ShowMeta("keywords")   // Вывод мета тега keywords // Выводится через $APPLICATION->ShowHead() ?>
    <? } */?>

    <?
    $pageInstance->addCss('//fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic&subset=latin,cyrillic-ext,cyrillic');
    $pageInstance->addCss(SITE_TEMPLATE_PATH . '/project.css');
    $pageInstance->addCss(SITE_TEMPLATE_PATH . '/css/jquery.fancybox.css');
    $pageInstance->addCss(SITE_TEMPLATE_PATH . '/css/bootstrap-modal.min.css');
    $pageInstance->addCss(SITE_TEMPLATE_PATH."/сss/bootstrap-modal.min.css");
    $pageInstance->addCss(SITE_TEMPLATE_PATH."/css/jquery-ui.css");
    $pageInstance->addCss(SITE_TEMPLATE_PATH."/css/jquery.fancybox.css");
    $pageInstance->addCss("/colorbox/colorbox.css");
    $pageInstance->addCss(SITE_TEMPLATE_PATH."/css/style.css");
    $pageInstance->addCss(SITE_TEMPLATE_PATH."/project.css");
    $pageInstance->addCss(SITE_TEMPLATE_PATH."/css/style_new_design.css");

    $pageInstance->addJs(SITE_TEMPLATE_PATH . '/js/jquery-2.1.3.min.js');
    $pageInstance->addJs(SITE_TEMPLATE_PATH . '/js/jquery-migrate-1.2.1.js');
    $pageInstance->addJs('/colorbox/jquery.colorbox.js');
    $pageInstance->addJs('//maps.googleapis.com/maps/api/js?sensor=false');
    $pageInstance->addJs(SITE_TEMPLATE_PATH . '/js/bootstrap-modal.min.js');
    $pageInstance->addJs(SITE_TEMPLATE_PATH . '/js/bootstrap-modalmanager.min.js');
    $pageInstance->addJs(SITE_TEMPLATE_PATH . '/project.js');
    $pageInstance->addJs(SITE_TEMPLATE_PATH . '/js/filter.js');
    $pageInstance->addJs(SITE_TEMPLATE_PATH . '/js/jquery.equalheights.js');
    $pageInstance->addJs(SITE_TEMPLATE_PATH . '/js/jquery.flexslider.js');
    $pageInstance->addJs(SITE_TEMPLATE_PATH . '/js/jquery.formstyler.js');
    $pageInstance->addJs(SITE_TEMPLATE_PATH . '/js/jquery.raty.js');
    $pageInstance->addJs(SITE_TEMPLATE_PATH . '/js/jquery-ui.min.js');
    $pageInstance->addJs(SITE_TEMPLATE_PATH . '/js/mapmarker.jquery.js');
    $pageInstance->addJs(SITE_TEMPLATE_PATH . '/js/jquery.arcticmodal.js');
    $pageInstance->addJs(SITE_TEMPLATE_PATH . '/js/jquery.maskedinput.js');
    $pageInstance->addJs(SITE_TEMPLATE_PATH . '/owl-carousel/owl.carousel.js');
    $pageInstance->addJs(SITE_TEMPLATE_PATH . '/js/jquery.fancybox-1.2.1.pack.js');
    $pageInstance->addJs(SITE_TEMPLATE_PATH . '/js/jquery.easing.1.3.js');
    $pageInstance->addJs(SITE_TEMPLATE_PATH . '/js/jquery.mousewheel-3.0.4.pack.js');
    $pageInstance->addJs(SITE_TEMPLATE_PATH . '/js/script.js');
    ?>
    <?$APPLICATION->ShowHead();?>


    <title><?$APPLICATION->ShowTitle(false)?></title>

    <meta name="format-detection" content="telephone=yes" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta name='yandex-verification' content='4d5c3ad5e5616770' />
    <meta name='yandex-verification' content='557078134c1f7e89' />

    <link rel="icon" href="<?=SITE_TEMPLATE_PATH?>/images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="<?=SITE_TEMPLATE_PATH?>/images/favicon.ico" type="image/x-icon" />
    <!--[if lt IE 8]>
        <div style=' clear: both; text-align:center; position: relative;'>
            <a href="//windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
                <img src="//storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
            </a>
        </div>
    <![endif]-->
    <!--[if lt IE 9]>
        <script src="<?=SITE_TEMPLATE_PATH?>/js/html5.js"></script>
        <link rel="stylesheet" type="text/css" media="screen" href="<?=SITE_TEMPLATE_PATH?>/css/ie.css">
    <![endif]-->

    <meta name="google-site-verification" content="XWpKFVWd-OxYerH0APZky2z6vq8dmvyRjQOZdFDR0PA" />
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-85801335-1', 'auto');
        ga('require', 'linkid');
        ga('send', 'pageview');

    </script>

    <?
        $array_link = array(
            '/order/quick_credit.php',
            '/search/',
            '/search/map.php',
            '/club/search/',
            '/club/group/search/',
            '/club/forum/search/',
            '/communication/forum/search/',
            '/communication/blog/search.php',
            '/club/gallery/tags/',
            '/examples/my-components/',
            '/examples/download/download_private/',
            '/auth/',
            '/auth.php',
            '/personal/',
            '/communication/forum/user/',
            '/e-store/paid/detail.php',
            '/e-store/affiliates/',
            '/club/',
            '/club/messages/',
            '/club/log/',
            '/content/board/my/',
            '/content/links/my/',
        );
    ?>

<? if (in_array($_SERVER['REQUEST_URI'], $array_link)) { ?>
    <meta name="googlebot" content="noindex">
<? } ?>


</head>
<body <?if ($curPage != SITE_DIR."index.php"):?>class="inner"<?endif;?>>
    <section class="wrap">
    <section class="content">
    <section class="info_box light_font">
        <?$APPLICATION->ShowPanel();?>
        <div class="container clearfix">
            <div class="grid_40">
                <div class="clearfix">
                    <div class="f_left">
                        <a href="tel:+7 (499) 650-52-09" class="info_phone_num d_ib white" onClick="yaCounter22871155.reachGoal('tel'); ga('send', 'pageview', '/tel/');"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/phone.php"), false);?></a>
                        <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/email_href.php"), false);?>
                    </div>
                    <div class="f_right">
                        <a href="/contacts/info/" class="info_link"><i class="icon-clock"></i>ПН-ПТ 10:00 — 21:00 СБ-ВС 10:00 — 19:00</a>
                        <?if($USER->IsAuthorized()):?>
                            <a href="/personal/" class="info_link authorized"><i class="icon-battery"></i><span>Личный кабинет</span></a>
                        <?else:?>
                            <div class="info_link modal_btn auth_link" data-modal="#authorization"><i class="icon-battery"></i><span>Авторизация</span></div>
                        <?endif;?>
                        <?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "mini-basket", array(
                                "PATH_TO_BASKET" => SITE_DIR."personal/cart/",
                                "PATH_TO_PERSONAL" => SITE_DIR."personal/",
                                "SHOW_PERSONAL_LINK" => "N",
                                "SHOW_NUM_PRODUCTS" => "Y",
                                "SHOW_TOTAL_PRICE" => "N",
                                "SHOW_PRODUCTS" => "N",
                                "POSITION_FIXED" =>"N"
                            ),
                            false,
                            array()
                        );?>

                        <span id="callback_form_link" class="big_callback_btn d_ib modal_btn" data-modal="#callback_form">Заказать звонок</span>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <header>
        <div class="container">
            <div class="clearfix">
                <!-- logo -->
                <div class="grid_13">
                    <div class="logo clearfix">
                        <a href="/" class="logotype" title="АКБ">
                            <img src="/bitrix/templates/akk/images/logo.png" class="logotype__img" alt="АКБ">
                            <?$style = (strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox')) ? 'style="letter-spacing: -0.06px;"' : ''?>
                            <div class="logotype__text" <?=$style?>>
                                <span class="logotype__text-title">Аккумуляторы</span>
                                <p>и авто аксессуары по лучшей цене</p>
                            </div>
                        </a>
                    </div>
                </div>
                <!-- !! -->

                <!-- work time -->
                <div class="grid_20">
                    <div class="header__menu_links">
                        <a href="/about/shop/" class="mini_menu_link">О магазине</a>
                        <a href="/about/shop/sertifikaty-i-litsenzii/" class="mini_menu_link">Сертификаты и лицензии</a>
                        <a href="/about/shop/voprosy-i-otvety/" class="mini_menu_link">Вопросы и ответы</a>
                        <a href="/about/shop/garantiya-i-vozvrat/" class="mini_menu_link">Гарантия и возврат</a>
                        <a href="/process/delivery/" class="mini_menu_link">Доставка и оплата</a>
                        <a href="/contacts/info/" class="mini_menu_link">Контакты</a>
                    </div>
                    <?/*редизайн<div class="work_time wrapper reg fs_12">
                        <div class="work_time_item f_left">
                            <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/schedule-left.php"), false);?>
                        </div>
                        <div class="work_time_item f_left">
                            <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/schedule-right.php"), false);?>
                        </div>
                    </div>*/?>
                </div>
                <!-- !! -->

                <!-- search -->
                <div class="grid_7">
                    <form class="search_form" action="/search/">
                        <input type="text" name="q" class="search_form_input" placeholder="Поиск по сайту" value="" <?/*onBlur="if(this.value=='') this.value='Поиск по сайту'" onFocus="if(this.value =='Введите запрос' ) this.value=''"*/?>>
                        <button class="search_form_btn"><i class="icon-search"></i></button>
                    </form>
                </div>
                <!-- !! -->
            </div>
            <?/*<hr class="header_separator">*/?>
        </div>
    </header>

    <?
    CModule::IncludeModule("iblock");
    $news = CIBlock::GetList(array(), array('CODE' => 'news'))->Fetch();
    if ($news['DESCRIPTION']){ ?>
        <div class="container akkum-information">
            <div class="working__time">
                <img width="18" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAMAAABhEH5lAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAdVBMVEUAAAAPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIAAAA1XNlKAAAAJXRSTlMADCcEZMf+KNLrvQ1jmjbO8/mqe7ToAu0BA/KlPlyJQbigGjiiF7urNAAAAAFiS0dEAIgFHUgAAAAJcEhZcwAACxIAAAsSAdLdfvwAAACKSURBVBjTbVDXEoNACOSqJWq6xhJN4/9/MQrcTSZz+wKzsLAAwFBaK/iBsc4jemdNYLIcBXkmTIERBXGGe8pdRX2b1nK9hoaiXSknmv2Bglu3+y05ns4XrngFmpJraVrZooXC7tYPgWIhjjBFoYy/w4xhvJhY4IHRBFt9vt6faDVxUOrs1HP+XvgFfTEY4hOeY8AAAAAASUVORK5CYII=" height="18" title="" alt="">&nbsp;<?=$news['DESCRIPTION']?>
            </div>
            <br>
        </div>
    <? } ?>

    <?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"top_menu",
	array(
		"ROOT_MENU_TYPE" => "top",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "1",
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N",
		"USE_EXT" => "N",
		"COMPONENT_TEMPLATE" => "top_menu"
	),
	false
);

        ?>


        <?/*редизайн<?if ($curPage == SITE_DIR."index.php"):?>
        <?$APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "slider",
            Array(
                "IBLOCK_TYPE" => "services",
                "IBLOCK_ID" => "4",
                "NEWS_COUNT" => "20",
                "SORT_BY1" => "ACTIVE_FROM",
                "SORT_ORDER1" => "DESC",
                "SORT_BY2" => "SORT",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "",
                "FIELD_CODE" => array("", ""),
                "PROPERTY_CODE" => array("YOUTUBE", "TYPE", "PRICE"),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_BROWSER_TITLE" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "INCLUDE_SUBSECTIONS" => "Y",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "PAGER_TEMPLATE" => ".default",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N"
            )
        );?>
<?endif;?>*/?>

    <article id="content" <?if ($curPage != SITE_DIR."index.php"):?>class="catalog_content"<?endif;?>>

        <div class="container">
            <?if ($curPage != SITE_DIR."index.php" && strpos($APPLICATION->GetCurDir(), "/personal/")===false):?>
                <?
                if(strpos($APPLICATION->GetCurDir(), "/catalog/")!==false)
                {
                    $start = 1;
                }else{
                    $start = 0;
                }
                ?>
                <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
                        "START_FROM" => $start,
                        "PATH" => "",
                        "SITE_ID" => "-"
                    ),
                    false,
                    Array('HIDE_ICONS' => 'Y')
                );?>
            <?endif?>

        <?
        if(
            strpos($APPLICATION->GetCurDir(), "/personal/")===false && $curPage != SITE_DIR."index.php" &&
            strpos($APPLICATION->GetCurDir(), "/catalog/")===false && strpos($APPLICATION->GetCurDir(), "/search/")===false &&
            strpos($APPLICATION->GetCurDir(), "/budget/")===false && strpos($APPLICATION->GetCurDir(), "/search/")===false &&
             strpos($APPLICATION->GetCurDir(), "/articles/")===false && strpos($APPLICATION->GetCurDir(), "/popular/")===false
        ){
            ?>
            <div class="clearfix">
                <div class="grid_26 prefix_2 resp_p0">
                    <div class="article_text_page">
            <?
        }
        ?>
