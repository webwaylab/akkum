						<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
						 if(
									strpos($APPLICATION->GetCurDir(), "/personal/")===false && $curPage != SITE_DIR."index.php" &&
									strpos($APPLICATION->GetCurDir(), "/catalog/")===false && strpos($APPLICATION->GetCurDir(), "/search/")===false &&
									strpos($APPLICATION->GetCurDir(), "/articles/")===false && strpos($APPLICATION->GetCurDir(), "/budget/")===false &&
									strpos($APPLICATION->GetCurDir(), "/popular/")===false
								){
						$ibloc=1;
						if(strpos($APPLICATION->GetCurDir(), "/stati/")===false)
						$ibloc=5;
						?>
                    </div>
                </div>
                <aside class="grid_8 prefix_2 resp_col28p">
                    <div class="fs_24 black reg mb25">Мы в соц. сетях</div>
                    <div class="vk_box">
                        <script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>
                        <div id="vk_groups"></div>
                        <script type="text/javascript">
                        VK.Widgets.Group("vk_groups", {mode: 0, width: "220", height: "321", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 83340791);
                        </script>
                    </div>
                    <?$APPLICATION->IncludeComponent(
                    	"bitrix:news.list",
                    	"articles",
                    	Array(
                    		"IBLOCK_TYPE" => "news",
							"IBLOCK_ID" => $ibloc, //"5",
                    		"NEWS_COUNT" => "3",
                    		"SORT_BY1" => "ACTIVE_FROM",
                    		"SORT_ORDER1" => "DESC",
                    		"SORT_BY2" => "SORT",
                    		"SORT_ORDER2" => "ASC",
                    		"FILTER_NAME" => "",
                    		"FIELD_CODE" => array("", ""),
                    		"PROPERTY_CODE" => array('LABEL'),
                    		"CHECK_DATES" => "Y",
                    		"DETAIL_URL" => "",
                    		"AJAX_MODE" => "N",
                    		"AJAX_OPTION_JUMP" => "N",
                    		"AJAX_OPTION_STYLE" => "Y",
                    		"AJAX_OPTION_HISTORY" => "N",
                    		"CACHE_TYPE" => "A",
                    		"CACHE_TIME" => "36000000",
                    		"CACHE_FILTER" => "N",
                    		"CACHE_GROUPS" => "Y",
                    		"PREVIEW_TRUNCATE_LEN" => "100",
                    		"ACTIVE_DATE_FORMAT" => "j F Y",
                    		"SET_TITLE" => "N",
                    		"SET_BROWSER_TITLE" => "N",
                    		"SET_META_KEYWORDS" => "N",
                    		"SET_META_DESCRIPTION" => "N",
                    		"SET_STATUS_404" => "N",
                    		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    		"ADD_SECTIONS_CHAIN" => "N",
                    		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    		"PARENT_SECTION" => "",
                    		"PARENT_SECTION_CODE" => "",
                    		"INCLUDE_SUBSECTIONS" => "Y",
                    		"DISPLAY_DATE" => "Y",
                    		"DISPLAY_NAME" => "Y",
                    		"DISPLAY_PICTURE" => "Y",
                    		"DISPLAY_PREVIEW_TEXT" => "Y",
                    		"PAGER_TEMPLATE" => ".default",
                    		"DISPLAY_TOP_PAGER" => "N",
                    		"DISPLAY_BOTTOM_PAGER" => "N",
                    		"PAGER_TITLE" => "Новости",
                    		"PAGER_SHOW_ALWAYS" => "N",
                    		"PAGER_DESC_NUMBERING" => "N",
                    		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    		"PAGER_SHOW_ALL" => "N",
                            "NEWS_TITLE" => "Статьи",
                            "LABEL_CSS" => "blue_label"
                    	)
                    );?>
                </aside>
            </div>
            <?
        }
        ?>
<?
 if(
            strpos($APPLICATION->GetCurDir(), "/personal/")===false && $curPage != SITE_DIR."index.php" &&
            strpos($APPLICATION->GetCurDir(), "/catalog/")===false && strpos($APPLICATION->GetCurDir(), "/search/")===false &&
			strpos($APPLICATION->GetCurDir(), "/news/")===false && strpos($APPLICATION->GetCurDir(), "/articles/")===false
        ){
            ?>
                    </div>
                </div>
            </div>
            <?
        }
        ?>
        </div>
    </article>
    </section>
    <footer class="footer">
        <div class="container">
            <div class="clearfix">
                <div class="grid_10">
                    <p class="footer_copyright">
                        <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/copyright.php"), false);?>
                    </p>

                    <div class="footer-address">
                        <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/footer_address.php"), false);?>
                    </div>

                    <a href="tel:+7 (499) 650-52-09" onclick="yaCounter22871155.reachGoal('tel'); ga('send', 'pageview', '/tel/');" class="footer_phone">
                        <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/phone.php"), false);?>
                    </a>

                    <span class="product_btn footer_callback modal_btn" data-modal="#callback_form">Заказать обратный звонок</span>

                    <div class="footer_img-box">
                        <img src="/bitrix/templates/akk/images/mastercard.png" class="footer_img" alt="MasterCard">
                        <img src="/bitrix/templates/akk/images/visa.png" class="footer_img" alt="Visa">
                    </div>
                </div>
                <div class="grid_8 prefix_3">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "footer_menu",
                        array(
                    	"ROOT_MENU_TYPE" => "bottom",
                    		"MENU_CACHE_TYPE" => "Y",
                    		"MENU_CACHE_TIME" => "36000000",
                    		"MENU_CACHE_USE_GROUPS" => "Y",
                    		"MENU_CACHE_GET_VARS" => "",
                    		"MAX_LEVEL" => "1",
                    		"ALLOW_MULTI_SELECT" => "N",
                    		"CHILD_MENU_TYPE" => "left",
                    		"DELAY" => "N",
                    		"USE_EXT" => "N",
                    	),
                    	false
                    );?>

					<ul class="footer_social">
	     				<li class="footer_social_li icon-vkontakte"><a href="" class="footer_social_link vk">vk</a></li>
						<li class="footer_social_li icon-facebook"><a href="" class="footer_social_link fb">fb</a></li>
						<li class="footer_social_li icon-instagram"><a href="" class="footer_social_link inst">inst</a></li>
					</ul>

                </div>
                <div class="grid_8 prefix_1">
                    <?$APPLICATION->IncludeComponent(
                    	"bitrix:news.list",
                    	"news_in_footer",
                    	array(
                    		"IBLOCK_TYPE" => "news",
                    		"IBLOCK_ID" => $_REQUEST["ID"],
                    		"NEWS_COUNT" => "2",
                    		"SORT_BY1" => "ACTIVE_FROM",
                    		"SORT_ORDER1" => "DESC",
                    		"SORT_BY2" => "SORT",
                    		"SORT_ORDER2" => "ASC",
                    		"FILTER_NAME" => "",
                    		"FIELD_CODE" => array(
                    			0 => "",
                    			1 => "",
                    		),
                    		"PROPERTY_CODE" => array(
                    			0 => "",
                    			1 => "",
                    		),
                    		"CHECK_DATES" => "Y",
                    		"DETAIL_URL" => "",
                    		"AJAX_MODE" => "N",
                    		"AJAX_OPTION_JUMP" => "N",
                    		"AJAX_OPTION_STYLE" => "Y",
                    		"AJAX_OPTION_HISTORY" => "N",
                    		"CACHE_TYPE" => "A",
                    		"CACHE_TIME" => "36000000",
                    		"CACHE_FILTER" => "N",
                    		"CACHE_GROUPS" => "Y",
                    		"PREVIEW_TRUNCATE_LEN" => "",
                    		"ACTIVE_DATE_FORMAT" => "j F Y",
                    		"SET_TITLE" => "N",
                    		"SET_BROWSER_TITLE" => "Y",
                    		"SET_META_KEYWORDS" => "Y",
                    		"SET_META_DESCRIPTION" => "Y",
                    		"SET_STATUS_404" => "N",
                    		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    		"ADD_SECTIONS_CHAIN" => "N",
                    		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    		"PARENT_SECTION" => "",
                    		"PARENT_SECTION_CODE" => "",
                    		"INCLUDE_SUBSECTIONS" => "Y",
                    		"PAGER_TEMPLATE" => ".default",
                    		"DISPLAY_TOP_PAGER" => "N",
                    		"DISPLAY_BOTTOM_PAGER" => "Y",
                    		"PAGER_TITLE" => "Новости",
                    		"PAGER_SHOW_ALWAYS" => "N",
                    		"PAGER_DESC_NUMBERING" => "N",
                    		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    		"PAGER_SHOW_ALL" => "N",
                    		"AJAX_OPTION_ADDITIONAL" => "",
                    		"BLOCK_LABEL" => "Новости"
                    	),
                    	false
                    );?>

					<a href="http://webway.ru/" target="_blank" class="footer_webway">Создано Webway</a>


                </div>
                <div class="grid_8 prefix_1">
                    <?$APPLICATION->IncludeComponent(
                    	"bitrix:news.list",
                    	"news_in_footer",
                    	array(
                    		"IBLOCK_TYPE" => "news",
                    		"IBLOCK_ID" => "5",
                    		"NEWS_COUNT" => "2",
                    		"SORT_BY1" => "ACTIVE_FROM",
                    		"SORT_ORDER1" => "DESC",
                    		"SORT_BY2" => "SORT",
                    		"SORT_ORDER2" => "ASC",
                    		"FILTER_NAME" => "",
                    		"FIELD_CODE" => array(
                    			0 => "",
                    			1 => "",
                    		),
                    		"PROPERTY_CODE" => array(
                    			0 => "LABEL",
                    			1 => "",
                    		),
                    		"CHECK_DATES" => "Y",
                    		"DETAIL_URL" => "",
                    		"AJAX_MODE" => "N",
                    		"AJAX_OPTION_JUMP" => "N",
                    		"AJAX_OPTION_STYLE" => "Y",
                    		"AJAX_OPTION_HISTORY" => "N",
                    		"CACHE_TYPE" => "A",
                    		"CACHE_TIME" => "36000000",
                    		"CACHE_FILTER" => "N",
                    		"CACHE_GROUPS" => "Y",
                    		"PREVIEW_TRUNCATE_LEN" => "",
                    		"ACTIVE_DATE_FORMAT" => "j F Y",
                    		"SET_TITLE" => "N",
                    		"SET_BROWSER_TITLE" => "N",
                    		"SET_META_KEYWORDS" => "N",
                    		"SET_META_DESCRIPTION" => "N",
                    		"SET_STATUS_404" => "N",
                    		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    		"ADD_SECTIONS_CHAIN" => "N",
                    		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    		"PARENT_SECTION" => "",
                    		"PARENT_SECTION_CODE" => "",
                    		"INCLUDE_SUBSECTIONS" => "Y",
                    		"PAGER_TEMPLATE" => ".default",
                    		"DISPLAY_TOP_PAGER" => "N",
                    		"DISPLAY_BOTTOM_PAGER" => "N",
                    		"PAGER_TITLE" => "Новости",
                    		"PAGER_SHOW_ALWAYS" => "N",
                    		"PAGER_DESC_NUMBERING" => "N",
                    		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    		"PAGER_SHOW_ALL" => "Y",
                    		"AJAX_OPTION_ADDITIONAL" => "",
                            "BLOCK_LABEL" => "Статьи"
                    	),
                    	false
                    );?>
                </div>
            </div>
        </div>
    </footer>
</section>
    <div class="d_none">
        <div id="sitepath" data-path="<?php SITE_TEMPLATE_PATH ?>" data-base="<?php echo SITE_TEMPLATE_PATH ?>"></div>

        <div id="callback_form" class="sm_modal sm_modal_inner form_ajax">
            <span class="arcticmodal-close"></span>
            <div id="callback_form_inner"></div>
        </div>

        <div id="success-callback" class="sm_modal">
        </div>

        <div id="success" class="sm_modal">
        </div>

        <div id="sale" class="sale_box p_rel">
        </div>

        <div id="order_in_advance_form" class="sm_modal sm_modal_inner form_ajax">
            <span class="arcticmodal-close"></span>
            <div id="order_in_advance_form_inner"></div>
        </div>

        <?if(!$USER->IsAuthorized()):?>
            <div id="authorization" class="sm_modal p_rel">
                <div id="auth"></div>
            </div>
            <div id="registration" class="sm_modal p_rel">
                <div id="registr"></div>
            </div>
            <div id="backup_pass" class="sm_modal p_rel">
                <div id="backup_pass_inner"></div>
            </div>
        <?endif;?>

    </div>

<!-- Yandex.Metrika informer -->
<?/*<a href="https://metrika.yandex.ru/stat/?id=22871155&amp;from=informer"
target="_blank" rel="nofollow"><img src="https://informer.yandex.ru/informer/22871155/3_1_FFFFFFFF_EFEFEFFF_0_pageviews"
style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" class="ym-advanced-informer" data-cid="22871155" data-lang="ru" /></a>
*/?>
<!-- /Yandex.Metrika informer -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter22871155 = new Ya.Metrika({
                    id:22871155,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    ecommerce:"dataLayer"
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/22871155" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<!--LiveInternet counter--><script type="text/javascript"><!--
new Image().src = "//counter.yadro.ru/hit?r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random();//--></script><!--/LiveInternet-->

<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'E7hhEXsqjW';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->

<script type="text/javascript" src="/bitrix/js/targets.js"></script>


<a href='#' id='Go_Top' class="go_top_link icon-up-3" title="Наверх"></a>


</body>
</html>
