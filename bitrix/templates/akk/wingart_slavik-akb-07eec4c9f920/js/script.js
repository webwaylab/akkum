$(document).ready(function() {
  
  // Init plugins

  if($('.flexslider').length){
    $('.flexslider').flexslider({
      animation: "slide",
      
    });  
  }

  if($('.styler').length){
    $('.styler').styler();
  }

  $(".menu_item").each(function(){
  	if($(this).children("div.sub_menu").length){
  		$(this).find(">a").addClass("hasUl").append("<i class='menu-arrow'></i>");
  	}
  })

  $("#delivery_row").find(">label").on("click", function(){
    var currentIndex = $(this).index();

    $("#delivery_details").find(">div").slideUp(200);
    $("#delivery_details").find(">div").eq(currentIndex-1).slideToggle();
  })

  if($(".datepicker").length){
    $( ".datepicker" ).datepicker();
  }

  if($(".product_page_tabs").length){
    $( ".product_page_tabs" ).tabs();
  }

  if($(".product_rating").length){
    $('.product_rating').each(function(){
      $(this).raty({
        score: function(){
          return $(this).data('score');
        }
      })
    })
  }

  $("#payment_row").find(">label").on("click", function(){
    var currentInd = $(this).index();
    if(currentInd == 2){
      $("#payment_details").slideDown();
    }
    else{
      $("#payment_details").slideUp();
    }
  })

  $("#show_categories_btn").on("click", function(event){
    $(this).toggleClass("active");
    event.preventDefault();
  })

  $(document).on("click", function(event) {
    if ($(event.target).closest("#show_categories_btn, #catalog_categories").length) return;
    $("#show_categories_btn").removeClass("active");
    event.stopPropagation();
  })

  // $("#catalog_categories a").on("click", function(event){
  //     var currentVal = $(this).text();
  //     $("#content").find("#current_category_val").text(currentVal);

  //     event.preventDefault();
  // })

  var productImgBox = $("#product_img");
  $(productImgBox).find(".product_thumb").on("click", function(event){
    var newImg = $(this).attr("href");

    productImgBox.find(".active").removeClass("active");
    $(this).addClass("active");

    productImgBox.find("#main_product_img").attr("src", newImg);
    
    event.preventDefault();
  })


  $("a.hasUl").on("click", function(){
    var curentParent = $(this).parent();
    if(curentParent.hasClass("active")){
      curentParent.toggleClass("active")
    }
    else{
      $(this).parents("nav").find("li.active").removeClass("active");
      curentParent.toggleClass("active");
    }
    
    $("nav").addClass("active");
  })

  $(document).on("click", function(event) {
    if ($(event.target).closest(".menu").length) return;
    $(".menu_item.active, nav").removeClass("active");
    event.stopPropagation();
  })

  $(".toggle_btn").on("click", function(event){
    var target = $(this).data("target"),
        targetClass = $(this).data("target-class");

    $(target).addClass(targetClass);
    event.preventDefault();
  })

  $(".close_tooltip").on("click", function(){
    $(this).parents(".tooltip_box").removeClass("active");
  })

  $(".modal_btn").on("click", function(){
    var modalTarget = $(this).data("modal");
    $.arcticmodal('close');
    $(modalTarget).arcticmodal();
    event.preventDefault();
  })

  if($(".phone_mask").length){
    $(".phone_mask").each(function(){
      $(this).mask("+7(999) 999-99-99");
    })
  }

  var qtyProductImg = $(".product_thumbs_inner").find(">.product_thumb");

  if(qtyProductImg.length > 4){
    
    $(".product_thumbs_inner").addClass("owl-carousel");

    $(".product_thumbs_inner").owlCarousel({

        autoPlay: false,
        navigation:true,
        items : 4,
        itemsDesktop : [1199,4],
        itemsDesktopSmall : false
   
    });

  }

  


  if($(".slider_range").length){
     $(".slider_range" ).each(function(){
      var startVal = $(this).data("start"),
          endVal   = $(this).data("end"),
          maxVal   = $(this).data("max");

      $(this).slider({
        range: true,
        min: 0,
        max: maxVal,
        values: [ startVal, endVal],
        create: function( event, ui ) {
          $(this).parent().find(".amount_start").val(startVal);
          $(this).parent().find(".amount_end").val(endVal);
        },
        slide: function( event, ui ) {
          $(this).parent().find(".amount_start").val(ui.values[ 0 ]);
          $(this).parent().find(".amount_end").val(ui.values[ 1 ]);
        }
      })
    })
  }

  $(".filter_link").on("click", function(){
    $(this).toggleClass("active");
  })

  
  var map;
  var mainPoint = new google.maps.LatLng(55.7198127, 37.7274254);

  var MY_MAPTYPE_ID = 'custom_style';

  function initialize() {

    var featureOpts = [
    {
        "featureType": "landscape",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "stylers": [
                {
                    "hue": "#00aaff"
                },
                {
                    "saturation": -100
                },
                {
                    "gamma": 2.15
                },
                {
                    "lightness": 12
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "lightness": 24
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "geometry",
            "stylers": [
                {
                    "lightness": 57
                }
            ]
        }
    ];

    var mapOptions = {
      zoom: 18,
      center: mainPoint,
      panControl: false,
      zoomControl: true,
      scrollwheel: false,
      mapTypeControlOptions: {
        mapTypeIds: [google.maps.MapTypeId.ROADMAP, MY_MAPTYPE_ID]
      },
      mapTypeId: MY_MAPTYPE_ID
    };

    map = new google.maps.Map(document.getElementById('map-canvas'),
        mapOptions);

    var styledMapOptions = {
      name: 'Custom Style'
    };

    var marker = new google.maps.Marker({
        position: mainPoint,
        map: map,
        icon: 'https://wecoders.net/images/custom_marker.png',
        title: 'Hello World!'
    });

    var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);

    map.mapTypes.set(MY_MAPTYPE_ID, customMapType);
}

  google.maps.event.addDomListener(window, 'load', initialize);

  var $el, leftPos, newWidth,
        $mainNav = $("ul.menu");
    
    $mainNav.append("<li id='magic-line'></li>");
    var $magicLine = $("#magic-line");
    
    $magicLine
        .width($(".current>a").width())
        .css("left", $(".current").position().left)
        .data("origLeft", $magicLine.position().left)
        .data("origWidth", $magicLine.width());
        
    $("ul.menu li").find("a").hover(function() {
        $el = $(this);
        leftPos = $el.parent().position().left;
        newWidth = $el.parent().width();
        $magicLine.stop().animate({
            left: leftPos,
            width: newWidth
        });
    }, function() {
        $magicLine.stop().animate({
            left: $magicLine.data("origLeft"),
            width: $magicLine.data("origWidth")
        });    
    });
  

})

if (/android|iphone|ipod|ipad|series60|symbian|windows ce|blackberry/i.test(navigator.userAgent)) {
    $("body").addClass("device");
}