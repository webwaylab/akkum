$(document).ready(function () {
    if ($('.filter_row').length > 0) {
        pasteFilterText();
    } else {
        $('.sort-wrapper').on('ajaxloaded', function () {
            pasteFilterText();
        })
    }
});

function pasteFilterText() {
    $('.hits.sales').text('хиты продаж');
    $('.hits.actions').text('акции');

    $('body').on('click', '.filter_item span', function (event) {
        document.location.href = $(this).attr("rel");
    });
}
