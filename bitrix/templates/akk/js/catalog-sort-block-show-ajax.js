/** Show sort block with ajax call after page load (for SEO) */
$(function () {
    var sortBlockWrapper = $('.sort-wrapper');
    if (sortBlockWrapper.length > 0) {
        $.ajax({
            method: 'POST',
            cache: false,
            url: window.location.href,
            data: {
                'SHOW_AJAX_SORT': 'Y'
            },
            success: function (response) {
                sortBlockWrapper.html(response);
                sortBlockWrapper.trigger('ajaxloaded');
            }
        });
    }
});