$(document).ready(function () {

    // Init plugins

    // datepicker localization
    $.datepicker.regional['ru'] = {
        closeText: 'Закрыть',
        prevText: '&#x3c;Пред',
        nextText: 'След&#x3e;',
        currentText: 'Сегодня',
        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
            'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
            'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
        dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
        dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
        dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        weekHeader: 'Не',
        dateFormat: 'dd.mm.yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };

    $.datepicker.setDefaults($.datepicker.regional['ru']);

    if ($('.flexslider').length) {
        $('.flexslider').flexslider({
            animation: "slide",

        });
    }

    if ($('.styler').length) {
        $('.styler').styler();
    }

    if ($(".datepicker").length) {
        $(".datepicker").datepicker();
    }

    if ($(".product_page_tabs").length) {
        $(".product_page_tabs").tabs();
    }

    if ($(".product_rating").length) {
        $('.product_rating').each(function () {
            $(this).raty({
                score: function () {
                    return $(this).data('score');
                }
            })
        })
    }

    $("#show_categories_btn").on("click", function (event) {
        $(this).toggleClass("active");
        event.preventDefault();
    });

    $(document).on("click", function (event) {
        if ($(event.target).closest("#show_categories_btn, #catalog_categories").length) return;
        $("#show_categories_btn").removeClass("active");
        event.stopPropagation();
    });

    var productImgBox = $("#product_img");
    $(productImgBox).find(".product_thumb").first().addClass('active');
    $(productImgBox).find(".product_thumb").on("click", function (event) {
        var newImg = $(this).attr("href");

        productImgBox.find(".active").removeClass("active");
        $(this).addClass("active");

        productImgBox.find(".main_product_img").attr("src", newImg);

        event.preventDefault();
    });

    var productImgBoxModal = $("#product_img_modal");

    $(productImgBoxModal).find(".product_thumb_modal").on("click", function (event) {
        var newImgModal = $(this).attr("href");

        productImgBoxModal.find(".active").removeClass("active");
        $(this).addClass("active");

        productImgBoxModal.find(".main_product_img_modal").attr("src", newImgModal);

        event.preventDefault();

    });

    var qtyProductImgModal = $(".product_thumbs_inner_modal").find(">.product_thumb_modal");

    if (qtyProductImgModal.length > 4) {

        $(".product_thumbs_inner_modal").addClass("owl-carousel");

        $(".product_thumbs_inner_modal").owlCarousel({

            autoPlay: false,
            navigation: true,
            items: 4,
            itemsDesktop: [1199, 4],
            itemsDesktopSmall: false

        });

    }


    $("#tab8 .owl-carousel").owlCarousel({

        autoPlay: false,
        navigation: true,
        items: 5,
        margin: 15,
        loop: true,
        itemsDesktop: [1199, 4],
        itemsDesktopSmall: false

    });


    $(document).on("click", function (event) {
        if ($(event.target).closest(".menu").length) return;
        $(".menu_item.active, nav").removeClass("active");
        event.stopPropagation();
    });

    $(".toggle_btn").on("click", function (event) {
        $(".tooltip_box").removeClass("active");
        var target = $(this).data("target"),
            targetClass = $(this).data("target-class");

        $(target).addClass(targetClass);
        event.preventDefault();
    });

    $("body").on("click", ".close_tooltip", function () {
        $(this).parents(".tooltip_box").removeClass("active");
    });

    $("body").on("click", ".modal_btn", function (e) {
        var modalTarget = $(this).data("modal");
        $.arcticmodal('close');
        $(modalTarget).arcticmodal();
        event.preventDefault();
    });

    if ($(".phone_mask").length) {
        $(".phone_mask").each(function () {
            $(this).mask("+7(999) 999-99-99");
        })
    }

    var qtyProductImg = $(".product_thumbs_inner").find(">.product_thumb");
    if (qtyProductImg.length > 3) {
        $(".product_thumbs_inner").bxSlider({
            minSlides: 3,
            maxSlides: 3,
            moveSlides: 1,
            slideWidth: 120,
            slideMargin: 20,
            pager: false,
            infiniteLoop: false
        });
        /*
      $(".product_thumbs_inner").addClass("owl-carousel");

      $(".product_thumbs_inner").owlCarousel({

          autoPlay: false,
          navigation:true,
          items : 4,
          itemsDesktop : [1199,4],
          itemsDesktopSmall : false

      });
      */

    }

    if ($(".slider_range").length) {
        $(".slider_range").each(function () {
            var startVal = $(this).data("start"),
                endVal = $(this).data("end"),
                maxVal = $(this).data("max");

            $(this).slider({
                range: true,
                min: 0,
                max: maxVal,
                values: [startVal, endVal],
                create: function (event, ui) {
                    $(this).parent().find(".amount_start").val(startVal);
                    $(this).parent().find(".amount_end").val(endVal);
                },
                slide: function (event, ui) {
                    $(this).parent().find(".amount_start").val(ui.values[0]);
                    $(this).parent().find(".amount_end").val(ui.values[1]);
                }
            })
        })
    }

    function toggleVideo(state) {
        // if state == 'hide', hide. Else: show video
        var div = document.getElementById("popupVid");
        var iframe = div.getElementsByTagName("iframe")[0].contentWindow;
        // div.style.display = state == 'hide' ? 'none' : '';
        func = state == 'hide' ? 'pauseVideo' : 'playVideo';
        // iframe.postMessage('{"event":"command","func":"' + func + '","args":""}','*');
    }

    $("#pause").on("click", function () {
        toggleVideo("hide")
    });

    $('div.old_price').parents().addClass('old_price_action');
    //new auto


    //форма консультации
    $('.btn-consultation').click(function (event) {
        yaCounter22871155.reachGoal('zvonok');
        ga('send', 'event', 'forma', 'zvonok');
        event.preventDefault();
        var name = $(".consultation_callback_form input[name=name]").val();
        var phone = $(".consultation_callback_form input[name=phone]").val();
        error = false;
        $('.consultation_callback_form .error p').html("");
        $('.consultation_callback_form .error').hide();
        if (name.length == 0) {
            error = true;
            $('.consultation_callback_form .error p').html("Введите имя");
            $('.consultation_callback_form .error').show();
        }
        if (phone.length == 0) {
            error = true;
            $('.consultation_callback_form .error p').html("Введите телефон");
            $('.consultation_callback_form .error').show();
        }
        if (!error) {
            $.post("/actions.php",
                {action: "callback", name: name, phone: phone}, function (data) {
                    if (data.status == true) {
                        alert("Ваше сообщение успешно отправлено.");
                    }
                }, 'json');
        }
    });


});


if ($('.tabs_inner_list li span').hasClass('active')) {
    $('.tab-link,.tab-content').removeClass('current');
    $('.tabs_inner_list li span.active').closest('.tab-content ').addClass('current');
    var tid = $('.tabs_inner_list li span.active').closest('.tab-content ').attr('id');
    $(".tab-link").each(function () {
        if ($(this).attr('data-tab') == tid) {
            $(this).addClass('current');
        }
    });
}

$('ul.tabs li').click(function () {
    var tab_id = $(this).attr('data-tab');

    $('ul.tabs li').removeClass('current');
    $('.tab-content').removeClass('current');

    $(this).addClass('current');
    $("#" + tab_id).addClass('current');
});

/*
function sticky_relocate() {
    var window_top = $(window).scrollTop();
    var div_top = $('#sticky-anchor').offset().top - 48;
    if (window_top > div_top) {
        $('.brand__tabs').addClass('stick');
        $('#sticky-anchor').height($('.brand__tabs').outerHeight());
    } else {
        $('.brand__tabs').removeClass('stick');
        $('#sticky-anchor').height(0);
    }
}

$(function() {
    $(window).scroll(sticky_relocate);
    sticky_relocate();
});
*/

function MainMenu() {
    this.$item = $('.menu_item');
    this.$itemWithChildren = $('a.hasUl');
    this.$magicLine = $("#magic-line");

    this.lineTimeout = 0;
}

MainMenu.prototype.init = function () {
    this.initMagicLine();
    this.bindEvents();
};

MainMenu.prototype.bindEvents = function () {
    var self = this;

    if (!isMobile()) {
        this.$item.hover(function () {
            $(this).addClass('active');
            self.moveMagicLine($(this));
        }, function () {
            $(this).removeClass('active');
            self.resetMagicLine();
        });
    }

    this.$itemWithChildren.on('click', function (e) {
        e.preventDefault();
        if ($(this).parent().hasClass('active')) {
            $(this).parent().toggleClass('active');
        } else {
            $('.menu_item').removeClass('active');
            $(this).parent().addClass('active');
        }
    });

    $(window).on('resize', function () {
        self.initMagicLine();
    });
};

MainMenu.prototype.initMagicLine = function () {
    var $currentItem = $('.menu .current > a');

    if (typeof $currentItem.position() === 'undefined') {
        this.$magicLine.hide();
        return false;
    }

    this.$magicLine
        .width($currentItem.width())
        .css('left', $currentItem.position().left)
        .data('orig-left', this.$magicLine.position().left)
        .data('orig-width', this.$magicLine.width());
};

MainMenu.prototype.moveMagicLine = function (el) {
    var $currentItem = el.find('a');

    if ($currentItem.hasClass('hasUl')) {
        return false;
    } else {
        clearTimeout(this.lineTimeout);
    }

    var left = $currentItem.position().left,
        width = el.width();

    this.$magicLine.stop().animate({
        left: left,
        width: width
    });
};

MainMenu.prototype.resetMagicLine = function () {
    var self = this;
    this.lineTimeout = setTimeout(function () {
        self.$magicLine.stop().animate({
            left: self.$magicLine.data("orig-left"),
            width: self.$magicLine.data("orig-width")
        });
    }, 300);
};

function isMobile() {
    return /android|iphone|ipod|ipad|series60|symbian|windows ce|blackberry/i.test(navigator.userAgent);
}

if (/android|iphone|ipod|ipad|series60|symbian|windows ce|blackberry/i.test(navigator.userAgent)) {
    $("body").addClass("device");
}


//flex for similar products
(function () {

    // store the slider in a local variable
    var $window = $(window),
        flexslider = {vars: {}};

    // tiny helper function to add breakpoints
    function getGridSize() {
        return (window.innerWidth < 600) ? 2 :
            (window.innerWidth < 900) ? 3 : 5;
    }

    // $(function() {
    // SyntaxHighlighter.all();
    // });

    $window.load(function () {
        if ($('#similar .similar-products ul li').length > 4) {
            $('#similar .similar-products').flexslider({
                animation: "slide",
                animationLoop: false,
                move: 1,
                mousewheel: true,
                minItems: 5,
                touch: true,
                itemWidth: 191,
                minItems: getGridSize(), // use function to pull in initial value
                maxItems: getGridSize() // use function to pull in initial value
            });
        }
        if ($('#also .similar-products ul li').length > 4) {
            $('#also .similar-products').flexslider({
                animation: "slide",
                animationLoop: true,
                move: 1,
                touch: true,
                mousewheel: true,
                minItems: 5,
                itemWidth: 191,
                minItems: getGridSize(), // use function to pull in initial value
                maxItems: getGridSize() // use function to pull in initial value
            });
        }
    });


    // check grid size on resize event
    $window.resize(function () {
        var gridSize = getGridSize();

        flexslider.vars.minItems = gridSize;
        flexslider.vars.maxItems = gridSize;
    });
}());

$(document).ready(function () {
    $("#tabs__brands").tabs({active: 0});

    var isInnerPage = !!$('body').hasClass('inner');

    $("#tabs__menu").tabs({
        active: isInnerPage ? false : 0,
        collapsible: isInnerPage,
        event: "mouseover"/*,
        show: isInnerPage ? { effect: "slideDown", duration: 250 } : null,
        hide: isInnerPage ? { effect: "slideUp", duration: 250 } : null
        */
    })
    .mouseleave(function () {
        if (isInnerPage) {
            $(this).tabs("option", "active", false);
        }
    });

    //Начало проверки url для определения активной вкладки в главном меню
    /*
    if (window.location.href.indexOf("catalog/accessories/") > -1) {
        $("#tabs__menu").tabs({
            active: 1,
            event: "mouseover"
        });
    } else if (window.location.href.indexOf("catalog/masla/") > -1) {
        $("#tabs__menu").tabs({
            active: 2,
            event: "mouseover"
        });
    } else {
        $("#tabs__menu").tabs({
            active: 0,
            event: "mouseover"
        });
    }
    */
    //конец проверки url для определения активной вкладки в главном меню

    var base_url = $('#sitepath').attr('data-path');
    var base_tmpl = $('#sitepath').attr('data-base');

    /*start load ajax content callback*/
    function getCallBack() {
        $.ajax({
            url: base_url + '/ajax/callback.php',
            success: function (response) {
                $('#callback_form_inner').html(response);
                if ($("body").find(".phone_mask").length) {
                    $("body").find(".phone_mask").each(function () {
                        $(this).mask("+7(999) 999-99-99");
                    })
                }
            }
        });
        return false;
    }

    $('#callback_form_link, .footer_callback, .button.big_callback_btn.d_ib.modal_btn').on('click', function (event) {
        getCallBack();
    });
    /*end load ajax content callback*/

    // Order form opem
    function getOrderInAdvance() {
        $.ajax({
            url: base_url + '/ajax/order-in-advance.php',
            data: {
                'PRODUCT_NAME': $('h1', '.product_item__card').text(),
                'PRODUCT_QUANTITY': parseFloat($('input[type="number"][id$="_quantity"]', '.product_item__card').val())
            },
            success: function (response) {
                $('#order_in_advance_form_inner').html(response);
                if ($('body').find('#order_in_advance_form .phone_mask').length) {
                    $('body').find('#order_in_advance_form .phone_mask').mask('+7(999) 999-99-99');
                }
            }
        });
        return false;
    }

    $('.do-order-in-advance', '.product_item__card').on('click', function () {
        getOrderInAdvance();
    });

    /*start load ajax content review (#sale) */
    function getSaleReview() {
        $.ajax({
            url: base_url + '/ajax/sale.php',
            success: function (response) {
                $('#sale').html(response);
            }
        });
        return false;
    }

    $('#sale_review').on('click', function (event) {
        getSaleReview();
    });
    /*end load ajax content review (#sale) */

    /*start load ajax content #authorization */
    function getAuth() {
        $.ajax({
            url: base_url + '/ajax/auth.php',
            success: function (response) {
                var auth = $(response).find('.auth_helper');
                $('#auth').html(auth);

                var reg = $(response).find('#reg_helper');
                $('#registr').html(reg);

                var recover = $(response).find('#recover_helper');
                $('#backup_pass_inner').html(recover);
                if ($("body").find(".phone_mask").length) {
                    $("body").find(".phone_mask").each(function () {
                        $(this).mask("+7(999) 999-99-99");
                    })
                }
            }
        });
        return false;
    }

    $('div.info_link').on('click', function (event) {
        getAuth();
    });
    $('body').on('click', '.arcticmodal-close', function (event) {
        event.preventDefault();
        /* Act on the event */
        $.arcticmodal('close');
    });
    /*end load ajax content authorization */

    /*start load ajax content call click*/

    $('.product_btn.action-block.toggle_btn').on('click', function (event) {
        var tip = $(this).closest('.product_btn-bottom-wrap').find('.form_wrapper');
        var dataid = tip.attr('data-id');
        var data_call = tip.attr('data-call');
        $.ajax({
            url: base_url + '/ajax/call-click.php',
            success: function (response) {
                $(tip).html(response);
                $(tip).find('input[type="hidden"]').val(dataid);
                $(tip).find('img').attr('src', base_tmpl + '/images/done_form_icon_green.png');
                if ($("body").find(".phone_mask").length) {
                    $("body").find(".phone_mask").each(function () {
                        $(this).mask("+7(999) 999-99-99");
                    })
                }
                $(tip).find('form').addClass(data_call);
                $(tip).find('form').addClass('active');
            }
        });
        return false;
    });
    /*end load ajax content call click */

    $('.spoiler-body,.spoiler-body-akk,.akkum-information_spoiler-body').hide();
    $('.spoiler-title').click(function () {
        $(this).toggleClass('opened').toggleClass('closed').nextAll('div.spoiler-body').slideToggle();
        if ($(this).hasClass('opened')) {
            $(this).html('<span>Свернуть</span>');
        }
        else {
            $(this).html('<span>Все марки</span>');
        }
    });


    $('.spoiler-title-akk').click(function () {
        $(this).toggleClass('opened').toggleClass('closed').nextAll('div.spoiler-body-akk').slideToggle();
        if ($(this).hasClass('opened')) {
            $(this).html('<span>Свернуть</span>');
        }
        else {
            $(this).html('<span>Все бренды</span>');
        }
    });


    $('.akkum-information_spoiler-title').click(function () {
        $(this).toggleClass('akkum-information_opened').toggleClass('akkum-information_closed').nextAll('div.akkum-information_spoiler-body').slideToggle();
    });


    $('.filter_item.drop .filter_link').on('click', function (event) {
        $('.filter_item').removeClass('active');
        $(this).parent('.filter_item').addClass('active');
        return false;
    });
    $(document).click(function (e) {
        $('.filter_item').removeClass('active');
    });

    // Set maximum quantity on manual input
    $('input[type="number"][id$="_quantity"]', '.product_item__card').on('input', function () {
        var maxQtty = parseFloat($(this).prop('max'));
        if (maxQtty > 0 && $(this).val() > maxQtty) {
            $(this).val(maxQtty);
            return false;
        }
    });

    // Tabs
    $('[data-tabs-id]').on('click', function() {
        var jthis = $(this),
            jthisId = jthis.data('tabs-id'),
            jthisGroup = jthis.closest('[data-tabs-group]').data('tabs-group');

        $('[data-tabs-group=' + jthisGroup + '] > [data-tabs-id]').removeClass('active');
        $('[data-tabs-content-group=' + jthisGroup + '] > [data-tabs-content-id]').removeClass('active').hide();
        jthis.addClass('active');
        $('[data-tabs-content-group=' + jthisGroup + '] > [data-tabs-content-id=' + jthisId + ']').addClass('active').show();
        return false;
    });
    if ($('[data-tabs-id]').filter('.active').length === 0) {
        $('[data-tabs-content-group]').each(function() {
            $($('[data-tabs-content-id]'), $(this)).hide();
        });
        $('[data-tabs-group]').each(function() {
            $($('[data-tabs-id]:first-child'), $(this)).trigger('click');
        });
    }

});

jQuery(function () {
    $("#Go_Top").hide().removeAttr("href");
    if ($(window).scrollTop() >= "250") $("#Go_Top").fadeIn("slow");
    var scrollDiv = $("#Go_Top");
    $(window).scroll(function () {
        if ($(window).scrollTop() <= "250") $(scrollDiv).fadeOut("slow");
        else $(scrollDiv).fadeIn("slow")
    });
    $("#Go_Top").click(function () {
        $("html, body").animate({scrollTop: 0}, "slow")
    });
});

// Custom JivoSite open trigger
function jivo_onLoadCallback() {
    if ($('#jivo-iframe-container').length > 0) {
        $('.jivo-custom-trigger').on('click', function () {
            if (typeof jivo_api !== 'undefined') {
                jivo_api.open();
            } else {
                $('jdiv[class^="hoverl_"]').trigger('click');
            }
        });
    }
}

jQuery(document).ready(function(){
	jQuery(".text-top").appendTo(".TOP_TEXT");
});

$(document).ready(function() {
	$("a.gallery").fancybox();
});