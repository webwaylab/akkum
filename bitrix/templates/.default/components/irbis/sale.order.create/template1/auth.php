<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<h2 class="black fs_24 reg mb28">Оформление заказа</h2>

<fieldset class="create_order_step">    
    <h3 class="fs_18 black reg mb15">Контактная информация</h3>
    <div class="clearfix">
        <div class="order_form_col f_left">
            <div class="form_label">Ваше имя:</div>
            <input type="text" name="USER[NAME]" value="<?=$arResult["USER"]["NAME"]?>" class="txt_field">
        </div>
        <div class="order_form_col f_left">
            <div class="form_label">Адрес эл. почты:</div>
            <input type="text" name="USER[EMAIL]" value="<?=$arResult["USER"]["EMAIL"]?>" class="txt_field">
        </div>
        <div class="order_form_col f_left">
            <div class="form_label">Номер телефона:</div>
            <input type="text" name="USER[PERSONAL_PHONE]" value="<?=$arResult["USER"]["PERSONAL_PHONE"]?>" class="txt_field phone_mask">
        </div>
    </div>
    <?
    /*if(!empty($arResult["ERRORS"]["USER"]) && is_array($arResult["ERRORS"]["USER"]))
    {
        foreach($arResult["ERRORS"]["USER"] as $error)
        {
            echo $error."<br />";
        }   
    }*/
    ?>
</fieldset>