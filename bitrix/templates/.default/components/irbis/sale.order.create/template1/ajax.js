/*function submitForm(val)
{
    var oform = $("#ORDER_FORM");
    var destination = $("body").scrollTop();
    
    console.log(oform.serialize());
    
    $.ajax({
        type: "POST",
        url: oform.attr("action"),
        data: oform.serialize(),
        success: function(res)
        {
            $("#order_form_content").html(res);
            $('html, body').animate({scrollTop: destination}, 0);
            
            $('.styler').styler();
            
            $(".phone_mask").mask("+7(999) 999-99-99");
            
            if($("input[name='DELIVERY']:checked").length>0)
            {
                $("#delivery_details").find(">div").slideToggle();
            }
        }
    });

	return true;
}*/

var BXFormPosting = false;
function submitForm(val)
{
    console.log($("#ORDER_FORM").serialize());
    
    
	if (BXFormPosting === true)
		return true;

	BXFormPosting = true;
	if(val != 'Y')
		BX('confirmorder').value = 'N';

	var orderForm = BX('ORDER_FORM');
	BX.showWait();
	BX.ajax.submit(orderForm, ajaxResult);

	return true;
}

function ajaxResult(res)
{
	var orderForm = BX('ORDER_FORM');
	try
	{
		// if json came, it obviously a successfull order submit

		var json = JSON.parse(res);
		BX.closeWait();

		if (json.error)
		{
			BXFormPosting = false;
			return;
		}
		else if (json.redirect)
		{
			window.top.location.href = json.redirect;
		}
	}
	catch (e)
	{
		// json parse failed, so it is a simple chunk of html

		BXFormPosting = false;
		BX('order_form_content').innerHTML = res;

        $('.styler').styler();
        $(".phone_mask").mask("+7(999) 999-99-99");
        $( ".datepicker" ).datepicker();
        
        /*if($("input[name='DELIVERY']:checked").length>0)
        {
            $("#delivery_details").find(">div").slideToggle();
        }*/
	}

	BX.closeWait();
	BX.onCustomEvent(orderForm, 'onAjaxSuccess');
}

$(function(){   
    
    /*if($("input[name='DELIVERY']:checked").length>0)
    {
        $("#delivery_details").find(">div").slideToggle();
    }*/
      
    $(document).on('change click', 'input[name="PAYMENT"]', function(){
        submitForm("Y");
    });
    
    $(document).on('change click', 'input[name="DELIVERY"]', function(){
        submitForm("Y");
    });
    
    $(document).on('click', '.item-delete', function(e){
        e.preventDefault();
        $(this).prev("input").val("Y");
        submitForm("Y");
    });
    
    /*$(document).on('click', '.item-change-price', function(){
        alert(1);
        submitForm("Y");
    });
    
    $(".item-change-price").click(function(){
        alert(1231); 
    });
    
    $(".extra_label").on("click", function(){
        var curCheckbox = $(this).find(".jq-checkbox");
       
       alert(1);
        
    })*/
       
});