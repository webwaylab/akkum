<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<fieldset class="create_order_step">
    <h3 class="fs_18 black reg mb17">Подтверждение</h3>
    
    <ul class="cart_total">
        <li class="cart_total_item">Ваше имя: <?=$arResult["USER"]["NAME"]?></li>
        <li class="cart_total_item">Электронная почта: <?=$arResult["USER"]["EMAIL"]?></li>
        <li class="cart_total_item">Телефон: <?=$arResult["USER"]["PERSONAL_PHONE"]?></li>
        <li class="cart_total_item">Точное время доставки уточнит менеджер <br>при согласовании деталей заказа</li>
    </ul>
    
    <div class="cart_total_sum">
        <div class="fs_14">Общая сумма</div>
        <div class="product_item_price heavy"><?=number_format($arResult["TOTAL_PRICE"], 0, "", " ")?> <span class="ruble">руб.</span></div>
    </div>
    
    <?
    if(!empty($arResult["ERROR_MESSAGE"]))
    {
        ?><div style='color: red; padding: 10px; margin: 20px 0; border: 1px solid;'><?=$arResult["ERROR_MESSAGE"]?></div><?
    }
    ?>
    
    <input type="hidden" name="create" value="" />
    <button class="btn2 grn_skin" onclick="yaCounter22871155.reachGoal('zakaz'); ga('send', 'event', 'knopka', 'zakaz'); $('input[name=create]').val('Y'); submitForm('Y'); return false;">Оформить заказ</button>
    
</fieldset>
