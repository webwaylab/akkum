<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<fieldset class="create_order_step">
    <h3 class="fs_18 black reg mb15">Дополнительно</h3>
    <div class="form_row1 fs_12 order_form_col1 f_left">
        <div class="mb20">
            <div class="form_label1">Марка авто:</div>
            <input type="text" name="BRAND_AUTO" class="txt_field" value="<?=$arResult["BRAND_AUTO"]?>">
        </div>
        <div class="wrapper">
            <div class="form_label1">Комменатрий:</div>
            <textarea name="COMMENT" class="txt_field area_h113"><?=$arResult["COMMENT"]?></textarea>
        </div>
    </div>
</fieldset>