<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (count($arResult["ITEMS"]) < 1)
	return;
?>

<div class="h3 fs_24 black reg mb25"><a href="/news/"><?=$arParams["NEWS_TITLE"]?></a></div>
<div class="aside_posts wrapper">
    <?
    foreach($arResult["ITEMS"] as $arItem)
    {
        //CDev::pre($arItem);
        ?>
        <div class="aside_post_item">
            <div class="news_list_item_data">
                <span class="<?=$arParams["LABEL_CSS"]?> reg"><?=$arItem["PROPERTIES"]["LABEL"]["VALUE"]?></span>
                <time><?=$arItem["DISPLAY_ACTIVE_FROM"]?></time>
            </div>
            <div class="black fs_18 reg"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>
            <div class="news_list_item_content">
                <?=htmlspecialchars_decode($arItem["PREVIEW_TEXT"])?>
            </div>
        </div>
        <?
    }
    ?>      
</div>
