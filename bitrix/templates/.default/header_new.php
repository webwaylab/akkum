<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
global $USER, $APPLICATION;
CUtil::InitJSCore();
CJSCore::Init(array("fx"));
$curPage = $APPLICATION->GetCurPage(true);
?>
<!DOCTYPE html>
<html lang="<?=LANGUAGE_ID?>">
<head>
    <?$APPLICATION->ShowHead();?>

<? 

	$seo_url = array(
		'/catalog/accessories/zaryadnoe_ustrojstvo/',
		'/catalog/accessories/nabor_instrumentov_dlya_avto/',
		'/catalog/avtomobilnye/',
		'/catalog/battery/zver/',
		'/catalog/battery/varta/',
		'/catalog/battery/varta/blue_dynamic/',
		'/catalog/battery/varta/silver_dynamic/',
		'/catalog/accessories/nagruzochnye_vilki/',
		'/catalog/battery/gelevye/akb/',
		'/catalog/battery/gelevye/',
		'/catalog/battery/bosch/',
		'/catalog/battery/bosch/s4_silver/',
		'/catalog/battery/mutlu/',
		'/',
		'/catalog/battery/moto_akkumulyatory/',
		'/catalog/battery/tudor/',
		'/catalog/auto/',
		'/catalog/battery/afa/',
		'/catalog/battery/solite/',
		'/catalog/battery/titan/',
		'/catalog/battery/aktex/',
		'/catalog/battery/vaiper/',
		'/catalog/battery/royal/',
		'/catalog/battery/cobat/',
		'/catalog/battery/tyumen/',
		'/catalog/battery/tyumen/lider/',
		'/catalog/battery/bolk_moto/',
		'/catalog/battery/sebang/',
	);

    if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/d-robots.php')) {
    include_once ($_SERVER['DOCUMENT_ROOT'] . '/d-robots.php');
    $dRobots = dRobots::fromFile();
    $noindex = $dRobots->checkUrl($_SERVER['REQUEST_URI']) ? '<meta name="googlebot" content="noindex">' . PHP_EOL : '';
    } else $noindex = '';
     
    //Вывод в шаблоне
    echo $noindex;



    $dir = $APPLICATION->GetCurDir();
    if($dir='/' && empty($noindex)){
        if(isset($_GET['PAGEN_1'])){
            echo '<meta name="robots" content="none"/>';
        }
    }
?>
    
    <meta http-equiv="Content-Type" content="text/html; charset=<?=LANG_CHARSET;    //Установка кодировки сайта ?> " />
        
    <? if (in_array($_SERVER['REQUEST_URI'], $seo_url)) { ?>    
		<? $APPLICATION->ShowMeta("keywords")   // Вывод мета тега keywords ?> 
	<? } ?>
	<? $APPLICATION->ShowMeta("description") // Вывод мета тега description ?> 
	<? $APPLICATION->ShowCSS(); // Подключение основных файлов стилей template_styles.css и styles.css ?> 
	<? $APPLICATION->ShowHeadStrings() // Отображает специальные стили, JavaScript ?> 
	<? $APPLICATION->ShowHeadScripts() // Вывода служебных скриптов ?> 
    
    <title><?$APPLICATION->ShowTitle()?></title>
    <meta name="format-detection" content="telephone=yes" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta name='yandex-verification' content='4d5c3ad5e5616770' />
    <link rel="icon" href="<?=SITE_TEMPLATE_PATH?>/images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="<?=SITE_TEMPLATE_PATH?>/images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" media="all" href="<?=SITE_TEMPLATE_PATH?>/css/style.css">
    
    <link rel="stylesheet" type="text/css" media="screen" href="<?=SITE_TEMPLATE_PATH?>/css/jquery.fancybox.css">
    <meta name='yandex-verification' content='557078134c1f7e89' />
    <link rel="stylesheet" type="text/css" media="screen" href="<?=SITE_TEMPLATE_PATH?>/project.css">
	<link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery-2.1.3.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery-migrate-1.2.1.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/project.js"></script>
    <!--[if lt IE 8]>
        <div style=' clear: both; text-align:center; position: relative;'>
            <a href="https://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
                <img src="https://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
            </a>
        </div>
    <![endif]-->
    <!--[if lt IE 9]>
        <script src="<?=SITE_TEMPLATE_PATH?>/js/html5.js"></script>
        <link rel="stylesheet" type="text/css" media="screen" href="<?=SITE_TEMPLATE_PATH?>/css/ie.css">
    <![endif]-->
    <meta name="google-site-verification" content="XWpKFVWd-OxYerH0APZky2z6vq8dmvyRjQOZdFDR0PA" />
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-68093848-1', 'auto');
  ga('send', 'pageview');

</script>

<?

	$array_link = array(
		'/order/quick_credit.php',
		'/search/',
		'/search/map.php',
		'/club/search/',
		'/club/group/search/',
		'/club/forum/search/',
		'/communication/forum/search/',
		'/communication/blog/search.php',
		'/club/gallery/tags/',
		'/examples/my-components/',
		'/examples/download/download_private/',
		'/auth/',
		'/auth.php',
		'/personal/',
		'/communication/forum/user/',
		'/e-store/paid/detail.php',
		'/e-store/affiliates/',
		'/club/',
		'/club/messages/',
		'/club/log/',
		'/content/board/my/',
		'/content/links/my/',
	);

?>

<? if (in_array($_SERVER['REQUEST_URI'], $array_link)) { ?>
	<meta name="googlebot" content="noindex">
<? } ?>


</head>
<body>
    <section class="info_box light_font">
        <?$APPLICATION->ShowPanel();?>
        <div class="container clearfix">
            <div class="grid_38 prefix_2">
                <div class="clearfix">
                    <div class="f_left">
                        <a href="tel:+7 (499) 650-52-09" class="info_phone_num d_ib white" onClick="yaCounter22871155.reachGoal('tel'); ga('send', 'pageview', '/tel/');"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/phone.php"), false);?></a>
                        <a href="mailto:<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/email.php"), false);?>" class="light_font"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/email.php"), false);?></a>
                    </div>
                    <div class="f_right">
                        <a href="/contacts/info/" class="info_link"><span class="sprite-clock_icon" id="i"></span>Режим работы</a>
                        <?if($USER->IsAuthorized()):?>
                            <a href="/personal/" class="info_link"><span class="sprite-user_icon" id="i"></span>Личный кабинет</a>
                        <?else:?>
                            <a href="#" class="info_link modal_btn" data-modal="#authorization"><span class="sprite-user_icon"></span>Войти</a>
                        <?endif;?>
                        <?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "mini-basket", array(
								"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
								"PATH_TO_PERSONAL" => SITE_DIR."personal/",
								"SHOW_PERSONAL_LINK" => "N",
								"SHOW_NUM_PRODUCTS" => "Y",
								"SHOW_TOTAL_PRICE" => "N",
								"SHOW_PRODUCTS" => "N",
								"POSITION_FIXED" =>"N"
							),
							false,
							array()
						);?>
                        
                        <a href="#" class="big_callback_btn d_ib modal_btn" data-modal="#callback_form"><span class="sprite-white_phone_icon"></span>Заказать обратный звонок</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
 	<header>
        <div class="container">
            <div class="clearfix">
                <!-- logo -->
                <div class="grid_17 prefix_2">
                    <div class="logo clearfix">
                        <a href="/" class="logo_link">АКБ</a>
                        <div class="logo_text wrapper">
                            <a href="/">
                                <div class="bold fs_30 reg black mb2">Аккумуляторы</div>
                                и аксессуары по лучшей цене
                            </a>
                        </div>
                    </div>
                </div>
                <!-- !! -->
                
                <!-- work time -->
                <div class="grid_9">
                    <div class="work_time wrapper reg fs_12">
                        <div class="work_time_item f_left">
                            <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/schedule-left.php"), false);?>
                        </div>
                        <div class="work_time_item f_left">
                            <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/schedule-right.php"), false);?>
                        </div>
                    </div>
                </div>
                <!-- !! -->
                
                <!-- search -->
                <div class="grid_10">
                    <form class="search_form" action="/search/">
                        <input type="text" name="q" class="search_form_input" value="Введите запрос" onBlur="if(this.value=='') this.value='Введите запрос'" onFocus="if(this.value =='Введите запрос' ) this.value=''">
                        <button class="search_form_btn"></button>
                    </form>
                </div>
                <!-- !! -->
            </div>
            <hr class="header_separator">
        </div>
		
		<div class="container">
            <div class="clearfix">
				<div class="header-info-text">
				
				<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/header_info_text.inc.php"), false);?>
				
				</div>
			</div>
			
		</div>
		
		
    </header>
    
    <?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"top_menu", 
	array(
		"ROOT_MENU_TYPE" => "top",
		"MENU_CACHE_TYPE" => "Y",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "1",
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N"
	),
	false
);?>
    
    <?if ($curPage == SITE_DIR."index.php"):?>
        <?$APPLICATION->IncludeComponent(
        	"bitrix:news.list",
        	"slider",
        	Array(
        		"IBLOCK_TYPE" => "services",
        		"IBLOCK_ID" => "4",
        		"NEWS_COUNT" => "20",
        		"SORT_BY1" => "ACTIVE_FROM",
        		"SORT_ORDER1" => "DESC",
        		"SORT_BY2" => "SORT",
        		"SORT_ORDER2" => "ASC",
        		"FILTER_NAME" => "",
        		"FIELD_CODE" => array("", ""),
        		"PROPERTY_CODE" => array("YOUTUBE", "TYPE", "PRICE"),
        		"CHECK_DATES" => "Y",
        		"DETAIL_URL" => "",
        		"AJAX_MODE" => "N",
        		"AJAX_OPTION_JUMP" => "N",
        		"AJAX_OPTION_STYLE" => "Y",
        		"AJAX_OPTION_HISTORY" => "N",
        		"CACHE_TYPE" => "A",
        		"CACHE_TIME" => "36000000",
        		"CACHE_FILTER" => "N",
        		"CACHE_GROUPS" => "Y",
        		"PREVIEW_TRUNCATE_LEN" => "",
        		"ACTIVE_DATE_FORMAT" => "d.m.Y",
        		"SET_TITLE" => "N",
        		"SET_BROWSER_TITLE" => "N",
        		"SET_META_KEYWORDS" => "N",
        		"SET_META_DESCRIPTION" => "N",
        		"SET_STATUS_404" => "N",
        		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        		"ADD_SECTIONS_CHAIN" => "N",
        		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
        		"PARENT_SECTION" => "",
        		"PARENT_SECTION_CODE" => "",
        		"INCLUDE_SUBSECTIONS" => "Y",
        		"DISPLAY_DATE" => "Y",
        		"DISPLAY_NAME" => "Y",
        		"DISPLAY_PICTURE" => "Y",
        		"DISPLAY_PREVIEW_TEXT" => "Y",
        		"PAGER_TEMPLATE" => ".default",
        		"DISPLAY_TOP_PAGER" => "N",
        		"DISPLAY_BOTTOM_PAGER" => "N",
        		"PAGER_TITLE" => "Новости",
        		"PAGER_SHOW_ALWAYS" => "N",
        		"PAGER_DESC_NUMBERING" => "N",
        		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        		"PAGER_SHOW_ALL" => "N"
        	)
        );?>
    <?endif;?>
    
    <article id="content" <?if ($curPage != SITE_DIR."index.php"):?>class="catalog_content"<?endif;?>>
        <div class="container">
      
        <?if ($curPage != SITE_DIR."index.php" && strpos($APPLICATION->GetCurDir(), "/personal/")===false):?>
            <?
            if(strpos($APPLICATION->GetCurDir(), "/catalog/")!==false)
            {
                $start = 1;
            }else{
                $start = 0;
            }
            ?>
            <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
    				"START_FROM" => $start,
    				"PATH" => "",
    				"SITE_ID" => "-"
    			),
    			false,
    			Array('HIDE_ICONS' => 'Y')
    		);?>
        <?endif?>
        
        <?
        if(
            strpos($APPLICATION->GetCurDir(), "/personal/")===false && $curPage != SITE_DIR."index.php" && 
            strpos($APPLICATION->GetCurDir(), "/catalog/")===false && strpos($APPLICATION->GetCurDir(), "/search/")===false && 
            strpos($APPLICATION->GetCurDir(), "/news/")===false && strpos($APPLICATION->GetCurDir(), "/articles/")===false
        ){
            ?>
            <div class="clearfix">
                <div class="grid_24 prefix_2 resp_p0">
                    <div class="article_text_page">
            <?
        }
        ?>
