$(function () {

	jsToggle();
	jsSlider();
	jsSelect();
	jsTabs();
	jsMask();
	jsFlyingHint();
	jsPopup();
	jsDatePicker();
	jsExchangePopUp();

	$(window).on('load resize scroll', function () {
		var wT = $(window).scrollTop(), wH = $(window).height();
		if (wT > wH) {
			$('.js-scroll2top').addClass('is-visible');
		}
		else {
			$('.js-scroll2top').removeClass('is-visible');
		}
	});

    $(document).ready(function () {
        $('.notice').each(function () {
            if ($(this).text().trim() != '') {
                $(this).show();
            }
        })
    });

	$('.js-scroll2top').on('click', function () {
		$('body,html').animate({scrollTop: 0}, 500);
	});
	$('.checkout__item-header').on('click', function (e) {
		e.preventDefault();
		$(this).parent().toggleClass('-active');
	});

	$('.btn_next_step').on('click', function (e) {
		e.preventDefault();
		var $curItem = $(this).closest('.checkout__item');
		$curItem.toggleClass('-active -complete');
		$curItem.next('.checkout__item').addClass('-active');
	})
	$('.slick-dots, .slick-arrow').click(function () {
		$(this).parent().find('.flying-hint').each(function () { $(this).hide()})
	})

	var dialogLogin     = $('#dialogLogin').dialog({
		border   : 0,
		autoOpen : false,
		resizable: false,
		height   : 'auto',
		maxWidth : 280,
		modal    : true,
		title    : 'Вход',
		closeText: 'Закрыть',
		create   : function (event, ui) {
			var widget = $(this).dialog("widget");
			$(".ui-dialog-titlebar-close", widget).html('<span class="custom-popup__hide"></span>');
		}
	});
	var dialogReg       = $('#dialogReg').dialog({
		border   : 0,
		autoOpen : false,
		resizable: false,
		height   : 'auto',
		maxWidth : 280,
		modal    : true,
		title    : 'Регистрация',
		closeText: 'Закрыть',
		create   : function (event, ui) {
			var widget = $(this).dialog("widget");
			$(".ui-dialog-titlebar-close", widget).html('<span class="custom-popup__hide"></span>');
		}
	});
	var dialogFogotPass = $('#dialogFogotPass').dialog({
		border   : 0,
		autoOpen : false,
		resizable: false,
		height   : 'auto',
		maxWidth : 280,
		modal    : true,
		title    : 'Восстановление пароля',
		closeText: 'Закрыть',
		create   : function (event, ui) {
			var widget = $(this).dialog("widget");
			$(".ui-dialog-titlebar-close", widget).html('<span class="custom-popup__hide"></span>');
		}
	});


	$(".btn_login").on('click', function (e) {
		e.preventDefault();
		dialogReg.dialog("close");
		dialogFogotPass.dialog("close");
		dialogLogin.dialog("open");
	});
	$(".btn_reg").on('click', function (e) {
		e.preventDefault();
		dialogLogin.dialog("close");
		dialogReg.dialog("open");
	});
	$(".btn_fogot_pass").on('click', function (e) {
		e.preventDefault();
		dialogLogin.dialog("close");
		dialogFogotPass.dialog("open");
	});

});

function jsPopup () {
	$('.js-popup').each(function () {
		var s = {
			width  : '100%',
			opacity: .7
		}, o  = {}, s = $.extend(s, o);
		$.each($(this).data(), function (k, v) {o[k] = v});
		s = $.extend(s, o);
		$(this).colorbox(s);
	});
	$('.js-popup-hide').on('click', function () {
		$.colorbox.close();
	});
};

function jsToggle () {
	$('[data-toggleclass]').on('click', function () {
		var data = $(this).data('toggleclass');
		$(data.target).toggleClass(data.className);
	});
	$('.js-toggle-wrap').each(function () {
		var wrap   = $(this),
			toggle = $('.js-toggle', wrap),
			hidden = $('[data-hidden]', wrap);
		toggle.on('click', function () {
			var text = $(this).data('text');
			wrap.toggleClass('show-hidden');
			hidden.toggleClass('hidden');
			wrap.hasClass('show-hidden') ? toggle.text(text.hide) : toggle.text(text.show);
		});
	});
}

function jsSlider () {
	$('.js-slider').slick({
		speed         : 1000,
		touchMove     : false,
		touchThreshold: 100,
		prevArrow     : '<i class="slick-arrow slick-prev fa fa-angle-left" />',
		nextArrow     : '<i class="slick-arrow slick-next fa fa-angle-right" />'
	});
}

function jsSelect () {
	$('select.js-select').chosen({
		disable_search        : true,
		inherit_select_classes: true
	});
}

function jsRangeSlider (slider, min, max, valMin, valMax, inpMin, inpMax) {
	$(slider).slider({
		range : true,
		min   : min,
		max   : max,
		values: [valMin, valMax],
		slide : function (event, ui) {
			$(inpMin).val(ui.values[0]);
			$(inpMax).val(ui.values[1]);
		}
	});
	$(inpMin).on('change', function () {
		var val = $(this).val();
		$(slider).slider('values', 0, val);
	});
	$(inpMax).on('change', function () {
		var val = $(this).val();
		$(slider).slider('values', 1, val);
	});
	$('.ui-slider-handle', '.ui-slider-handle:active').draggable({
		axis : 'x',
		containment: 'parent'
	});
}

function jsTabs () {
	$('.js-tabs').each(function () {
		var tabs = $(this),
			lnk  = $('[data-target]', tabs),
			tab  = $('[data-tab]', tabs);
		lnk.on('click', function () {
			var target = $(this).data('target');
			lnk.removeClass('current');
			$(this).addClass('current');
			tab.hide();
			$('[data-tab="' + target + '"]', tabs).show();
		});
	});
}

function jsMask () {
	$('[data-masked]').each(function () {
		var mask = $(this).data('masked');
		$(this).mask(mask);
	});
}

function jsFlyingHint () {
	var $jsFlyingHint = $('[data-flying-hint]');
	$jsFlyingHint.each(function () {
		$($(this).data('flying-hint')).appendTo('body');
	});
	$('[data-flying-hint]').on('click', function (e) {
		var top  = $(this).offset().top + $(this).outerHeight() + 14,
			hint = $(this).data('flying-hint');
		$('.flying-hint__wrap').show();
		$('.flying-hint').hide();
		$(hint).css({top: top}).show();
		e.preventDefault();
	});
	$('.js-flying-hint-hide').on('click', function () {
		$('.flying-hint').hide();
	});
}

function jsDatePicker () {
	$.datepicker.setDefaults($.datepicker.regional["ru"]);
	$(".datepicker").datepicker({
		showOtherMonths  : true,
		selectOtherMonths: false,
		beforeShow       : function (input) {
			setTimeout(function () {
				$('.ui-datepicker-prev').html('<i class="fa fa-angle-left" style=""></i>');
				$('.ui-datepicker-next').html('<i class="fa fa-angle-right" style=""></i>');
			}, 1);
		},
		onChangeMonthYear: function (yy, mm, inst) {
			setTimeout(function () {
				$('.ui-datepicker-prev').html('<i class="fa fa-angle-left" style=""></i>');
				$('.ui-datepicker-next').html('<i class="fa fa-angle-right" style=""></i>');
			}, 1);
		}
	});
}

function jsExchangePopUp() {
	$('#help').click(function() {
		$('#exchange-popup').addClass('exchange-popup_show');
	});
	$('#exchange-popup__hide').click(function() {
		$('#exchange-popup').removeClass('exchange-popup_show');
	});

}

$(function () {
	$('.catalog').on('click', '.warning__close', function () {
		$('.warning__old-battery').remove();
	});
});


$(function (){
	$('.info__block-right-open-map > img').click(function() {
		$('#minimap').addClass('minimap_open');
		$('body').css('overflow', 'hidden');

	});
	$('.minimap__close').click(function() {
		$('#minimap').removeClass('minimap_open');
		$('body').css('overflow', '');
	});
	$('#minimap').click(function() {
		$('#minimap').removeClass('minimap_open');
		$('body').css('overflow', '');
	});
});

$(document).ready(function(){
	phpUrl = $('#ajaxload').data('url');
	$('.content_items').on('click', '#ajaxload',function(){
		$.ajax({
			url: phpUrl,
			method: 'GET',
			dataType: 'html',
			data: {
					"ajax" : "Y"
				  },
			beforeSend: function() {
				$(".catalog__items").append('<div id="preloader"></div>');
			}
			}).done(function(data){
				$('#preloader').remove();
				divWithItems = $(data).find(".content_items").get(0).innerHTML;
				phpUrl = $(data).find(".show-more").data('url');
				$('.nav_string').replaceWith(divWithItems);
			});
	});

    $(".select").select2({
        minimumResultsForSearch: -1
    });
    $('body').on('click', '.catalog__icons__item i', function(){
        $('.catalog__icons__item .hidden').hide();
        $(this).find('.hidden').show();
    });
    $('body').on('click','.catalog__icons__item i span', function(){
        $('.catalog__icons__item .hidden').hide();
        $(this).closest('i').find('div').css('display', 'none').hide().removeAttr('style');
        return false;
    });
});

