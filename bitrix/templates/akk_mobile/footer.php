<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
        </div><!-- /catalog -->
    </div><!-- /content -->

    <footer class="footer">
        <div class="container">
            <div class="footer__info">
                <div><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/copyright.php"), false);?></div>
                <div class="footer-address">
                    <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/footer_address.php"), false);?>
                </div>
                <a href="tel:+7 (499) 650-52-09"
                   onclick="yaCounter22871155.reachGoal('tel'); ga('send', 'pageview', '/tel/');">
                    <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/phone.php"), false);?>
                </a>
            </div>

            <a href="<?= $APPLICATION->GetCurPage() ?>?desktop=y"><?= Loc::getMessage('FOOTER_DESKTOP_SITE') ?></a>
            <a href="javascript:" class="btn btn_wide iconed ico_phone" data-flying-hint="#form-callback"><span><?= Loc::getMessage('FOOTER_PHONE') ?></span></a>
            <div class="footer__logos">
                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include_mobile/footer_logo.php"), false);?>
            </div>
            <?$APPLICATION->IncludeComponent(
                "bitrix:menu",
                "footer_menu",
                array(
                    "ROOT_MENU_TYPE" => "bottom",
                    "MENU_CACHE_TYPE" => "Y",
                    "MENU_CACHE_TIME" => "36000000",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => "",
                    "MAX_LEVEL" => "1",
                    "ALLOW_MULTI_SELECT" => "N",
                    "CHILD_MENU_TYPE" => "left",
                    "DELAY" => "N",
                    "USE_EXT" => "N",
                ),
                false
            );?>

            <?$APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "news_in_footer_mobile",
                array(
                    "IBLOCK_TYPE" => "news",
                    "IBLOCK_ID" => $_REQUEST["ID"],
                    "NEWS_COUNT" => "2",
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => "",
                    "FIELD_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "PROPERTY_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "ACTIVE_DATE_FORMAT" => "j F Y",
                    "SET_TITLE" => "N",
                    "SET_BROWSER_TITLE" => "Y",
                    "SET_META_KEYWORDS" => "Y",
                    "SET_META_DESCRIPTION" => "Y",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "PAGER_TEMPLATE" => ".default",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "BLOCK_LABEL" => "Новости"
                ),
                false
            );?>

            <?$APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "news_in_footer_mobile",
                array(
                    "IBLOCK_TYPE" => "news",
                    "IBLOCK_ID" => "5",
                    "NEWS_COUNT" => "2",
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => "",
                    "FIELD_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "PROPERTY_CODE" => array(
                        0 => "LABEL",
                        1 => "",
                    ),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "ACTIVE_DATE_FORMAT" => "j F Y",
                    "SET_TITLE" => "N",
                    "SET_BROWSER_TITLE" => "N",
                    "SET_META_KEYWORDS" => "N",
                    "SET_META_DESCRIPTION" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "PAGER_TEMPLATE" => ".default",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "Y",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "BLOCK_LABEL" => "Статьи"
                ),
                false
            );?>
        </div>
    </footer>
    </div><!-- /main-wrapper -->

    <i class="scroll2top js-scroll2top"></i>

<!--<div id="success" class="sm_modal">-->
<!--    <div class="sm_modal_inner al_center">-->
<!--        <div class="heavy fs_36 lts reg mb26">Спасибо</div>-->
<!--        Ваша заявка принята. Наш менеджер перезвонит Вам в ближайшее время, расскажет о аккумуляторах, доставке и ответит на все ваши вопросы.-->
<!--    </div>-->
<!--</div>-->

    <div class="flying-hint form-callback" id="form-callback">
        <div class="container">
            <div class="flying-hint__wrap">
                <i class="flying-hint__hide js-flying-hint-hide"></i>
                <div class="flying-hint__body">
                    <h3><?=Loc::getMessage('FOOTER_PHONE')?>:</h3>
                    <form class="callback-form">
                        <div class="error"></div>
                        <fieldset>
                            <ul>
                                <li>
                                    <input type="text" name="name" value="" placeholder="<?=Loc::getMessage('CALLBACK_FORM_NAME')?>" />
                                </li>
                                <li>
                                    <input type="text" name="phone" value="" data-masked="+7(999)999-99-99" placeholder="+7(___)___-__-__" />
                                </li>
                            </ul>
                        </fieldset>
                        <input type="submit" onClick="yaCounter22871155.reachGoal('zvonok'); ga('send', 'event', 'forma', 'zvonok');" value="<?=Loc::getMessage('PHONE_CALL')?>" />
                    </form>
                </div>
            </div>
            <div id="success-callback" class="flying-hint flying-hint__wrap">
                <i class="flying-hint__hide js-flying-hint-hide"></i>
                <div class="flying-hint__body">
                        <div class="heavy fs_36 lts reg mb26"><?=Loc::getMessage('PHONE_SUCCESSFUL_TITLE')?></div>
                        <?=Loc::getMessage('PHONE_SUCCESSFUL')?>
                </div>
            </div> 
        </div>
    </div>
	
    <?
    $arJs = [
        SITE_TEMPLATE_PATH . '/js/libs/jquery-ui.min.js',
        SITE_TEMPLATE_PATH . '/js/libs/jquery.form-styler/jquery.formstyler.js',
        SITE_TEMPLATE_PATH . '/js/slick.min.js',
        SITE_TEMPLATE_PATH . '/js/chosen.min.js',
        SITE_TEMPLATE_PATH . '/js/chosen.jquery.min.js',
        SITE_TEMPLATE_PATH . '/js/colorbox.min.js',
        SITE_TEMPLATE_PATH . '/js/jquery.maskedinput.min.js',
        SITE_TEMPLATE_PATH . '/js/datepicker-ru.js',
        SITE_TEMPLATE_PATH . '/js/script.js',
	    SITE_TEMPLATE_PATH . '/js/chosen.jquery.min.js',
        SITE_TEMPLATE_PATH . '/project.js'
    ];

    foreach ($arJs as $js) {
        Asset::getInstance()->addJs($js);
    } ?>
	
	
<!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) { (w[c] = w[c] || []).push(function() {
            try { w.yaCounter22871155 = new Ya.Metrika({id:22871155, clickmap:true, trackLinks:true, accurateTrackBounce:true}); }
            catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";
            if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript>
        <div><img src="//mc.yandex.ru/watch/22871155" style="position:absolute; left:-9999px;" alt="" /></div>
    </noscript>
<!-- /Yandex.Metrika counter -->
<!--LiveInternet counter-->
    <script type="text/javascript"><!--
        new Image().src = "//counter.yadro.ru/hit?r"+
            escape(document.referrer)+((typeof(screen)=="undefined")?"":
                ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
                    screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
            ";"+Math.random();//-->
    </script>
<!--/LiveInternet-->
</body>
</html>
<?
exit;
?>