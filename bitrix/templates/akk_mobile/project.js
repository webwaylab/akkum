$(document).ready(function() {
    $("body").on("submit", ".login-form", function(e){
    	e.preventDefault();
    	params=$(this).serialize()+"&action=login";
        $(".login-form").find(".error").hide().html("");
        $.ajax({
            type: 'POST',
            url: '/auth/ajax.php',
            data: params,
            dataType: 'json',
            success: function(result){
                if(result.status)
                    document.location.reload();
                else
                    $(".login-form").find(".error").show().html(result.message);
            }
        });
    });

    $("body").on("submit", ".register-form", function(e){
    	e.preventDefault();
    	params=$(this).serialize()+"&action=register";
        $(".register-form").find(".error").hide().html("");
        $.ajax({
            type: 'POST',
            url: '/auth/ajax.php',
            data: params,
            dataType: 'json',
            success: function(result){
                console.log(result);
                if(result.status){
                    $(".register-form").find(".error").show().html(result.message);
                    document.location.reload();
                }else{
                    $(".register-form").find(".error").show().html(result.message);
                }
            }
        });
    });

    $("body").on("submit", ".recovery-form", function(e){
    	e.preventDefault();
    	params=$(this).serialize()+"&action=recovery";
        $(".recovery-form").find(".error").hide().html("");
        $.ajax({
            type: 'POST',
            url: '/auth/ajax.php',
            data: params,
            dataType: 'json',
            success: function(result){
                if(result.status)
                    $(".recovery-form").find(".error").show().html(result.message);//location.reload();
                else
                    $(".recovery-form").find(".error").show().html(result.message);
            }
        });
         
    });


    /*
    * Это был наиболее изящный способ сделать как на декстопе и не сильно говнокодить. =(
    */ 

    $("body").on("submit", ".callback-form", function(e){
        e.preventDefault();
        if($(".callback-form input[name=name]").val().length == 0) {
            $(".callback-form input[name=name]").val($(".callback-form input[name=name]").attr('placeholder'));
        }
    	params=$(this).serialize()+"&action=callback";
        $(".callback-form").find(".error").hide().html("");
        $.ajax({
            type: 'POST',
            url: '/actions.php',
            data: params,
            dataType: 'json',
            success: function(result)
            {
                if(result.status)
                {
                    $(".callback-form").find(".error").hide().html("");
                    $(".callback-form").parent().parent().hide();
                    $(".callback-form input[type=text]").each(function(e){$(this).val('');});
                    $("#success-callback").show();
                } else {
                    $(".callback-form").find(".error").show().html(result.message);
                    if (($(".callback-form input[name=name]").val() == $(".callback-form input[name=name]").attr('placeholder'))) {
                        $(".callback-form input[name=name]").val('');
                    }
                }
            }
        });
    });

    $("body").on("submit", ".one-click", function(e){
    	e.preventDefault();
        var self = $(this);
    	params = $(this).serialize()+"&action=one-click";
        self.find(".error").hide().html("");
        $.ajax({
            type: 'POST',
            url: '/actions.php',
            data: params,
            dataType: 'json',
            success: function(result){
                if(result.status)
                {
                    self.find(".error").hide().html("");
                    self.parent().parent().hide();
                    self.parent().parent().siblings(".flying-hint__wrap.order_success_message").show();
                } else {
                    self.find(".error").show().html(result.message);
                }
            }
        });
    });

    $('.catalog__orderinadvance').on('click', function () {
        $('.product_quantity', 'form.order-in-advance').text($('input[type="number"][id$="_quantity"]').val());
    });

    $("body").on("submit", ".order-in-advance", function(e){
        e.preventDefault();

        var self = $(this),
            params = $(this).serialize() + "&action=order-in-advance";

        $('input[name="product_quantity"]', self).val(parseFloat($('input[type="number"][id$="_quantity"]').val()));

        self.find(".error").hide().html("");

        $.ajax({
            type: 'POST',
            url: '/actions.php',
            data: params,
            dataType: 'json',
            success: function (result) {
                if (result.status) {
                    self.find(".error").hide().html("");
                    self.parent().parent().hide();
                    self.parent().parent().siblings(".flying-hint__wrap.order_success_message").show();
                } else {
                    self.find(".error").show().html(result.message);
                }
            }
        });

        return false;
    });

});