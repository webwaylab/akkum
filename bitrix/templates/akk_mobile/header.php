<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Localization\Loc;
global $USER, $APPLICATION;
Loc::loadMessages(__FILE__);
CUtil::InitJSCore();
CJSCore::Init(array("fx", "ajax"));
$curPage = $APPLICATION->GetCurPage(true);
$dir = $APPLICATION->GetCurDir();
?>

<!DOCTYPE html>
<html lang="<?= LANGUAGE_ID ?>">
    <head>
<? $APPLICATION->ShowHead(); ?>

        <? $seo_url = array(
            '/catalog/accessories/zaryadnoe_ustrojstvo/',
            '/catalog/accessories/nabor_instrumentov_dlya_avto/',
            '/catalog/avtomobilnye/',
            '/catalog/battery/zver/',
            '/catalog/battery/varta/',
            '/catalog/battery/varta/blue_dynamic/',
            '/catalog/battery/varta/silver_dynamic/',
            '/catalog/accessories/nagruzochnye_vilki/',
            '/catalog/battery/gelevye/akb/',
            '/catalog/battery/gelevye/',
            '/catalog/battery/bosch/',
            '/catalog/battery/bosch/s4_silver/',
            '/catalog/battery/mutlu/',
            '/',
            '/catalog/battery/moto_akkumulyatory/',
            '/catalog/battery/tudor/',
            '/catalog/auto/',
            '/catalog/battery/afa/',
            '/catalog/battery/solite/',
            '/catalog/battery/titan/',
            '/catalog/battery/aktex/',
            '/catalog/battery/vaiper/',
            '/catalog/battery/royal/',
            '/catalog/battery/cobat/',
            '/catalog/battery/tyumen/',
            '/catalog/battery/tyumen/lider/',
            '/catalog/battery/bolk_moto/',
            '/catalog/battery/sebang/',
        );

        if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/d-robots.php'))
        {
            include_once($_SERVER['DOCUMENT_ROOT'] . '/d-robots.php');
            $dRobots = dRobots::fromFile();
            $noindex = $dRobots->checkUrl($_SERVER['REQUEST_URI']) ? '<meta name="googlebot" content="noindex">' . PHP_EOL : '';
        }
        else
        {
            $noindex = '';
        }

        //Вывод в шаблоне
        echo $noindex;

        if (preg_match('#^/\?PAGEN_1=#', $_SERVER['REQUEST_URI'])) {
            echo '<meta name="robots" content="noindex, follow"/>';
        } ?>

        <meta http-equiv="Content-Type"
              content="text/html; charset=<?= LANG_CHARSET; ?> "/>

        <? if (in_array($_SERVER['REQUEST_URI'], $seo_url)) {
            $APPLICATION->ShowMeta("keywords");   // Вывод мета тега keywords
        } ?>

        <?  $APPLICATION->ShowMeta("description");?>

        <title><? $APPLICATION->ShowTitle(false) ?></title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE"/>
        <meta name="format-detection" content="telephone=no"/>
        <meta name='yandex-verification' content='4d5c3ad5e5616770'/>
        <meta name='yandex-verification' content='557078134c1f7e89'/>
        <meta name="google-site-verification" content="XWpKFVWd-OxYerH0APZky2z6vq8dmvyRjQOZdFDR0PA"/>

        <?
        $arCss = [
            SITE_TEMPLATE_PATH . '/js/libs/normalize-css/normalize.css',
            SITE_TEMPLATE_PATH . '/js/libs/font-awesome/css/font-awesome.css',
            SITE_TEMPLATE_PATH . '/css/fonts.css',
            SITE_TEMPLATE_PATH . '/css/jquery.formstyler.css',
            SITE_TEMPLATE_PATH . '/css/jquery-ui.min.css',
            SITE_TEMPLATE_PATH . '/css/slick.css',
            SITE_TEMPLATE_PATH . '/css/style.css'
        ];

        $arJs = [
            SITE_TEMPLATE_PATH . '/js/libs/modernizer/modernizr.js',
            SITE_TEMPLATE_PATH . '/js/libs/jquery/dist/jquery.js',
            'https://maps.googleapis.com/maps/api/js?sensor=false'
        ];

        foreach ($arCss as $css) {
            Asset::getInstance()->addCss($css);
        }

        foreach ($arJs as $js) {
            Asset::getInstance()->addJs($js);
        } ?>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-68093848-1', 'auto');
            ga('send', 'pageview');
        </script>

        <?
        $array_link = array(
            '/order/quick_credit.php',
            '/search/',
            '/search/map.php',
            '/club/search/',
            '/club/group/search/',
            '/club/forum/search/',
            '/communication/forum/search/',
            '/communication/blog/search.php',
            '/club/gallery/tags/',
            '/examples/my-components/',
            '/examples/download/download_private/',
            '/auth/',
            '/auth.php',
            '/personal/',
            '/communication/forum/user/',
            '/e-store/paid/detail.php',
            '/e-store/affiliates/',
            '/club/',
            '/club/messages/',
            '/club/log/',
            '/content/board/my/',
            '/content/links/my/',
        );

        if (in_array($_SERVER['REQUEST_URI'], $array_link)): ?>
            <meta name="googlebot" content="noindex">
        <? endif; ?>
    </head>

    <body class="<?= $APPLICATION->GetCurDir() == "/" ? 'is-burger-opened' : '' ?>">
        <? $APPLICATION->ShowPanel(); ?>
        <div class="main-wrapper">
            <header class="header">
                <div class="header__buttons">
                    <? if ($USER->IsAuthorized()): ?>
                        <a href="/personal/" class="btn iconed ico_login"><span><?= Loc::getMessage('HEADER_PERSONAL_CABINET') ?></span></a>
                    <? else: ?>
                       <?$APPLICATION->IncludeComponent(
                        "bitrix:system.auth.form",
                        "popup_auth",
                        Array(
                            "REGISTER_URL" => "",
                            "FORGOT_PASSWORD_URL" => "",
                            "PROFILE_URL" => "/personal/",
                            "SHOW_ERRORS" => "N"
                        )
                    );?>
                    <? endif; ?>
                    <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                       "AREA_FILE_SHOW" => "file",
                       "PATH" => "/include_mobile/header_phone.php",
                       "EDIT_TEMPLATE" => ""
                       ),
                       false
                    );?>
                    <a href="javascript:" class="btn iconed ico_phone" data-flying-hint="#form-callback"><span><?= Loc::getMessage('HEADER_PHONE') ?></span></a>
                </div>

                <div class="container">
                    <div class="header__body clearfix">
                        <span class="header__btn header__burger"
                              data-toggleclass='{"target": "body", "className": "is-burger-opened"}'><span></span></span>

                        <? $APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "mini-basket_mobile", array(
                            "PATH_TO_BASKET" => SITE_DIR . "personal/cart/",
                            "PATH_TO_PERSONAL" => SITE_DIR . "personal/",
                            "SHOW_PERSONAL_LINK" => "N",
                            "SHOW_NUM_PRODUCTS" => "Y",
                            "SHOW_TOTAL_PRICE" => "N",
                            "SHOW_PRODUCTS" => "N",
                            "POSITION_FIXED" => "N"
                        ),
                            false,
                            array()
                        ); ?>

                        <a href="/" class="logo">
                            <img src="<?= SITE_TEMPLATE_PATH . '/images/logo.png' ?>" alt="<?= Loc::getMessage('HEADER_LOGO_TEXT') ?>">
                            <span class="logo__text"><?= Loc::getMessage('HEADER_LOGO_TEXT') ?></span>
                        </a>
                    </div>
    <?
    CModule::IncludeModule("iblock");
    $news = CIBlock::GetList(array(), array('CODE' => 'news'))->Fetch();
    if ($news['DESCRIPTION']){ ?>
        <div class="container akkum-information">
            <div class="working__time">
                <img width="18" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAMAAABhEH5lAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAdVBMVEUAAAAPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIPWLIAAAA1XNlKAAAAJXRSTlMADCcEZMf+KNLrvQ1jmjbO8/mqe7ToAu0BA/KlPlyJQbigGjiiF7urNAAAAAFiS0dEAIgFHUgAAAAJcEhZcwAACxIAAAsSAdLdfvwAAACKSURBVBjTbVDXEoNACOSqJWq6xhJN4/9/MQrcTSZz+wKzsLAAwFBaK/iBsc4jemdNYLIcBXkmTIERBXGGe8pdRX2b1nK9hoaiXSknmv2Bglu3+y05ns4XrngFmpJraVrZooXC7tYPgWIhjjBFoYy/w4xhvJhY4IHRBFt9vt6faDVxUOrs1HP+XvgFfTEY4hOeY8AAAAAASUVORK5CYII=" height="18" title="" alt="">&nbsp;<?=$news['DESCRIPTION']?>
            </div>
            <br>
        </div>
    <? } ?>

                    <div class="header__toggled">
                        <div class="form-search">
                            <form action="/search/">
                                <input type="text" name="q" placeholder="<?= Loc::getMessage('HEADER_QUESTION_SEARCH') ?>"/>
                                <button type="submit"></button>
                            </form>
                        </div>
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "top_menu_mobile",
                            array(
                                "ROOT_MENU_TYPE" => "top",
                                "MENU_CACHE_TYPE" => "Y",
                                "MENU_CACHE_TIME" => "36000000",
                                "MENU_CACHE_USE_GROUPS" => "Y",
                                "MENU_CACHE_GET_VARS" => array(),
                                "MAX_LEVEL" => "1",
                                "ALLOW_MULTI_SELECT" => "N",
                                "CHILD_MENU_TYPE" => "left",
                                "DELAY" => "N"
                            ),
                            false
                        ); ?>
                    </div>
                </div><!-- /container -->
            </header>

            <div class="content container">

                <? $catalogSectionRoot = 0;

                $arParams = array(
                    "IBLOCK_ID" => "2"
                );
                //распределим по основынм разделам
				$page_uri = $_SERVER["REQUEST_URI"];
				$actualFilters = array();
				if(strstr($page_uri, "/moto-akb/") || $_REQUEST["arrFilter_50"] == "1768101828"){
					$catalogSectionRoot = 137;
				}elseif(strstr($page_uri, "/truck-akb/") || $_REQUEST["arrFilter_50"] == "255313712"){
					$catalogSectionRoot = 367;
				}elseif(strstr($page_uri, "/domkraty_podstavki_upory_pod_mashinu/")){
					$catalogSectionRoot = 307;
				}elseif(strstr($page_uri, "/nabor_instrumentov/")){
					$catalogSectionRoot = 278;
				}elseif(strstr($page_uri, "/accessories/")){
					$catalogSectionRoot = 19;
				}elseif(strstr($page_uri, "/masla/")){
					$catalogSectionRoot = 351;
				}elseif(strstr($page_uri, "/auto-akb/") || strstr($page_uri, "/battery/")){
					$catalogSectionRoot = 453;
				}

                $arResult["VARIABLES"]["SECTION_ID"] = $catalogSectionRoot;

                $arFilter = array(
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                    "ACTIVE" => "Y",
                    "GLOBAL_ACTIVE" => "Y",
                );
                if (0 < intval($arResult["VARIABLES"]["SECTION_ID"]))
                {
                    $arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
                }
                elseif ('' != $arResult["VARIABLES"]["SECTION_CODE"])
                {
                    $arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];
                }

                $obCache = new CPHPCache();
                if ($obCache->InitCache(36000, serialize($arFilter), "/iblock/catalog"))
                {
                    $arCurSection = $obCache->GetVars();
                }
                elseif ($obCache->StartDataCache())
                {
                    $arCurSection = array();
                    $dbRes = CIBlockSection::GetList(array(), $arFilter, false, array("ID", "NAME", "SECTION_PAGE_URL"));

                    if(defined("BX_COMP_MANAGED_CACHE"))
                    {
                        global $CACHE_MANAGER;
                        $CACHE_MANAGER->StartTagCache("/iblock/catalog");

                        if ($arCurSection = $dbRes->GetNext())
                        {
                            $CACHE_MANAGER->RegisterTag("iblock_id_".$arParams["IBLOCK_ID"]);
                        }
                        $CACHE_MANAGER->EndTagCache();
                    }
                    else
                    {
                        if(!$arCurSection = $dbRes->GetNext())
                            $arCurSection = array();
                    }

                    $obCache->EndDataCache($arCurSection);
                }

                if (!isset($arCurSection))
                {
                    $arCurSection = array();
                } ?>

            <? if ($APPLICATION->GetCurDir() == "/" || strrpos($APPLICATION->GetCurDir(),"catalog/") !== false):?>
             <div class="catalog-filter <?=$APPLICATION->ShowProperty('product_detail')?>">
                <span class="btn btn_o btn_wide iconed ico_filter"
                      data-toggleclass='{"target": "body", "className":
                      "is-filter-params-opened"}'><span><?=Loc::getMessage('HEADER_FILTER_PARAMS')?></span></span>
                <?if(!(strstr($page_uri, "/accessories/") || strstr($page_uri, "/masla/"))):?>
                <span class="btn btn_o btn_wide iconed ico_filter_car"
                      data-toggleclass='{"target": "body", "className":
                      "is-filter-auto-opened"}'><span><?=Loc::getMessage('HEADER_FILTER_AUTO')?></span></span>
                <?endif;?>
                <div class="custom-popup filter-params-popup">
                    <div class="custom-popup__body">
                        <div class="container">
                            <span class="custom-popup__hide" data-toggleclass='{"target": "body", "className": "is-filter-params-opened"}'></span>
                            <h2 class="custom-popup__title"><?=Loc::getMessage('HEADER_FILTER_PARAMS')?></h2>
                            <div>
                            	<?if($APPLICATION->GetCurPage(false) === '/' || strstr($page_uri, "/brand/")):?>
                                    <?$APPLICATION->IncludeComponent(
                                        "bitrix:catalog.smart.filter",
                                        "",
                                        array(
                                            "IBLOCK_TYPE" => "catalog",
                                            "IBLOCK_ID" => "2",
                                            "SECTION_ID" => "18",
                                            "FILTER_NAME" => "",
                                            "FILTER_URL" => "/catalog/battery/",
                                            "PRICE_CODE" => "",
                                            "CACHE_TYPE" => "A",
                                            "CACHE_TIME" => "36000000",
                                            "CACHE_GROUPS" => "Y",
                                            "SAVE_IN_SESSION" => "N",
                                            "FILTER_VIEW_MODE" => "VERTICAL",
                                            "XML_EXPORT" => "Y",
                                            "SECTION_TITLE" => "NAME",
                                            "SECTION_DESCRIPTION" => "DESCRIPTION",
                                            'HIDE_NOT_AVAILABLE' => "N",
                                            "TEMPLATE_THEME" => "green"
                                        ),
                                        $component,
                                        array('HIDE_ICONS' => 'Y')
                                    );?>
                            	<?elseif($catalogSectionRoot):?>
                                    <?$APPLICATION->IncludeComponent(
                                        "bitrix:catalog.smart.filter",
                                        "",
                                        array(
                                            "IBLOCK_TYPE" => "catalog",
                                            "IBLOCK_ID" => "2",
                                            "SECTION_ID" => $arCurSection['ID'],
                                            "FILTER_NAME" => "",
                                            "FILTER_URL" => $arCurSection['SECTION_PAGE_URL'],
                                            "PRICE_CODE" => "",
                                            "CACHE_TYPE" => "A",
                                            "CACHE_TIME" => "36000000",
                                            "CACHE_GROUPS" => "Y",
                                            "SAVE_IN_SESSION" => "N",
                                            "FILTER_VIEW_MODE" => "VERTICAL",
                                            "XML_EXPORT" => "Y",
                                            "SECTION_TITLE" => "NAME",
                                            "SECTION_DESCRIPTION" => "DESCRIPTION",
                                            'HIDE_NOT_AVAILABLE' => "N",
                                            "TEMPLATE_THEME" => "green"
                                        ),
                                        $component,
                                        array('HIDE_ICONS' => 'Y')
                                    );?>
								<?else:?>
                                    <?$APPLICATION->IncludeComponent(
                                        "bitrix:catalog.smart.filter",
                                        "head",
                                        array(
                                            "IBLOCK_TYPE" => "catalog",
                                            "IBLOCK_ID" => "2",
                                            "SECTION_ID" => "0",
                                            "FILTER_NAME" => "",
                                            "PRICE_CODE" => "",
                                            "CACHE_TYPE" => "A",
                                            "CACHE_TIME" => "36000000",
                                            "CACHE_GROUPS" => "Y",
                                            "SAVE_IN_SESSION" => "N",
                                            "FILTER_VIEW_MODE" => "VERTICAL",
                                            "XML_EXPORT" => "Y",
                                            "SECTION_TITLE" => "NAME",
                                            "SECTION_DESCRIPTION" => "DESCRIPTION",
                                            'HIDE_NOT_AVAILABLE' => "N",
                                            "TEMPLATE_THEME" => "green"
                                        ),
                                        $component,
                                        array('HIDE_ICONS' => 'Y')
                                    );?>
        						<?endif;?>
                                </div>
                        </div>
                    </div><!-- /custom-popup__body -->
                </div><!-- /filter-params-popup -->
                <div class="custom-popup filter-auto-popup">
                    <div class="custom-popup__body">
                        <div class="container">
                            <span class="custom-popup__hide" data-toggleclass='{"target": "body", "className": "is-filter-auto-opened"}'></span>
                            <h2 class="custom-popup__title"><?=Loc::getMessage('HEADER_FILTER_AUTO')?></h2>
                            <div>
					            <?$APPLICATION->IncludeComponent(
						            "irbis:model.filter",
						            "mobile",
						            array(
							            "BRAND_HL" => 5,
							            "MODEL_HL" => 3,
							            "MODEL_ROW_HL" => 4,
							            "FILTER_NAME" => "",
							            "SELECT_FIELD" => ""
						            ),
						            $component,
						            array("HIDE_ICONS" => "Y")
					            ); ?>
                            </div>
                        </div>
                    </div><!-- /custom-popup__body -->
                </div><!-- /filter-auto-popup -->

            </div><!-- /catalog-filter -->
            <?endif?>

        <div class="notice" style="display: none;">
            <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/header_info_text.inc.php"), false);?>
        </div>

        <div class="catalog">

            <?$APPLICATION->ShowViewContent('link_back');?>

            <? if ($curPage != SITE_DIR."index.php"): ?>
                <h1><? $APPLICATION->ShowTitle() ?></h1>
            <? else: ?>
                <h3 class="ttu"><?= Loc::getMessage('HEADER_TITLE') ?></h3>
            <? endif; ?>
