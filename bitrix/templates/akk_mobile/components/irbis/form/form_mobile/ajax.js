$(document).ready(function() {
    $("body").on("submit", ".opt-form", function(e){
    	e.preventDefault();
    	var oform = $(".opt-form");
        oform.find(".error").hide().html("");
        $.ajax({
            type: "POST",
            url: oform.attr("action"),
            data: oform.serialize()+"&is_ajax_post=Y",
            dataType: 'json',
            success: function(result){
                if(result.status) {
                    oform.find(".error").show().html(result.message);
                    oform.find("input[type='text'], textarea").val("");
                }else{
                    oform.find(".error").show().html(result.message);
                }
            }
        });
    });
});