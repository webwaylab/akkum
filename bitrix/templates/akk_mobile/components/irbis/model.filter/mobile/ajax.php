<?
define("STOP_STATISTICS", true);
define("PUBLIC_AJAX_MODE", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
__IncludeLang(GetLangFileName(dirname(__FILE__)."/lang/", "/template.php"));

global $APPLICATION;

//if(check_bitrix_sessid()):?>
    <?$APPLICATION->IncludeComponent(
    	"irbis:model.filter",
    	"",
    	array(
    		"BRAND_HL" => 5,
            "MODEL_HL" => 3,
            "MODEL_ROW_HL" => 4,
            "FILTER_NAME" => "",
            "SELECT_FIELD" => $_REQUEST["field"],
    	),
    	$component,
    	array("HIDE_ICONS" => "Y")
    );
    ?>
<?//endif;?>

<?die();?>