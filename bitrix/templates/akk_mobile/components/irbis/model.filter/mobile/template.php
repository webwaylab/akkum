<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<script src="<?=$templateFolder?>/ajax.js"></script>
<form method="GET" id="model-filter-form" data-ajax-action="<?=$templateFolder?>/ajax.php">
    <input type="hidden" name="filter_form" value="model-form" />
    <fieldset>
        <ul>
            <li>
                <label class="catalog-filter__label"><?=Loc::getMessage('BRAND')?>:</label>
                <select class="js-select" data-placeholder="<?=Loc::getMessage('CHOOSE')?>" name="BRAND">
                    <option></option>
                    <?
                    foreach($arResult["BRANDS"] as $arBrand): ?>
                    <option value="<?=$arBrand["ID"]?>"
                            <?if($arBrand["ID"]==$arResult["BRAND"]):?>selected<?endif;?>
                            data-code="<?=$arBrand["url_code"]?>">
                        <?=$arBrand["NAME"]?>
                    </option>
                    <?endforeach; ?>
                </select>
            </li>

            <?if(!empty($arResult["BRAND"])):?>
                <li>
                    <label class="catalog-filter__label"><?=Loc::getMessage('MODELS')?>:</label>
                    <select class="js-select" data-placeholder="<?=Loc::getMessage('CHOOSE')?>" name="MODEL">
                        <option></option>
                        <? foreach($arResult["MODELS"] as $arModel): ?>
                            <option value="<?=$arModel["ID"]?>"
                                    <?if($arModel["ID"]==$arResult["MODEL"]):?>selected<?endif;?>
                                    data-code="<?=$arModel["url_code"]?>" >
                                <?=$arModel["NAME"]?>
                            </option>
                        <? endforeach; ?>
                    </select>
                </li>
            <?endif;?>

            <?if(!empty($arResult["MODEL"])):?>
                <li>
                    <label class="catalog-filter__label"><?=Loc::getMessage('MODEL_ROW')?>:</label>
                    <select class="js-select" data-placeholder="<?=Loc::getMessage('CHOOSE')?>" name="MODEL_ROW">
                        <option></option>
                        <? foreach($arResult["MODELS_ROWS"] as $arModelRow): ?>
                            <option value="<?=$arModelRow["ID"]?>"
                                    <?if($arModelRow["ID"]==$arResult["MODEL_ROW"]):?>selected<?endif;?>
                                    data-code="<?=$arModelRow["url_code"]?>" >
                                <?=$arModelRow["NAME"]?>
                            </option>
                        <? endforeach; ?>
                    </select>
                </li>
            <?endif;?>
        </ul>
    </fieldset>
</form>
