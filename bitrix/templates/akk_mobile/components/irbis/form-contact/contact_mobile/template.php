<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<h2>Отправить сообщение</h2>
<script src="<?=$templateFolder."/ajax.js"?>"></script>
<form action="<?=$APPLICATION->GetCurPage();?>" method="POST" enctype="multipart/form-data" class="opt-form">
    <?=bitrix_sessid_post()?>
    <div class="wrapper">

        <div class="mb_10" style="margin-bottom: 20px;">
            <label style="font-size: 16px;">Имя*:</label>
            <input type="text" class="txt_field" name="NAME" value="">
        </div>
        <div class="mb_10" style="margin-bottom: 20px;">
            <label style="font-size: 16px;">Телефон*:</label>
            <input type="text" class="txt_field" name="PHONE" value="">
        </div>
        <div class="mb_10" style="margin-bottom: 20px;">
            <label style="font-size: 16px;">Сообщение*:</label>
            <textarea name="PREVIEW_TEXT" class="txt_field mb10"></textarea>
        </div>
        <div class="error bordered"></div>
        <button class="btn2 grn_skin">Отправить</button>
    </div>
</form>
