<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="checkout__item <?= $_POST["is_ajax_post"] == "Y" ? '-complete' : '' ?>" id="checkout2" data-step="delivery">
    <div class="checkout__item-header">
        <span class="num">2</span>
        <p class="h2"><?=GetMessage('ORDER_DELIVERY_TITLE')?></p>
        <i class="fa fa-angle-down"></i>
    </div>
    <div class="checkout__item-content">
        <fieldset>
            <ul>
                <li>
                    <p class="catalog-filter__label"><?=GetMessage('ORDER_CITY')?>:</p>
                    <select class="js-select" data-placeholder="select.js-select" name="LOCATION">
                        <?foreach($arResult["LOCATION_LIST"] as $arLocation):?>
                            <option value="<?=$arLocation["ID"]?>" <?=($arLocation["ID"]==$arResult["LOCATION"]) ? 'selected' : '' ?>>
                                <?=$arLocation["CITY_NAME"]?>
                            </option>
                        <?endforeach?>
                    </select>
                </li>
                <li>
                    <p class="catalog-filter__label"><?=GetMessage('ORDER_DELIVERY')?>:</p>
                    <select class="js-select" data-placeholder="select.js-select" name="DELIVERY">
                        <?foreach($arResult["DELIVERY_LIST"] as $arDelivery):?>
                            <option value="<?=$arDelivery["ID"]?>" <?=($arDelivery["ID"]==$arResult["DELIVERY"]) ? 'selected' : ''?>>
                                <?=$arDelivery["NAME"]?> - <?=intval($arDelivery["PRICE"])==0 ? GetMessage('ORDER_FREE') : number_format($arDelivery["PRICE"], 0, "", " ").' '.GetMessage('ORDER_RUB') ?>
                            </option>
                        <?endforeach?>
                    </select>
                </li>
                <?switch($arResult['DELIVERY']):
                    case '2':?>
                        <div>
                            <div class="d_t">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include_mobile/delivery-pickup.php"), false);?>
                            </div>
                        </div>
                    <?break?>
                    <?case '1':?>
                        <li>
                            <p class="catalog-filter__label"><?=GetMessage('ORDER_ADDRESS')?>:</p>
                            <textarea name="ADDRESS"><?=$arResult["ADDRESS"]?></textarea>
                        </li>
                        <li class="datepickerWrap">
                            <p class="catalog-filter__label"><?=GetMessage('ORDER_DATE')?>:</p>
                            <input class="datepicker" type="text" name="DATE_DELIVERY" value="<?=$arResult["DATE_DELIVERY"]?>" />
                            <i class="ui-datepicker-trigger fa fa-calendar"></i>
                        </li>
                    <?break;?>
                    <?case "3":?>
                        <li>
                            <p class="catalog-filter__label"><?=GetMessage('ORDER_ADDRESS')?>:</p>
                            <textarea name="ADDRESS"><?=$arResult["ADDRESS"]?></textarea>
                        </li>
                        <li class="datepickerWrap">
                            <p class="catalog-filter__label"><?=GetMessage('ORDER_DATE')?>:</p>
                            <input class="datepicker" type="text" name="DATE_DELIVERY" value="<?=$arResult["DATE_DELIVERY"]?>" />
                            <i class="ui-datepicker-trigger fa fa-calendar"></i>
                        </li>
                        <li>
                            <p class="catalog-filter__label"><?=GetMessage('ORDER_TIME')?>:</p>
                            <input type="radio" name="DELIVERY_TIME" value="С 12:00 до 18:00" id="radio0" <?=("С 12:00 до 18:00" == $arResult["DELIVERY_TIME"]) ? 'checked' : ''?> />
                            <label for="radio0"><?=GetMessage('ORDER_TIME_1')?></label>
                        </li>
                        <li>
                            <input type="radio" name="DELIVERY_TIME" value="С 18:00 до 22:00" id="radio1" <?=("С 18:00 до 22:00" == $arResult["DELIVERY_TIME"]) ? 'checked' : ''?> />
                            <label for="radio1"><?=GetMessage('ORDER_TIME_2')?></label>
                        </li>
                    <?break;?>
                    <?case "4":?>
                    <?case "5":?>
                        <li>
                            <p class="catalog-filter__label"><?=GetMessage('ORDER_ADDRESS')?>:</p>
                            <textarea name="ADDRESS"><?=$arResult["ADDRESS"]?></textarea>
                            <span class="grn_font"><?=GetMessage('ORDER_TIME_DELIVERY')?></span>
                        </li>
                    <?break;?>
                <?endswitch;?>
            </ul>
        </fieldset>
        <a href="javascript:" class="btn full-width btn_next_step"><?=GetMessage('ORDER_NEXT')?></a>
    </div>
</div>
