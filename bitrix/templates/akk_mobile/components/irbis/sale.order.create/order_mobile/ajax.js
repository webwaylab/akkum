var BXFormPosting = false;
function submitForm(val)
{
	if (BXFormPosting === true)
		return true;

	BXFormPosting = true;
	if(val != 'Y')
		BX('confirmorder').value = 'N';

	var orderForm = BX('ORDER_FORM');
	BX.showWait();
	BX.ajax.submit(orderForm, ajaxResult);

	return true;
}

function ajaxResult(res)
{
	var orderForm = BX('ORDER_FORM');
	try
	{
		// if json came, it obviously a successfull order submit

		var json = JSON.parse(res);
		BX.closeWait();

		if (json.error)
		{
			BXFormPosting = false;
			return;
		}
		else if (json.redirect)
		{
			window.top.location.href = json.redirect;
		}
	}
	catch (e)
	{
		// json parse failed, so it is a simple chunk of html

		BXFormPosting = false;
		BX('order_form_content').innerHTML = res;

        $('.styler').styler();
        $(".phone_mask").mask("+7(999) 999-99-99");
        $( ".datepicker" ).datepicker();
	}

	BX.closeWait();
	BX.onCustomEvent(orderForm, 'onAjaxSuccess');
}

$(function(){   
    $(document).on('change click', 'input[name="PAYMENT"]', function(){  
        submitForm("Y");
        formId = $(this).parents('div.checkout__item').attr('id');
    });
    
    $(document).on('change', 'select[name="DELIVERY"]', function(){
        submitForm("Y"); 
        formId = $(this).parents('div.checkout__item').attr('id');
    });
    
    $(document).on('click', '.item-delete', function(e){
        e.preventDefault();
        $(this).prev("input").val("Y");
        submitForm("Y");
    }); 
});