<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

    <div class="checkout__item <?= $_POST["is_ajax_post"] != "Y" ? '-active' : '-complete' ?>" id="checkout1">
        <div class="checkout__item-header">
            <span class="num">1</span>
            <p class="h2"><?=GetMessage('ORDER_CONTACTS')?></p>
            <i class="fa fa-angle-down"></i>
        </div>
        <div class="checkout__item-content">
            <fieldset>
                <ul>
                    <li>
                        <p class="catalog-filter__label"><?=GetMessage('ORDER_USER_NAME')?>:</p>
                        <input type="text" name="USER[NAME]" value="<?=$arResult["USER"]["NAME"]?>" />
                    </li>
                    <li>
                        <p class="catalog-filter__label"><?=GetMessage('ORDER_MAIL')?>:</p>
                        <input type="text" name="USER[EMAIL]" value="<?=$arResult["USER"]["EMAIL"]?>" />
                    </li>
                    <li>
                        <p class="catalog-filter__label"><?=GetMessage('ORDER_PHONE')?>:</p>
                        <input type="text" name="USER[PERSONAL_PHONE]" value="<?=$arResult["USER"]["PERSONAL_PHONE"]?>" data-masked="+7(999)999-99-99" placeholder="+7(___)___-__-__" />
                    </li>
                </ul>
            </fieldset>
            <a href="javascript:" class="btn full-width btn_next_step"><?=GetMessage('ORDER_NEXT')?></a>
        </div>
    </div>
