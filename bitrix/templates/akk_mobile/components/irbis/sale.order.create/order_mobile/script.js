$(document).ready(function(){
	BX.addCustomEvent('onAjaxSuccess', function(){
		jsSelect();
        $(".select").select2({
            minimumResultsForSearch: -1
        });
		$(function(){
			$('.checkout__item.-complete .checkout__item-header').on('click', function (e) {
				$(this).parent().toggleClass('-active');
			});
		$('.btn_next_step').on('click', function (e) {
			e.preventDefault();
			var $curItem = $(this).closest('.checkout__item');
			if ($curItem.data('step') == 'delivery') {
				$curItem.toggleClass('-active');
			}else{
				$curItem.toggleClass('-active -complete');
			};
			$curItem.next('.checkout__item').addClass('-active');
		})
		});
		if(formId.length){
			$('#' + formId).addClass('-active');
			delete(formId);
		}
	});
});

