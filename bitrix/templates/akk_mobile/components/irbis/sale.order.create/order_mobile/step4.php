<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="checkout__item" id="checkout4">
        <div class="checkout__item-header">
            <span class="num">4</span>
            <p class="h2"><?=GetMessage('ORDER_ADDITIONAL')?></p>
            <i class="fa fa-angle-down"></i>
        </div>
        <div class="checkout__item-content">
            <fieldset>
                <ul>
                    <li>
                        <p class="catalog-filter__label"><?=GetMessage('ORDER_BRAND')?>:</p>
                        <input type="text" name="BRAND_AUTO" value="<?=$arResult["BRAND_AUTO"]?>" />
                    </li>
                    <li>
                        <p class="catalog-filter__label"><?=GetMessage('ORDER_COMMENT')?>:</p>
                        <textarea name="COMMENT"><?=$arResult["COMMENT"]?></textarea>
                    </li>
                </ul>
            </fieldset>
            <a href="javascript:" class="btn full-width btn_next_step"><?=GetMessage('ORDER_NEXT')?></a>
        </div>
    </div>
