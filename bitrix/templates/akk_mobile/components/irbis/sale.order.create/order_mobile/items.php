<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["BASKET_ITEMS"] as $arItem):?>
    <?$arItem["SUM"] = $arItem["PRICE"]*$arItem["QUANTITY"];?>
    <div class="cart__item" id="item-<?=$arItem["ID"]?>">
        <div class="cart__item__header">
            <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="cart__item-title"><?=$arItem["NAME"]?></h2>
            <input type="hidden" name="DELETE_<?=$arItem["ID"]?>" value="N">
            <span class="cart__item__del item-delete"></span>
        </div>
        <div class="catalog__item">
            <figure>
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE_SRC"]?>" alt="<?=$arItem["NAME"]?>" /></a>
            </figure>
        </div>

        <fieldset>
            <ul>
                <li>
                    <div class="cart__item-amount__price">
                        <p><?=GetMessage('ORDER_COUNT')?>:</p>
                        <div class="cart__item-amount">
                            <select class="select" data-placeholder="select.js-select" name="QUANTITY_<?=$arItem["ID"]?>" onchange='submitForm("Y");'>
                                <?for($i=1; $i<100; $i++):?>
                                    <option value="<?=$i?>" <?=($i==$arItem["QUANTITY"]) ? 'selected' : '' ?>><?=$i?></option>
                                <?endfor?>
                            </select>
                        </div>
                        <div class="cart__item-price"><?=number_format($arItem["SUM"], 0, "", " ")?>&nbsp;<span class="ruble">руб.</span></div>
                    </div>
                </li>
                <?if($arItem["PROPERTY_INSTALL_VALUE"]=="да"):?>
                    <li>
                        <input
                        type="checkbox"
                        id="cart_chk2"
                        name="INSTALL_<?=$arItem["ID"]?>"
                        <?/*onchange='submitForm("Y");'*/?>
                        <?=($arResult["INSTALL_".$arItem["ID"]]=="on") ? 'checked' : ''?>
                        />
                        <label for="cart_chk2"><?=GetMessage('ORDER_INSTALL')?></label>
                    </li>
                <?endif?>
                <?if(intval($arItem["PROPERTY_PRICE_CHANGE_VALUE"])>0):?>
                <li>
                    <input
                    type="checkbox"
                    id="cart_chk1"
                    name="CHANGED_PRICE_<?=$arItem["ID"]?>"
                    onchange='submitForm("Y");'
                    <?=($arResult["CHANGED_PRICE_".$arItem["ID"]]=="on") ? 'checked' : ''?>
                    />
                    <label for="cart_chk1"><?=GetMessage('ORDER_USED')?></label>
                </li>
                <?endif?>
            </ul>
        </fieldset>
    </div>
<?endforeach?>

<div class="cart__item-more" data-modal="#sale" class="modal_btn">
    <?=GetMessage('ORDER_SALE')?>
    <i class="fa fa-question-circle-o"></i>
</div>
<div class="cart__total">
    <span><?=GetMessage('ORDER_SUM')?></span>
    <span class="cart__total-price"><?=number_format($arResult["ORDER_PRICE"], 0, "", " ")?>&nbsp;<span class="ruble">руб.</span></span>
</div>