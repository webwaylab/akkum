<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="checkout__item" id="checkout5">
    <div class="checkout__item-header">
        <span class="num">5</span>
        <p class="h2"><?=GetMessage('ORDER_CONFIRM')?></p>
        <i class="fa fa-angle-down"></i>
    </div>
    <div class="checkout__item-content">
        <p><?=GetMessage('ORDER_USER_NAME')?>: <?=$arResult["USER"]["NAME"]?></p>
        <p><?=GetMessage('ORDER_MAIL')?>: <?=$arResult["USER"]["EMAIL"]?></p>
        <p><?=GetMessage('ORDER_PHONE')?>: <?=$arResult["USER"]["PERSONAL_PHONE"]?></p>
        <div class="notice"><?=GetMessage('ORDER_TEXT')?></div>
        <?if(!empty($arResult["ERROR_MESSAGE"])):?>
            <div class="error"><?=$arResult["ERROR_MESSAGE"]?></div>
        <?endif?>
        <input type="submit" name="" value="<?=GetMessage('ORDER_SEND')?>" class="full-width" onclick="yaCounter22871155.reachGoal('zakaz'); ga('send', 'event', 'knopka', 'zakaz'); $('input[name=create]').val('Y'); submitForm('Y'); return false;"/>
        <input type="hidden" name="create" value="" />
    </div>

</div>
