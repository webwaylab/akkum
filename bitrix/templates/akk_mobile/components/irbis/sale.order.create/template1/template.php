<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CJSCore::Init(array('fx', 'popup', 'window', 'ajax'));
?>
<a name="order_form"></a>
<script src="<?=$templateFolder."/ajax.js"?>"></script> 
    <div class="cart">
 
        <?
        if($_POST["is_ajax_post"] != "Y")
        {
        	?><form action="<?=$APPLICATION->GetCurPage();?>" method="POST" name="ORDER_FORM" id="ORDER_FORM" enctype="multipart/form-data" class="create_order">
        	<?=bitrix_sessid_post()?>
            <div id="order_form_content">
        	<?
        }
        else
        {    
        	$APPLICATION->RestartBuffer();
        }

        if(!empty($arResult["SUCCESS_MESSAGE"]))
        {
            ?>
            <h2><?echo $arResult["SUCCESS_MESSAGE"];?></h2>
            <p><?=GetMessage('ORDER_SUCCESS')?></p>
            <?
        }else{
            
            if(empty($arResult["BASKET_ITEMS"]))
            {
                echo "<?=GetMessage('ORDER_BASKET_EMPTY')?>";
            }else{

                include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/items.php");
                ?>
                <h2 class="h1"><?=GetMessage('ORDER_TITLE')?></h2>
                <div class="checkout">
                    <?
                    include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/auth.php");
                	include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/delivery.php");
                	include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/payment.php");
                    include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/step4.php");
                	include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/summary.php");
                    ?>
                </div>
                <?
            }
        }

        if($_POST["is_ajax_post"] != "Y")
        {
        	?>
                </div>
				<input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">
				<input type="hidden" name="json" value="Y">
				<?/*<div class="bx_ordercart_order_pay_center"><a href="javascript:void();" onclick="submitForm('Y'); return false;" id="ORDER_CONFIRM_BUTTON" class="checkout"><?=GetMessage("SOA_TEMPL_BUTTON")?></a></div>*/?>
			</form>
        	<?
        }
        else
        {
        	die();
        }
        ?>
    </div>  