<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="checkout__item" id="checkout3">
        <div class="checkout__item-header">
            <span class="num">3</span>
            <p class="h2"><?=GetMessage('ORDER_PAY_TITLE')?></p>
            <i class="fa fa-angle-down"></i>
        </div>
        <div class="checkout__item-content">
            <fieldset>
                <ul>
                    <?
                    foreach($arResult["PAYMENT_LIST"] as $arPayment):?>
                        <li>
                            <? //Если элемент первый, то добавляем в него заголовок ?>
                            <?if($count++ == 0):?>
                                <p class="catalog-filter__label"><?=GetMessage('ORDER_CHOOSE_PAY')?>:</p>
                            <?endif?>
                            <input type="radio" name="PAYMENT" value="<?=$arPayment["ID"]?>" id="<?=$arPayment["ID"]?>" <?=($arPayment["ID"]==$arResult["PAYMENT"]) ? 'checked' : ''?> />
                            <label for="<?=$arPayment["ID"]?>"><?=$arPayment["NAME"]?></label>
                        </li>
                    <?endforeach?>
                    <div class="d_none" <?=($arResult["PAYMENT"]==6) ? 'style="display:block"' : ''?>>
                        <li>
                            <p class="catalog-filter__label"><?=GetMessage('ORDER_NAME_COMPANY')?>:</p>
                            <input type="text" name="ANKETA[FULL_NAME]" value="<?=$arResult["ANKETA"]["FULL_NAME"]?>" />
                        </li>
                        <li>
                            <p class="catalog-filter__label"><?=GetMessage('ORDER_INN')?>:</p>
                            <input type="text" name="ANKETA[INN]" value="<?=$arResult["ANKETA"]["INN"]?>" />
                        </li>
                        <li>
                            <p class="catalog-filter__label"><?=GetMessage('ORDER_KPP')?>:</p>
                            <input type="text" name="ANKETA[KPP]" value="<?=$arResult["ANKETA"]["KPP"]?>" />
                        </li>
                        <li>
                            <p class="catalog-filter__label"><?=GetMessage('ORDER_CHECKING_NUMBER')?>:</p>
                            <input type="text" name="ANKETA[RS]" value="<?=$arResult["ANKETA"]["RS"]?>" />
                        </li>
                        <li>
                            <p class="catalog-filter__label"><?=GetMessage('ORDER_BIK')?>:</p>
                            <input type="text" name="ANKETA[BIK]" value="<?=$arResult["ANKETA"]["BIK"]?>" />
                        </li>
                        <li>
                            <p class="catalog-filter__label"><?=GetMessage('ORDER_NAME_BANK')?>:</p>
                            <input type="text" name="ANKETA[BANK_NAME]" value="<?=$arResult["ANKETA"]["BANK_NAME"]?>" />
                        </li>
                        <li>
                            <p class="catalog-filter__label"><?=GetMessage('ORDER_COMPANY_ADDRESS')?>:</p>
                            <textarea name="ANKETA[ADDRESS]"><?=$arResult["ANKETA"]["ADDRESS"]?></textarea>
                        </li>
                    </div>
                </ul>
            </fieldset>
            <a href="javascript:" class="btn full-width btn_next_step"><?=GetMessage('ORDER_NEXT')?></a>
        </div>
    </div>
