<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
CModule::IncludeModule("iblock");
global $APPLICATION, $USER, $arrFilter;

if(!$arResult["VARIABLES"]["SECTION_ID"]){
    CHTTP::SetStatus("404 Not Found");
    @define("ERROR_404","Y");
}

$arParams["ELEMENT_SORT_FIELD2"] = "ACTIVE";
$arParams["ELEMENT_SORT_ORDER2"] = "DESC";
$arParams["ELEMENT_SORT_FIELD"] = 'CATALOG_PRICE_1';
$arParams["ELEMENT_SORT_ORDER"] = 'asc';

//hide filter
$showFilter = false;
$arCurrentPath = explode('/', $arResult['VARIABLES']['SECTION_CODE_PATH']);
foreach ($arParams['FILTER_SHOW_SECTIONS'] as $sectionName) {
    if (in_array($sectionName, $arCurrentPath)) {
        $showFilter = true;
        break;
    }
}
$head_section = $sectionName;
if(is_array($arResult["SEOTAG"])){
	$head_section = "battery";
    $showFilter = true;
    $sectionName = "battery";
    //выведем фильтр чтобы отобразить элементы
    $APPLICATION->IncludeComponent(
        "bitrix:catalog.smart.filter",
        "nofilter",
        array(
            "IBLOCK_TYPE" => "catalog",
            "IBLOCK_ID" => "2",
            "SECTION_ID" => "18",
            "FILTER_NAME" => "",
            "PRICE_CODE" => "",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36000000",
            "CACHE_GROUPS" => "Y",
            "SAVE_IN_SESSION" => "N",
            "FILTER_VIEW_MODE" => "VERTICAL",
            "XML_EXPORT" => "Y",
            "SECTION_TITLE" => "NAME",
            "SECTION_DESCRIPTION" => "DESCRIPTION",
            'HIDE_NOT_AVAILABLE' => "N",
            "TEMPLATE_THEME" => "green"
        ),
        $component,
        array('HIDE_ICONS' => 'Y')
    );
}

if (isset($arParams['USE_COMMON_SETTINGS_BASKET_POPUP']) && $arParams['USE_COMMON_SETTINGS_BASKET_POPUP'] == 'Y')
{
    $basketAction = (isset($arParams['COMMON_ADD_TO_BASKET_ACTION']) ? $arParams['COMMON_ADD_TO_BASKET_ACTION'] : '');
}
else
{
    $basketAction = (isset($arParams['SECTION_ADD_TO_BASKET_ACTION']) ? $arParams['SECTION_ADD_TO_BASKET_ACTION'] : '');
}
$intSectionID = 0;
if($arResult['NEED_BACK_LINK']) {
	$this->SetViewTarget('link_back'); ?>
    <noindex><a rel="nofollow" class="lnk-back" href="<?=$arResult['UPPER_SECTION_PATH']?>"><i class="fa fa-angle-left"></i><?=$arResult['UPPER_SECTION_NAME']?></a></noindex>
	<? $this->EndViewTarget();
}
?>
<div class="catalog-section__descr">
    <div class = "TOP_TEXT"></div>
</div>

<noindex>
    <div class="catalog__sort">
        <div class="catalog__sort__label"><?= GetMessage('SORT_TO') ?></div>
        <?
        if(strpos($APPLICATION->GetCurDir(), "/catalog/battery/")===false){
	        $arSort = array(
				"catalog_PRICE_1" => "Цене",
	        );
        }else{
	        $arSort = array(
				"catalog_PRICE_1" => "Цене",
				"PROPERTY_CAPACITY" => "Емкости",
				"PROPERTY_POLARITY" => "Полярности",
	        );
        }
        ?>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
        <fieldset>
            <ul>
                <li id="select">
                    <select name="catalog-sort" class="js">
                        <? foreach($arSort as $sortKey => $sortTitle):
                            if(isset($_GET["sortFields"]) && $_GET["sortFields"] == $sortKey)
                            {
                                $sortOrder = "asc";
                                if($sortKey=="PROPERTY_POLARITY"){
	                                $arrow = "(сначала Обратная)";
                                }else{
	                                $arrow = "&#x2191";
                                }
								$active = "";
                                if($_GET["sortOrder"] == "asc")
                                {
									$active = "selected";
                                }
                            }
                            else
                            {
                                $sortOrder = "asc";
                                if($sortKey=="PROPERTY_POLARITY"){
	                                $arrow = "(сначала Обратная)";
                                }else{
	                                $arrow = "&#x2191";
                                }
								$active = "";
                                if($sortKey == "catalog_PRICE_1")
                                {
                                    $active = "selected";
                                }
                            }
                            $url = $APPLICATION->GetCurPageParam("sortFields=".$sortKey."&sortOrder=".$sortOrder, array("sortFields", "sortOrder")); ?>

                            <option value="<?=$url?>" <?=$active?>><?=$sortTitle?> <?=$arrow?></option>
                            <?
							if(isset($_GET["sortFields"]) && $_GET["sortFields"] == $sortKey)
                            {
                                $sortOrder = "desc";
                                if($sortKey=="PROPERTY_POLARITY"){
	                                $arrow = "(сначала Прямая)";
                                }else{
	                                $arrow = "&#x2193"; //Стрелочки
                                }
								$active = "";
                                if($_GET["sortOrder"] == "desc")
                                {
									$active = "selected";
                                }
                            }
                            else
                            {
                                $active = "";
                                $sortOrder = "desc";
                                if($sortKey=="PROPERTY_POLARITY"){
	                                $arrow = "(сначала Прямая)";
                                }else{
	                                $arrow = "&#x2193"; //Стрелочки
                                }
                            }
                            $url = $APPLICATION->GetCurPageParam("sortFields=".$sortKey."&sortOrder=".$sortOrder, array("sortFields", "sortOrder")); ?>

                            <option value="<?=$url?>" <?=$active?>><?=$sortTitle?> <?=$arrow?></option>

                        <? endforeach;
                        if(isset($_GET["sortFields"]))
                        {
                            $arParams["ELEMENT_SORT_FIELD"] = $_GET["sortFields"];
                            $arParams["ELEMENT_SORT_ORDER"] = $_GET["sortOrder"];
                        }
                        else
                        {
                            $arParams["ELEMENT_SORT_FIELD"] = "catalog_PRICE_1";
                            $arParams["ELEMENT_SORT_ORDER"] = 'asc';
                        } ?>
                    </select>
                    <script>
                        $("#select .js").change(function () {
                            window.location.assign($(this).val());
                        });
                        $("#select .js").select2({
                                minimumResultsForSearch: -1
                            });
                    </script>
                </li>

                <? if(isset($_GET["SALELEADER"]) && !empty($_GET["SALELEADER"]))
                {
                    $arrFilter["=PROPERTY_SALELEADER"] = 90;
                    $urlHit = $APPLICATION->GetCurPageParam("", array("SALELEADER", "SPECIALOFFER"));
                }
                else
                {
                    $urlHit = $APPLICATION->GetCurPageParam("SALELEADER=да", array("SALELEADER", "SPECIALOFFER"));
                }

                if(isset($_GET["SPECIALOFFER"]) && !empty($_GET["SPECIALOFFER"]))
                {
                    $arrFilter["=PROPERTY_SPECIALOFFER"] = 91;
                    $urlSale = $APPLICATION->GetCurPageParam("", array("SPECIALOFFER", "SALELEADER"));
                }
                else
                {
                    $urlSale = $APPLICATION->GetCurPageParam("SPECIALOFFER=да", array("SPECIALOFFER", "SALELEADER"));
                } ?>
				<?
        		if(!empty($arResult['VARIABLES']['SECTION_ID']) && intval($arResult['VARIABLES']['SECTION_CODE'])){
        			if($arResult['VARIABLES']['SECTION_ID'] == 413 || $arResult['VARIABLES']['SECTION_ID'] == 419){
	        			$arResult['VARIABLES']['SECTION_ID'] = 367;
        			}else{
        				$arResult['VARIABLES']['SECTION_ID'] = 453;
        			}
					$capasity = explode("-", $arResult['VARIABLES']['SECTION_CODE']);
					$arrFilter[">=PROPERTY_CAPACITY"] = $capasity[0];
					$arrFilter["<=PROPERTY_CAPACITY"] = $capasity[1];
        		}
				?>

                <li>
                    <input type="checkbox" name="catalog-sort" onchange="window.location.assign('<?=$urlHit?>')" id="catalog-sort-hits" <?= !empty($_GET["SALELEADER"]) ? 'checked' : ''?>/><label for="catalog-sort-hits"><?= GetMessage('SHOP_HIT') ?></label>
                </li>
                <li>
                    <input type="checkbox" name="catalog-sort" onchange="window.location.assign('<?=$urlSale?>')" id="catalog-sort-stock" <?= !empty($_GET["SPECIALOFFER"]) ? 'checked' : ''?>/><label for="catalog-sort-stock"><?= GetMessage('SHOP_STOCK') ?></label>
                </li>
            </ul>
        </fieldset>
    </div>
</noindex>

			<?
			if(isset($_GET["sortFields"]))
			{
				$arParams["ELEMENT_SORT_FIELD"] = $_GET["sortFields"];
				$arParams["ELEMENT_SORT_ORDER"] = $_GET["sortOrder"];

					if(ToUpper($_GET["sortFields"]) != 'CATALOG_PRICE_1'){
						$arParams["ELEMENT_SORT_FIELD2"] = 'CATALOG_PRICE_1';
						$arParams["ELEMENT_SORT_ORDER2"] = 'asc';
					}

				$arParams["HIDE_NOT_AVAILABLE"] = 'Y';
			}
			?>

<div class="catalog__items">
    <?$intSectionID = $APPLICATION->IncludeComponent(
        "pixelplus:catalog.section",
        "index_mobile",
        array(
            "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            'HEAD_SECTION'=> $head_section,
            "ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
            "ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
            "ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
            "ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
            "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
            "META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
            "META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
            "BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
            "INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
            "BASKET_URL" => $arParams["BASKET_URL"],
            "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
            "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
            "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
            "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
            "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
            "FILTER_NAME" => $arParams["FILTER_NAME"],
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => $arParams["CACHE_TIME"],
            "CACHE_FILTER" => $arParams["CACHE_FILTER"],
            "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
            "SET_TITLE" => $arParams["SET_TITLE"],
            "SET_STATUS_404" => $arParams["SET_STATUS_404"],
            "DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
            "PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
            "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
            "PRICE_CODE" => $arParams["PRICE_CODE"],
            "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
            "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

            "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
            "USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
            "ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
            "PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
            "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

            "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
            "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
            "PAGER_TITLE" => $arParams["PAGER_TITLE"],
            "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
            "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
            "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
            "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
            "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],

            "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
            "OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
            "OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
            "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
            "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
            "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
            "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
            "OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

            "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
            "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
            "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
            "DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
            'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
            'CURRENCY_ID' => $arParams['CURRENCY_ID'],
            'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],

            'LABEL_PROP' => $arParams['LABEL_PROP'],
            'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
            'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],

            'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
            'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
            'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
            'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
            'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
            'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
            'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
            'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
            'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
            'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],

            'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
            "ADD_SECTIONS_CHAIN" => "N",
            'ADD_TO_BASKET_ACTION' => $basketAction,
            'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
            'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
            'SHOW_DEACTIVATED' => 'Y'
        ),
        $component
    );

    $GLOBALS['CATALOG_CURRENT_SECTION_ID'] = $intSectionID;
    unset($basketAction); ?>
</div>
<?
//сео теги

if(is_array($arResult["SEOTAG"])){
	$ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues(17, $arResult["SEOTAG"]["ID"]);

	$IPROPERTY = $ipropValues->getValues();

	$APPLICATION->SetPageProperty("title", $IPROPERTY["ELEMENT_META_TITLE"]);
	$APPLICATION->SetPageProperty("keywords", $IPROPERTY["ELEMENT_META_KEYWORDS"]);
	$APPLICATION->SetPageProperty("description", $IPROPERTY["ELEMENT_META_DESCRIPTION"]);
	$APPLICATION->SetTitle($IPROPERTY["ELEMENT_PAGE_TITLE"]);
}
?>
