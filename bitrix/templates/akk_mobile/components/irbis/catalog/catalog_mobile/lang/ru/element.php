<?php
$MESS["PHONE_SEND_FORM"] = "Заказать звонок";
$MESS["FEATURES"] = "Характеристики";
$MESS["DESCRIPTION"] = "Описание";
$MESS["REVIEWS"] = "Отзывы";
$MESS["SIMILAR_PRODUCTS"] = "Похожие товары";
$MESS["SIMILAR_PRODUCTS_VIEW"] = "Вместе с этим товаром смотрят";
$MESS["BACK_LINK"] = "Назад";