<? if ( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true )die();
if ( (substr_count($arResult['VARIABLES']['SECTION_CODE_PATH'], '/')) > 3 ) {
	$sectionPath = substr($arResult['VARIABLES']['SECTION_CODE_PATH'], 0, strpos($arResult['VARIABLES']['SECTION_CODE_PATH'],
		$arResult['VARIABLES']['SECTION_CODE']));
	$section = explode('/', $sectionPath);
	$section = $section[sizeof($section) - 2];
	$res = CIBlockSection::GetList(
		array(), Array('IBLOCK_ID' => $arParams['IBLOCK_ID'],
		               'CODE'      => $section,
	), false,
		array('IBLOCK_ID',
			'ID',
			'NAME',
		));
	$section = ($res->getNext());
	$arResult['UPPER_SECTION_NAME'] = $section['NAME'];
	$arResult['UPPER_SECTION_PATH'] = $sectionPath;
	$arResult['NEED_BACK_LINK'] = true;
} else {
	$arResult['NEED_BACK_LINK'] = false;
}
?>