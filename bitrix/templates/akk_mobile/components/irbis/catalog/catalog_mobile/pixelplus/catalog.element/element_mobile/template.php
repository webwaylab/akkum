<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//$APPLICATION->SetPageProperty("product_detail", "filter_hide");
$urt_temp = array(
    '/catalog/accessories/areometry/areometr_universalnyy_steklyannyy_orion_ar_02/',
    '/catalog/accessories/areometry/areometr_victor_v644/',
    '/catalog/accessories/chargers/zaryadnoe_ustroystvo_orion_pw150_6a_12v/',
    '/catalog/accessories/chargers/zaryadnoe_ustroystvo_kulon_100/',
    '/catalog/accessories/chargers/zaryadnoe_ustroystvo_zu_75a_tambov/',
    '/catalog/accessories/chargers/zaryadnoe_ustroystvo_zu_75a1_tambov/',
    '/catalog/accessories/chargers/zaryadnoe_ustroystvo_orion_pw265_6a_12v/',
    '/catalog/accessories/chargers/zaryadnoe_ustroystvo_kulon_707a_7a_strelochnyy_indikator/',
    '/catalog/accessories/chargers/zaryadnoe_ustroystvo_orion_pw270_6a_12v/',
    '/catalog/accessories/chargers/zaryadnoe_ustroystvo_zu_120_tambov/',
    '/catalog/accessories/chargers/zaryadnoe_ustroystvo_zu_120m_tambov/',
    '/catalog/accessories/chargers/zaryadnoe_ustroystvo_zu_120m_3_tambov/',
    '/catalog/accessories/chargers/zaryadnoe_ustroystvo_12v_24v_230vt_380vt_16a_90_180ach_220v_bolk_master_cb_16a/',
    '/catalog/accessories/chargers/zaryadnoe_ustroystvo_t_1021/',
    '/catalog/accessories/chargers/zaryadnoe_ustroystvo_orion_pw325_15a_12v/',
    '/catalog/accessories/chargers/zaryadnoe_ustroystvo_12v_24v_430vt_720vt_25a_140_300ach_220v_bolk_master_cb_25a/',
    '/catalog/accessories/chargers/zaryadnoe_ustroystvo_kulon_715d_15a_zh_k_indikator/',
    '/catalog/accessories/chargers/zaryadnoe_ustroystvo_12v_24v_510vt_850vt_30a_170_350ach_220v_bolk_master_cb_30a/',
    '/catalog/accessories/chargers/zaryadnoe_ustroystvo_t_1001ar_avtomat_reversivnyy_tok/',
    '/catalog/accessories/chargers/zaryadno_puskovoe_ustroystvo_sonar_209/',
    '/catalog/accessories/chargers/zaryadno_puskovoe_ustroystvo_zpu_start_1/',
    '/catalog/accessories/chargers/zaryadnoe_ustroystvo_12v_15a_220v_defort_dbc_15/',
    '/catalog/accessories/chargers/stantsiya_energeticheskaya_c_kompressorom_i_fonarem_1100a_12v_i_220v_bolk_ps1100a_c/',
    '/catalog/accessories/chargers/zaryadno_puskovoe_ustroystvo_t_1012a_avtomat_reversivnyy_tok/',
    '/catalog/accessories/chargers/zaryadno_puskovoe_ustroystvo_zpu_12_24/',
    '/catalog/accessories/avtokhimiya/zhir_dlya_klemm_akkumulyatora_0_05kg_lm_3140_7643/',
    '/catalog/accessories/avtokhimiya/ochistitel_klemm_akkumulyatora_aerozol_141ml_gunk_btc6/',
    '/catalog/accessories/elektrolit/dreko_elektrolit_kislotnyy_1l/',
    '/catalog/accessories/krepezhi_i_kozhukhi/poddon_akkumulyatornoy_batarei/',
    '/catalog/accessories/krepezhi_i_kozhukhi/krepezh_dlya_akkumulyatora_s_plankoy_komplekt_3_sht_/',
    '/catalog/accessories/krepezhi_i_kozhukhi/termozashchita_dlya_akkumulyatora_190kh250kh180mm_neylonovaya_chernaya/',
    '/catalog/accessories/nabor_avtomobilista/nabor_avtomobilista_provoda_prikurivaniya_zhilet_avariynyy_tros_perchatki_sumka_kachok_a210/',
    '/catalog/accessories/nezamerzaika/nezamerzayushchaya_zhidkost_5l_30/',
);

$this->setFrameMode(true);
$templateLibrary = array('popup');
$currencyList = '';
if (!empty($arResult['CURRENCIES']))
{
	$templateLibrary[] = 'currency';
	$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}
$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
	'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME'],
	'TEMPLATE_LIBRARY' => $templateLibrary,
	'CURRENCIES' => $currencyList
);
unset($currencyList, $templateLibrary);
$strMainID = $this->GetEditAreaId($arResult['ID']);
$arItemIDs = array(
	'ID' => $strMainID,
	'PICT' => $strMainID.'_pict',
	'DISCOUNT_PICT_ID' => $strMainID.'_dsc_pict',
	'STICKER_ID' => $strMainID.'_sticker',
	'BIG_SLIDER_ID' => $strMainID.'_big_slider',
	'BIG_IMG_CONT_ID' => $strMainID.'_bigimg_cont',
	'SLIDER_CONT_ID' => $strMainID.'_slider_cont',
	'SLIDER_LIST' => $strMainID.'_slider_list',
	'SLIDER_LEFT' => $strMainID.'_slider_left',
	'SLIDER_RIGHT' => $strMainID.'_slider_right',
	'OLD_PRICE' => $strMainID.'_old_price',
	'PRICE' => $strMainID.'_price',
	'DISCOUNT_PRICE' => $strMainID.'_price_discount',
	'SLIDER_CONT_OF_ID' => $strMainID.'_slider_cont_',
	'SLIDER_LIST_OF_ID' => $strMainID.'_slider_list_',
	'SLIDER_LEFT_OF_ID' => $strMainID.'_slider_left_',
	'SLIDER_RIGHT_OF_ID' => $strMainID.'_slider_right_',
	'QUANTITY' => $strMainID.'_quantity',
	'QUANTITY_DOWN' => $strMainID.'_quant_down',
	'QUANTITY_UP' => $strMainID.'_quant_up',
	'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
	'QUANTITY_LIMIT' => $strMainID.'_quant_limit',
	'BASIS_PRICE' => $strMainID.'_basis_price',
	'BUY_LINK' => $strMainID.'_buy_link',
	'ADD_BASKET_LINK' => $strMainID.'_add_basket_link',
	'BASKET_ACTIONS' => $strMainID.'_basket_actions',
	'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
	'COMPARE_LINK' => $strMainID.'_compare_link',
	'PROP' => $strMainID.'_prop_',
	'PROP_DIV' => $strMainID.'_skudiv',
	'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
	'OFFER_GROUP' => $strMainID.'_set_group_',
	'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
);
$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
$templateData['JS_OBJ'] = $strObName;

$strTitle = (
	isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"] != ''
	? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]
	: $arResult['NAME']
);
$strAlt = (
	isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"] != ''
	? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]
	: $arResult['NAME']
);

reset($arResult['MORE_PHOTO']);
$arFirstPhoto = current($arResult['MORE_PHOTO']); ?>

<div id = <?=$strMainID?>>
<?if ($arParams['DISPLAY_COMPARE']):?>
    <a href="javascript:void(0);" class="lnk-compare" id="<? echo $arItemIDs['COMPARE_LINK']; ?>"><?= GetMessage('COMPARE') ?></a>
<?endif;?>

<div class="catalog__icons catalog__icons__item">
    <?if($arResult["PROPERTIES"]["SALELEADER"]["VALUE"]=="да"):?>
        <i><img src="<?= SITE_TEMPLATE_PATH . '/images/icon_good.png' ?>"
                alt="<?=$arResult["PROPERTIES"]["SALELEADER"]["NAME"];?>" />
                <div class="hidden recommend">
                    <span class="exchange-popup__hiden"></span>
                    <p>Рекомендуется магазином</p>
                </div>
        </i>
    <?endif;?>
    <?if($arResult["PROPERTIES"]["SPECIALOFFER"]["VALUE"]=="да"):?>
        <i><img src="<?= SITE_TEMPLATE_PATH . '/images/icon_percent.png' ?>"
                alt="<?=$arResult["PROPERTIES"]["SPECIALOFFER"]["NAME"];?>" />
                <div class="hidden sale">
                    <span class="exchange-popup__hiden"></span>
                    <p>Сделаем скидку</p>
                </div>
        </i>
    <?endif;?>
    <?if($arResult["PROPERTIES"]["FREESHIPPING"]["VALUE"]=="да"):?>
        <i><img src="<?= SITE_TEMPLATE_PATH . '/images/icon_track.png' ?>"
                alt="<?=$arResult["PROPERTIES"]["FREESHIPPING"]["NAME"];?>" />
                <div class="hidden delivery">
                    <span class="exchange-popup__hiden"></span>
                    <p>Бесплатная доставка</p>
                </div>
        </i>
    <?endif;?>
</div>

<div class="gallery">
    <div class="js-slider" data-slick='{"arrows": false, "dots": true}'>
        <? if (!empty($arResult['DETAIL_PICTURE'])): ?>
            <div class="gallery__item">
                <figure>
                    <img src="<?= $arResult['DETAIL_PICTURE']['SRC']; ?>" alt="<?= $strAlt; ?>" id="<?=$arItemIDs['PICT']; ?>"/>
                </figure>
            </div>
        <? endif; ?>
        <? foreach ($arResult['MORE_PHOTO'] as &$arOnePhoto): ?>
            <div class="gallery__item">
                <figure>
                    <img src="<?= $arOnePhoto['SRC']; ?>" alt="" />
                </figure>
            </div>
        <? endforeach;
        if(count($arResult["MORE_PHOTO_AQUAKOSH"]) > 0):
            foreach($arResult["MORE_PHOTO_AQUAKOSH"] as $arPHOTO):
                if (is_array($arPHOTO)): ?>
                    <div class="gallery__item">
                        <figure>
                            <img src="<?= $arPHOTO["THUMB"]["SRC"]; ?>" alt="" />
                        </figure>
                    </div>
                <? endif;
            endforeach;
        endif;?>
    </div>
</div>

<div class="catalog_detail__prices">
    <div id="<?= $arItemIDs['PRICE']; ?>" class="price_old"><?= number_format($arResult['MIN_PRICE']['DISCOUNT_VALUE_VAT'], 0, "", " ")?>&nbsp;<span class="ruble">руб.</span></div>
    <?if ($arParams["ROOT_ITEM"] == "battery"):?>
        <div class="price_low">
            <span class="product_price"><?=number_format($arResult["PROPERTIES"]["PRICE_CHANGE"]["VALUE"], 0, "", " ")?>&nbsp;<span class="ruble">руб.</span></span>
            <span class="help" id="help"><?= GetMessage('EXCHANGE') ?></span>
        </div>
	    <div class="exchange-popup" id="exchange-popup">
	        <span class="exchange-popup__hide" id="exchange-popup__hide"></span>
	        <p><?= GetMessage('PRICE_EXCHANGE') ?></p>
	    </div>
    <?elseif($arResult["PROPERTIES"]["PRICE_CHANGE"]["VALUE"]):?>
        <div class="price_low">
            <span class="product_price"><?=number_format($arResult["PROPERTIES"]["PRICE_CHANGE"]["VALUE"], 0, "", " ")?>&nbsp;<span class="ruble">руб.</span></span>
            <span class="help" id="help"><?= GetMessage('EXCHANGE2') ?></span>
        </div>
	    <div class="exchange-popup" id="exchange-popup" style="top: -125px;">
	        <span class="exchange-popup__hide" id="exchange-popup__hide"></span>
	        <p><?= GetMessage('PRICE_EXCHANGE2') ?></p>
	    </div>
    <?endif;?>
</div> <!-- /catalog_detail__prices -->

<div class="catalog_detail__btns">
    <?php
    $canBuy = $arResult['CAN_BUY'];
    $buyBtnMessage = ($arParams['MESS_BTN_BUY'] != '' ? $arParams['MESS_BTN_BUY'] : GetMessage('CT_BCE_CATALOG_BUY'));
    $addToBasketBtnMessage = ($arParams['MESS_BTN_ADD_TO_BASKET'] != '' ? $arParams['MESS_BTN_ADD_TO_BASKET'] : GetMessage('CT_BCE_CATALOG_ADD'));
    $notAvailableMessage = ($arParams['MESS_NOT_AVAILABLE'] != '' ? $arParams['MESS_NOT_AVAILABLE'] : GetMessageJS('CT_BCE_CATALOG_NOT_AVAILABLE'));
    $showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
    $showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);

    $showSubscribeBtn = false;
    $compareBtnMessage = ($arParams['MESS_BTN_COMPARE'] != '' ? $arParams['MESS_BTN_COMPARE'] : GetMessage('CT_BCE_CATALOG_COMPARE'));
    ?>
    <?php if ($arParams['USE_PRODUCT_QUANTITY'] == 'Y' && $canBuy) { ?>
        <fieldset>
            <ul>
                <li>
                    <?php /*
                    <label class="catalog-filter__label"><?= GetMessage('COUNT_ITEMS') ?></label>
                    <select class="js-select chosen-wide" id="<? echo $arItemIDs['QUANTITY']; ?>">
                        <? for($i=1; $i<20; $i++): ?>
                            <option value="<?=$i?>"><?=$i?></option>
                        <? endfor; ?>
                    </select>
                    */ ?>

                    <div class="catalog-filter__label"><?= GetMessage('COUNT_ITEMS') ?></div>
                    <div class="qtty">
                        <div class="qtty__field">
                            <input type="number" min="1" max="<?= ($arResult['CATALOG_QUANTITY'] > 0) ? $arResult['CATALOG_QUANTITY'] : '999'; ?>" value="1" id="<?= $arItemIDs['QUANTITY']; ?>">
                        </div>
                        <div class="qtty__spin minus" id="<?= $arItemIDs['QUANTITY_DOWN']; ?>"></div>
                        <div class="qtty__spin plus" id="<?= $arItemIDs['QUANTITY_UP']; ?>"></div>
                    </div>
                </li>
            </ul>
        </fieldset>
    <?php } ?>

    <?if($canBuy): ?>
        <?php if ($canBuy && $arResult['CATALOG_QUANTITY'] > 0) { ?>
            <span class="btn full-width iconed ico_buy buy_product" id="<?=$arItemIDs['BUY_LINK']; ?>">
                <span class="buy_product--cart">
                <?= GetMessage('CT_BCE_CATALOG_BUY') ?>
                </span>
            </span>
            <span class="item_buttons_counter_block" id="<?=$arItemIDs['BASKET_ACTIONS']; ?>" style="display: none;"></span>

            <span class="catalog__quickbuy btn btn_o buy_click_product" data-flying-hint="#form-oneclick" data-class="active" id="one_item_one_click">
                <?= GetMessage('CT_BCE_CATALOG_BUY_CLICK') ?>
            </span>

            <div>
                <div class="flying-hint form-oneclick" id="form-oneclick">
                    <div class="container">
                        <div class="flying-hint__wrap">
                            <i class="flying-hint__hide js-flying-hint-hide"></i>
                            <div class="flying-hint__body">
                                <h3>Введите номер телефона:</h3>
                                <form class="phone_order_form_ one-click">
                                    <input type="hidden" name="PRODUCT_ID" value="<?=$arItem["ID"]?>"/>
                                    <div class="error"></div>
                                    <fieldset><ul>
                                            <li>
                                                <input type="text" name="PERSONAL_PHONE" value="" data-masked="+7(999)999-99-99" placeholder="+7(___)___-__-__" />
                                            </li>
                                        </ul></fieldset>
                                    <input type="submit" name="" value="Купить" />
                                </form>
                            </div>
                        </div>
                        <div class="flying-hint form-oneclick flying-hint__wrap order_success_message">
                            <i class="flying-hint__hide js-flying-hint-hide"></i>
                            <div class="flying-hint__body">
                                <p><img src="<?=SITE_TEMPLATE_PATH?>/images/done_form_icon_green.png" height="33" width="36" alt=""></p>
                                <h4 class="bold">Ваш заказ принят.</h4>
                                Ожидайте звонка. Спасибо!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } else { ?>
            <button class="catalog__orderinadvance btn btn_o buy_click_product" data-flying-hint="#form-orderinadvance" data-class="active">Под заказ</button>

            <div>
                <div class="flying-hint form-orderinadvance" id="form-orderinadvance">
                    <div class="container">
                        <div class="flying-hint__wrap">
                            <i class="flying-hint__hide js-flying-hint-hide"></i>
                            <div class="flying-hint__body">
                                <form class="phone_order_form_ order-in-advance">
                                    <div class="error"></div>
                                    <fieldset><ul>
                                            <li>
                                                <p><?= $arResult['NAME']; ?> (<span class="product_quantity"></span> шт.)</p>
                                                <input type="hidden" name="product_name" value="<?= $arResult['NAME']; ?>">
                                                <input type="hidden" name="product_quantity" value="">
                                            </li>
                                            <li>
                                                <input type="text" name="name" value="" placeholder="Ваше имя">
                                            </li>
                                            <li>
                                                <input type="text" name="phone" value="" data-masked="+7(999)999-99-99" placeholder="Ваш телефон" />
                                            </li>
                                        </ul></fieldset>
                                    <input type="submit" name="" value="Заказать" />
                                </form>
                            </div>
                        </div>
                        <div class="flying-hint form-orderinadvance flying-hint__wrap order_success_message">
                            <i class="flying-hint__hide js-flying-hint-hide"></i>
                            <div class="flying-hint__body">
                                <p><img src="<?=SITE_TEMPLATE_PATH?>/images/done_form_icon_green.png" height="33" width="36" alt=""></p>
                                <h4 class="bold">Ваш заказ принят.</h4>
                                Ожидайте звонка. Спасибо!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    <? else: ?>
        <div><?= GetMessage('NOT_AVAILABLE') ?></div>
    <? endif; ?>
</div>

<?$this->SetViewTarget('element_property');?>
    <? if (!empty($arResult['DISPLAY_PROPERTIES'])): ?>

        <?if($arParams["ROOT_ITEM"] == "battery"):?>
            <ul class="catalog_detail__params">
                <? foreach ($arResult['DISPLAY_PROPERTIES'] as &$arOneProp): ?>
                    <?if(in_array($arOneProp['CODE'], array("ARTNUMBER", "TYPE", "CAPACITY", "POLARITY", "START_TOK", "GARANTY", "COUNTRY_MANUFACTURE", "BRAND", "MODEL", "MODEL_ROW")) && $arOneProp['DISPLAY_VALUE']):?>
                        <li class="catalog_detail__param prop_<?= strtolower($arOneProp['CODE']); ?>">
                            <span class="catalog_detail__param_label"><?= $arOneProp['NAME']; ?></span>
                            <span class="catalog_detail__param_value"><?= (is_array($arOneProp['DISPLAY_VALUE'])
                                ? implode(' / ', $arOneProp['DISPLAY_VALUE'])
                                : $arOneProp['DISPLAY_VALUE']); ?></span>
                        </li>
                    <?endif;?>
                <? endforeach;
                unset($arOneProp); ?>

                <li class="catalog_detail__param prop_dimension">
                    <span class="catalog_detail__param_label"><?= GetMessage('DIMENSIONS') ?></span>
                    <span class="catalog_detail__param_value"><?=$arResult['PROPERTIES']["LENGTH"]['VALUE']?>×<?=$arResult['PROPERTIES']["WIDTH"]['VALUE']?>×<?=$arResult['PROPERTIES']["HEIGHT"]['VALUE']?></span>
                </li>
            </ul>
        <?elseif($arParams["ROOT_ITEM"] == "masla"):?>
            <ul class="catalog_detail__params">
                <? foreach ($arResult['DISPLAY_PROPERTIES'] as &$arOneProp): ?>
                    <?if(in_array($arOneProp['CODE'], array("ARTNUMBER", "OBEM_UPAKOVKI_L_MASLO", "KLASS_VYAZKOSTI_SAE_MASLO", "TIP_MASLO", "PROIZVODITEL_MASLO")) && $arOneProp['DISPLAY_VALUE']):?>
                        <? $value = (is_array($arOneProp['DISPLAY_VALUE']) ? implode(' / ', $arOneProp['DISPLAY_VALUE']) : $arOneProp['DISPLAY_VALUE']) ?>
                        <li class="catalog_detail__param prop_<?= strtolower($arOneProp['CODE']); ?>">
                            <?if($arOneProp['CODE']=="OBEM_UPAKOVKI_L_MASLO"):?>
                                <?= GetMessage('VOLUME') ?><?= $value ?>
                                <?continue;?>
                            <?endif;?>
                            <?if($arOneProp['CODE']=="KLASS_VYAZKOSTI_SAE_MASLO"):?>
                                <?= GetMessage('VISCOSITY') ?><?= $value ?>
                                <?continue;?>
                            <?endif;?>
                            <?if($arOneProp['CODE']=="TIP_MASLO"):?>
                                <?= GetMessage('TYPE_VALUE') ?><?= $value ?>
                                <?continue;?>
                            <?endif;?>
                            <?if($arOneProp['CODE']=="PROIZVODITEL_MASLO"):?>
                                <?= GetMessage('MANUFACTURER') ?><?= $value ?>
                                <?continue;?>
                            <?endif;?>

                            <span class="catalog_detail__param_label"><?= $arOneProp['NAME']; ?></span>
                            <span class="catalog_detail__param_value"><?= $value ?></span>
                        </li>
                    <?endif;?>
                <? endforeach;
                unset($arOneProp); ?>
            </ul>
        <?elseif($arParams["ROOT_ITEM"] == "accessories"):?>
            <ul class="catalog_detail__params">
                <? foreach ($arResult['DISPLAY_PROPERTIES'] as &$arOneProp): ?>
                    <?if(in_array($arOneProp['CODE'], array("ARTNUMBER", "MANUFACTURE_2", "GRUZOPODEMNOST_DOMKRATY", "VYSOTA_PODKHVATA_DOMKRATY", "VYSOTA_PODEMA_DOMKRATY")) && $arOneProp['DISPLAY_VALUE']):?>
                        <li class="catalog_detail__param prop_<?= strtolower($arOneProp['CODE']); ?>">
                        	<?
							$pos = mb_strpos($arOneProp["NAME"], "(Домкраты)");
							if($pos > 0){
								$arOneProp["NAME"] = mb_substr($arOneProp["NAME"], 0, $pos);
							}
							?>
                            <span class="catalog_detail__param_label"><?= $arOneProp['NAME']; ?></span>
                            <span class="catalog_detail__param_value"><?= (is_array($arOneProp['DISPLAY_VALUE'])
                                ? implode(' / ', $arOneProp['DISPLAY_VALUE'])
                                    : $arOneProp['DISPLAY_VALUE']); ?></span>
                        </li>
                    <?endif;?>
                <? endforeach;
                unset($arOneProp); ?>
            </ul>
        <?endif;?>
    <? endif; ?>
<?$this->EndViewTarget();?>

<?$this->SetViewTarget('element_description');?>
    <?= strip_tags($arResult["~DETAIL_TEXT"]) ?>
<?$this->EndViewTarget();?>

<? $emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
if ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET'] && !$emptyProductProperties): ?>
    <div id="<? echo $arItemIDs['BASKET_PROP_DIV']; ?>" style="display: none;">
        <? if (!empty($arResult['PRODUCT_PROPERTIES_FILL'])):
			foreach ($arResult['PRODUCT_PROPERTIES_FILL'] as $propID => $propInfo): ?>
	            <input type="hidden" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo htmlspecialcharsbx($propInfo['ID']); ?>">
                <? if (isset($arResult['PRODUCT_PROPERTIES'][$propID]))
                unset($arResult['PRODUCT_PROPERTIES'][$propID]);
			endforeach;
		endif;
		$emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
		if (!$emptyProductProperties): ?>
	        <table>
                <? foreach ($arResult['PRODUCT_PROPERTIES'] as $propID => $propInfo): ?>
	                <tr>
                        <td><? echo $arResult['PROPERTIES'][$propID]['NAME']; ?></td>
	                    <td>
                            <? if('L' == $arResult['PROPERTIES'][$propID]['PROPERTY_TYPE'] && 'C' == $arResult['PROPERTIES'][$propID]['LIST_TYPE']):
                                    foreach($propInfo['VALUES'] as $valueID => $value): ?>
                                        <label>
                                            <input type="radio" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo $valueID; ?>" <? echo ($valueID == $propInfo['SELECTED'] ? '"checked"' : ''); ?>><? echo $value; ?>
                                        </label>
                                    <? endforeach;
                                else: ?>
                                    <select name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]"><?
                                    foreach($propInfo['VALUES'] as $valueID => $value): ?>
                                        <option value="<? echo $valueID; ?>" <? echo ($valueID == $propInfo['SELECTED'] ? '"selected"' : ''); ?>><? echo $value; ?></option>
                                    <? endforeach; ?>
                                    </select>
                            <? endif; ?>
	                    </td>
                    </tr>
                <? endforeach; ?>
	        </table>
        <? endif; ?>
    </div>
<? endif;

if ($arResult['MIN_PRICE']['DISCOUNT_VALUE'] != $arResult['MIN_PRICE']['VALUE'])
{
    $arResult['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arResult['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'];
    $arResult['MIN_BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arResult['MIN_BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'];
}
	$arJSParams = array(
		'CONFIG' => array(
			'USE_CATALOG' => $arResult['CATALOG'],
			'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
			'SHOW_PRICE' => (isset($arResult['MIN_PRICE']) && !empty($arResult['MIN_PRICE']) && is_array($arResult['MIN_PRICE'])),
			'SHOW_DISCOUNT_PERCENT' => ($arParams['SHOW_DISCOUNT_PERCENT'] == 'Y'),
			'SHOW_OLD_PRICE' => ($arParams['SHOW_OLD_PRICE'] == 'Y'),
			'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
			'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
			'SHOW_BASIS_PRICE' => ($arParams['SHOW_BASIS_PRICE'] == 'Y'),
			'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
			'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y')
		),
		'VISUAL' => array(
			'ID' => $arItemIDs['ID'],
		),
		'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
		'PRODUCT' => array(
			'ID' => $arResult['ID'],
			'PICT' => $arFirstPhoto,
			'NAME' => $arResult['~NAME'],
			'SUBSCRIPTION' => true,
			'PRICE' => $arResult['MIN_PRICE'],
			'BASIS_PRICE' => $arResult['MIN_BASIS_PRICE'],
			'SLIDER_COUNT' => $arResult['MORE_PHOTO_COUNT'],
			'SLIDER' => $arResult['MORE_PHOTO'],
			'CAN_BUY' => $arResult['CAN_BUY'],
			//'CHECK_QUANTITY' => $arResult['CHECK_QUANTITY'],
            'CHECK_QUANTITY' => true, // !!!
			'QUANTITY_FLOAT' => is_double($arResult['CATALOG_MEASURE_RATIO']),
			'MAX_QUANTITY' => $arResult['CATALOG_QUANTITY'],
			'STEP_QUANTITY' => $arResult['CATALOG_MEASURE_RATIO'],
		),
		'BASKET' => array(
			'ADD_PROPS' => ($arParams['ADD_PROPERTIES_TO_BASKET'] == 'Y'),
			'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
			'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
			'EMPTY_PROPS' => $emptyProductProperties,
			'BASKET_URL' => $arParams['BASKET_URL'],
			'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
			'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
		)
	);
	if ($arParams['DISPLAY_COMPARE'])
	{
		$arJSParams['COMPARE'] = array(
			'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
			'COMPARE_PATH' => $arParams['COMPARE_PATH']
		);
	}
	unset($emptyProductProperties);
	CJSCore::Init(array("jquery"));
?>
<script type="text/javascript">
var <? echo $strObName; ?> = new JCCatalogElement(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
BX.message({
	ECONOMY_INFO_MESSAGE: '<? echo GetMessageJS('CT_BCE_CATALOG_ECONOMY_INFO'); ?>',
	BASIS_PRICE_MESSAGE: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_BASIS_PRICE') ?>',
	TITLE_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_TITLE_ERROR') ?>',
	TITLE_BASKET_PROPS: '<? echo GetMessageJS('CT_BCE_CATALOG_TITLE_BASKET_PROPS') ?>',
	BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
	BTN_SEND_PROPS: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_SEND_PROPS'); ?>',
	BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_BASKET_REDIRECT') ?>',
	BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE'); ?>',
	BTN_MESSAGE_CLOSE_POPUP: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE_POPUP'); ?>',
	TITLE_SUCCESSFUL: '<? echo GetMessageJS('CT_BCE_CATALOG_ADD_TO_BASKET_OK'); ?>',
	COMPARE_MESSAGE_OK: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_OK') ?>',
	COMPARE_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_UNKNOWN_ERROR') ?>',
	COMPARE_TITLE: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_TITLE') ?>',
	BTN_MESSAGE_COMPARE_REDIRECT: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT') ?>',
	SITE_ID: '<? echo SITE_ID; ?>'
});
</script>
</div>
