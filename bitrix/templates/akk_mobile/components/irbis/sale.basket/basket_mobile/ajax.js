var BXFormPosting = false;
function submitForm(val)
{
	if (BXFormPosting === true)
		return true;

	BXFormPosting = true;
	if(val != 'Y')
		BX('confirmorder').value = 'N';

	var orderForm = BX('ORDER_FORM');
	BX.showWait();
	BX.ajax.submit(orderForm, ajaxResult);

	return true;
}

function ajaxResult(res)
{
	var orderForm = BX('ORDER_FORM');
	try
	{
		var json = JSON.parse(res);
		BX.closeWait();
		if (json.error)
		{
			BXFormPosting = false;
			return;
		}
		else if (json.redirect)
		{
			window.top.location.href = json.redirect;
		}
	}
	catch (e)
	{
		BXFormPosting = false;
		BX('order_form_content').innerHTML = res;
        if($("input[name='DELIVERY']:checked").length>0)
        {
            $("#delivery_details").find(">div").slideToggle();
        }
	}
	BX.closeWait();
	BX.onCustomEvent(orderForm, 'onAjaxSuccess');
}

$(function(){   
    
    if($("input[name='DELIVERY']:checked").length>0)
    {
        $("#delivery_details").find(">div").slideToggle();
    }
      
    $(document).on('change click', 'input[name="PAYMENT"]', function(){
        submitForm("Y");
    });
    
    $(document).on('change click', 'input[name="DELIVERY"]', function(){
        submitForm("Y");
    });
    
    $(document).on('click', '.item-delete', function(e){
        e.preventDefault();
        $(this).prev("input").val("Y");
        submitForm("Y");
    });
});