<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>

<? //Определяем, есть ли расхождения во флагах сдачи отработанных аккумуляторов - замена АКБ должна оформляться отдельным заказом.
$arFlagsChangeAKB = array();
foreach($arResult["BASKET_ITEMS"] as $arItem) {
    if (intval($arItem["PROPERTY_PRICE_CHANGE_VALUE"]) > 0) {
        // Вторая сумма задана - проверяем, установлен ли флаг замены АКБ.
        if($arResult["CHANGED_PRICE_".$arItem["ID"]]=="on") {
            // Замена АКБ.
            $arFlagsChangeAKB[] = "Y";
        } else {
            // Возможность замены АКБ есть, но НЕ выбрана.
            $arFlagsChangeAKB[] = "N";
        }
    } else {
        // Вторая сумма НЕ задана, следовательно эта позиция НЕ имеет возможности замены АКБ.
        $arFlagsChangeAKB[] = "N";
    }
}
$disableMakeOrder = false;
if (count(array_count_values($arFlagsChangeAKB)) > 1 ) {
    // В корзине разношёрстных значений флагов замены АКБ несколько - нельзя оформлять заказ.
    $disableMakeOrder = true;
}?>

<? foreach($arResult["BASKET_ITEMS"] as $arItem):
    $arItem["SUM"] = $arItem["PRICE"] * $arItem["QUANTITY"]; ?>
    <div class="cart__item" id="item-<?=$arItem["ID"]?>">
        <div class="cart__item__header">
            <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="cart__item-title"><?=$arItem["NAME"]?></a></h2>
            <input type="hidden" name="DELETE_<?=$arItem["ID"]?>" value="N">
            <span class="cart__item__del item-delete"></span>
        </div>
        <div class="catalog__item">
            <figure>
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE_SRC"]?>" alt="<?=$arItem["NAME"]?>" /></a>
            </figure>
        </div>

        <fieldset>
            <ul>
                <li>
                    <div class="cart__item-amount__price">
                        <p><?= Loc::getMessage('COUNT') ?></p>
                        <div class="cart__item-amount">
                            <select class="select item-number" data-placeholder="select.js-select" name="QUANTITY_<?=$arItem["ID"]?>" onchange='submitForm("Y");'>
                                <? for($i=1; $i<100; $i++): ?>
                                    <option value="<?=$i?>" <?if($i==$arItem["QUANTITY"]):?>selected<?endif;?>><?=$i?></option>
                                <? endfor;?>
                            </select>
                        </div>
                        <div class="cart__item-price"><?=number_format($arItem["SUM"], 0, "", " ")?>&nbsp;<span class="ruble">руб.</span></div>
                    </div>
                </li>
                <?if(intval($arItem["PROPERTY_PRICE_CHANGE_VALUE"]) > 0): ?>
                    <li>
                        <input type="checkbox"
                               name="CHANGED_PRICE_<?=$arItem["ID"]?>"
                               id="cart_chk_<?=$arItem["ID"]?>"
                               onchange='submitForm("Y");'
                               <?if($arResult["CHANGED_PRICE_".$arItem["ID"]]=="on"):?>checked<?endif;?>/>
                        <label for="cart_chk_<?=$arItem["ID"]?>">
																		<?if(strstr($arItem["DETAIL_PAGE_URL"], "/catalog/battery/")):?>
													Сдача отработанного аккумулятора аналогичной ёмкости
												<?else:?>
													Предъявляю купон на скидку
												<?endif;?>
</label>
                    </li>
                <? endif; ?>
            </ul>
        </fieldset>
    </div>
<? endforeach; ?>
<div class="cart__total">
    <span><?= Loc::getMessage('SUMM') ?></span>
    <span class="cart__total-price"><?=number_format($arResult["ORDER_PRICE"], 0, "", " ")?>&nbsp;<span class="ruble">руб.</span></span>
</div>
<? if ($disableMakeOrder == true): ?>
        <section class="warning__old-battery">
            <div class="warning">
                <div class="warning__close"></div>
                <?= Loc::getMessage('WARNING') ?>
            </div>
        </section>
    <span class="btn btn_common green-button full-width" style="background: red">Оформление заказа недоступно</span>
<? else: ?>
    <a class="btn btn_common green-button full-width" href="/personal/order/make/"><?= Loc::getMessage('ORDER_MAKE') ?></a>
<? endif; ?>