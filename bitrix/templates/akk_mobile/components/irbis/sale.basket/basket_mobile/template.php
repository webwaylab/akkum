<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CJSCore::Init(array('fx', 'popup', 'window', 'ajax')); ?>

<a name="order_form"></a>
<script src="<?=$templateFolder."/ajax.js"?>"></script>
<? if($_POST["is_ajax_post"] != "Y"): ?>
    <form action="<?=$APPLICATION->GetCurPage();?>"
          method="POST"
          name="ORDER_FORM"
          id="ORDER_FORM"
          enctype="multipart/form-data"
          class="create_order">
	<?=bitrix_sessid_post()?>
    <div id="order_form_content" class="cart">
<? else:
	$APPLICATION->RestartBuffer();
endif;

if(!empty($arResult["ERROR"]) && $arResult["USER_VALS"]["FINAL_STEP"] == "Y"):
	foreach($arResult["ERROR"] as $v):
		echo ShowError($v);
	endforeach;?>
	<script type="text/javascript">
		top.BX.scrollToNode(top.BX('ORDER_FORM'));
	</script>
<? endif;

if(empty($arResult["BASKET_ITEMS"])):?>
    <span><?= GetMessage('NO_BASKET') ?></span>
<? else:
    include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/items.php");
endif;

if($_POST["is_ajax_post"] != "Y"): ?>
        </div>
		<input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">
		<input type="hidden" name="json" value="Y">
	</form>
<? else:
	die();
endif;?>