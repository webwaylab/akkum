<?
CModule::IncludeModule("iblock");
IncludeTemplateLangFile(__FILE__);
global $USER;

$res = CIBlockElement::GetByID($arParams["PRODUCT_ID"]);
$arResult["PRODUCT"] = $res->GetNext();

foreach($arResult["ITEMS"] as &$arItem)
{
    $res = CUser::GetByID($arItem["PROPERTIES"]["USER_ID"]["VALUE"]);
    $arItem["USER"] = $res->GetNext();
}

if($USER->IsAuthorized())
{
    $res = CUser::GetByID($USER->GetID());
    $arResult["CURRENT_USER"] = $res->GetNext();
}
$arResult["IS_AUTH"] = $USER->IsAuthorized();

//AJAX
if(check_bitrix_sessid() && $_REQUEST["action"]=="addComment")
{
    $APPLICATION->RestartBuffer();
    if (!defined('PUBLIC_AJAX_MODE'))
    {
        define('PUBLIC_AJAX_MODE', true);
    }

    CModule::IncludeModule("iblock");
    global $USER;

    $array = array();
    $msg = "";

    $res = CIBlockElement::GetByID($arParams["PRODUCT_ID"]);
    $arProduct = $res->GetNext();

    if(empty($_POST["PLUS"]) && empty($_POST["MINUS"]))
    {
        $msg = "Введите отзыв!<br />";
    }

    if (empty($_POST["NAME"]))
    {
        $msg.= "Введите свое имя!";
    }

    if(empty($msg))
    {
        $el = new CIBlockElement;

        $PROP = array();
        $PROP['USER_ID'] = $USER->GetID();
        $PROP["PRODUCT_ID"] = $arProduct["ID"];
        $PROP["PLUS"][0] = Array("VALUE" => Array ("TEXT" => strip_tags($_POST["PLUS"]), "TYPE" => "text"));
        $PROP["MINUS"][0] = Array("VALUE" => Array ("TEXT" => strip_tags($_POST["MINUS"]), "TYPE" => "text"));
        $PROP["NAME"] = htmlspecialcharsbx($_POST["NAME"]);

        $arLoadProductArray = Array(
            "MODIFIED_BY"    => $USER->GetID(),
            "IBLOCK_SECTION_ID" => false,
            "IBLOCK_ID"      => "6",
            "PROPERTY_VALUES"=> $PROP,
            "NAME"           => $arProduct["NAME"],
            "ACTIVE"         => "N",
            "DATE_ACTIVE_FROM" => date("d.m.Y")
        );

        if($el->Add($arLoadProductArray))
        {
            $array["status"] = true;
            $array["message"] = "<font class='green'>После модерации ваш отзыв появится на сайте!</font>";
        }
    }else{
        $array["status"] = false;
        $array["message"] = $msg;
    }

    echo json_encode($array);

    require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');
    die();
}
?>

<script src="<?=SITE_TEMPLATE_PATH?>/ajaxupload.js"></script>
<script type="text/javascript">
$(function(){
    $("body").on("submit", "#comment-form", function(e){
    	e.preventDefault();
    	params=$(this).serialize()+"&action=addComment";
        var form = $(this);
        form.find(".error").hide().html("");
        form.find("fieldset").show();
        $.ajax({
            type: 'POST',
            url: '<?=$GLOBALS["APPLICATION"]->GetCurDir()?>?action=addComment',
            data: params,
            dataType: 'json',
            success: function(result){
                if(result.status)
                {
                    form.find("textarea").val("");
                    form.find(".error").show().html(result.message),
                    $('.error').addClass('success');
                    form.find("fieldset").hide();
                    form.find("#another_review").show();
                }
                else
                    form.find(".error").show().html(result.message);
            }
        });
    });
    $('#another_review').on('click',function(e) {
        $(this).hide();
        $('#comment-form fieldset').show();
        $('#comment-form .error').html("");
    });
});
</script>
