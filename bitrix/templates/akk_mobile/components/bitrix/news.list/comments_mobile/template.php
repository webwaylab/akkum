<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
global $USER; ?>

<div class="testimonials">
    <? foreach($arResult["ITEMS"] as $arItem): ?>
        <div class="testimonial">
            <div class="testimonial__meta clearfix">
                <figure>
                    <? if($arItem["USER"]["PERSONAL_PHOTO"]): ?>
                        <img src="<?=CFile::GetPath($arItem["USER"]["PERSONAL_PHOTO"])?>">
                    <? endif; ?>
                </figure>
                <strong>
                        <?=$arItem["PROPERTIES"]["NAME"]["VALUE"]?>
                </strong>
                <span><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span>
            </div>
            <dl>
                <dt><?= GetMessage('DIGNITY') ?></dt>
                <dd><?=strip_tags($arItem["PROPERTIES"]["PLUS"]["VALUE"]["TEXT"])?></dd>
                <dt><?= GetMessage('DISADVANTAGES') ?></dt>
                <dd><?=strip_tags($arItem["PROPERTIES"]["MINUS"]["VALUE"]["TEXT"])?></dd>
            </dl>
        </div>
    <? endforeach; ?>

    <div class="testimonials__form">
        <a href="javascript:" class="btn btn_o full-width js-popup" data-inline="true" data-href="#popup-form-testimonial" data-class-name="popup-form-wrap"><?= GetMessage('WRITE_A_REVIEW') ?></a>
        <div class="hidden">
            <div class="popup-form" id="popup-form-testimonial">
                <i class="flying-hint__hide js-popup-hide"></i>                
                <form action="<?=$GLOBALS["APPLICATION"]->GetCurDir()?>" METHOD="POST" id="comment-form" autocomplete="off">
                    <?=bitrix_sessid_post()?>
                    <h2><?= GetMessage('WRITE_A_REVIEW') ?></h2>
                    <div class="error"></div>
                        <fieldset>
                            <ul>
                                <li>
                                    <? $user = $USER->IsAuthorized() ? $arResult["CURRENT_USER"]["NAME"] . ' ' . $arResult["CURRENT_USER"]["LAST_NAME"] : ''; ?>
                                    <label><?= GetMessage('NAME') ?></label>
                                    <input type="text" name="NAME" value="<?= $user ?>" />
                                </li>
                                <li>
                                    <label><?= GetMessage('WRITE_A_REVIEW_PLUS') ?></label>
                                    <textarea name="PLUS" rows="6"></textarea>
                                </li>
                                <li>
                                    <label><?= GetMessage('WRITE_A_REVIEW_MINUS') ?></label>
                                    <textarea name="MINUS" rows="6"></textarea>
                                </li>
                            </ul>
                            <input type="submit" name="" value="<?= GetMessage('SEND_A_REVIEW') ?>" />
                        </fieldset>
                        <input type="button" id = "another_review" style='display: none' value="<?= GetMessage('NEW_REVIEW') ?>" />
                </form>
            </div>
        </div>
    </div>
</div>