<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if (empty($arResult["ITEMS"])) {
	return;
}
$arParams["BLOCK_LABEL"] = trim(strip_tags($arParams["BLOCK_LABEL"]));
?>

<div class="list-articles">
    <a href="<?=str_replace('#SITE_DIR#', '', $arResult["LIST_PAGE_URL"])?>" class="list-articles__title">
        <?=$arParams["BLOCK_LABEL"]?>
    </a>
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <article>
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                <span><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span>
                <?=$arItem["NAME"]?>
            </a>
        </article>
    <?endforeach;?>
</div>
