<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
if (count($arResult["ITEMS"]) < 1) return; ?>

<section class="articles">
    <h2><a href="/stati/"><?=$arParams["NEWS_TITLE"]?></a></h2>
    <?
    foreach($arResult["ITEMS"] as $arItem): ?>
        <article class="articles__item">
            <div class="articles__item__header">
                <span><?=$arItem["PROPERTIES"]["LABEL"]["VALUE"]?></span>
                <?=$arItem["DISPLAY_ACTIVE_FROM"]?>
            </div>
            <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h2>
            <div class="articles__item__preview"><?=htmlspecialchars_decode($arItem["PREVIEW_TEXT"])?></div>
        </article>
    <? endforeach; ?>
</section>
