<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CModule::IncludeModule("iblock");
global $APPLICATION;

$childs = array();
$items = array();
foreach($arResult as $key => $arItem)
{
    $level2 = 0;
    $SectionID = 0;
    $items[$key] = $arItem;
    $items[$key]['HAS_CHILDREN'] = false;
        
    if($arItem["TEXT"]=="Каталог аккумуляторов")
    {
        $SectionID = 18;
    }
    
    if($arItem["TEXT"]=="Аксессуары для автомобиля")
    {
        $SectionID = 19;
    }

    if($arItem["TEXT"]=="Масла и технические жидкости")
    {
        $SectionID = 351;
    }
    
    if($SectionID>0)
    {
        $arSections = array();
        $rsParentSection = CIBlockSection::GetByID($SectionID);
        
        if ($arParentSection = $rsParentSection->GetNext())
        {
            if(strpos($APPLICATION->GetCurDir(), $arParentSection["SECTION_PAGE_URL"])!==false)
            {
                $items[$key]["SELECTED"] = "Y";
            }
            
            $arFilter = array(
                'IBLOCK_ID' => CATALOG_ID, 
                '>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'],
                '<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'],
                '>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL'],
                '<DEPTH_LEVEL' => 4,
                'GLOBAL_ACTIVE' => "Y"
            );
            $rsSect = CIBlockSection::GetList(array('DEPTH_LEVEL' => 'asc', 'name'=>"asc", "sort"=>"asc"), $arFilter, false);
            while ($arSect = $rsSect->GetNext())
            {
                if($arSect["DEPTH_LEVEL"]==2)
                {
			$items[$key]['HAS_CHILDREN'] = true;
			$items[$key]['CHILDREN'][] = $arSect;
                    $level2++;
                }else{
                    $children[$arSect["IBLOCK_SECTION_ID"]][] = $arSect;
                }
                
            }
        }

        $arSection = explode('/', $arItem['LINK']);
		//CDev::pre($arItem);
        $items[$key]['SECTION_CODE'] = $arSection[2]?$arSection[2]:$arSection[1];
		$items[$key]['SECTION_ID'] = $SectionID;
    }
}

$arResult = array();
$arResult["ITEMS"] = $items;
$arResult['CHILDREN'] = $children;