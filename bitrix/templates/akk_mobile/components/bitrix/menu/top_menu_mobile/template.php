<?
if ( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true ) die();
$this->setFrameMode(true);

if ( empty($arResult) ) {
    return;
}
$url_no_view = array(
    '/catalog/battery/varta/',
    '/catalog/battery/zver/',
    '/catalog/battery/mutlu/',
    '/catalog/battery/titan/',
    '/catalog/battery/bosch/',
    '/catalog/battery/tyumen/',
    '/catalog/battery/bolk_moto/',
    '/catalog/battery/tudor/',
    '/catalog/battery/solite/',
    '/catalog/battery/vaiper/',
    '/catalog/battery/royal/',
    '/catalog/battery/sebang/',
    '/catalog/battery/cobat/',
    '/catalog/battery/aktex/',
    '/catalog/battery/afa/',
    '/catalog/avtomobilnye/',
); ?>
<nav class="nav_main qwerty">
    <ul>
        <? foreach ( $arResult['ITEMS'] as $arItem ): ?>
            <li class="menu__item">
                <? if ( $arItem["HAS_CHILDREN"] && count($arItem["CHILDREN"]) > 0 ): ?>
                    <?
                        $_arItem = $arItem;
                        $_arItem["CHILDREN"] = array();
                        /*print_r("<pre>");
                        print_r($_arItem);
                        print_r("</pre>");*/
                    ?>
                    <a href="javascript:" class="is-parent" data-toggleclass='{"target":"#menu-popup-<?=$arItem['SECTION_CODE']?$arItem['SECTION_CODE']:$arItem['SECTION_ID']?>","className":"visible"}'>
                        <?=$arItem['TEXT']?>
                    </a>
                    <div id="menu-popup-<?=$arItem['SECTION_CODE']?$arItem['SECTION_CODE']:$arItem['SECTION_ID']?>" class="popup-menu custom-popup">
                        <div class="custom-popup__body">
                            <div class="container">
                                <span class="custom-popup__hide"
                                      data-toggleclass='{"target":"#menu-popup-<?=$arItem['SECTION_CODE']?$arItem['SECTION_CODE']:$arItem['SECTION_ID']?>","className":"visible"}'></span>
                                <h2><?=$arItem['TEXT']?></h2>
                                <ul>
                                    <? foreach ( $arItem["CHILDREN"] as $key => $arChild ): ?>
                                            <? if ( isset($arResult['CHILDREN'][$arChild['ID']])
                                                && sizeof($arResult['CHILDREN'][$arChild['ID']]) > 0
                                            ): ?>
                                            <li class="popup-menu__item" id="popup-submenu__item-<?=$arItem['SECTION_CODE']?$arItem['SECTION_CODE']:$arItem['SECTION_ID']?><?=$key?>">
                                                <?
                                                    $arChild["SECTION_PAGE_URL"] = str_replace("/battery/auto-akb/ADDBRAND_", "/brand/", $arChild["SECTION_PAGE_URL"]);
                                                ?>
                                                <a href="javascript:void(0);" data-toggleclass='{"target":"#popup-submenu__item-<?=$arItem['SECTION_CODE']?$arItem['SECTION_CODE']:$arItem['SECTION_ID']?><?=$key?>", "className": "popup-submenu__item-opened"}'
                                                   style="text-decoration: inherit; color:inherit"
                                                   class="is-parent">
                                                    <?=$arChild["NAME"]?>
                                                </a>
                                                <button class="arrow is-parent"
                                                   data-toggleclass='{"target":"#popup-submenu__item-<?=$arItem['SECTION_CODE']?$arItem['SECTION_CODE']:$arItem['SECTION_ID']?><?=$key?>", "className": "popup-submenu__item-opened"}'>
                                                </button>
                                                <div class="popup-submenu">
                                                    <ul>
                                                        <? foreach ( $arResult['CHILDREN'][$arChild['ID']] as $arElement ): ?>
                                                            <li class="popup-submenu__item">
                                                    <?
                                                        $arElement["SECTION_PAGE_URL"] = str_replace("/battery/auto-akb/ADDBRAND_", "/brand/", $arElement["SECTION_PAGE_URL"]);
                                                    ?>
                                                                <a href="<?=$arElement["SECTION_PAGE_URL"]?>"
                                                                   style="text-decoration: inherit; color:inherit">
                                                                    <?=$arElement["NAME"]?>
                                                                </a>
                                                            </li>
                                                        <? endforeach; ?>

                                                    </ul>
                                                </div>
                                            </li>
                                            <?else:?>
                                            <li class="popup-menu__item">
                                                <?
                                                    $arChild["SECTION_PAGE_URL"] = str_replace("/battery/auto-akb/ADDBRAND_", "/brand/", $arChild["SECTION_PAGE_URL"]);
                                                ?>
                                                <a href="<?=$arChild["SECTION_PAGE_URL"]?>"
                                                   style="text-decoration: inherit; color:inherit">
                                                    <?=$arChild["NAME"]?>
                                                </a>
                                            </li>
                                            <? endif ?>
                                    <? endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                <? else: ?>
                    <a href="<?=$arItem["LINK"]?>">
                        <?=$arItem["TEXT"]?>
                    </a>
                <? endif; ?>
            </li>
        <? endforeach; ?>
    </ul>
</nav>