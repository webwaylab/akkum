<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if (empty($arResult))
	return;
?>

<nav class="nav_footer">
    <ul>
        <?foreach($arResult as $arItem):?>
            <?if ($arItem["DEPTH_LEVEL"] == "1"):?>
                <li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
            <?endif?>
        <?endforeach;?>
    </ul>
</nav>
