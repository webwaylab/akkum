<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<div class="wrapper catalog_nav_wrap">
    <div class="catalog_nav wrapper">
        <ul class="catalog_nav_list heavy">
            <?
            foreach($arResult as $arItem):
            	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
            		continue;
            ?>
            	<?if($arItem["SELECTED"]):?>
                    
            		<li class="catalog_nav_list_item current"><a href="<?=$arItem["LINK"]?>" class="catalog_nav_list_link"><?=$arItem["TEXT"]?></a></li>
            	<?else:?>
            		<li class="catalog_nav_list_item"><a href="<?=$arItem["LINK"]?>" class="catalog_nav_list_link"><?=$arItem["TEXT"]?></a></li>
            	<?endif?>
            	
            <?endforeach?>
        </ul>
    </div>
</div>
<?endif?>