<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true); 

$page_uri = $APPLICATION->GetCurDir();
$actualFilters = array();
if(strstr($page_uri, "/battery/") 
	|| strstr($page_uri, "/catalog/brand/") 
	|| strstr($page_uri, "/budget/") 
	|| strstr($page_uri, "/popular/") 
	|| strstr($page_uri, "/catalog/auto/")
	|| $page_uri == "/"
	|| $page_uri == "/index.php"
	){
	$actualFilters = array("MANUFACTURE", "BASE", "TYPE", "CAPACITY", "POLARITY", "START_TOK", "TOKEND", "LENGTH", "WIDTH", "HEIGHT");//, "MANUFACTURE_2"
}elseif(strstr($page_uri, "/domkraty_podstavki_upory_pod_mashinu/")){
	$actualFilters = array("BASE", "LENGTH", "WIDTH", "HEIGHT");
	foreach($arResult["ITEMS"] as $f_item){
		if(strstr($f_item["CODE"], "_DOMKRATY")){
			$actualFilters[] = $f_item["CODE"];
		}
	}
}elseif(strstr($page_uri, "/nabor_instrumentov/")){
	$actualFilters = array("MANUFACTURE_2", "TOOLS_NUMBER_ITEMS", "BASE");
	foreach($arResult["ITEMS"] as $f_item){
		if(strstr($f_item["CODE"], "TOOLS_")){
			$actualFilters[] = $f_item["CODE"];
		}
	}
}elseif(strstr($page_uri, "/masla/")){
	$actualFilters = array("MANUFACTURE", "BASE");
	foreach($arResult["ITEMS"] as $f_item){
		if(strstr($f_item["CODE"], "_MASLO")){
			$actualFilters[] = $f_item["CODE"];
		}
	}
}
/*
print_r("<pre>");
print_r($actualFilters);
print_r($arResult["ITEMS"]);
print_r("</pre>");
*/
?>
<noindex>
    <form name="<? echo $arResult["FILTER_NAME"] . "_form" ?>" action="<? echo $arParams["FILTER_URL"] ?>" method="get">
        <? foreach ($arResult["HIDDEN"] as $arItem): ?>
            <input type="hidden" name="<? echo $arItem["CONTROL_NAME"] ?>" id="<? echo $arItem["CONTROL_ID"] ?>"
                   value="<? echo $arItem["HTML_VALUE"] ?>"/>
        <? endforeach; ?>
        <fieldset>
            <ul>
                <? $arBoxes = array();
                //not prices
                foreach ($arResult["ITEMS"] as $key => $arItem):
                    if (empty($arItem["VALUES"]) || isset($arItem["PRICE"])) continue;
					
					if(count($actualFilters) && !in_array($arItem["CODE"], $actualFilters)){
						continue;
					}
					$pos = mb_strpos($arItem["NAME"], "(Домкраты)");
					if($pos > 0){
						$arItem["NAME"] = mb_substr($arItem["NAME"], 0, $pos);
					}

                    $arCur = current($arItem["VALUES"]);
                    switch ($arItem["DISPLAY_TYPE"]):

                        //NUMBERS_WITH_SLIDER
                        case "A": ?>
                            <li>
                                <label class="catalog-filter__label"><?= $arItem["NAME"] ?>:</label>
                                <div class="fields-range">
                                    <input type="text"
                                           name="<?= $arItem["VALUES"]["MIN"]["CONTROL_NAME"] ?>"
                                           id="<?= $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
														value="<?=intval($arItem["VALUES"]["MIN"]["HTML_VALUE"])!=intval($arItem["VALUES"]["MIN"]["VALUE"]) ? $arItem["VALUES"]["MIN"]["HTML_VALUE"] : ""?>"
	            										placeholder="<?=$arItem["VALUES"]["MIN"]["VALUE"]?>"
                                    />
                                    <input type="text"
                                           name="<?= $arItem["VALUES"]["MAX"]["CONTROL_NAME"] ?>"
                                           id="<?= $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
														value="<?=intval($arItem["VALUES"]["MAX"]["HTML_VALUE"])!=intval($arItem["VALUES"]["MAX"]["VALUE"]) ? $arItem["VALUES"]["MAX"]["HTML_VALUE"] : ""?>"
	            										placeholder="<?=$arItem["VALUES"]["MAX"]["VALUE"]?>"
                                    />
                                </div>
                                <div class="range-slider" id="range-slider-<?= $arItem['CODE'] ?>"></div>
                                    <?
                                    $min = $arItem["VALUES"]["MIN"]["VALUE"];
                                    $max = $arItem["VALUES"]["MAX"]["VALUE"];
                                    $minVal = $arItem["VALUES"]["MIN"]["HTML_VALUE"] ?: $min;
                                    $maxVal = $arItem["VALUES"]["MAX"]["HTML_VALUE"] ?: $max;
									/*print_r("<pre>");									
									print_r($arItem);									
									print_r("</pre>");*/
                                    ?>
								<?if($max && $max):?>
                                <script type="text/javascript">
                                    $(function () {
                                        jsRangeSlider(
                                            '#range-slider-<?= $arItem['CODE'] ?>',
                                            <?= $min ?>, <?= $max ?>,
                                            <?= $minVal ?>, <?= $maxVal ?>,
                                            '#<?= $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>',
                                            '#<?= $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>');
                                    });
                                </script>
								<?endif;?>
                            </li>
                            <? break;

                        //NUMBERS
                        case "B":
                            $arBoxes[] = $arItem;
                            break;

                        //DROPDOWN
                        case "P":
                            $checkedItemExist = false; ?>
                                <li>
                                    <label class="catalog-filter__label"><?= $arItem["NAME"] ?>:</label>
                                    <input
                                            style="display: none"
                                            type="radio"
                                            name="<?=$arCur["CONTROL_NAME_ALT"]?>"
                                            id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
                                            value=""
                                        />
                                        <?foreach ($arItem["VALUES"] as $val => $ar):?>
                                            <input
                                                style="display: none"
                                                type="radio"
                                                name="<?=$ar["CONTROL_NAME_ALT"]?>"
                                                id="<?=$ar["CONTROL_ID"]?>"
                                                value="<? echo $ar["HTML_VALUE_ALT"] ?>"
                                                <? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
                                            />
                                        <?endforeach?>
                                    <select class="js-select"
                                            data-placeholder="<?= GetMessage('FILTER_TO_SELECT') ?>"
                                            onchange="$('#'+$(this).find(':selected').attr('for')).attr('checked', true); smartFilter.selectDropDownItem(this, $(this).find(':selected').data('control')); return false;">
                                        <option></option>
                                        <? foreach ($arItem["VALUES"] as $val => $ar):
                                            $class = "";
                                            if ($ar["CHECKED"])
                                                $class .= " selected";
                                            if ($ar["DISABLED"])
                                                continue;
												//$class .= " disabled";
                                            ?>
                                            <option <?= $class ?>
                                                    for="<?=$ar["CONTROL_ID"]?>"
                                                    data-role="label_<?= $ar["CONTROL_ID"] ?>"
                                                    data-control="<?= CUtil::JSEscape($ar["CONTROL_ID"]) ?>"><?= $ar["VALUE"] ?></option>
                                        <?endforeach ?>
                                    </select>
                                </li>
                            <? break;

                        //CHECKBOXES
                        default: ?>
                            <li>
                                <label class="catalog-filter__label"><?= $arItem["NAME"] ?>:</label>
                                <? foreach ($arItem["VALUES"] as $val => $ar):?>
                                	<?if((!strstr($page_uri, "/truck-akb/") && ($ar["CONTROL_ID"]=="arrFilter_51_2136814527" || $ar["CONTROL_ID"]=="arrFilter_51_3864289797")) || 
										(strstr($page_uri, "/truck-akb/") && ($ar["CONTROL_ID"]=="arrFilter_51_2396732099" || $ar["CONTROL_ID"]=="arrFilter_51_1889509032"))):?>
                                		<?continue;?>
                                	<?endif;?>
                                    <input
                                            type="checkbox"
                                            value="<? echo $ar["HTML_VALUE"] ?>"
                                            name="<? echo $ar["CONTROL_NAME"] ?>"
                                            id="<? echo $ar["CONTROL_ID"] ?>"
                                            <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
                                    />
                                    <label for="<? echo $ar["CONTROL_ID"] ?>" data-role="label_<?= $ar["CONTROL_ID"] ?>">
                                        <? echo $ar["VALUE"]; ?>
                                    </label>
                                <?endforeach; ?>
                            </li>
                    <? endswitch; ?>
                <? endforeach; ?>

				<?if(strstr($page_uri, "/battery/") || $page_uri == "/"):?>
                <li>
                    <label class="catalog-filter__label"><?= GetMessage('FILTER_GABARITS') ?></label>
                    <div class="fields-size">
                        <? foreach ($arBoxes as $key => $arItem): ?>
                            <input
                                    class="max-price"
                                    type="text"
                                    xname="<? echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"] ?>"
                                    xid="<? echo $arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>"
                                    value="<?= ($arItem["VALUES"]["MAX"]["HTML_VALUE"] == '' ? '' : ($arItem["VALUES"]["MAX"]["HTML_VALUE"] - 9)) ?>"
                                    size="5"
                                    onkeyup="$('#<? echo $arItem["VALUES"]["MIN"]["CONTROL_ID"] ?>').val($(this).val()*1-9);
                                            $('#<? echo $arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>').val($(this).val()*1+9);
                                            smartFilter.keyup($('#<? echo $arItem["VALUES"]["MIN"]["CONTROL_ID"] ?>')); smartFilter.keyup(this)"
                            />
                            <input
                                    class="min-price"
                                    type="hidden"
                                    name="<? echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"] ?>"
                                    id="<? echo $arItem["VALUES"]["MIN"]["CONTROL_ID"] ?>"
                                    value="<? echo $arItem["VALUES"]["MAX"]["HTML_VALUE"] ?>"
                                    size="5"
                                    onkeyup="smartFilter.keyup(this)"
                            />
                            <input
                                    class="max-price"
                                    type="hidden"
                                    name="<? echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"] ?>"
                                    id="<? echo $arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>"
                                    value="<? echo $arItem["VALUES"]["MAX"]["HTML_VALUE"] ?>"
                                    size="5"
                                    onkeyup="smartFilter.keyup(this)"
                            />
                        <? endforeach; ?>
                    </div>
                </li>
                <?endif;?>
            </ul>
        </fieldset>

        <div class="catalog-filter__btns">
            <input type="submit" id="set_filter" name="set_filter" value="<?= GetMessage('CT_BCSF_FILTER_OK') ?>"/>
            <input type="submit" id="del_filter" name="del_filter" value="<?= GetMessage("CT_BCSF_DEL_FILTER") ?>"
                   style="display: none;"/>
            <span class="lnk-js ttu"
                  onclick="location.href='<?=$page_uri;?>'; return false;"><?= GetMessage('CT_BCSF_FILTER_CLEAR') ?></span>
        </div>
    </form>
</noindex>
<script>
    var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>');
</script>
