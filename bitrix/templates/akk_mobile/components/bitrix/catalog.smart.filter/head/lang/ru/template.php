<?
$MESS ['CT_BCSF_FILTER_TITLE'] = "Подбор параметров";
$MESS ['CT_BCSF_FILTER_FROM'] = "От";
$MESS ['CT_BCSF_FILTER_TO'] = "До";
$MESS ['CT_BCSF_SET_FILTER'] = "Показать";
$MESS ['CT_BCSF_DEL_FILTER'] = "Сбросить";
$MESS ['CT_BCSF_FILTER_COUNT'] = "Выбрано: #ELEMENT_COUNT#";
$MESS ['CT_BCSF_FILTER_SHOW'] = "Показать";
$MESS ['CT_BCSF_FILTER_ALL'] = "Все";
$MESS ['CT_BCSF_FILTER_CLEAR'] = "Сбросить фильтр";
$MESS ['CT_BCSF_FILTER_OK'] = "Применить фильтр";

$MESS ['FILTER_GABARITS'] = "Габариты Д-Ш-В:";
$MESS ['FILTER_NAME'] = "Фильтр";
$MESS ['FILTER_TO_PARAMETERS'] = "По параметрам";
$MESS ['FILTER_TO_AVTO'] = "По марке авто";
$MESS ['FILTER_TO_SELECT'] = "Выберите";
