<?
$MESS["CT_BCS_TPL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["CT_BCS_TPL_MESS_BTN_BUY"] = "Купить";
$MESS["CT_BCS_TPL_MESS_BTN_ADD_TO_BASKET"] = "В корзину";
$MESS["CT_BCS_TPL_MESS_PRODUCT_NOT_AVAILABLE"] = "Нет в наличии";
$MESS["CT_BCS_TPL_MESS_BTN_DETAIL"] = "Подробнее";
$MESS["CT_BCS_TPL_MESS_BTN_SUBSCRIBE"] = "Подписаться";
$MESS["CT_BCS_CATALOG_BTN_MESSAGE_BASKET_REDIRECT"] = "Перейти в корзину";
$MESS["ADD_TO_BASKET_OK"] = "Товар добавлен в корзину";
$MESS["CT_BCS_TPL_MESS_PRICE_SIMPLE_MODE"] = "от #PRICE# за #MEASURE#";
$MESS["CT_BCS_TPL_MESS_MEASURE_SIMPLE_MODE"] = "#VALUE# #UNIT#";
$MESS["CT_BCS_TPL_MESS_BTN_COMPARE"] = "Сравнить";
$MESS["CT_BCS_CATALOG_TITLE_ERROR"] = "Ошибка";
$MESS["CT_BCS_CATALOG_TITLE_BASKET_PROPS"] = "Свойства товара, добавляемые в корзину";
$MESS["CT_BCS_CATALOG_BASKET_UNKNOWN_ERROR"] = "Неизвестная ошибка при добавлении товара в корзину";
$MESS["CT_BCS_CATALOG_BTN_MESSAGE_SEND_PROPS"] = "Выбрать";
$MESS["CT_BCS_CATALOG_BTN_MESSAGE_CLOSE"] = "Закрыть";
$MESS["CT_BCS_CATALOG_BTN_MESSAGE_CLOSE_POPUP"] = "Продолжить покупки";
$MESS["CT_BCS_CATALOG_BTN_MESSAGE_BASKET_REDIRECT"] = "Перейти в корзину";
$MESS["CT_BCS_CATALOG_MESS_COMPARE_OK"] = "Товар добавлен в список сравнения";
$MESS["CT_BCS_CATALOG_MESS_COMPARE_TITLE"] = "Сравнение товаров";
$MESS["CT_BCS_CATALOG_MESS_COMPARE_UNKNOWN_ERROR"] = "При добавлении товара в список сравнения произошла ошибка";
$MESS["CT_BCS_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT"] = "Перейти в список сравнения";
$MESS["CT_BCS_CATALOG_BTN_MESSAGE_COMPARE"] = "К сравнению";
$MESS["CT_BCS_CATALOG_BTN_DIMENSIONS"] = "Габариты: ";
$MESS["CT_BCS_CATALOG_BTN_CAPACITY"] = "Ёмкость: ";
$MESS["CT_BCS_CATALOG_BTN_AMPERS"] = " А/ч";
$MESS["CT_BCS_CATALOG_BTN_EXCHANGE"] = "при обмене";
$MESS["CT_BCS_CATALOG_BTN_ALL"] = "Показать все";

$MESS["INRUSH_CURRENT"] = "Пусковой ток: ";
$MESS["INRUSH_CURRENT_VALUE"] = " А";
$MESS["CT_BCS_TPL_MESS_BTN_BUY_CLICK"] = "Купить в 1 клик";

$MESS["VOLUME"] = "Объём: ";
$MESS["VOLUME_VALUE"] = "л.";
$MESS["VISCOSITY"] = "Вязкость: ";
$MESS["TYPE_VALUE"] = "Тип: ";
$MESS["MANUFACTURER"] = "Производитель: ";
?>