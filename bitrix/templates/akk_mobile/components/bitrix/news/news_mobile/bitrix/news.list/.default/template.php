<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if (count($arResult["ITEMS"]) < 1) return; ?>

<section class="articles">
    <div class="content_items">
        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('NEWS_ELEMENT_DELETE_CONFIRM')));
            ?>
                <article class="articles__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <div class="articles__item__header">
                        <span><?= $arItem["PROPERTIES"]["LABEL"]["VALUE"]?></span>
                        <?= $arItem["DISPLAY_ACTIVE_FROM"]?>
                    </div>

                    <?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
                        <h2>
                            <?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || $arResult["USER_HAVE_ACCESS"]):?>
                                <a href="<?= $arItem["DETAIL_PAGE_URL"]?>"><?= $arItem["NAME"]?></a>
                            <?else:?>
                                <?= $arItem["NAME"]?>
                            <?endif;?>
                        </h2>
                    <?endif;?>

                    <?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
                        <div class="articles__item__preview"><?= $arItem["PREVIEW_TEXT"];?></div>
                    <?endif;?>
                </article>
        <?endforeach;?>
        <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
            <?
            echo "<div class='nav_string'>";
            echo $arResult["NAV_STRING"];
            echo "</div>";
            ?>
        <?endif;?>
    </div>
</section>


