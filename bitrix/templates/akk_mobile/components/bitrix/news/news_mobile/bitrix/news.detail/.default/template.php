<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div>
    <div class="articles__item__header">
        <span><?=$arResult["PROPERTIES"]["LABEL"]["VALUE"]?></span>
        <?= $arResult["DISPLAY_ACTIVE_FROM"]?>
    </div>

    <div>
        <?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
            <img class="image-detail" src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" alt="<?=$arResult["NAME"]?>"  title="<?=$arResult["NAME"]?>" />
        <?endif?>

        <?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):?>
            <?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?>
        <?endif;?>

        <?if($arResult["NAV_RESULT"]):?>
            <?= $arResult["NAV_TEXT"];?>
            <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br /><?=$arResult["NAV_STRING"]?><?endif;?>
        <?elseif(strlen($arResult["DETAIL_TEXT"])>0):?>
            <?= $arResult["DETAIL_TEXT"];?>
        <?else:?>
            <?= $arResult["PREVIEW_TEXT"];?>
        <?endif?>
    </div>
</div>