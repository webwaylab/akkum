

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$strTitle = "";
?>
<?
	$url_no_view = array(
		'/catalog/battery/varta/',		
		'/catalog/battery/zver/',		
		'/catalog/battery/mutlu/',
		'/catalog/battery/titan/',
		'/catalog/battery/bosch/',
		'/catalog/battery/tyumen/',
		'/catalog/battery/bolk_moto/',
		'/catalog/battery/tudor/',
		'/catalog/battery/solite/',
		'/catalog/battery/vaiper/',
		'/catalog/battery/royal/',
		'/catalog/battery/sebang/',
		'/catalog/battery/cobat/',
		'/catalog/battery/aktex/',
		'/catalog/battery/afa/',
		'/catalog/avtomobilnye/', 
	);
?>




<div class="sub_menu" id="catalog_categories">
    <div class="sub_menu_inner">
	<?
    //CDev::pre($arResult["CHILDS"]);
    $cols = 4;
    $sectionCount = count($arResult["SECTIONS"]);
    
    //$sectionCount = $sectionCount - 15;
    
    $sectionRows = ceil($sectionCount/$cols);
    for($column=0,$i=0; $column<$cols; $column++)
    {
		
		//if (!in_array($arChild["SECTION_PAGE_URL"], $url_no_view)) { 
		
        ?>
        <div class="sub_menu_col">
        <?
        for($j=0; $j<$sectionRows && $i<$sectionCount; $j++,$i++) 
        {
            $arSection = $arResult["SECTIONS"][$i];
            ?>
            <div class="sub_menu_group">
                <a href="<?=$arSection["SECTION_PAGE_URL"]?>"><div class="black fs_18 reg sub_menu_title"><?=$arSection["NAME"]?></div></a>
                <ul class="list1">
                    <?
                    if(count($arResult["CHILDS"][$arSection["ID"]])>0)
                    {
                        foreach($arResult["CHILDS"][$arSection["ID"]] as $child)
                        {
                            ?>
                            <li class="list1_item"><a href="<?=$child["SECTION_PAGE_URL"]?>" class="list1_link"><?=$child["NAME"]?></a></li>
                            <?
                        }
                    }
                    ?>
                </ul>  
            </div>
            <?
        }
        ?>
        </div>
        <?
		//}
    }    
    
	/*$TOP_DEPTH = $arResult["SECTION"]["DEPTH_LEVEL"];
	$CURRENT_DEPTH = $TOP_DEPTH;

	foreach($arResult["SECTIONS"] as $arSection)
	{
		$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
		$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')));
		if($CURRENT_DEPTH < $arSection["DEPTH_LEVEL"])
		{
			echo "\n",str_repeat("\t", $arSection["DEPTH_LEVEL"]-$TOP_DEPTH),"<ul>";
		}
		elseif($CURRENT_DEPTH == $arSection["DEPTH_LEVEL"])
		{
			echo "</li>";
		}
		else
		{
			while($CURRENT_DEPTH > $arSection["DEPTH_LEVEL"])
			{
				echo "</li>";
				echo "\n",str_repeat("\t", $CURRENT_DEPTH-$TOP_DEPTH),"</ul>","\n",str_repeat("\t", $CURRENT_DEPTH-$TOP_DEPTH-1);
				$CURRENT_DEPTH--;
			}
			echo "\n",str_repeat("\t", $CURRENT_DEPTH-$TOP_DEPTH),"</li>";
		}

		$count = $arParams["COUNT_ELEMENTS"] && $arSection["ELEMENT_CNT"] ? "&nbsp;(".$arSection["ELEMENT_CNT"].")" : "";

		if ($_REQUEST['SECTION_ID']==$arSection['ID'])
		{
			$link = '<b>'.$arSection["NAME"].$count.'</b>';
			$strTitle = $arSection["NAME"];
		}
		else
		{
			$link = '<a href="'.$arSection["SECTION_PAGE_URL"].'">'.$arSection["NAME"].$count.'</a>';
		}

		echo "\n",str_repeat("\t", $arSection["DEPTH_LEVEL"]-$TOP_DEPTH);
		?><li id="<?=$this->GetEditAreaId($arSection['ID']);?>"><?=$link?><?

		$CURRENT_DEPTH = $arSection["DEPTH_LEVEL"];
	}

	while($CURRENT_DEPTH > $TOP_DEPTH)
	{
		echo "</li>";
		echo "\n",str_repeat("\t", $CURRENT_DEPTH-$TOP_DEPTH),"</ul>","\n",str_repeat("\t", $CURRENT_DEPTH-$TOP_DEPTH-1);
		$CURRENT_DEPTH--;
	}*/
	?>
    </div>
</div>


