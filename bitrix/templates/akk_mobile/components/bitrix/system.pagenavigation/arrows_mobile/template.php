<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$ClientID = 'navigation_'.$arResult['NavNum'];

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
?>
<div class="nav_pager">	
		<div class="nav_pager__res">		
			<?echo GetMessage("results")?> 1 - <?=$arResult["NavLastRecordShow"]?> <?=GetMessage("nav_from")?> <?=$arResult["NavRecordCount"]?>
		</div>
	</div>
	<?
	$sNextHref = '';
	if ($arResult["NavPageNomer"] < $arResult["NavPageCount"])
	{
		$bNextDisabled = false;
		$sNextHref = $arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["NavPageNomer"]+1);
	}
	else
	{
		$bNextDisabled = true;
	}
	?>
	<?if (!$bNextDisabled):?>
		<a class="btn btn_common green-button show-more" id="ajaxload" data-url="<?=$sNextHref;?>" data-showingResults="<?=$arResult["NavLastRecordShow"]?>" data-allResults="<?=$arResult["NavRecordCount"]?>" style="width:100%"><?=GetMessage("next")?></a>
	<?endif;?>