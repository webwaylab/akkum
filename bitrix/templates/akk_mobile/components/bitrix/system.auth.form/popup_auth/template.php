<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'])
	ShowMessage($arResult['ERROR_MESSAGE']);
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<?if($arResult['FORM_TYPE'] == "login"):?>
<a href="javascript:" class="btn iconed ico_login btn_login"><span><?=Loc::getMessage('AUTH_LOGIN_BUTTON')?></span></a>
<div id="dialogLogin" class="dialogContent">
	<form class='login-form'>
		<div class="error"></div>
		<fieldset>
			<ul>
				<li><input type="text" name="email" value="" placeholder="<?=Loc::getMessage('LOGIN_PLACEHOLDER')?>"></li>
				<li><input type="password" name="password" value="" placeholder="<?=Loc::getMessage('PASS')?>"></li>
				<li><a href="" class="btn_fogot_pass"><?=Loc::getMessage('AUTH_FORGOT_PASSWORD_2')?></a></li>
				<li><input type="submit" name="" value="<?=Loc::getMessage('AUTH_LOGIN_BUTTON_BIG')?>" class="full-width"></li>
			</ul>
		</fieldset>
	</form>
	<div class="more">
		<p>Еще не зарегистрированы?</p>
		<a href="" class="btn btn_o full-width btn_reg"><?=Loc::getMessage('AUTH_REGISTER')?></a>
	</div>
</div>
<div id="dialogReg" class="dialogContent">
	<form class = 'register-form'>
		<div class="error"></div>
		<fieldset>
			<ul>
				<li><input type="text" name="name" value="" placeholder="<?=Loc::getMessage('NAME')?>"></li>
				<li><input type="text" name="email" value="" placeholder="<?=Loc::getMessage('EMAIL')?>"></li>
				<li><input type="password" name="password" value="" placeholder="<?=Loc::getMessage('PASS')?>"></li>
				<li><input type="text" name="phone" value="" data-masked="+7(999)999-99-99" placeholder="<?=Loc::getMessage('PHONE')?>"></li>
				<li><input type="submit" name="" value="<?=Loc::getMessage('AUTH_REGISTER')?>" class="full-width"></li>
			</ul>
		</fieldset>
	</form>
	<div class="more">
		<p><?=Loc::getMessage('REGISTRED')?></p>
		<a href="" class="btn btn_o full-width btn_login"><?=Loc::getMessage('AUTH_LOGIN_BUTTON_BIG')?></a>
	</div>
</div>
<div id="dialogFogotPass" class="dialogContent">
	<p><?=Loc::getMessage('RECOVERY_TEXT')?></p>
	<form class="recovery-form">
		<div class="error"></div>
		<fieldset>
			<ul>
				<li><input type="text" name="email" value="" placeholder="<?=Loc::getMessage('EMAIL')?>"></li>
				<li><input type="submit" name="" value="<?=Loc::getMessage('NEW_PASS')?>" class="full-width"></li>
			</ul>
		</fieldset>
	</form>
	<div class="more">
		<p><?=Loc::getMessage('REMEMBERED')?></p>
		<a href="" class="btn btn_o full-width btn_login"><?=Loc::getMessage('AUTH_LOGIN_BUTTON_BIG')?></a>
	</div>
</div>
<?else:?>
	<a href="<?=$arParams['PROFILE_URL']?>" class="btn iconed ico_login"><span><?=Loc::getMessage('AUTH_PROFILE')?></span></a>
<?endif;?>