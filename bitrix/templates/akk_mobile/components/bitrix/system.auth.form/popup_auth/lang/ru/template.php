<?
$MESS["AUTH_LOGIN_BUTTON"] = "Войти";
$MESS["AUTH_LOGIN_BUTTON_BIG"] = "ВОЙТИ";
$MESS["AUTH_LOGIN"] = "Логин";
$MESS["AUTH_PASSWORD"] = "Ваш пароль";
$MESS["AUTH_REMEMBER_ME"] = "Запомнить меня на этом компьютере";
$MESS["AUTH_FORGOT_PASSWORD_2"] = "Забыли пароль?";
$MESS["AUTH_REGISTER"] = "Регистрация";
$MESS["AUTH_LOGOUT_BUTTON"] = "Выйти";
$MESS["AUTH_PROFILE"] = "Личный кабинет";
$MESS["AUTH_REMEMBER_SHORT"] = "Запомнить меня";
$MESS["AUTH_SECURE_NOTE"]="Перед отправкой формы авторизации пароль будет зашифрован в браузере. Это позволит избежать передачи пароля в открытом виде.";
$MESS["AUTH_NONSECURE_NOTE"]="Пароль будет отправлен в открытом виде. Включите JavaScript в браузере, чтобы зашифровать пароль перед отправкой.";
$MESS["auth_form_comp_otp"] = "Одноразовый пароль:";
$MESS["auth_form_comp_otp_remember_title"] = "Запомнить код на этом компьютере";
$MESS["auth_form_comp_otp_remember"] = "Запомнить код";
$MESS["auth_form_comp_auth"] = "Авторизация";
$MESS['REGISTRED'] = 'Уже зарегистрированы?';
$MESS['RECOVERY_TEXT'] = 'На указанный вами адрес электронной почты будет выслана инструкция по восстановлению пароля';
$MESS['REMEMBERED'] = 'Вспомнили пароль?';
$MESS['NAME'] = 'Ваше имя';
$MESS['EMAIL'] = 'Ваш адрес эл.почты';
$MESS['PASS'] = 'Ваш пароль';
$MESS['PHONE'] = 'Ваш номер телефона';
$MESS['LOGIN_PLACEHOLDER'] = 'Ваш логин или почта';
$MESS['NEW_PASS'] = 'Сбросить пароль';
?>