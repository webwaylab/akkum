$(function(){ 
    var dialogLogin = $('#dialogLogin').dialog({
        border: 0,
        autoOpen: false,
        resizable: false,
        height: 'auto',
        maxWidth: 280,
        modal: true,
        title: 'Вход',
        closeText: 'Закрыть',
        create: function(event, ui) {
            var widget = $(this).dialog("widget");
            $(".ui-dialog-titlebar-close", widget).html('<span class="custom-popup__hide"></span>');
        }
    });
    var dialogReg = $('#dialogReg').dialog({
        border: 0,
        autoOpen: false,
        resizable: false,
        height: 'auto',
        maxWidth: 280,
        modal: true,
        title: 'Регистрация',
        closeText: 'Закрыть',
        create: function(event, ui) {
            var widget = $(this).dialog("widget");
            $(".ui-dialog-titlebar-close", widget).html('<span class="custom-popup__hide"></span>');
        }
    });
    var dialogFogotPass = $('#dialogFogotPass').dialog({
        border: 0,
        autoOpen: false,
        resizable: false,
        height: 'auto',
        maxWidth: 280,
        modal: true,
        title: 'Восстановление пароля',
        closeText: 'Закрыть',
        create: function(event, ui) {
            var widget = $(this).dialog("widget");
            $(".ui-dialog-titlebar-close", widget).html('<span class="custom-popup__hide"></span>');
        }
    });

    $(".btn_login").on('click', function (e) {
       e.preventDefault();
       dialogReg.dialog("close");
       dialogFogotPass.dialog("close");
       dialogLogin.dialog("open");
    });
    $(".btn_reg").on('click', function (e) {
       e.preventDefault();
       dialogLogin.dialog("close");
       dialogReg.dialog("open");
    });
    $(".btn_fogot_pass").on('click', function (e) {
       e.preventDefault();
       dialogLogin.dialog("close");
       dialogFogotPass.dialog("open");
    });

});