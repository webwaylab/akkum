<?php
$MESS["CT_BCS_CATALOG_BTN_DIMENSIONS"] = "Габариты: ";
$MESS["CT_BCS_CATALOG_BTN_CAPACITY"] = "Ёмкость: ";
$MESS["CT_BCS_CATALOG_BTN_AMPERS"] = " А/ч";
$MESS["CT_BCS_CATALOG_BTN_EXCHANGE"] = "при обмене";
$MESS["CT_BCS_CATALOG_BTN_ALL"] = "Показать все";

$MESS["INRUSH_CURRENT"] = "Пусковой ток: ";
$MESS["INRUSH_CURRENT_VALUE"] = " А";
$MESS["CT_BCS_TPL_MESS_BTN_BUY_CLICK"] = "Купить в 1 клик";

$MESS["VOLUME"] = "Объём: ";
$MESS["VOLUME_VALUE"] = "л.";
$MESS["VISCOSITY"] = "Вязкость: ";
$MESS["TYPE_VALUE"] = "Тип: ";
$MESS["MANUFACTURER"] = "Производитель: ";