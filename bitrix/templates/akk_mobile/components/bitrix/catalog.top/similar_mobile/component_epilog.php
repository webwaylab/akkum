<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader;

global $APPLICATION;
switch ($arParams['VIEW_MODE']) {
    case 'BANNER':
        $APPLICATION->AddHeadScript($templateFolder . '/banner/script.js');
    case 'SLIDER':
        $APPLICATION->AddHeadScript($templateFolder . '/slider/script.js');
        break;
    case 'SECTION':
    default:
        $APPLICATION->AddHeadScript($templateFolder . '/section/script.js');
        break;
}
if (isset($templateData['TEMPLATE_LIBRARY']) && !empty($templateData['TEMPLATE_LIBRARY'])) {
    $loadCurrency = false;
    if (!empty($templateData['CURRENCIES'])) {
        $loadCurrency = Loader::includeModule('currency');
    }
    CJSCore::Init($templateData['TEMPLATE_LIBRARY']);
    if ($loadCurrency) {
        ?>
        <script type="text/javascript">
            BX.Currency.setCurrencies(<? echo $templateData['CURRENCIES']; ?>);
        </script>
        <?
    }
}
?>