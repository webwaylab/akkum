<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
global $APPLICATION; ?>

<? foreach ($arResult['ITEMS'] as $key => $arItem):
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
    $strMainID = $this->GetEditAreaId($arItem['ID']);

    $arItemIDs = array(
        'ID' => $strMainID,
        'PICT' => $strMainID.'_pict',
        'SECOND_PICT' => $strMainID.'_secondpict',
        'MAIN_PROPS' => $strMainID.'_main_props',

        'QUANTITY' => $strMainID.'_quantity',
        'QUANTITY_DOWN' => $strMainID.'_quant_down',
        'QUANTITY_UP' => $strMainID.'_quant_up',
        'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
        'BUY_LINK' => $strMainID.'_buy_link',
        'BASKET_ACTIONS' => $strMainID.'_basket_actions',
        'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
        'SUBSCRIBE_LINK' => $strMainID.'_subscribe',
        'COMPARE_LINK' => $strMainID.'_compare_link',

        'PRICE' => $strMainID.'_price',
        'DSC_PERC' => $strMainID.'_dsc_perc',
        'SECOND_DSC_PERC' => $strMainID.'_second_dsc_perc',

        'PROP_DIV' => $strMainID.'_sku_tree',
        'PROP' => $strMainID.'_prop_',
        'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
        'BASKET_PROP_DIV' => $strMainID.'_basket_prop'
    );

    $strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
    $productTitle = (
        isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])&& $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
        ? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
        : $arItem['NAME']
    );
    $imgTitle = (
        isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
        ? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
        : $arItem['NAME']
    );
    $changePrice = $arItem["PROPERTIES"]["PRICE_CHANGE"]["VALUE"]; ?>

    <div>
        <div class="catalog__item product_item" data-href="<?=$arItem["DETAIL_PAGE_URL"]?>" id="<? echo $strMainID; ?>">
            <div class="catalog__icons">
                <?if($arItem["PROPERTIES"]["SALELEADER"]["VALUE"]=="да"):?>
                    <i><img src="<?= SITE_TEMPLATE_PATH . '/images/ico_class.png' ?>"
                            alt="<?=$arItem["PROPERTIES"]["SALELEADER"]["NAME"];?>" /></i>
                <?endif;?>
                <?if($arItem["PROPERTIES"]["FREESHIPPING"]["VALUE"]=="да"):?>
                    <i><img src="<?= SITE_TEMPLATE_PATH . '/images/ico_delivery.svg' ?>"
                            alt="<?=$arItem["PROPERTIES"]["FREESHIPPING"]["NAME"];?>" /></i>
                <?endif;?>
                <?if($arItem["PROPERTIES"]["SPECIALOFFER"]["VALUE"]=="да"):?>
                    <i><img src="<?= SITE_TEMPLATE_PATH . '/images/ico_gift.svg' ?>"
                            alt="<?=$arItem["PROPERTIES"]["SPECIALOFFER"]["NAME"];?>" /></i>
                <?endif;?>
            </div>

            <figure>
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                    <img src="<? echo $arItem['PREVIEW_PICTURE']['SRC']; ?>" alt="" id="<? echo $arItemIDs['SECOND_PICT']; ?>"/>
                </a>
            </figure>

            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="catalog__item-title"><?=$arItem['NAME']?></a>

            <div class="catalog__item-price" id="<? echo $arItemIDs['PRICE']; ?>">
                <? if(!empty($changePrice)):?>
                <span class="catalog__item-prices">
                                <span class="catalog__price"><?=number_format($changePrice, 0, "", " ")?>&nbsp;<span class="ruble">руб.</span> <em>
												<?if(strstr($arItem["DETAIL_PAGE_URL"], "/catalog/battery/")):?>
													при обмене
												<?else:?>
													по купону
												<?endif;?>
								</em></span>
                    <? endif;

                    if (!empty($arItem['MIN_PRICE'])):
                        if ('N' == $arParams['PRODUCT_DISPLAY_MODE'] && isset($arItem['OFFERS']) && !empty($arItem['OFFERS'])):?>
                            <span class="catalog__price catalog__price_old">
                                    <?=  GetMessage(
                                        'CT_BCS_TPL_MESS_PRICE_SIMPLE_MODE',
                                        array(
                                            '#PRICE#' => $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'],
                                            '#MEASURE#' => GetMessage(
                                                'CT_BCS_TPL_MESS_MEASURE_SIMPLE_MODE',
                                                array(
                                                    '#VALUE#' => $arItem['MIN_PRICE']['CATALOG_MEASURE_RATIO'],
                                                    '#UNIT#' => $arItem['MIN_PRICE']['CATALOG_MEASURE_NAME']
                                                )
                                            )
                                        )
                                    ); ?>
                                </span>
                        <? else: ?>
                            <span class="catalog__price catalog__price_old"><?= number_format($arItem['MIN_PRICE']['DISCOUNT_VALUE_VAT'], 0, "", " ") ?>&nbsp;<span class="ruble">руб.</span></span>
                        <? endif;
                    endif;

                    if(!empty($changePrice)): ?>
                            </span>
            <? endif; ?>

                <?if ($arItem['CAN_BUY']):?>
                    <input type="hidden" class="bx_col_input"
                           id="<? echo $arItemIDs['QUANTITY']; ?>"
                           name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>"
                           value="<? echo $arItem['CATALOG_MEASURE_RATIO']; ?>">

                    <span id="<? echo $arItemIDs['BUY_LINK']; ?>" class="btn" rel="nofollow"><?= GetMessage('CT_BCS_TPL_MESS_BTN_BUY') ?></span>

                    <div id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>" class="bx_catalog_item_controls_blocktwo"></div>
                <?endif;?>
            </div>

            <div class="catalog__item-meta">
                <? //определение родителя
                $root_item = "";
                if(strstr($arItem['DETAIL_PAGE_URL'], '/masla/'))
                    $root_item = "masla";
                elseif(strstr($arItem['DETAIL_PAGE_URL'], '/battery/'))
                    $root_item = "battery";
                elseif(strstr($arItem['DETAIL_PAGE_URL'], '/accessories/'))
                    $root_item = "accessories";

                $nav = CIBlockSection::GetNavChain(false,$arItem['~IBLOCK_SECTION_ID']);
                $arSectionPath = $nav->GetNext();?>
                <?if($root_item == "battery"):?>
                    <?if($arItem['PROPERTIES']["CAPACITY"]['VALUE']):?>
                        <div><?= GetMessage('CT_BCS_CATALOG_BTN_CAPACITY') . $arItem['PROPERTIES']["CAPACITY"]['VALUE'] . GetMessage('CT_BCS_CATALOG_BTN_AMPERS')?></div>
                    <?endif;?>

                    <?if($arItem['PROPERTIES']["START_TOK"]['VALUE']):?>
                        <div><?= GetMessage('INRUSH_CURRENT') . $arItem['PROPERTIES']["START_TOK"]['VALUE'] . GetMessage('INRUSH_CURRENT_VALUE')?></div>
                    <?endif;?>

                    <?if($arItem['PROPERTIES']["POLARITY"]['VALUE']):?>
                        <div><?=$arItem['PROPERTIES']["POLARITY"]['NAME']?>: <?=$arItem['PROPERTIES']["POLARITY"]['VALUE']?></div>
                    <?endif;?>

                    <?if (!empty($arItem['PROPERTIES']["LENGTH"]['VALUE']) &&
                        !empty($arItem['PROPERTIES']["WIDTH"]['VALUE']) &&
                        !empty($arItem['PROPERTIES']["HEIGHT"]['VALUE'])):?>
                        <div>
                            <?= GetMessage('CT_BCS_CATALOG_BTN_DIMENSIONS')
                            . $arItem['PROPERTIES']["LENGTH"]['VALUE'] . '×'
                            . $arItem['PROPERTIES']["WIDTH"]['VALUE'] . '×'
                            . $arItem['PROPERTIES']["HEIGHT"]['VALUE']?>
                        </div>
                    <?endif;?>
                <?endif;?>

                <?if($root_item == "accessories"):?>
                    <?if($arItem['PROPERTIES']["ARTNUMBER"]['VALUE']):?>
                        <div><?=$arItem['PROPERTIES']["ARTNUMBER"]['NAME']?>: <?=$arItem['PROPERTIES']["ARTNUMBER"]['VALUE']?></div>
                    <?endif;?>

                    <?if($arItem['PROPERTIES']["MANUFACTURE_2"]['VALUE']):?>
                        <div><?=$arItem['PROPERTIES']["MANUFACTURE_2"]['NAME']?>: <?=$arItem['PROPERTIES']["MANUFACTURE_2"]['VALUE']?></div>
                    <?endif;?>
                <?endif;?>

                <?if($root_item == "masla"):?>
                    <?if($arItem['PROPERTIES']["ARTNUMBER"]['VALUE']):?>
                        <div><?=$arItem['PROPERTIES']["ARTNUMBER"]['NAME']?>: <?=$arItem['PROPERTIES']["ARTNUMBER"]['VALUE']?></div>
                    <?endif;?>

                    <?if($arItem['PROPERTIES']["OBEM_UPAKOVKI_L_MASLO"]['VALUE']):?>
                        <div><?= GetMessage('VOLUME') . $arItem['PROPERTIES']["OBEM_UPAKOVKI_L_MASLO"]['VALUE'] . GetMessage('VOLUME_VALUE')?></div>
                    <?endif;?>

                    <?if($arItem['PROPERTIES']["KLASS_VYAZKOSTI_SAE_MASLO"]['VALUE']):?>
                        <div><?= GetMessage('VISCOSITY') . $arItem['PROPERTIES']["KLASS_VYAZKOSTI_SAE_MASLO"]['VALUE']?></div>
                    <?endif;?>

                    <?if($arItem['PROPERTIES']["TIP_MASLO"]['VALUE']):?>
                        <div><?= GetMessage('TYPE_VALUE') . $arItem['PROPERTIES']["TIP_MASLO"]['VALUE']?></div>
                    <?endif;?>

                    <?if($arItem['PROPERTIES']["PROIZVODITEL_MASLO"]['VALUE']):?>
                        <div><?= GetMessage('MANUFACTURER') . $arItem['PROPERTIES']["PROIZVODITEL_MASLO"]['VALUE']?></div>
                    <?endif;?>
                <?endif;?>
            </div>

            <?if ($arItem['CAN_BUY']):?>
                <a href="#" class="catalog__quickbuy btn btn_o"
                   data-flying-hint="#form-oneclick_<?=$arItem["ID"]?>"><?= GetMessage('CT_BCS_TPL_MESS_BTN_BUY_CLICK') ?></a>
            <?endif;?>

            <div class="flying-hint form-oneclick" id="form-oneclick_<?=$arItem["ID"]?>">
                <div class="container">
                    <div class="flying-hint__wrap">
                        <i class="flying-hint__hide js-flying-hint-hide"></i>
                        <div class="flying-hint__body">
                            <h3>Введите номер телефона:</h3>
                            <form class="phone_order_form_ one-click">
                                <input type="hidden" name="PRODUCT_ID" value="<?=$arItem["ID"]?>"/>
                                <div class="error"></div>
                                <fieldset><ul>
                                        <li>
                                            <input type="text" name="PERSONAL_PHONE" value="" data-masked="+7(999)999-99-99" placeholder="+7(___)___-__-__" />
                                        </li>
                                    </ul></fieldset>
                                <input type="submit" name="" value="Купить" />
                            </form>
                        </div>
                    </div>
                    <div class="flying-hint flying-hint__wrap order_success_message">
                        <i class="flying-hint__hide js-flying-hint-hide"></i>
                        <div class="flying-hint__body">
                            <p><img src="<?=SITE_TEMPLATE_PATH?>/images/done_form_icon_green.png" height="33" width="36" alt=""></p>
                            <h4 class="bold">Ваш заказ принят.</h4>
                            Ожидайте звонка. Спасибо!
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <? $showSubscribeBtn = false;
        $compareBtnMessage = ($arParams['MESS_BTN_COMPARE'] != '' ? $arParams['MESS_BTN_COMPARE'] : GetMessage('CT_BCS_TPL_MESS_BTN_COMPARE'));
        if (!isset($arItem['OFFERS']) || empty($arItem['OFFERS'])):
            $emptyProductProperties = empty($arItem['PRODUCT_PROPERTIES']);
            if ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET'] && !$emptyProductProperties): ?>
                <div id="<? echo $arItemIDs['BASKET_PROP_DIV']; ?>" style="display: none;">
                    <? if (!empty($arItem['PRODUCT_PROPERTIES_FILL']))
                    {
                        foreach ($arItem['PRODUCT_PROPERTIES_FILL'] as $propID => $propInfo): ?>
                            <input type="hidden" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo htmlspecialcharsbx($propInfo['ID']); ?>">
                            <?
                            if (isset($arItem['PRODUCT_PROPERTIES'][$propID]))
                            {
                                unset($arItem['PRODUCT_PROPERTIES'][$propID]);
                            }
                        endforeach;
                    }
                    $emptyProductProperties = empty($arItem['PRODUCT_PROPERTIES']);
                    if (!$emptyProductProperties): ?>
                        <table>
                            <? foreach ($arItem['PRODUCT_PROPERTIES'] as $propID => $propInfo): ?>
                                <tr>
                                    <td><? echo $arItem['PROPERTIES'][$propID]['NAME']; ?></td>
                                    <td>
                                        <? if('L' == $arItem['PROPERTIES'][$propID]['PROPERTY_TYPE'] && 'C' == $arItem['PROPERTIES'][$propID]['LIST_TYPE']):
                                            foreach($propInfo['VALUES'] as $valueID => $value): ?>
                                                <label>
                                                    <input type="radio"
                                                           name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]"
                                                           value="<? echo $valueID; ?>"
                                                        <?= ($valueID == $propInfo['SELECTED'] ? '"checked"' : ''); ?>>

                                                    <? echo $value; ?>
                                                </label>
                                            <? endforeach;
                                        else: ?>
                                            <select name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]">
                                                <? foreach($propInfo['VALUES'] as $valueID => $value): ?>
                                                    <option value="<? echo $valueID; ?>"
                                                        <? echo ($valueID == $propInfo['SELECTED'] ? 'selected' : ''); ?>>

                                                        <? echo $value; ?>
                                                    </option>
                                                <? endforeach;?>
                                            </select>
                                        <? endif; ?>
                                    </td>
                                </tr>
                            <? endforeach; ?>
                        </table>
                    <? endif; ?>
                </div>
            <? endif;
        endif; ?>

        <? $arJSParams = array(
            'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
            'SHOW_QUANTITY' => ($arParams['USE_PRODUCT_QUANTITY'] == 'Y'),
            'SHOW_ADD_BASKET_BTN' => false,
            'SHOW_BUY_BTN' => true,
            'SHOW_ABSENT' => true,
            'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
            'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
            'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
            'PRODUCT' => array(
                'ID' => $arItem['ID'],
                'NAME' => $productTitle,
                'PICT' => ('Y' == $arItem['SECOND_PICT'] ? $arItem['PREVIEW_PICTURE_SECOND'] : $arItem['PREVIEW_PICTURE']),
                'CAN_BUY' => $arItem["CAN_BUY"],
                'SUBSCRIPTION' => ('Y' == $arItem['CATALOG_SUBSCRIPTION']),
                'CHECK_QUANTITY' => $arItem['CHECK_QUANTITY'],
                'MAX_QUANTITY' => $arItem['CATALOG_QUANTITY'],
                'STEP_QUANTITY' => $arItem['CATALOG_MEASURE_RATIO'],
                'QUANTITY_FLOAT' => is_double($arItem['CATALOG_MEASURE_RATIO']),
                'SUBSCRIBE_URL' => $arItem['~SUBSCRIBE_URL'],
                'BASIS_PRICE' => $arItem['MIN_BASIS_PRICE']
            ),
            'VISUAL' => array(
                'ID' => $arItemIDs['ID'],
                'PICT_ID' => ('Y' == $arItem['SECOND_PICT'] ? $arItemIDs['SECOND_PICT'] : $arItemIDs['PICT']),
                'QUANTITY_ID' => $arItemIDs['QUANTITY'],
                'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
                'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
                'PRICE_ID' => $arItemIDs['PRICE'],
                'BUY_ID' => $arItemIDs['BUY_LINK'],
                'BASKET_PROP_DIV' => $arItemIDs['BASKET_PROP_DIV'],
                'BASKET_ACTIONS_ID' => $arItemIDs['BASKET_ACTIONS'],
                'NOT_AVAILABLE_MESS' => $arItemIDs['NOT_AVAILABLE_MESS'],
                'COMPARE_LINK_ID' => $arItemIDs['COMPARE_LINK']
            ),
            'BASKET' => array(
                'ADD_PROPS' => ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET']),
                'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
                'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
                'EMPTY_PROPS' => $emptyProductProperties,
                'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
                'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
            ),
            'LAST_ELEMENT' => $arItem['LAST_ELEMENT']
        );
        if ($arParams['DISPLAY_COMPARE'])
        {
            $arJSParams['COMPARE'] = array(
                'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
                'COMPARE_PATH' => $arParams['COMPARE_PATH']
            );
        } ?>

        <script type="text/javascript">
            var <? echo $strObName; ?> = new JCCatalogTopSection(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
        </script>
    </div>
<? endforeach; ?>