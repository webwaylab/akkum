<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arResult["THEME"] = COption::GetOptionString("main", "wizard_eshop_adapt_theme_id", "blue", SITE_ID);

global $USER, $APPLICATION;

//AJAX
if($_REQUEST["action"]=="uploadImage") 
{
    $APPLICATION->RestartBuffer();
    if (!defined('PUBLIC_AJAX_MODE')) 
    {
        define('PUBLIC_AJAX_MODE', true);
    }
    //header('Content-type: application/json');
    
    global $USER;
    $uid = $USER->GetID();

    $rsUser = CUser::GetByID($uid);
    $arUser = $rsUser->Fetch();

    $formats = array(".jpg", ".gif", ".png");
	$allow=0;
	//проверяем формат изображения
	foreach ($formats as $item) 
    {
		if(preg_match("/$item\$/i", $_FILES['myfile']['name'])) {
			$allow=$allow+1;
		}
	}

	if($allow==0)
    {
	   $msg.="Формат изображения не соответствует. ";
    }

	if($_FILES["myfile"]["size"] > 1024*10*1024)
    {
	   $msg.="Размер файла превышает 3МБ. ";
    }
	if($msg=="")
    {
		$uploaddir = '../../upload/';
		$uploadfile = $uploaddir . basename($_FILES['myfile']['name']);
		if (copy($_FILES['myfile']['tmp_name'], $uploadfile)) 
        {
            $arFile = CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT'].'/upload/'.$_FILES["myfile"]["name"]);
            $arFile['del'] = "Y";
            $arFile['old_file'] = $arUser['PERSONAL_PHOTO'];
            $arFile["MODULE_ID"] = "main";
            $fields['PERSONAL_PHOTO'] = $arFile;

            $user = new CUser;
            $user->Update($uid, $fields);
            $strError .= $user->LAST_ERROR;
            if(empty($strError))
            {
                $rsUser = CUser::GetByID($uid);
                $arUser = $rsUser->Fetch();

                echo CFile::GetPath($arUser["PERSONAL_PHOTO"]);
                $updated=false;
            }
		}
		else{
			$updated=false;
		}
        unlink($_SERVER['DOCUMENT_ROOT'].'/upload/'.$_FILES["myfile"]["name"]);
	}
	else{
		$updated=false;
	}
    
    require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');
    die();
}

$rsUser = CUser::GetByID($USER->GetID());
$arUser = $rsUser->Fetch();
$arResult["arUser"] = $arUser;
?>

<script src="<?=SITE_TEMPLATE_PATH?>/ajaxupload.js"></script>
<script type="text/javascript">
$(function(){
    var button = $(".logo-img"), interval;
	$.ajax_upload(button, {
		action : '<?=$GLOBALS["APPLICATION"]->GetCurDir()?>?action=uploadImage',
		name : 'myfile',
		onSubmit : function(file, ext) {
            this.disable();
		},
		onComplete : function(file, response) {
            if(response!="")
            {
                $(".logo-img").attr("src", response);
            }else{
                alert("Ошибка при загрузке");
            }
            this.enable();
		}
	});
});
</script>