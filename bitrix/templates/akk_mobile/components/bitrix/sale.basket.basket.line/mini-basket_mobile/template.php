<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>

<a href="<?=$arParams['PATH_TO_BASKET']?>" class="header__btn header__cart" data-cart-seo-link="<?= str_replace('/', '_', $arParams['PATH_TO_BASKET']); ?>">
    <? if (!empty($arResult['NUM_PRODUCTS'])): ?>
        <span><?= $arResult['NUM_PRODUCTS'] ?></span>
    <? endif; ?>
</a>