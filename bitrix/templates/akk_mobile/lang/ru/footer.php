<?php
$MESS['FOOTER_DESKTOP_SITE'] = 'Полная версия сайта';
$MESS['FOOTER_PHONE'] = 'Заказать обратный звонок';
$MESS['PHONE_CALL'] = 'Обратный звонок';
$MESS['PHONE_SUCCESSFUL'] = 'Ваша заявка принята. Наш менеджер перезвонит Вам в ближайшее время и ответит на все ваши вопросы.';
$MESS['PHONE_SUCCESSFUL_TITLE'] = 'Спасибо';
$MESS['CALLBACK_FORM_NAME'] = 'Ваше имя';