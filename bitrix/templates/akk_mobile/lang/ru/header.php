<?php
$MESS['HEADER_TITLE'] = 'Каталог аккумуляторов';
$MESS['HEADER_PHONE'] = 'Заказать обратный звонок';
$MESS['HEADER_TIME'] = 'Режим работы';
$MESS['HEADER_PERSONAL_CABINET'] = 'Личный кабинет';
$MESS['HEADER_LOGIN'] = 'Войти';
$MESS['HEADER_LOGO_TEXT'] = 'АКБ';
$MESS['HEADER_QUESTION_SEARCH'] = 'Введите запрос';
$MESS['HEADER_FILTER_AUTO'] = 'Подбор по марке авто';
$MESS['HEADER_FILTER_PARAMS'] = 'Подбор по параметрам';