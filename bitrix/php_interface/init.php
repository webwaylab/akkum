<?
define("CATALOG_ID", 2);
define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/php/logs.txt");
define("DEVELOPER_IP", "");

include $_SERVER['DOCUMENT_ROOT'].'/bitrix/php_interface/functions.php';

global $SERVER_PORT,$HTTP_HOST; 
if (($pos = strpos($HTTP_HOST,':')) !== false) 
$HTTP_HOST = substr($HTTP_HOST,0,$pos); 
$SERVER_PORT = 443; 

$_SERVER["SERVER_PORT"] = $SERVER_PORT; 
$_SERVER["HTTP_HOST"] = $HTTP_HOST; 

// Классы
$sClassesPath = BX_PERSONAL_ROOT.'/php_interface/classes/';
CModule::AddAutoloadClasses(
	'',
	array(
		'CDev' => $sClassesPath.'CDev.php',
        'Csv' => $sClassesPath.'Csv.php',
		'Mobile_Detect' => $sClassesPath.'Mobile_Detect.php',
        'Mobile' => $sClassesPath.'Mobile.php',
	)
);

Mobile::isMobile();

AddEventHandler("main",'OnProlog', 'setCurrentSectioCodeBySectionCodePath');
AddEventHandler('main', 'OnEpilog', '_Check404Error', 1);
AddEventHandler('catalog', 'OnProductUpdate', 'setProductSize');
AddEventHandler('catalog', 'OnProductAdd', 'setProductSize');
AddEventHandler("main", "OnBeforeUserLogin", Array("CUserEx", "OnBeforeUserLogin"));
AddEventHandler("main", "OnBeforeUserRegister", Array("CUserEx", "OnBeforeUserRegister"));
AddEventHandler("main", "OnAfterUserUpdate", Array("CUserEx", "OnAfterUserUpdateHandler"));
AddEventHandler("main", "OnBeforeUserUpdate", Array("CUserEx", "OnBeforeUserUpdateHandler"));

function setCurrentSectioCodeBySectionCodePath()
{
     $path = explode('/', $_REQUEST['SECTION_PATH']);
     $_REQUEST['SECTION_CODE'] = array_pop($path);
}

function _Check404Error()
{
   if (defined('ERROR_404') && ERROR_404=='Y' && !defined('ADMIN_SECTION'))
   {
       GLOBAL $APPLICATION;
       $APPLICATION->RestartBuffer();
       include $_SERVER['DOCUMENT_ROOT'].'/bitrix/templates/'.SITE_TEMPLATE_ID.'/header.php';
       require ($_SERVER['DOCUMENT_ROOT'].'/404.php');
       include $_SERVER['DOCUMENT_ROOT'].'/bitrix/templates/'.SITE_TEMPLATE_ID.'/footer.php';
   }
}

function setProductSize($ID, $arFields)
{
    CModule::IncludeModule("iblock");

    $arCodes = array("LENGTH", "WIDTH", "HEIGHT");

    foreach($arCodes as $code)
    {
        if(intval($arFields[$code])>0)
            CIBlockElement::SetPropertyValueCode($ID, $code, $arFields[$code]);
    }
}

function getAkkModelArray($section_code, $type, $row_count){
	$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_MANUFACTURE", "PROPERTY_TYPE", "PREVIEW_PICTURE", "DETAIL_PICTURE");
	$arFilter = Array("IBLOCK_ID"=> IB_CATALOG, "SECTION_CODE"=> $section_code, "INCLUDE_SUBSECTIONS"=>"Y", /*"ACTIVE_DATE"=>"Y",*/ /*"ACTIVE"=>"Y",*/ "SECTION_ACTIVE"=>"Y", "PROPERTY_TYPE_VALUE"=>$type,
						array("LOGIC" => "OR",array("!PREVIEW_PICTURE"=>false, "!DETAIL_PICTURE"=>false)));
	$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);

	$modelArray = array();

	while($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields();

		if ($arFields['PROPERTY_MANUFACTURE_VALUE'] == '') continue;

		$name = mb_strtolower($arFields['PROPERTY_MANUFACTURE_VALUE'], 'UTF-8');
		$file = '/upload/brand/' . str_replace(' ', '_', $name) . '.png';

		if (empty($modelArray[$name])){
			if (file_exists($_SERVER['DOCUMENT_ROOT'] . $file)){

				$modelArray[$name] = array(
					//'origin'=> $ob,
					'PROPERTY_TYPE_ENUM_ID' => $arFields["PROPERTY_TYPE_ENUM_ID"],
					'PROPERTY_TYPE_VALUE' => $arFields["PROPERTY_TYPE_VALUE"],
					'PROPERTY_TYPE_VALUE_ID' => $arFields["PROPERTY_TYPE_VALUE_ID"],
					'PROPERTY_MANUFACTURE_VALUE' => $arFields["PROPERTY_MANUFACTURE_VALUE"],
					'PROPERTY_MANUFACTURE_VALUE_ID' => $arFields["PROPERTY_MANUFACTURE_VALUE_ID"],
					'NAME' => $arFields['PROPERTY_MANUFACTURE_VALUE'],
					'CODE' => $name,
					'FILE' => $file,
					'URL' => '/catalog/brand/' . str_replace(' ', '_', $name) . '/'
				);


				if($arFields["PREVIEW_PICTURE"]){
					$PREVIEW_PICTURE = CFile::GetById($arFields["PREVIEW_PICTURE"])->Fetch();
					//CDev::pre($PREVIEW_PICTURE);
					if($PREVIEW_PICTURE){
						$modelArray[$name]["PREVIEW_PICTURE"] = "/upload/".$PREVIEW_PICTURE["SUBDIR"]."/".$PREVIEW_PICTURE["FILE_NAME"];
						$modelArray[$name]["FILE"] = "/upload/".$PREVIEW_PICTURE["SUBDIR"]."/".$PREVIEW_PICTURE["FILE_NAME"];
					}
				}
				if($arFields["DETAIL_PICTURE"]){
					$DETAIL_PICTURE = CFile::GetById($arFields["DETAIL_PICTURE"])->Fetch();
					//CDev::pre($DETAIL_PICTURE);
					if($DETAIL_PICTURE){
						$modelArray[$name]["DETAIL_PICTURE"] = "/upload/".$DETAIL_PICTURE["SUBDIR"]."/".$DETAIL_PICTURE["FILE_NAME"];
						$modelArray[$name]["FILE"] = "/upload/".$DETAIL_PICTURE["SUBDIR"]."/".$DETAIL_PICTURE["FILE_NAME"];
					}
				}
			}
		}
	}
	ksort($modelArray);

	$_modelArray = array();
	$column_num = 0;
	$row_num = 0;
	foreach($modelArray as $modelItem){
		if($row_num == $row_count){
			$row_num  = 0;
			$column_num++;
		}
		$_modelArray[$column_num][$row_num] = $modelItem;
		$row_num++;
	}
	$modelArray = $_modelArray;
	return $modelArray;
}

class CUserEx
{
    function OnBeforeUserLogin($arFields)
    {
        $filter = Array("=EMAIL" =>$arFields["LOGIN"]);
        $rsUsers = CUser::GetList(($by="LAST_NAME"), ($order="asc"), $filter);
        if($user = $rsUsers->GetNext())
            $arFields["LOGIN"] = $user["LOGIN"];
    }

    function OnBeforeUserRegister($arFields)
    {
        $arFields["LOGIN"] = $arFields["EMAIL"];
    }

    /**
     * Приводим телефон пользователя из маски в номер
     */
    function OnBeforeUserUpdateHandler(&$arFields)
    {
        if(is_set($arFields, "PERSONAL_PHONE") && strlen($arFields["PERSONAL_PHONE"])>0)
        {
            $arFields["PERSONAL_PHONE"] = phoneToNumber($arFields["PERSONAL_PHONE"]);
        }
    }

    /**
     * При загрузке аватара уменьшаем его размер до 150х150px
     */
    function OnAfterUserUpdateHandler(&$arFields)
    {
        $imageMaxWidth = 150; // Максимальная ширина уменьшенной картинки
        $imageMaxHeight = 150; // Максимальная высота уменьшенной картинки

        $rsUser = CUser::GetByID($arFields["ID"]);
        $arUser = $rsUser->Fetch();

        if(intval($arUser["PERSONAL_PHOTO"])>0)
        {
            $arFile = CFile::GetFileArray($arUser["PERSONAL_PHOTO"]);

            // проверяем, что файл является картинкой
            if (!CFile::IsImage($arFile["FILE_NAME"]))
                continue;

            // Если размер больше допустимого
            if ($arFile["WIDTH"] > $imageMaxWidth || $arFile["HEIGHT"] > $imageMaxHeight)
            {
                // Временная картинка
                $tmpFilePath = $_SERVER['DOCUMENT_ROOT']."/upload/tmp/".$arFile["FILE_NAME"];

                // Уменьшаем картинку
                $resizeRez = CFile::ResizeImageFile( // уменьшение картинки для превью
                    $source = $_SERVER['DOCUMENT_ROOT'].$arFile["SRC"],
                    $dest = $tmpFilePath,
                    array(
                        'width' => $imageMaxWidth,
                        'height' => $imageMaxHeight
                    ),
                    $resizeType = BX_RESIZE_IMAGE_EXACT,//BX_RESIZE_IMAGE_PROPORTIONAL, // метод ресайза
                    $waterMark = array(), // водяной знак (пустой)
                    $jpgQuality = 95 // качество уменьшенной картинки в процентах
                );

                // Записываем изменение в свойство
                if ($resizeRez)
                {
                    $arNewFile = CFile::MakeFileArray($tmpFilePath);

                    $arNewFile['del'] = "Y";
                    $arNewFile['old_file'] = $arUser['PERSONAL_PHOTO'];
                    $arNewFile["MODULE_ID"] = "main";
                    $fields['PERSONAL_PHOTO'] = $arNewFile;

                    $user = new CUser;
                    $user->Update($arUser["ID"], $fields);

                    // Удалим временный файл
                    unlink($tmpFilePath);
                }
            }
        }
    }
}
//AddEventHandler("iblock", "OnStartIBlockElementUpdate", Array("WebwayClass", "OnStartIBlockElementEditHandler"));
//AddEventHandler("iblock", "OnStartIBlockElementAdd", Array("WebwayClass", "OnStartIBlockElementEditHandler"));
AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("WebwayClass", "OnAfterIBlockElementAddHandler"));
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", Array("WebwayClass", "OnAfterIBlockElementUpdateHandler"));
AddEventHandler("iblock", "OnSuccessCatalogImport1C", Array("WebwayClass", "OnSuccessCatalogImport1CHandler"));

function GetEmkostAKK($arFields){
	$res = 0;

	$emkosty = array();
	$emkost = array("MIN"=>35,"MAX"=>45,"SECTION_ID"=>"20");
	$emkosty[] = $emkost;
	$emkost = array("MIN"=>50,"MAX"=>58,"SECTION_ID"=>"22");
	$emkosty[] = $emkost;
	$emkost = array("MIN"=>60,"MAX"=>68,"SECTION_ID"=>"60");
	$emkosty[] = $emkost;
	$emkost = array("MIN"=>70,"MAX"=>77,"SECTION_ID"=>"98");
	$emkosty[] = $emkost;
	$emkost = array("MIN"=>80,"MAX"=>95,"SECTION_ID"=>"66");
	$emkosty[] = $emkost;
	$emkost = array("MIN"=>100,"MAX"=>110,"SECTION_ID"=>"26");
	$emkosty[] = $emkost;
	foreach($arFields["PROPERTY_VALUES"]['50'] as $value){
		if(intval($value['VALUE']) == 76){
			foreach($arFields["PROPERTY_VALUES"]['52'] as $emkost){
				if($emkost['VALUE']){
					foreach($emkosty as $opr_emkost){
						$emkost['VALUE'] = intval($emkost['VALUE']);
						if($opr_emkost["MIN"]<=$emkost['VALUE'] && $emkost['VALUE']<=$opr_emkost["MAX"]){
							$res = $opr_emkost["SECTION_ID"];
						}
					}
				}
			}
		}
	}

	return $res;
}

class WebwayClass
{
	// создаем обработчик события "OnSuccessCatalogImport1C"
	function OnSuccessCatalogImport1CHandler($arParams, $arFile)
	{
		//заменяем наименования
		CModule::IncludeModule("iblock");
		$bs = new CIBlockSection;
		$arFields = Array(
			"NAME" => "Автомобильные аккумуляторы",
			"CODE" => "auto-akb",
		);
		$res = $bs->Update(453, $arFields);

		$arFields = Array(
			"NAME" => "Грузовые аккумуляторы",
			"CODE" => "truck-akb",
		);
		$res = $bs->Update(367, $arFields);

		$arFields = Array(
			"NAME" => "Мото aккумуляторы",
			"CODE" => "moto-akb",
		);
		$res = $bs->Update(137, $arFields);
	}
	
	// создаем обработчик события "OnBeforeIBlockElementUpdate"
	function OnStartIBlockElementEditHandler(&$arFields)
	{
		$res = GetEmkostAKK($arFields);
		if($res){
			$arFields["IBLOCK_SECTION"] = array(0=>$res);
		}
		/*
		if($arFields['PREVIEW_PICTURE']['old_file']){
			$arFields['PREVIEW_PICTURE'] = array(
				'name'=>'',
				'type'=>'',
				'tmp_name'=>'',
				'error'=>'4',
				'size'=>'0',
				'description'=>'',
				'MODULE_ID'=>'iblock',
				'old_file'=>$arFields['PREVIEW_PICTURE']['old_file']
			);
		}
		if($arFields['DETAIL_PICTURE']['old_file']){
			$arFields['DETAIL_PICTURE'] = array(
				'name'=>'',
				'type'=>'',
				'tmp_name'=>'',
				'error'=>'4',
				'size'=>'0',
				'description'=>'',
				'MODULE_ID'=>'iblock',
				'old_file'=>$arFields['DETAIL_PICTURE']['old_file']
			);
		}
		*/
	}

    function OnAfterIBlockElementAddHandler($arFields){
        if ($arFields['IBLOCK_ID'] == CATALOG_ID){
            updateVendor($arFields['ID']);
        }
    }

    function OnAfterIBlockElementUpdateHandler($arFields){
        if ($arFields['IBLOCK_ID'] == CATALOG_ID){
            updateVendor($arFields['ID']);
        }
    }
}
require_once(dirname(__FILE__) . '/include/qsoft/const.php');

// проверяет и заполняет свойство "производитель" у товаров
function updateVendor($id){

    if ($id > 0){
        CModule::IncludeModule("iblock");
        $el = CIBlockElement::GetList(array(), array('IBLOCK_ID' => CATALOG_ID, 'ID' => $id), false, false, array('ID' , 'NAME', 'PROPERTY_MANUFACTURE_2', 'PROPERTY_PROIZVODITEL_MASLO', 'PROPERTY_TIP_GIBRIDNYE_AKKUMULYATORY_DOP_SVOYSTVA_SPRAVOCHN', 'PROPERTY_TIP_AKKUMULYATORA_DOP_SVOYSTVA_SPRAVOCHNIKA_NOMENK', 'PROPERTY_TYPE'))->Fetch();

        // у масла производитель указан в другом свойстве, копируем в основное
        if ($el['ID'] && empty($el['PROPERTY_MANUFACTURE_2_VALUE']) && $el['PROPERTY_PROIZVODITEL_MASLO_VALUE']){
            CIBlockElement::SetPropertyValuesEx($el['ID'], CATALOG_ID, array('MANUFACTURE_2' => $el['PROPERTY_PROIZVODITEL_MASLO_VALUE'], 'MANUFACTURE' => $el['PROPERTY_PROIZVODITEL_MASLO_VALUE']));
        }

        // тип Гибридные
        if ($el['ID'] && $el['PROPERTY_TIP_GIBRIDNYE_AKKUMULYATORY_DOP_SVOYSTVA_SPRAVOCHN_VALUE']=='да'){
            CIBlockElement::SetPropertyValuesEx($el['ID'], CATALOG_ID, array('TYPE' => 149));
        }
        // тип обслуживаемые
        if ($el['ID'] && $el['PROPERTY_TIP_AKKUMULYATORA_DOP_SVOYSTVA_SPRAVOCHNIKA_NOMENK_VALUE']=='Обслуживаемый '){
            CIBlockElement::SetPropertyValuesEx($el['ID'], CATALOG_ID, array('TYPE' => 150));
        }
        // тип необслуживаемые
        if ($el['ID'] && $el['PROPERTY_TIP_AKKUMULYATORA_DOP_SVOYSTVA_SPRAVOCHNIKA_NOMENK_VALUE']=='Необслуживаемый'){
            CIBlockElement::SetPropertyValuesEx($el['ID'], CATALOG_ID, array('TYPE' => 153));
        }
    }
}

/**
 * Набор функция для отладки
 * p - просто печатаем на экран
 * pe - печатаем и завершаем выполнение страницы
 * pa - печатаем только под админом
 */
if (!function_exists('p')) {
  function p($obj) {
    echo "***<pre>";
    print_r($obj);
    echo "</pre>";
  }
}

if (!function_exists('pe')) {
  function pe($obj) {
    echo "***<pre>";
    print_r($obj);
    echo "</pre>";
    die();
  }
}

if (!function_exists('pa')) {
  function pa($obj) {
    global $USER;
    if ($USER->IsAdmin()) {
      echo "***<pre>";
      print_r($obj);
      echo "</pre>";
    }
  }
}

function AgentUpdateEmkost() {
	CModule::IncludeModule("iblock");
	$res = 0;

	$emkosty = array();
	$emkost = array("MIN"=>35,"MAX"=>45,"SECTION_ID"=>"20");
	$emkosty[] = $emkost;
	$emkost = array("MIN"=>50,"MAX"=>58,"SECTION_ID"=>"22");
	$emkosty[] = $emkost;
	$emkost = array("MIN"=>60,"MAX"=>68,"SECTION_ID"=>"60");
	$emkosty[] = $emkost;
	$emkost = array("MIN"=>70,"MAX"=>77,"SECTION_ID"=>"98");
	$emkosty[] = $emkost;
	$emkost = array("MIN"=>80,"MAX"=>95,"SECTION_ID"=>"66");
	$emkosty[] = $emkost;
	$emkost = array("MIN"=>100,"MAX"=>110,"SECTION_ID"=>"26");
	$emkosty[] = $emkost;

	$arSelect = Array("ID", "PROPERTY_CAPACITY");
	$arFilter = Array("IBLOCK_ID"=>2, "SECTION_ID"=>"453", "INCLUDE_SUBSECTIONS"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	while($arFields = $res->GetNext())
	{
		print_r($arFields);
					if($arFields['PROPERTY_CAPACITY_VALUE']){
						foreach($emkosty as $opr_emkost){
							$emkost['VALUE'] = intval($arFields['PROPERTY_CAPACITY_VALUE']);
							if($opr_emkost["MIN"]<=$emkost['VALUE'] && $emkost['VALUE']<=$opr_emkost["MAX"]){
								$res1 = $opr_emkost["SECTION_ID"];
							}
						}
					}
		print_r($res1);
	
		$el = new CIBlockElement;
		$arLoadProductArray = Array(
		  "IBLOCK_SECTION" => array(0=>$res1),          // элемент лежит в корне раздела
		);
		$res2 = $el->Update($arFields["ID"], $arLoadProductArray);	
	}
}

/*
 * I did not write this, it's not true! It's bullshit!
 * I did not write this! I did NOT! *drops the bottle*
 *
 * Oh hi, Mark.
 *
 * Редирект с корявых URL
 * вида /catalog/auto/mercedes_benz/cls_c219/cls_350_272_hp/%D0%9C%D0%BE%D1%82%D0%BE%20a%D0%BA%D0%BA%D1%83%D0%BC%D1%83%D0%BB%D1%8F%D1%82%D0%BE%D1%80%D1%8B/
 * которые конвертируются браузером в /catalog/auto/mercedes_benz/cls_c219/cls_350_272_hp/Мото%20aккумуляторы/
 */
global $APPLICATION;
$curUrl = $APPLICATION->GetCurPage();
if (preg_match('/^\/catalog\/auto\/(.+)\/(?:[а-яА-Я]+\s?[а-яa]*)\/$/u', $curUrl, $matches)) {
	// [а-яa] - ПОТОМУ ЧТО! © (в некоторых случаях буква "а" в слове "aккумуляторы" латинская, а не кириллическая
	LocalRedirect('/catalog/auto/' . $matches[1] . '/', false, '301 Moved permanently');
}