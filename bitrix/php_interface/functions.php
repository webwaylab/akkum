<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;

function phoneToNumber($phone)
{
    return str_replace(array("(", ")", "-", "+", " "), array("", "", "", "", ""), $phone);
}

function checkPhoneNumber($phone)
{
    return preg_match("/^([0-9]{11})$/", $phone);
}

// Create battery filter for car manufacturer
function getManufacturerInformation($manufacturerID)
{
    if (intval($manufacturerID) > 0) {
        if (!Loader::IncludeModule('iblock'))
            return false;

        $IBlockElement = new CIBlockElement;

        // At first get all manufacturer's models
        $modelsFilter = array('IBLOCK_ID' => 14, 'ACTIVE' => 'Y', 'PROPERTY_BRAND_ID' => $manufacturerID);
        $modelsSelect = array('ID');

        $modelsRes = $IBlockElement->GetList(array('NAME' => 'ASC'), $modelsFilter, false, false, $modelsSelect);

        $modificationsFilter = array('IBLOCK_ID' => 15, 'ACTIVE' => 'Y');
        while ($arModel = $modelsRes->Fetch()) {
            $modificationsFilter['PROPERTY_MODEL_ID'][] = $arModel['ID'];
        }
        unset($modelsFilter, $modelsSelect, $modelsRes);

        // Now get all models' modifications
        $modificationsSelect = array(
            'PROPERTY_CAPACITY',
            'PROPERTY_POLARITY',
            'PROPERTY_LENGTH',
            'PROPERTY_WIDTH',
            'PROPERTY_HEIGHT'
        );

        $modificationsRes = $IBlockElement->GetList(array('NAME' => 'ASC'), $modificationsFilter, false, false, $modificationsSelect);

        // And finally we can create the filter
        $makeFilter = false;

        $arInterval = array(
            'PROPERTY_CAPACITY',
            'PROPERTY_LENGTH',
            'PROPERTY_WIDTH',
            'PROPERTY_HEIGHT'
        );

        while ($arModification = $modificationsRes->Fetch()) {
            foreach ($arInterval as $property) {
                if (!empty($arModification[$property . '_VALUE'])) {
                    if (strpos($arModification[$property . '_VALUE'], '-') !== false) {
                        $params = explode('-', $arModification[$property . '_VALUE']);

                        $paramMin = intval($params[0]);
                        $paramMax = intval($params[1]);

                        if (($paramMin > 0)
                            && (!isset($makeFilter['>=' . $property]) || $paramMin < $makeFilter['>=' . $property])) {
                            $makeFilter['>=' . $property] = $paramMin;
                        }

                        if (($paramMax > 0)
                            && (!isset($makeFilter['<=' . $property]) || $paramMax > $makeFilter['<=' . $property])) {
                            $makeFilter['<=' . $property] = $paramMax;
                        }
                    }
                }
            }

            // Add polarity filter (could be both values)
            if (!empty($arModification['PROPERTY_POLARITY_VALUE'])) {
                if ($arModification['PROPERTY_POLARITY_VALUE'] == 'прямая') {
                    if (strpos($makeFilter['?PROPERTY_POLARITY_VALUE'], 'Обратная') === false) {
                        $makeFilter['?PROPERTY_POLARITY_VALUE'] = 'Прямая [+ -]';
                    } elseif (strpos($makeFilter['?PROPERTY_POLARITY_VALUE'], 'Прямая') === false) {
                        $makeFilter['?PROPERTY_POLARITY_VALUE'] .= ' | Прямая [+ -]';
                    }
                }

                if ($arModification['PROPERTY_POLARITY_VALUE'] == 'обратная') {
                    if (strpos($makeFilter['?PROPERTY_POLARITY_VALUE'], 'Прямая') === false) {
                        $makeFilter['?PROPERTY_POLARITY_VALUE'] = 'Обратная [- +]';
                    } elseif (strpos($makeFilter['?PROPERTY_POLARITY_VALUE'], 'Обратная') === false) {
                        $makeFilter['?PROPERTY_POLARITY_VALUE'] .= ' | Обратная [- +]';
                    }
                }
            }
        }
        unset($modificationsFilter, $modificationsSelect, $modificationsRes, $arInterval);

        // Hack to show empty catalog if there is no any property values
        if (empty($makeFilter)) {
            $makeFilter = array(
                'PROPERTY_WIDTH' => 0,
                'PROPERTY_HEIGHT' => 0
            );
        }

        return $makeFilter;
    } else {
        return false;
    }
}

// Create battery filter for car model
function getModelInformation($modelID)
{
    if (intval($modelID) > 0) {
        if (!Loader::IncludeModule('iblock'))
            return false;

        $IBlockElement = new CIBlockElement;

        // Get all models' modifications
        $modificationsFilter = array('IBLOCK_ID' => 15, 'ACTIVE' => 'Y', 'PROPERTY_MODEL_ID' => $modelID);

        $modificationsSelect = array(
            'PROPERTY_CAPACITY',
            'PROPERTY_POLARITY',
            'PROPERTY_LENGTH',
            'PROPERTY_WIDTH',
            'PROPERTY_HEIGHT'
        );

        $modificationsRes = $IBlockElement->GetList(array('NAME' => 'ASC'), $modificationsFilter, false, false, $modificationsSelect);

        // Create the filter
        $makeFilter = false;

        $arInterval = array(
            'PROPERTY_CAPACITY',
            'PROPERTY_LENGTH',
            'PROPERTY_WIDTH',
            'PROPERTY_HEIGHT'
        );

        while ($arModification = $modificationsRes->Fetch()) {
            foreach ($arInterval as $property) {
                if (!empty($arModification[$property . '_VALUE'])) {
                    if (strpos($arModification[$property . '_VALUE'], '-') !== false) {
                        $params = explode('-', $arModification[$property . '_VALUE']);

                        $paramMin = intval($params[0]);
                        $paramMax = intval($params[1]);

                        if (($paramMin > 0)
                            && (!isset($makeFilter['>=' . $property]) || $paramMin < $makeFilter['>=' . $property])) {
                            $makeFilter['>=' . $property] = $paramMin;
                        }

                        if (($paramMax > 0)
                            && (!isset($makeFilter['<=' . $property]) || $paramMax > $makeFilter['<=' . $property])) {
                            $makeFilter['<=' . $property] = $paramMax;
                        }
                    }
                }
            }

            // Add polarity filter (could be both values)
            if (!empty($arModification['PROPERTY_POLARITY_VALUE'])) {
                if ($arModification['PROPERTY_POLARITY_VALUE'] == 'прямая') {
                    if (strpos($makeFilter['?PROPERTY_POLARITY_VALUE'], 'Обратная') === false) {
                        $makeFilter['?PROPERTY_POLARITY_VALUE'] = 'Прямая [+ -]';
                    } elseif (strpos($makeFilter['?PROPERTY_POLARITY_VALUE'], 'Прямая') === false) {
                        $makeFilter['?PROPERTY_POLARITY_VALUE'] .= ' | Прямая [+ -]';
                    }
                }

                if ($arModification['PROPERTY_POLARITY_VALUE'] == 'обратная') {
                    if (strpos($makeFilter['?PROPERTY_POLARITY_VALUE'], 'Прямая') === false) {
                        $makeFilter['?PROPERTY_POLARITY_VALUE'] = 'Обратная [- +]';
                    } elseif (strpos($makeFilter['?PROPERTY_POLARITY_VALUE'], 'Обратная') === false) {
                        $makeFilter['?PROPERTY_POLARITY_VALUE'] .= ' | Обратная [- +]';
                    }
                }
            }
        }
        unset($modificationsFilter, $modificationsSelect, $modificationsRes, $arInterval);

        // Hack to show empty catalog if there is no any property values
        if (empty($makeFilter)) {
            $makeFilter = array(
                'PROPERTY_WIDTH' => 0,
                'PROPERTY_HEIGHT' => 0
            );
        }

        return $makeFilter;
    } else {
        return false;
    }
}

function getModificationInformation($id)
{
    if (!Loader::IncludeModule("iblock"))
        return false;

    $makeFilter = array();
    $arInterval = array(
        "PROPERTY_CAPACITY",
        "PROPERTY_LENGTH",
        "PROPERTY_WIDTH",
        "PROPERTY_HEIGHT"
    );

    $arSelect = array(
        "PROPERTY_CAPACITY",
        "PROPERTY_POLARITY",
        "PROPERTY_LENGTH",
        "PROPERTY_WIDTH",
        "PROPERTY_HEIGHT"
    );

    $arFilter = Array("IBLOCK_ID" => 15, 'ACTIVE' => "Y", "=ID" => intval($id));
    $res = CIBlockElement::GetList(Array("NAME" => "ASC"), $arFilter, false, false, $arSelect);
    if ($arItem = $res->GetNext()) {

        foreach ($arInterval as $property) {
            if (!empty($arItem[$property . "_VALUE"])) {
                if (strpos($arItem[$property . "_VALUE"], "-") !== false) {
                    $params = explode("-", $arItem[$property . "_VALUE"]);

                    if (intval($params[0]) != $params[1]) {
                        $makeFilter[">=" . $property] = intval($params[0]);
                        $makeFilter["<=" . $property] = intval($params[1]);
                    } else {
                        $makeFilter["=" . $property] = intval($params[0]);
                    }
                }
            }
        }

        if (!empty($arItem["PROPERTY_POLARITY_VALUE"])) {
            if ($arItem["PROPERTY_POLARITY_VALUE"] == "прямая") {
                $makeFilter["PROPERTY_POLARITY_VALUE"] = "Прямая [+ -]";
            }
            if ($arItem["PROPERTY_POLARITY_VALUE"] == "обратная") {
                $makeFilter["PROPERTY_POLARITY_VALUE"] = "Обратная [- +]";
            }
        }

        //dbg($makeFilter);

        return $makeFilter;
    } else {
        return false;
    }
}

function getCatalogSectionTree($show_deactivated = false)
{
    CModule::IncludeModule("iblock");
    /*$obCache = new CPHPCache; 
    $life_time = 60*60*24*365; //24 часа
    $cache_id = "section_tree";  
    
    if($obCache->InitCache($life_time, $cache_id, "/"))
    {
        $vars = $obCache->GetVars();
        $arSections = $vars["SECTION_TREE"];
    }
    elseif ($obCache->StartDataCache())
    {*/
    $arSections = array();
    $arFilter = array(
        'IBLOCK_ID' => CATALOG_ID,
        //'GLOBAL_ACTIVE' => "Y"
    );
    if (!$show_deactivated) {
        $arFilter["GLOBAL_ACTIVE"] = "Y";
    }
    $rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'), $arFilter, false);
    while ($arSect = $rsSect->GetNext()) {
        $arSections[$arSect["ID"]] = $arSect;
    }
    /*
    $obCache->EndDataCache(array(
        "SECTION_TREE"    => $arSections
    ));
}*/

    return $arSections;
}

function getSectionByUrl($url)
{
    CModule::IncludeModule("iblock");
    $params = explode("/", $url);
    $code = $params[count($params) - 2];

    $arFilter = array(
        'IBLOCK_ID' => CATALOG_ID,
        'GLOBAL_ACTIVE' => "Y",
        "CODE" => $code
    );
    $rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'), $arFilter, false);
    while ($arSection = $rsSect->GetNext()) {
        if ($arSection["SECTION_PAGE_URL"] == $url) {
            return $arSection;
        }
    }
    return false;
}

function getElementByUrl($url)
{
    CModule::IncludeModule("iblock");
    $params = explode("/", $url);
    $code = $params[count($params) - 2];

    $arSelect = array("ID", "DETAIL_PAGE_URL");
    $arFilter = array(
        'IBLOCK_ID' => CATALOG_ID,
        'INCLUDE_SUBSECTIONS' => "Y",
        'GLOBAL_ACTIVE' => "Y",
        "CODE" => $code
    );
    $res = CIBlockElement::GetList(Array("NAME" => "ASC"), $arFilter, false, false, $arSelect);
    while ($arItem = $res->GetNext()) {
        //echo $arItem["DETAIL_PAGE_URL"];
        if ($arItem["DETAIL_PAGE_URL"] == $url) {
            return $arItem;
        }
    }
    return false;
}

function getElementByID($id)
{
    CModule::IncludeModule("iblock");

    $res = CIBlockElement::GetByID($id);
    $ar_res = $res->GetNext();

    return $ar_res;
}

function getBrandAutoByXmlId($xmlId)
{
    CModule::IncludeModule("highloadblock");
    //use Bitrix\Main\Entity;

    $hlblock = Bitrix\Highloadblock\HighloadBlockTable::getById(2)->fetch();
    $entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
    $entity_data_class = $entity->getDataClass();

    //BRANDS
    $arFilter = array(
        "UF_XML_ID" => $xmlId
    );
    $rsData = $entity_data_class::getList(array(
        'filter' => $arFilter,
        'select' => array('*'),
        'limit' => false,
        'order' => array(
            'UF_NAME' => 'ASC'
        ),
    ));
    if ($arBrand = $rsData->Fetch()) {
        return $arBrand["UF_NAME"];
    }

    return false;
}

if (!function_exists('__d')) {
    function __d($var, $isAdmin = false, $caller = null)
    {
        global $USER;
        if (!($USER->IsAdmin() || $isAdmin)) {
            return false;
        }

        if (!isset($caller)) {
            $caller = array_shift(debug_backtrace(1));
        }
        echo '<code>File: ' . $caller['file'] . ' / Line: ' . $caller['line'] . '</code>';
        echo '<pre>';
        echo print_r($var, true);
        echo '</pre>';
    }
}

function dbg($value)
{
    while (@ob_end_clean()) {
    }
    echo "<pre>";
    print_r($value);
    echo "</pre>";
    exit();
}

?>