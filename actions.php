<?
define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
$GLOBALS['APPLICATION']->RestartBuffer();
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");

global $USER;
if(!is_object($USER))
    $USER = new CUser;

$result = array();
$result['status'] = false;
$result['message'] = '';

$mode=$_REQUEST["action"];

switch($mode)
{
    case 'callback':
        $err = 0;
        $html = "";
        $name = htmlspecialcharsbx($_POST['name']);
        $phone = phoneToNumber($_POST['phone']);

        if(strlen($name)==0)
        {
            $html.="Введите имя!<br />";
            $err++;
        }

        
        if(!checkPhoneNumber($phone))
        {
            $html.="Введите номер телефона!<br />";
            $err++;
        }
        
        if($err==0)
        {
            $arEventFields = array(
                "NAME"  => $name,
                "PHONE" => $phone
            );
            CEvent::Send('CALLBACK', SITE_ID, $arEventFields, "N");

            $result['status'] = true;
        }else{
            $result['message'] = $html;
        }
    break;
    
    case 'one-click':
        $err = 0;
        $html = "";
        $phone = phoneToNumber($_POST['PERSONAL_PHONE']);
        $PRODUCT_ID = intval($_POST["PRODUCT_ID"]);
        
        if(!checkPhoneNumber($phone))
        {
            $html.="Введите номер телефона!<br />";
            $err++;
        }
        
        if($err==0)
        {
            Add2BasketByProductID(
                $PRODUCT_ID, 
                1, 
                array(), 
                array()
            );
            
            $total = 0;
            $arSelFields = array("ID", "PRODUCT_ID", "QUANTITY", "PRICE", "NAME");
            $dbBasketItems = CSaleBasket::GetList(
            		array("ID" => "ASC"),
            		array(
            				"FUSER_ID" => CSaleBasket::GetBasketUserID(),
            				"LID" => SITE_ID,
            				"ORDER_ID" => "NULL",
                            "PRODUCT_ID" => $PRODUCT_ID
            			),
            		false,
            		false,
            		$arSelFields
            	);
            if ($arItem = $dbBasketItems->GetNext())
            {
                $productName = $arItem["NAME"];
                $total+=$arItem["PRICE"];
            }
            
            $arOrder = array(
               "LID" => SITE_ID,
               "PERSON_TYPE_ID" => 1,
               "PAYED" => "N",
               "CANCELED" => "N",
               "STATUS_ID" => "N",
               "PRICE" => $total,
               "CURRENCY" => CSaleLang::GetLangCurrency(SITE_ID),
               "USER_ID" => 3,
               "PRICE_DELIVERY" => 0,
               "PAY_SYSTEM_ID" => 1,
               "DELIVERY_ID" => 1
            );
            
            $ORDER_ID = CSaleOrder::Add($arOrder);
            $ORDER_ID = IntVal($ORDER_ID);
            
            if ($ORDER_ID>0)
            {
                CSaleBasket::Update($arItem["ID"], array("ORDER_ID"=>$ORDER_ID, "QUANTITY"=>1));
                
                $fUserID = IntVal(CSaleBasket::GetBasketUserID(True));
                CSaleBasket::OrderBasket($ORDER_ID, $fUserID, SITE_ID);
                CSaleBasket::DeleteAll($fUserID);
                
                CSaleOrderPropsValue::Add(Array(
                    "ORDER_ID" => $ORDER_ID, 
                    "ORDER_PROPS_ID" => 3, 
                    "NAME" => "Телефон", 
                    "VALUE" => $phone, 
                    "CODE" => "PHONE"
                ));

                CSaleOrderPropsValue::Add(Array(
                    "ORDER_ID" => $ORDER_ID, 
                    "ORDER_PROPS_ID" => 6, 
                    "NAME" => "Местоположение", 
                    "VALUE" => "--", 
                    "CODE" => "LOCATION"
                ));
                
                $arEventFields = array(
                    "NAME"  => $productName,
                    "PHONE" => $phone,
                    "ORDER_ID" => $ORDER_ID
                );
                CEvent::Send('ONE_CLICK', SITE_ID, $arEventFields, "N");
            }

            $result['status'] = true;
        }else{
            $result['message'] = $html;
        }
    break;

    case 'order-in-advance':
        $err = 0;
        $html = '';
        
        $name = htmlspecialcharsbx(trim($_POST['name']));
        $phone = phoneToNumber(trim($_POST['phone']));
        $productName = htmlspecialcharsbx(trim($_POST['product_name']));
        $productQuantity = htmlspecialcharsbx(trim($_POST['product_quantity']));

        if (strlen($name) == 0) {
            $html .= 'Введите имя!<br>';
            $err++;
        }
        
        if (!checkPhoneNumber($phone)) {
            $html .= 'Введите номер телефона!<br>';
            $err++;
        }

        if ($err == 0) {
            $arEventFields = array(
                'NAME' => $name,
                'PHONE' => $phone,
                'PRODUCT_NAME' => $productName,
                'PRODUCT_QUANTITY' => $productQuantity
            );
            CEvent::Send('ORDER_IN_ADVANCE', SITE_ID, $arEventFields, 'N');

            $result['status'] = true;
        } else {
            $result['message'] = $html;
        }

        break;
}

exit(json_encode($result));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>