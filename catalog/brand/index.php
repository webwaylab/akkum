<?

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Интернет-магазин «Купить аккумулятор» реализует автомобильные аккумуляторы по выгодной цене. Гарантируем качество предлагаемой продукции. Наш телефон: +7 (499) 650-55-55.");
$APPLICATION->SetTitle("Автомобильные аккумуляторы в Москве | Интернет-магазин «Купить аккумулятор»");
$filterView = "VERTICAL";
$APPLICATION->AddChainItem('Каталог аккумуляторов', '/catalog/');
$APPLICATION->AddChainItem('Автомобильные аккумуляторы', '/catalog/brand/');


CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");
global $APPLICATION, $USER, $arrFilter, $catalogSectionRoot;

$catalogSectionRoot = 18;

$arParams = array(
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "2",
		"TEMPLATE_THEME" => "green",
		"HIDE_NOT_AVAILABLE" => "N",
		"BASKET_URL" => "/personal/cart/",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/catalog/",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"SET_TITLE" => "Y",
		"ADD_SECTION_CHAIN" => "Y",
		"ADD_ELEMENT_CHAIN" => "Y",
		"SET_STATUS_404" => "Y",
		"DETAIL_DISPLAY_NAME" => "N",
		"USE_ELEMENT_COUNTER" => "Y",
		"USE_FILTER" => "Y",
		"FILTER_NAME" => "",
		"FILTER_VIEW_MODE" => "VERTICAL",
);

$arResult["VARIABLES"]["SECTION_ID"] = $catalogSectionRoot;

$arFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ACTIVE" => "Y",
		"GLOBAL_ACTIVE" => "Y",
);
if (0 < intval($arResult["VARIABLES"]["SECTION_ID"]))
{
	$arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
}
elseif ('' != $arResult["VARIABLES"]["SECTION_CODE"])
{
	$arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];
}

$obCache = new CPHPCache();
if ($obCache->InitCache(36000, serialize($arFilter), "/iblock/catalog"))
{
	$arCurSection = $obCache->GetVars();
}
elseif ($obCache->StartDataCache())
{
	$arCurSection = array();
	$dbRes = CIBlockSection::GetList(array(), $arFilter, false, array("ID", "NAME"));

	if(defined("BX_COMP_MANAGED_CACHE"))
	{
		global $CACHE_MANAGER;
		$CACHE_MANAGER->StartTagCache("/iblock/catalog");

		if ($arCurSection = $dbRes->Fetch())
		{
			$CACHE_MANAGER->RegisterTag("iblock_id_".$arParams["IBLOCK_ID"]);
		}
		$CACHE_MANAGER->EndTagCache();
	}
	else
	{
		if(!$arCurSection = $dbRes->Fetch())
			$arCurSection = array();
	}

	$obCache->EndDataCache($arCurSection);
}
if (!isset($arCurSection))
{
	$arCurSection = array();
}
?>

<?/*
<div class="clearfix">
    <div class="grid_37 prefix_2">
        <div class="wrapper catalog_nav_wrap">
            <div class="catalog_nav f_left wrapper">
                <ul class="catalog_nav_list heavy">
                    <?                    
                    $currentRootSection = array();
                    $arFilter = array(
                        'IBLOCK_ID' => CATALOG_ID,
                        'DEPTH_LEVEL' => 1,
                        'GLOBAL_ACTIVE' => "Y"
                    );
                    $rsSect = CIBlockSection::GetList(array('sort' => 'asc'), $arFilter, false);
                    while ($arSection = $rsSect->GetNext())
                    {
                        if($arSection["ID"]==18)
                        {
                            $currentRootSection = $arSection;
                            $class = "current";
                        }else{
                            $class = "";
                        }
                        
                        ?>
                        <li class="catalog_nav_list_item <?=$class?>">
                            <a href="<?=$arSection["SECTION_PAGE_URL"]?>" class="catalog_nav_list_link"><?=$arSection["NAME"]?></a>
                        </li>
                        <?
                    }
                    ?>
                </ul>
            </div>
        
        </div>
    </div>  
</div>
*/?>

 <?                    
$currentRootSection = array();
$arFilter = array(
	'IBLOCK_ID' => CATALOG_ID,
	'DEPTH_LEVEL' => 1,
	'GLOBAL_ACTIVE' => "Y"
);
$rsSect = CIBlockSection::GetList(array('sort' => 'asc'), $arFilter, false);
while ($arSection = $rsSect->GetNext())
{
	if($arSection["ID"]==18)
	{
		$currentRootSection = $arSection;
		$class = "current";
	}else{
		$class = "";
	}
}
?>

<div class="clearfix">
	<?if(!defined('MOBILE')):?>
	<noindex>
    <aside class="grid_7 prefix_2 resp_col7">
    	<?//$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/left_menu.php"), false);?>
		
		<?$APPLICATION->IncludeComponent(
        	"irbis:model.filter",
        	"",
        	array(
        		"BRAND_HL" => 5,
                "MODEL_HL" => 3,
                "MODEL_ROW_HL" => 4,
                "FILTER_NAME" => "",
                "SELECT_FIELD" => ""
        	),
        	$component,
        	array("HIDE_ICONS" => "Y")
        );
        ?>
		
		<?$APPLICATION->IncludeComponent(
    		"bitrix:catalog.smart.filter",
    		"vertical_new",
    		array(
    			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
    			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
    			"SECTION_ID" => $arCurSection['ID'],
    			"FILTER_NAME" => $arParams["FILTER_NAME"],
    			"PRICE_CODE" => $arParams["PRICE_CODE"],
    			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
    			"CACHE_TIME" => $arParams["CACHE_TIME"],
    			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
    			"SAVE_IN_SESSION" => "N",
    			"FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
    			"XML_EXPORT" => "Y",
    			"SECTION_TITLE" => "NAME",
    			"SECTION_DESCRIPTION" => "DESCRIPTION",
    			'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
    			"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
				"FILTER_ACTION" => "/catalog/battery/"
    		),
    		$component,
    		array('HIDE_ICONS' => 'Y')
    	);?>
    </aside> <!-- left col END !! -->
    
    </noindex>
    <?endif;?>
    <article class="grid_29 prefix_1 resp_col29">
		<?if(!defined('MOBILE')):?>
        <div class="clearfix mb12 catalog_filter">
           <h1>Автомобильные аккумуляторы: продажа и обмен</h1>
            <div class = "TOP_TEXT"><p>На нашем сайте купить автомобильный аккумулятор проверенного качества не составит труда. В ассортименте магазина представлена продукция проверенных производителей, заслужившая доверие пользователей и ежегодно подтверждающая соответствие стандартам и заявленным характеристикам. Любой из предлагаемых автомобильных аккумуляторов можно купить дешевле, обменяв старую АКБ.</p></div>
        </div>
		<?endif;?>
		<noindex>
        <div class="filter_row clearfix grn_bg mb34">
            <div class="f_left">
            
            </div>
            <div class="f_right">
                
            </div>
        </div>
        </noindex>
        <div class="products brand-icon">
        <?php 
	        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_MANUFACTURE");
			$arFilter = Array("IBLOCK_ID"=> IB_CATALOG, "SECTION_CODE"=> SC_BATTERY, "INCLUDE_SUBSECTIONS"=>"Y", "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
			$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);

			$modelArray = array();
			while($ob = $res->GetNextElement()){
				$arFields = $ob->GetFields();
				if ($arFields['PROPERTY_MANUFACTURE_VALUE'] == '') continue;
				
				$name = mb_strtolower($arFields['PROPERTY_MANUFACTURE_VALUE'], 'UTF-8');
				$file = '/upload/brand/' . str_replace(' ', '_', $name) . '.png';

				if (empty($modelArray[$name])){
					if (file_exists($_SERVER['DOCUMENT_ROOT'] . $file)){
						$modelArray[$name] = array(
							'NAME' => $arFields['PROPERTY_MANUFACTURE_VALUE'],
							'CODE' => $name,
							'FILE' => $file,
							'URL' => '/catalog/brand/' . str_replace(' ', '_', $name) . '/'
						);
					}
				}
			}
			ksort($modelArray);
			
        ?>
        
        <? foreach($modelArray as $key => $img):?>
			<?if(file_exists($_SERVER["DOCUMENT_ROOT"].$img['FILE'])):?>
        	<a class="brand_link q" href = "/catalog/brand/<?=str_replace(' ', '_', $key)?>/">
        		<img src="<?=$img['FILE'];?>" alt="<?=$key;?>">
        	</a>
			<?endif;?>
        <? endforeach;?>
        </div>

<p>Представленная продукция характеризуется:</p>
<ul style="    list-style-type: disc;    margin-left: 25px;">
<li>достаточной стартерной мощностью;</li>
<li>низким расходом воды;</li>
<li>отсутствием обслуживания;</li>
<li>длительным сроком эксплуатации;</li>
<li>продолжительной гарантией.</li>
</ul>
<p>Наш магазин автоаккумуляторов расположен в Москве и работает без выходных. Доставка или самовывоз осуществляются в удобное для клиентов время в любой день недели. Заказы на сайте принимаются в круглосуточном режиме и обрабатываются максимально оперативно.</p>
<h2>Как быстро подобрать автомобильный аккумулятор?</h2>
<p>При выборе АКБ необходимо учитывать размеры корпуса, полярность, вид клемм. Для того чтобы максимально упростить пользователям подбор, на сайте организована сортировка батарей по марке и модели автомобиля.</p>
<p>Преимущества такой классификации очевидны:</p>
<ul style="    list-style-type: disc;    margin-left: 25px;">
<li>минимальные затраты времени на поиск продукции;</li>
<li>доступ ко всем товарам в каждой категории;</li>
<li>отсутствие ошибок в выборе.</li>
</ul>
<p>Мы оградили покупателей от ошибок, не ограничив выбор торговой марки и ценовой категории аккумулятора для автомобиля.</p>
<p>Еще один вариант точного выбора — помощь специалиста. Звоните нам по указанному телефону, и мы предоставим необходимую информацию.</p>
    </article>

</div>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>