<?if(defined('MOBILE')):?>
<noindex>
    <div class="catalog__sort">
        <div class="catalog__sort__label"><?= GetMessage('SORT_TO') ?></div>
        <?
	        $arSort = array(
				"catalog_PRICE_1" => "Цене",
				"PROPERTY_CAPACITY" => "Емкости",
				"PROPERTY_POLARITY" => "Полярности",
	        );
        ?>
        <fieldset>
            <ul>
                <li id="select">
                    <select name="catalog-sort" class="js-select">
                        <? foreach($arSort as $sortKey => $sortTitle):
                            if(isset($_GET["sortFields"]) && $_GET["sortFields"] == $sortKey)
                            {
                                $sortOrder = "asc";
                                if($sortKey=="PROPERTY_POLARITY"){
	                                $arrow = "(сначала Обратная)";
                                }else{
	                                $arrow = "&#x2191";
                                }
								$active = "";
                                if($_GET["sortOrder"] == "asc")
                                {
									$active = "selected";
                                }
                            }
                            else
                            {
                                $sortOrder = "asc";
                                if($sortKey=="PROPERTY_POLARITY"){
	                                $arrow = "(сначала Обратная)";
                                }else{
	                                $arrow = "&#x2191";
                                }
								$active = "";
                                if($sortKey == "catalog_PRICE_1")
                                {
                                    $active = "selected";
                                }
                            }
                            $url = $APPLICATION->GetCurPageParam("sortFields=".$sortKey."&sortOrder=".$sortOrder, array("sortFields", "sortOrder")); ?>

                            <option value="<?=$url?>" <?=$active?>><?=$sortTitle?> <?=$arrow?></option>
                            <?
							if(isset($_GET["sortFields"]) && $_GET["sortFields"] == $sortKey)
                            {
                                $sortOrder = "desc";
                                if($sortKey=="PROPERTY_POLARITY"){
	                                $arrow = "(сначала Прямая)";
                                }else{
	                                $arrow = "&#x2193"; //Стрелочки
                                }
								$active = "";
                                if($_GET["sortOrder"] == "desc")
                                {
									$active = "selected";
                                }
                            }
                            else
                            {
                                $active = "";
                                $sortOrder = "desc";
                                if($sortKey=="PROPERTY_POLARITY"){
	                                $arrow = "(сначала Прямая)";
                                }else{
	                                $arrow = "&#x2193"; //Стрелочки
                                }
                            }
                            $url = $APPLICATION->GetCurPageParam("sortFields=".$sortKey."&sortOrder=".$sortOrder, array("sortFields", "sortOrder")); ?>

                            <option value="<?=$url?>" <?=$active?>><?=$sortTitle?> <?=$arrow?></option>

                        <? endforeach;
                        if(isset($_GET["sortFields"]))
                        {
                            $arParams["ELEMENT_SORT_FIELD"] = $_GET["sortFields"];
                            $arParams["ELEMENT_SORT_ORDER"] = $_GET["sortOrder"];
                        }
                        else
                        {
                            $arParams["ELEMENT_SORT_FIELD"] = "catalog_PRICE_1";
                            $arParams["ELEMENT_SORT_ORDER"] = 'asc';
                        } ?>
                    </select>
                    <script>
                        var select = $('li#select');
                        select.on('click', 'ul.chosen-results li.active-result', function () {
                            window.location.assign(select.find('select.js-select').val());
                        })
                    </script>
                </li>

                <? if(isset($_GET["SALELEADER"]) && !empty($_GET["SALELEADER"]))
                {
                    $arrFilter["=PROPERTY_SALELEADER"] = 90;
                    $urlHit = $APPLICATION->GetCurPageParam("", array("SALELEADER", "SPECIALOFFER"));
                }
                else
                {
                    $urlHit = $APPLICATION->GetCurPageParam("SALELEADER=да", array("SALELEADER", "SPECIALOFFER"));
                }

                if(isset($_GET["SPECIALOFFER"]) && !empty($_GET["SPECIALOFFER"]))
                {
                    $arrFilter["=PROPERTY_SPECIALOFFER"] = 91;
                    $urlSale = $APPLICATION->GetCurPageParam("", array("SPECIALOFFER", "SALELEADER"));
                }
                else
                {
                    $urlSale = $APPLICATION->GetCurPageParam("SPECIALOFFER=да", array("SPECIALOFFER", "SALELEADER"));
                } ?>

                <li>
                    <input type="checkbox" name="catalog-sort" onchange="window.location.assign('<?=$urlHit?>')" id="catalog-sort-hits" <?= !empty($_GET["SALELEADER"]) ? 'checked' : ''?>/><label for="catalog-sort-hits">Хиты продаж</label>
                </li>
                <li>
                    <input type="checkbox" name="catalog-sort" onchange="window.location.assign('<?=$urlSale?>')" id="catalog-sort-stock" <?= !empty($_GET["SPECIALOFFER"]) ? 'checked' : ''?>/><label for="catalog-sort-stock">Акции</label>
                </li>
            </ul>
        </fieldset>
    </div>
    <br />
</noindex>
<?else:?>
    <div class="sort-wrapper">
        <?php
        Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/catalog-sort-block-show-ajax.js');

        if ($_REQUEST['SHOW_AJAX_SORT'] == 'Y') {
            $APPLICATION->RestartBuffer();
            ?>
            <div class="filter_row clearfix grn_bg mb34">
                <div class="f_left">
                    <div class="filter_label">Сортировать по:</div>
                    <?
                    $arSort = array(
                        "catalog_PRICE_1" => "цене",
                        "NAME" => "названию",
                        "PROPERTY_RATING" => "рейтингу",
                    );
                    foreach ($arSort as $sortKey => $sortTitle) {
                        if (isset($_GET["sortFields"]) && $_GET["sortFields"] == $sortKey) {
                            $activeClass = "active";
                            if ($_GET["sortOrder"] == "desc") {
                                $sortOrder = "asc";
                                $arrowClass = "arrow_down";
                            } else {
                                $sortOrder = "desc";
                                $arrowClass = "arrow_up";
                            }
                        } else {
                            if ($sortKey == "catalog_PRICE_1") {
                                $activeClass = "active";
                                $sortOrder = "desc";
                                $arrowClass = "arrow_up";
                            } else {
                                $activeClass = "";
                                $sortOrder = "desc";
                                $arrowClass = "arrow_down";
                            }
                        }
                        $url = $APPLICATION->GetCurPageParam("sortFields=" . $sortKey . "&sortOrder=" . $sortOrder,
                            array("sortFields", "sortOrder"));
                        ?>
                        <div class="filter_item">
                            <a href="<?= $url ?>"
                               class="filter_link <?= $activeClass ?> <?= $arrowClass ?>"><?= $sortTitle ?></a>
                        </div>
                        <?
                    }
                    if (isset($_GET["sortFields"])) {
                        $arParams["ELEMENT_SORT_FIELD"] = $_GET["sortFields"];
                        $arParams["ELEMENT_SORT_ORDER"] = $_GET["sortOrder"];
                    } else {
                        $arParams["ELEMENT_SORT_FIELD"] = 'catalog_PRICE_1';
                        $arParams["ELEMENT_SORT_ORDER"] = 'asc';
                    }
                    ?>
                </div>
                <div class="f_right">
                    <?
                    if (isset($_GET["SALELEADER"]) && !empty($_GET["SALELEADER"])) {
                        $arrFilter["PROPERTY_SALELEADER_VALUE"] = $_GET["SALELEADER"];
                        $urlHit = $APPLICATION->GetCurPageParam("", array("SALELEADER", "SPECIALOFFER"));
                    } else {
                        $urlHit = $APPLICATION->GetCurPageParam("SALELEADER=да", array("SALELEADER", "SPECIALOFFER"));
                    }

                    if (isset($_GET["SPECIALOFFER"]) && !empty($_GET["SPECIALOFFER"])) {
                        $arrFilter["PROPERTY_SPECIALOFFER_VALUE"] = $_GET["SPECIALOFFER"];
                        $urlSale = $APPLICATION->GetCurPageParam("", array("SPECIALOFFER", "SALELEADER"));
                    } else {
                        $urlSale = $APPLICATION->GetCurPageParam("SPECIALOFFER=да", array("SPECIALOFFER", "SALELEADER"));
                    }
                    ?>
                    <div class="filter_item">
                        <a href="<?= $urlHit ?>"
                           class="filter_link hits <? if (!empty($_GET["SALELEADER"])): ?>active<? endif; ?>">хиты продаж</a>
                    </div>
                    <div class="filter_item">
                        <a href="<?= $urlSale ?>"
                           class="filter_link hits <? if (!empty($_GET["SPECIALOFFER"])): ?>active<? endif; ?>">акции</a>
                    </div>
                </div>
            </div>
            <?php
            die();
        } ?>
    </div>
<?endif;?>
		<?
		if(isset($_GET["sortFields"]))
		{
			$arParams["ELEMENT_SORT_FIELD"] = $_GET["sortFields"];
			$arParams["ELEMENT_SORT_ORDER"] = $_GET["sortOrder"];

				if(ToUpper($_GET["sortFields"]) != 'CATALOG_PRICE_1'){
					$arParams["ELEMENT_SORT_FIELD2"] = 'CATALOG_PRICE_1';
					$arParams["ELEMENT_SORT_ORDER2"] = 'asc';
				}

			$arParams["HIDE_NOT_AVAILABLE"] = 'Y';
		}
		?>

<?
$template = defined('MOBILE') ? 'index_mobile' : 'index';
$component_name = defined('MOBILE') ? 'pixelplus:catalog.section' : 'bitrix:catalog.section';
$pageElementCount = defined('MOBILE') ? 12 : 24 ;
$lineElementCount = defined('MOBILE') ? 1 : 3 ;
$PAGERtemplate = defined('MOBILE') ? 'arrows_mobile' : 'arrows';

$intSectionID = $APPLICATION->IncludeComponent(
    $component_name,
    $template,
    array (
        'IBLOCK_TYPE' => 'catalog',
        'IBLOCK_ID' => '2',
		'HEAD_SECTION'=> "battery",
        "ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
        "ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
        "ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
        "ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
        'PROPERTY_CODE' =>
            array (
                0 => 'SALELEADER',
                1 => 'SPECIALOFFER',
                2 => 'NEWPRODUCT',
                3 => '',
            ),
        'META_KEYWORDS' => 'UF_KEYWORDS',
        'META_DESCRIPTION' => 'UF_META_DESCRIPTION',
        'BROWSER_TITLE' => 'UF_BROWSER_TITLE',
        'INCLUDE_SUBSECTIONS' => 'Y',
        'BASKET_URL' => '/personal/cart/',
        'ACTION_VARIABLE' => 'action',
        'PRODUCT_ID_VARIABLE' => 'id',
        'SECTION_ID_VARIABLE' => 'SECTION_ID',
        'PRODUCT_QUANTITY_VARIABLE' => 'quantity',
        'PRODUCT_PROPS_VARIABLE' => 'prop',
        'FILTER_NAME' => 'arrFilter',
        'CACHE_TYPE' => 'A',
        'CACHE_TIME' => '36000000',
        'CACHE_FILTER' => 'N',
        'CACHE_GROUPS' => 'Y',
        'SET_TITLE' => 'Y',
        'SET_STATUS_404' => 'Y',
        'DISPLAY_COMPARE' => 'Y',
        'PAGE_ELEMENT_COUNT' => $pageElementCount,
        'LINE_ELEMENT_COUNT' => $lineElementCount,
        'PRICE_CODE' =>
            array (
                0 => 'BASE',
            ),
        'USE_PRICE_COUNT' => 'N',
        'SHOW_PRICE_COUNT' => '1',
        'PRICE_VAT_INCLUDE' => 'Y',
        'USE_PRODUCT_QUANTITY' => 'Y',
        'ADD_PROPERTIES_TO_BASKET' => 'Y',
        'PARTIAL_PRODUCT_PROPERTIES' => 'N',
        'PRODUCT_PROPERTIES' =>
            array (
            ),
        'DISPLAY_TOP_PAGER' => 'N',
        'DISPLAY_BOTTOM_PAGER' => 'Y',
        'PAGER_TITLE' => 'Товары',
        'PAGER_SHOW_ALWAYS' => 'N',
		"PAGER_TEMPLATE" => $PAGERtemplate,
        'PAGER_DESC_NUMBERING' => 'N',
        'PAGER_DESC_NUMBERING_CACHE_TIME' => '36000000',
        'PAGER_SHOW_ALL' => 'N',
        'OFFERS_CART_PROPERTIES' =>
            array (
                0 => 'SIZES_SHOES',
                1 => 'SIZES_CLOTHES',
                2 => 'COLOR_REF',
            ),
        'OFFERS_FIELD_CODE' =>
            array (
                0 => 'NAME',
                1 => 'PREVIEW_PICTURE',
                2 => 'DETAIL_PICTURE',
                3 => '',
            ),
        'OFFERS_PROPERTY_CODE' =>
            array (
                0 => 'SIZES_SHOES',
                1 => 'SIZES_CLOTHES',
                2 => 'COLOR_REF',
                3 => 'MORE_PHOTO',
                4 => 'ARTNUMBER',
                5 => '',
            ),
        'OFFERS_SORT_FIELD' => 'sort',
        'OFFERS_SORT_ORDER' => 'asc',
        'OFFERS_SORT_FIELD2' => 'id',
        'OFFERS_SORT_ORDER2' => 'desc',
        'OFFERS_LIMIT' => '0',
        'SECTION_ID' => "",
        'SECTION_CODE' => "",
        'SECTION_URL' => '/catalog/#SECTION_CODE_PATH#/',
        'DETAIL_URL' => '/catalog/#SECTION_CODE_PATH#/#ELEMENT_CODE#/',
        'CONVERT_CURRENCY' => 'Y',
        'CURRENCY_ID' => 'RUB',
        'HIDE_NOT_AVAILABLE' => 'N',
        'LABEL_PROP' => '-',
        'ADD_PICT_PROP' => 'MORE_PHOTO',
        'PRODUCT_DISPLAY_MODE' => 'Y',
        'OFFER_ADD_PICT_PROP' => 'MORE_PHOTO',
        'OFFER_TREE_PROPS' =>
            array (
                0 => 'SIZES_SHOES',
                1 => 'SIZES_CLOTHES',
                2 => 'COLOR_REF',
                3 => '',
            ),
        'PRODUCT_SUBSCRIPTION' => NULL,
        'SHOW_DISCOUNT_PERCENT' => 'Y',
        'SHOW_OLD_PRICE' => 'Y',
        'MESS_BTN_BUY' => 'Купить',
        'MESS_BTN_ADD_TO_BASKET' => 'В корзину',
        'MESS_BTN_SUBSCRIBE' => NULL,
        'MESS_BTN_DETAIL' => 'Подробнее',
        'MESS_NOT_AVAILABLE' => 'Нет в наличии',
        'TEMPLATE_THEME' => 'green',
        'ADD_SECTIONS_CHAIN' => 'N',
        'ADD_TO_BASKET_ACTION' => 'ADD',
        'SHOW_CLOSE_POPUP' => 'N',
        'COMPARE_PATH' => '/catalog/compare/',
        "SHOW_ALL_WO_SECTION" => "Y",
        'VEHICLE_DATA' => array(
            'BRAND' => $selectedBrand['NAME'],
            'MODEL' => $selectedModel['NAME'],
            'MODIFICATION' => $selectedModification['NAME']
        )
    )
);
?>
