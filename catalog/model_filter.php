<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Интернет-магазин www.kupit-akkumulyator.ru предлагает купить мото и авто аккумуляторы с доставкой по Москве. Низкие цены. Скидки при сдаче БУ аккумуляторов.");
$APPLICATION->SetTitle("Аккумуляторы по марке автомобиля");

global $APPLICATION, $arrFilter, $catalogSectionRoot;
CModule::IncludeModule('iblock');

$title = 'Аккумуляторы по марке автомобиля';
$crumbsChain = [];
$APPLICATION->AddChainItem($title, '/catalog/auto/');

$arResult = [];
$catalogSectionRoot = 18;

$arParams = array(
    "IBLOCK_TYPE" => "catalog",
	"IBLOCK_ID" => "2",
	"TEMPLATE_THEME" => "green",
	"HIDE_NOT_AVAILABLE" => "N",
	"BASKET_URL" => "/personal/cart/",
	"ACTION_VARIABLE" => "action",
	"PRODUCT_ID_VARIABLE" => "id",
	"SECTION_ID_VARIABLE" => "SECTION_ID",
	"PRODUCT_QUANTITY_VARIABLE" => "quantity",
	"PRODUCT_PROPS_VARIABLE" => "prop",
	"SEF_MODE" => "Y",
	"SEF_FOLDER" => "/catalog/",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_FILTER" => "N",
	"CACHE_GROUPS" => "Y",
	"SET_TITLE" => "Y",
	"ADD_SECTION_CHAIN" => "Y",
	"ADD_ELEMENT_CHAIN" => "Y",
	"SET_STATUS_404" => "Y",
	"DETAIL_DISPLAY_NAME" => "N",
	"USE_ELEMENT_COUNTER" => "Y",
	"USE_FILTER" => "Y",
	"FILTER_NAME" => "",
	"FILTER_VIEW_MODE" => "VERTICAL",
);

$arResult["VARIABLES"]["SECTION_ID"] = $catalogSectionRoot;

$arFilter = array(
	"IBLOCK_ID" => $arParams["IBLOCK_ID"],
	"ACTIVE" => "Y",
	"GLOBAL_ACTIVE" => "Y",
);
if (0 < intval($arResult["VARIABLES"]["SECTION_ID"]))
{
	$arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
}
elseif ('' != $arResult["VARIABLES"]["SECTION_CODE"])
{
	$arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];
}

$obCache = new CPHPCache();
if ($obCache->InitCache(36000, serialize($arFilter), "/iblock/catalog"))
{
	$arCurSection = $obCache->GetVars();
}
elseif ($obCache->StartDataCache())
{
	$arCurSection = array();
	$dbRes = CIBlockSection::GetList(array(), $arFilter, false, array("ID", "NAME"));

	if(defined("BX_COMP_MANAGED_CACHE"))
	{
		global $CACHE_MANAGER;
		$CACHE_MANAGER->StartTagCache("/iblock/catalog");

		if ($arCurSection = $dbRes->Fetch())
		{
			$CACHE_MANAGER->RegisterTag("iblock_id_".$arParams["IBLOCK_ID"]);
		}
		$CACHE_MANAGER->EndTagCache();
	}
	else
	{
		if(!$arCurSection = $dbRes->Fetch())
			$arCurSection = array();
	}

	$obCache->EndDataCache($arCurSection);
}
if (!isset($arCurSection))
{
	$arCurSection = array();
}

$selectedBrand = false;
$selectedModel = false;
$selectedModification = false;

$path = explode("/", trim($APPLICATION->GetCurDir(), '/'));

$brands = [];
$dbRes = CIBlockElement::GetList(array('NAME'=>'asc'), array('IBLOCK_ID'=>13, 'ACTIVE'=>'Y'));
while ($res = $dbRes->Fetch()) {
    $code = $res['CODE'] ? $res['CODE'] : strtolower(preg_replace('#\s+|-#is', '_', CUtil::translit($res['NAME'], 'ru')));
    $res['url_code'] =  $code;
    $res['url'] =  '/catalog/auto/' . $code . '/';
    $brands[$code] = $res;
}

if (isset($path[2]) ) {
    $brandRequest = $path[2];

    if (isset($brands[$brandRequest])) {
        $arResult['section_tpl'] = "brand_models";
        $selectedBrand = $brands[$brandRequest];
        $title = 'Аккумуляторы для ' . $selectedBrand['NAME'];
        $crumbsChain[$selectedBrand['url']] = $selectedBrand['NAME'];

        if (!isset($path[3]) && !isset($path[4])) {
            $GLOBALS['arrFilter'] = getManufacturerInformation($brands[$brandRequest]['ID']);
        }

        $models = [];
        $dbRes = CIBlockElement::GetList(array('NAME' => 'asc'), array('IBLOCK_ID'=>14, 'ACTIVE'=>'Y', 'PROPERTY_BRAND_ID'=>$brands[$brandRequest]['ID']));
        while ($res = $dbRes->Fetch()) {
            $code = $res['CODE'] ? $res['CODE'] : strtolower(str_replace(['(', ')'], '', preg_replace('#\s+|-#is', '_', CUtil::translit($res['NAME'], 'ru'))));
            $res['url_code'] = $code;
            $res['url'] = $selectedBrand['url'] . $code . '/';
            $models[$code] = $res;
        }

        if (isset($path[3])) {
            $modelRequest = $path[3];

            if (isset($models[$modelRequest])) {
                $arResult['section_tpl'] = "model_modifications";
                $selectedModel = $models[$modelRequest];
                $title .= ' ' . $selectedModel['NAME'];
                $crumbsChain[$selectedModel['url']] = $selectedModel['NAME'];

                if (!isset($path[4])) {
                    $GLOBALS['arrFilter'] = getModelInformation($models[$modelRequest]['ID']);
                }

                $mods = [];
                $dbRes = CIBlockElement::GetList(array(),
                    array("IBLOCK_ID"=>15, 'ACTIVE'=>"Y", "=PROPERTY_MODEL_ID"=>$models[$modelRequest]['ID']),
                    false, false, ['PROPERTY_DATE_FROM', 'PROPERTY_DATE_TO', 'ID', 'CODE', 'NAME']);

                while ($res = $dbRes->Fetch()) {
                    $name = $res['NAME'] ;//. ' ' . $res['PROPERTY_DATE_FROM_VALUE'] . '-' .  $res['PROPERTY_DATE_TO_VALUE'];
                    $code = $res['CODE'] ? $res['CODE'] : strtolower(str_replace(['(', ')'], '', preg_replace('#\s+|-#is', '_', CUtil::translit($name, 'ru'))));
                    $res['url_code'] = $code;
                    $res['url'] = $selectedModel['url'] . $code . '/';
                    $mods[$code] = $res;
                }

                if (isset($path[4])) {
                    $modificationRequest = $path[4];

                    if (isset($mods[$modificationRequest])) {
                        $arResult['section_tpl'] = "modification_products";
                        $selectedModification = $mods[$modificationRequest];
                        $title .= ' ' . $selectedModification['NAME'];

                        // I WILL BREAK YOU, Demis Group
                        global $aSEOData;
                        $modelFullName = $selectedBrand['NAME'] . ' ' . $selectedModel['NAME'] . ' ' . $selectedModification['NAME'];
                        $aSEOData['title'] = 'Аккумуляторы для ' . $modelFullName . ' — купить в Москве, цены на автомобильные АКБ';
                        $aSEOData['h1'] = 'Аккумуляторы для автомобилей ' . $modelFullName;
                        $aSEOData['descr'] = 'Интернет-магазин kupit-akkumulyator.ru предлагает купить аккумуляторы для автомобиля ' . $modelFullName . ' с доставкой по Москве. Скидки при сдаче БУ аккумуляторов.';

                        $crumbsChain[$selectedModification['url']] = $selectedModification['NAME'];

                        $GLOBALS['arrFilter'] = getModificationInformation($selectedModification['ID']);

                        if (isset($path[5])) {
                            CHTTP::SetStatus('404 Not Found');
                            @define('ERROR_404', 'Y');
                        }
                    } else {
                        CHTTP::SetStatus('404 Not Found');
                        @define('ERROR_404', 'Y');
                    }
                }
            } else {
                CHTTP::SetStatus('404 Not Found');
                @define('ERROR_404', 'Y');
            }
        }
    } else {
        CHTTP::SetStatus('404 Not Found');
        @define('ERROR_404', 'Y');
    }
} else {
    $arResult['section_tpl'] = "choose_brand";
}

$i = 0;
foreach ($crumbsChain as $url => $name) {
    if ($i++ == count($crumbsChain)-1) {
        $name = $title;
    }
    $APPLICATION->AddChainItem($name, $url);
}
$APPLICATION->SetTitle($title);
?>

 <?
$currentRootSection = array();
$arFilter = array(
	'IBLOCK_ID' => CATALOG_ID,
	'DEPTH_LEVEL' => 1,
	'GLOBAL_ACTIVE' => "Y"
);
$rsSect = CIBlockSection::GetList(array('sort' => 'asc'), $arFilter, false);
while ($arSection = $rsSect->GetNext())
{
	if(strpos($APPLICATION->GetCurDir(), $arSection["SECTION_PAGE_URL"])!==false)
	{
		$currentRootSection = $arSection;
		$class = "current";
	}else{
		$class = "";
	}
}
?>
					
<?/*
<div class="clearfix">
    <div class="grid_37 prefix_2">
        <div class="wrapper catalog_nav_wrap">
            <div class="catalog_nav f_left wrapper">
                <ul class="catalog_nav_list heavy">
                    <?

                    $currentRootSection = array();
                    $arFilter = array(
                        'IBLOCK_ID' => CATALOG_ID,
                        'DEPTH_LEVEL' => 1,
                        'GLOBAL_ACTIVE' => "Y"
                    );
                    $rsSect = CIBlockSection::GetList(array('sort' => 'asc'), $arFilter, false);
                    while ($arSection = $rsSect->GetNext())
                    {
                        if(strpos($APPLICATION->GetCurDir(), $arSection["SECTION_PAGE_URL"])!==false)
                        {
                            $currentRootSection = $arSection;
                            $class = "current";
                        }else{
                            $class = "";
                        }

                        ?>
                        <li class="catalog_nav_list_item <?=$class?>">
                            <a href="<?=$arSection["SECTION_PAGE_URL"]?>" class="catalog_nav_list_link"><?=$arSection["NAME"]?></a>
                        </li>
                    <?
                    }
                    ?>
                </ul>
            </div>

            <div class="f_right catalog_search">
                <form class="search_form" method="GET" action="/search/">
                    <input type="text" name="q" class="search_form_input" value="Введите запрос" onBlur="if(this.value=='') this.value='Введите запрос'" onFocus="if(this.value =='Введите запрос' ) this.value=''">
                    <button class="search_form_btn"></button>
                </form>
            </div>

        </div>
    </div>
</div>
*/?>

<div class="clearfix">
<?if ( !defined('MOBILE') ):?>
    <aside class="grid_8 resp_col7">
        <?$APPLICATION->IncludeComponent(
            "irbis:model.filter",
            "",
            array(
                'modelFilter' => true,
                'selectedBrand' => $selectedBrand,
                'selectedModel' => $selectedModel,
                'selectedModification' => $selectedModification,
                "BRAND_HL" => 5,
                "MODEL_HL" => 3,
                "MODEL_ROW_HL" => 4,
                "FILTER_NAME" => "",
                "SELECT_FIELD" => ""
            ),
            $component,
            array("HIDE_ICONS" => "Y")
        );
        ?>
		<br>
        <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/banner-market.php"), false);?>
		<?$APPLICATION->IncludeComponent(
    		"bitrix:catalog.smart.filter",
    		"vertical_new",
    		array(
    			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
    			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
    			"SECTION_ID" => $arCurSection['ID'],
    			"FILTER_NAME" => $arParams["FILTER_NAME"],
    			"PRICE_CODE" => $arParams["PRICE_CODE"],
    			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
    			"CACHE_TIME" => $arParams["CACHE_TIME"],
    			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
    			"SAVE_IN_SESSION" => "N",
    			"FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
    			"XML_EXPORT" => "Y",
    			"SECTION_TITLE" => "NAME",
    			"SECTION_DESCRIPTION" => "DESCRIPTION",
    			'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
    			"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"]
    		),
    		$component,
    		array('HIDE_ICONS' => 'Y')
    	);?>
    </aside>
<? else: ?>
	<div id="bottom_part_filter">
		<? $APPLICATION->IncludeComponent(
			"irbis:model.filter",
			"mobile",
			array(
				"BRAND_HL"             => 5,
				"MODEL_HL"             => 3,
				"MODEL_ROW_HL"         => 4,
				"FILTER_NAME"          => "",
				'modelFilter'          => true,
				'selectedBrand'        => $selectedBrand,
				'selectedModel'        => $selectedModel,
				'selectedModification' => $selectedModification,
			),
			$component,
			array("HIDE_ICONS" => "Y")
		); ?>
    </div>
    <script type="text/javascript">
		if ($('#bottom_part_filter').html().length > 0) {
			$("#top_model_filter").html($('#bottom_part_filter').html());
			$('#bottom_part_filter').remove();
		}
    </script>
	<?if ( $arResult['section_tpl'] != 'modification_products' ) {?>
		<script type="text/javascript">
			$(document).ready(function () {
				$('#toggleFilter').click();
				$('#toggleModelFilter').click();
			});
		</script>
	<?}?>
<? endif; ?>
    <article class="grid_31 prefix_1 resp_col29">

        <div class="clearfix mb12 catalog_filter">
           <!-- <div class="model-filter-head black fs_24 reg f_left"><?=$title?></div>-->
        </div>
<?if ( !defined('MOBILE') ):?>
<!--h1-->
<? endif; ?>
<div class = "TOP_TEXT"></div>
        <?if ($arResult['section_tpl'] == 'choose_brand') {?>

            <div class="brand-list">

                <?foreach ($brands as $code => $item) {
                    $img = $item['PREVIEW_PICTURE'] ? CFile::ResizeImageGet($item['PREVIEW_PICTURE'], ['width'=>60, 'height'=>60])['src'] : '/upload/catalog_img/empty.png'?>
                    <a href="/catalog/auto/<?=$code?>/" class="item">
                        <div class="img">
                            <img src="<?=$img?>" alt=""/>
                        </div>
                        <div class="name"><?=$item['NAME']?></div>
                    </a>

                <?}?>

            </div>

        <?} else if ($arResult['section_tpl'] == 'brand_models') {?>

            <div class="models-list">

                <div class="column">

                    <?
                    $cnt = count($models) / 3;
                    $i = 0;
                    foreach ($models as $code => $item) {?>

                        <a href="/catalog/auto/<?=$selectedBrand['url_code'] .'/'. $item['url_code']?>/" class="item">
                            <div class="name"><?=$item['NAME']?></div>
                        </a>

                        <?if ($i % $cnt == 0) {?>
                            </div>
                            <div class="column">
                        <?}?>

                    <?}?>

                </div>
            </div>
            <br>
            <?php require_once 'model_section.php'; ?>

            <?if ($selectedBrand['DETAIL_TEXT']) {?>
                <div class="brand-text">
                    <?=$selectedBrand['DETAIL_TEXT']?>
                </div>
            <?}?>

        <?} else if ($arResult['section_tpl'] == 'model_modifications') {?>

        
            <div class="mods-list">

                <table>

                    <tr>
                        <th>Модификация</th>
                        <th>Начало выпуска</th>
                        <th>Окончание выпуска</th>
                    </tr>

                    <?foreach ($mods as $code => $item) {;?>
                        <tr>
                            <td>
                                <a href="/catalog/auto/<?=$selectedBrand['url_code'] .'/' . $selectedModel['url_code'] .'/'. $item['url_code']?>/" class="item"><?=$item['NAME']?></a>
                            </td>
                            <td><?= $item['PROPERTY_DATE_FROM_VALUE'] ? $item['PROPERTY_DATE_FROM_VALUE'] : '&mdash;' ?></td>
                            <td><?= $item['PROPERTY_DATE_TO_VALUE'] ? $item['PROPERTY_DATE_TO_VALUE'] : '&mdash;' ?></td>
                        </tr>
                    <?}?>

                </table>

            </div>
            <br>
            <?php require_once 'model_section.php'; ?>

        <?} else if ($arResult['section_tpl'] == 'modification_products') {
            require_once 'model_section.php';  ?>

        <?}?>

        <?php echo '<pre>'; print_r($arParams['BRAND_DATA']); echo '</pre>'; ?>

<div style="clear:both;"></div>
<?php if (!isset($path[2]) ) { ?>
<div class="section-description"></div>
<?php } ?>
    </article>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>