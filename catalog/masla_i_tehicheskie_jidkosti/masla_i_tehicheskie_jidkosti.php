<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Интернет-магазин www.kupit-akkumulyator.ru предлагает купить мото и авто аккумуляторы с доставкой по Москве. Низкие цены. Скидки при сдаче БУ аккумуляторов.");
$APPLICATION->SetTitle("Каталог аккумуляторов, купить мото и авто аккумуляторы в Москве");
$filterView = "VERTICAL";
$APPLICATION->AddChainItem(' Масла и технические жидкости', '/catalog/masla/');
$APPLICATION->AddChainItem($_REQUEST['BRAND_CODE'], '/catalog/masla/'.$_REQUEST['BRAND_CODE']);
?>

<?

if (empty($_REQUEST['BRAND_CODE']) || $_REQUEST['BRAND_CODE'] == ''){
	//CHTTP::SetStatus("404 Not Found");
	//@define("ERROR_404","Y");
	LocalRedirect("/catalog/masla/");
}
elseif (!empty($_REQUEST['BRAND_CODE']) && $_REQUEST['BRAND_CODE'] != '' && intval($_REQUEST['arrFilter_78']) <= 0){
	$_REQUEST['BRAND_CODE'] = str_replace('_', ' ', $_REQUEST['BRAND_CODE']);

	$arrFilter = array('%PROPERTY_MANUFACTURE' => $_REQUEST['BRAND_CODE']);
}


CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");
global $APPLICATION, $USER, $arrFilter, $catalogSectionRoot;

$catalogSectionRoot = 351;

$arParams = array(
    "IBLOCK_TYPE" => "catalog",
	"IBLOCK_ID" => "2",
	"TEMPLATE_THEME" => "green",
	"HIDE_NOT_AVAILABLE" => "N",
	"BASKET_URL" => "/personal/cart/",
	"ACTION_VARIABLE" => "action",
	"PRODUCT_ID_VARIABLE" => "id",
	"SECTION_ID_VARIABLE" => "SECTION_ID",
	"PRODUCT_QUANTITY_VARIABLE" => "quantity",
	"PRODUCT_PROPS_VARIABLE" => "prop",
	"SEF_MODE" => "Y",
	"SEF_FOLDER" => "/catalog/",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_FILTER" => "N",
	"CACHE_GROUPS" => "Y",
	"SET_TITLE" => "Y",
	"ADD_SECTION_CHAIN" => "Y",
	"ADD_ELEMENT_CHAIN" => "Y",
	"SET_STATUS_404" => "Y",
	"DETAIL_DISPLAY_NAME" => "N",
	"USE_ELEMENT_COUNTER" => "Y",
	"USE_FILTER" => "Y",
	"FILTER_NAME" => "",
	"FILTER_VIEW_MODE" => "VERTICAL",
);

$arResult["VARIABLES"]["SECTION_ID"] = $catalogSectionRoot;

$arFilter = array(
	"IBLOCK_ID" => $arParams["IBLOCK_ID"],
	"ACTIVE" => "Y",
	"GLOBAL_ACTIVE" => "Y",
);
if (0 < intval($arResult["VARIABLES"]["SECTION_ID"]))
{
	$arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
}
elseif ('' != $arResult["VARIABLES"]["SECTION_CODE"])
{
	$arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];
}

$obCache = new CPHPCache();
if ($obCache->InitCache(36000, serialize($arFilter), "/iblock/catalog"))
{
	$arCurSection = $obCache->GetVars();
}
elseif ($obCache->StartDataCache())
{
	$arCurSection = array();
	$dbRes = CIBlockSection::GetList(array(), $arFilter, false, array("ID", "NAME"));

	if(defined("BX_COMP_MANAGED_CACHE"))
	{
		global $CACHE_MANAGER;
		$CACHE_MANAGER->StartTagCache("/iblock/catalog");

		if ($arCurSection = $dbRes->Fetch())
		{
			$CACHE_MANAGER->RegisterTag("iblock_id_".$arParams["IBLOCK_ID"]);
		}
		$CACHE_MANAGER->EndTagCache();
	}
	else
	{
		if(!$arCurSection = $dbRes->Fetch())
			$arCurSection = array();
	}

	$obCache->EndDataCache($arCurSection);
}
if (!isset($arCurSection))
{
	$arCurSection = array();
}
?>

<?/*
<div class="clearfix">
    <div class="grid_37 prefix_2">
        <div class="wrapper catalog_nav_wrap">
            <div class="catalog_nav f_left wrapper">
                <ul class="catalog_nav_list heavy">
                    <?
                    $currentRootSection = array();
                    $arFilter = array(
                        'IBLOCK_ID' => CATALOG_ID,
                        'DEPTH_LEVEL' => 1,
                        'GLOBAL_ACTIVE' => "Y"
                    );
                    $rsSect = CIBlockSection::GetList(array('sort' => 'asc'), $arFilter, false);
                    while ($arSection = $rsSect->GetNext())
                    {
                        if($arSection["ID"]==$catalogSectionRoot)
                        {
                            $currentRootSection = $arSection;
                            $class = "current";
                        }else{
                            $class = "";
                        }

                        ?>
                        <li class="catalog_nav_list_item <?=$class?>">
                            <a href="<?=$arSection["SECTION_PAGE_URL"]?>" class="catalog_nav_list_link"><?=$arSection["NAME"]?></a>
                        </li>
                        <?
                    }
                    ?>
                </ul>
            </div>

            <div class="f_right catalog_search">
                <form class="search_form" method="GET" action="/search/">
                    <input type="text" name="q" class="search_form_input" value="Введите запрос" onBlur="if(this.value=='') this.value='Введите запрос'" onFocus="if(this.value =='Введите запрос' ) this.value=''">
                    <button class="search_form_btn"></button>
                </form>
            </div>

        </div>
    </div>
</div>
*/?>

<?
$currentRootSection = array();
$arFilter = array(
	'IBLOCK_ID' => CATALOG_ID,
	'DEPTH_LEVEL' => 1,
	'GLOBAL_ACTIVE' => "Y"
);
$rsSect = CIBlockSection::GetList(array('sort' => 'asc'), $arFilter, false);
while ($arSection = $rsSect->GetNext())
{
	if($arSection["ID"]==$catalogSectionRoot)
	{
		$currentRootSection = $arSection;
		$class = "current";
	}else{
		$class = "";
	}
}
?>

<div class="clearfix">
	<?if(!defined('MOBILE')):?>
	<noindex>
    <aside class="grid_8 resp_col7">
        <?/*$APPLICATION->IncludeComponent(
        	"irbis:model.filter",
        	"",
        	array(
        		"BRAND_HL" => 5,
                "MODEL_HL" => 3,
                "MODEL_ROW_HL" => 4,
                "FILTER_NAME" => "",
                "SELECT_FIELD" => ""
        	),
        	$component,
        	array("HIDE_ICONS" => "Y")
        );
        */?>


        <?//$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/banner-market.php"), false);?>
        <?/*$APPLICATION->IncludeComponent(
    		"bitrix:catalog.smart.filter",
    		"",
    		array(
    			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
    			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
    			"SECTION_ID" => $arCurSection['ID'],
    			"FILTER_NAME" => $arParams["FILTER_NAME"],
    			"PRICE_CODE" => $arParams["PRICE_CODE"],
    			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
    			"CACHE_TIME" => $arParams["CACHE_TIME"],
    			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
    			"SAVE_IN_SESSION" => "N",
    			"FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
    			"XML_EXPORT" => "Y",
    			"SECTION_TITLE" => "NAME",
    			"SECTION_DESCRIPTION" => "DESCRIPTION",
    			'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
    			"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"]
    		),
    		$component,
    		array('HIDE_ICONS' => 'Y')
    	);*/?>
    	<?/*$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			array(
				"AREA_FILE_SHOW" => "file",
				"PATH" => SITE_DIR."include/left_menu.php",
				"EDIT_TEMPLATE" => ""
			),
			false
		);*/?>

		<?$APPLICATION->IncludeComponent(
        	"irbis:model.filter",
        	"",
        	array(
        		"BRAND_HL" => 5,
                "MODEL_HL" => 3,
                "MODEL_ROW_HL" => 4,
                "FILTER_NAME" => "",
                "SELECT_FIELD" => ""
        	),
        	$component,
        	array("HIDE_ICONS" => "Y")
        );
        ?>

		<?$APPLICATION->IncludeComponent(
    		"bitrix:catalog.smart.filter",
    		"vertical_new",
    		array(
    			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
    			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
    			"SECTION_ID" => $arCurSection['ID'],
    			"FILTER_NAME" => $arParams["FILTER_NAME"],
    			"PRICE_CODE" => $arParams["PRICE_CODE"],
    			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
    			"CACHE_TIME" => $arParams["CACHE_TIME"],
    			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
    			"SAVE_IN_SESSION" => "N",
    			"FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
    			"XML_EXPORT" => "Y",
    			"SECTION_TITLE" => "NAME",
    			"SECTION_DESCRIPTION" => "DESCRIPTION",
    			'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
    			"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
				"FILTER_ACTION" => "/catalog/battery/"
    		),
    		$component,
    		array('HIDE_ICONS' => 'Y')
    	);?>
    </aside> <!-- left col END !! -->

    </noindex>
	<?endif;?>


    <article class="grid_31 prefix_1 resp_col29">
        <?
        if(isset($_GET["filter_form"]) && $_GET["filter_form"]=="model-form")
        {
            if(!empty($_GET["MODEL_ROW"]))
            {
                $addFilter = getModificationInformation($_GET["MODEL_ROW"]);
                $arrFilter = array_merge($arrFilter, $addFilter);
            }

            $arBrand = getElementByID($_GET["BRAND"]);
            $arModel = getElementByID($_GET["MODEL"]);

            $sectionName = "Купить аккумулятор для ".$arBrand["NAME"]." ".$arModel["NAME"];

        }else{
            if($APPLICATION->GetCurDir()=="/catalog/battery/" || isset($arrFilter["=PROPERTY_78"]) || isset($_GET["arrFilter_52_MIN"]) || isset($_GET["arrFilter_52_MAX"]))
            {
                $sectionName = "Аккумуляторы автомобильные";

                if(isset($arrFilter["=PROPERTY_78"]))
                {
                    $xmlValue = $arrFilter["=PROPERTY_78"][0];
                    $sectionName.= " ".getBrandAutoByXmlId($xmlValue);
                }

                if(intval($_GET["arrFilter_52_MIN"])>0 || intval($_GET["arrFilter_52_MAX"])>0)
                {
                    $sectionName.=" емкостью ";
                    if(intval($_GET["arrFilter_52_MIN"])>0)
                    {
                        $sectionName.= intval($_GET["arrFilter_52_MIN"]);
                    }

                    if(intval($_GET["arrFilter_52_MIN"])>0 && intval($_GET["arrFilter_52_MAX"])>0)
                    {
                        $sectionName.=" - ";
                    }

                    if(intval($_GET["arrFilter_52_MAX"])>0)
                    {
                        $sectionName.= intval($_GET["arrFilter_52_MAX"]);
                    }
                    $sectionName.= " А•ч";
                }

            }else{
                $sectionName = $currentRootSection["NAME"];
            }

            if ($_REQUEST['BRAND_CODE']){
            	$sectionName = 'Автомобильные аккумуляторы '. $_REQUEST['BRAND_CODE'];
            }
        }
        ?>
        <?
        if($arParams["USE_COMPARE"]=="Y")
        {
        	?><?$APPLICATION->IncludeComponent(
        		"bitrix:catalog.compare.list",
        		"",
        		array(
        			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
        			"NAME" => $arParams["COMPARE_NAME"],
        			"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
        			"COMPARE_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["compare"],
        			"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
        			"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
        			'POSITION_FIXED' => isset($arParams['COMPARE_POSITION_FIXED']) ? $arParams['COMPARE_POSITION_FIXED'] : '',
        			'POSITION' => isset($arParams['COMPARE_POSITION']) ? $arParams['COMPARE_POSITION'] : ''
        		),
        		$component,
        		array("HIDE_ICONS" => "Y")
        	);?><?
        }

        if (isset($arParams['USE_COMMON_SETTINGS_BASKET_POPUP']) && $arParams['USE_COMMON_SETTINGS_BASKET_POPUP'] == 'Y')
        {
        	$basketAction = (isset($arParams['COMMON_ADD_TO_BASKET_ACTION']) ? $arParams['COMMON_ADD_TO_BASKET_ACTION'] : '');
        }
        else
        {
        	$basketAction = (isset($arParams['SECTION_ADD_TO_BASKET_ACTION']) ? $arParams['SECTION_ADD_TO_BASKET_ACTION'] : '');
        }
        $intSectionID = 0;
        ?>
		<?if(!defined('MOBILE')):?>
        <div class="clearfix mb12 catalog_filter">
            <h2 class="black fs_24 reg f_left"><?=$sectionName;?></h2>
            <div class = "TOP_TEXT"></div>
        </div>
		<?endif;?>


		<noindex>
        <div class="filter_row clearfix grn_bg mb34">
            <div class="f_left">
                <div class="filter_label">Сортировать по:</div>
                <?
                $arSort = array(
                    "catalog_PRICE_1" => "цене",
                    //"NAME" => "названию",
                    //"PROPERTY_RATING" => "рейтингу",
                );
				if (strpos($APPLICATION->GetCurDir(), 'catalog/brand/') !== false){
					$arSort['PROPERTY_CAPACITY'] = "емкости";
					$arSort['PROPERTY_POLARITY'] = "полярности";
				}
                foreach($arSort as $sortKey=>$sortTitle)
                {
                    if(isset($_GET["sortFields"]) && $_GET["sortFields"]==$sortKey)
                    {
                        $activeClass = "active";
                        if($_GET["sortOrder"]=="desc")
                        {
                            $sortOrder = "asc";
                            $arrowClass = "arrow_down";
                        }else{
                            $sortOrder = "desc";
                            $arrowClass = "arrow_up";
                        }
                    }else{
                        if($sortKey=="catalog_PRICE_1")
                        {
                            $activeClass = "active";
                            $sortOrder = "desc";
                            $arrowClass = "arrow_up";
                        }else{
                            $activeClass = "";
                            $sortOrder = "desc";
                            $arrowClass = "arrow_down";
                        }
                    }
                    $url = $APPLICATION->GetCurPageParam("sortFields=".$sortKey."&sortOrder=".$sortOrder, array("sortFields", "sortOrder"));
						if($sortKey == 'PROPERTY_POLARITY'){
						?>
						<?if(!defined('MOBILE')):?>
						<div class="filter_item drop">
							<a href="#" class="filter_link <?=$arrowClass?>">полярности</a>
							<div class="dropdown">
								<ul>
									<li><span rel="/catalog/battery/80-95/?sortFields=PROPERTY_POLARITY&sortOrder=desc">Прямая</span></li>
									<li><span rel="/catalog/battery/80-95/?sortFields=PROPERTY_POLARITY&sortOrder=asc">Обратная</span></li>
								</ul>
							</div>
						</div>
						<?endif;?>
						<?
						}elseif($sortKey == 'DOMKRAT_SUBSECTION'){
							?>
							<div class="filter_item drop">
								<a href="#" class="filter_link <?=$arrowClass?>"><?=$sortTitle?></a>
								<div class="dropdown dropdown_large">
									<?$APPLICATION->IncludeComponent(
										"bitrix:catalog.smart.filter",
										"vertical_domkrat_type",
										array(
											"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
											"IBLOCK_ID" => $arParams["IBLOCK_ID"],
											"SECTION_ID" => $currentRootSection["ID"],
											"FILTER_NAME" => "arrFilter",
											"PRICE_CODE" => $arParams["PRICE_CODE"],
											"CACHE_TYPE" => "N",
											"CACHE_TIME" => "36000000",
											"CACHE_NOTES" => "",
											"CACHE_GROUPS" => "N",
											"SAVE_IN_SESSION" => "N",
											"DOP_FILTER" => $sectionName,
											"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"]
										),
										$component,
										array('HIDE_ICONS' => 'Y')
									);?>
									<!--<ul>
										<li><a href="/catalog/battery/80-95/?sortFields=PROPERTY_POLARITY&sortOrder=desc">Прямая</a></li>
									</ul>-->
								</div>
							</div>
							<?
						}else{
							?>
							<div class="filter_item">
								<span rel="<?=$url?>" class="filter_link <?=$activeClass?> <?=$arrowClass?>"><?=$sortTitle?></span>
							</div>
							<?
						}?>
                    <?
                }
                if(isset($_GET["sortFields"]))
                {
                    $arParams["ELEMENT_SORT_FIELD"] = $_GET["sortFields"];
                    $arParams["ELEMENT_SORT_ORDER"] = $_GET["sortOrder"];
                }else{
                    $arParams["ELEMENT_SORT_FIELD"] = "catalog_PRICE_1";
                    $arParams["ELEMENT_SORT_ORDER"] = 'asc';
                }
                ?>
            </div>
            <div class="f_right">
                <?
                if(isset($_GET["SALELEADER"]) && !empty($_GET["SALELEADER"]))
                {
                    $arrFilter["PROPERTY_SALELEADER_VALUE"] = $_GET["SALELEADER"];
                    $urlHit = $APPLICATION->GetCurPageParam("", array("SALELEADER", "SPECIALOFFER"));
                }else{
                    $urlHit = $APPLICATION->GetCurPageParam("SALELEADER=да", array("SALELEADER", "SPECIALOFFER"));
                }

                if(isset($_GET["SPECIALOFFER"]) && !empty($_GET["SPECIALOFFER"]))
                {
                    $arrFilter["PROPERTY_SPECIALOFFER_VALUE"] = $_GET["SPECIALOFFER"];
                    $urlSale = $APPLICATION->GetCurPageParam("", array("SPECIALOFFER", "SALELEADER"));
                }else{
                    $urlSale = $APPLICATION->GetCurPageParam("SPECIALOFFER=да", array("SPECIALOFFER", "SALELEADER"));
                }
                ?>
                <div class="filter_item">
                    <span rel="<?=$urlHit?>" class="filter_link hits sales <?if(!empty($_GET["SALELEADER"])):?>active<?endif;?>"></span>
                </div>

                <div class="filter_item">
                    <span rel="<?=$urlSale?>" class="filter_link hits actions <?if(!empty($_GET["SPECIALOFFER"])):?>active<?endif;?>"></span>
                </div>

            </div>
        </div>

        </noindex>
		<?
		$template = defined('MOBILE')?"index_mobile":"index";
		$pageElementCount = defined('MOBILE') ? 12 : 24 ;
		$lineElementCount = defined('MOBILE') ? 1 : 3 ;
		/*$template = "index";
		$pageElementCount = 24 ;
		$lineElementCount = 3 ;*/
		?>
        <?$APPLICATION->IncludeComponent(
	"pixelplus:catalog.section",
	$template,
	array(
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "2",
		'HEAD_SECTION'=> "battery",
		"SECTION_ID" => $catalogSectionRoot,
		"SECTION_CODE" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "UF_TEXT_TOP",
			1 => "",
		),
		"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
		"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER2" => "desc",
		"FILTER_NAME" => "arrFilter",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "N",
		"HIDE_NOT_AVAILABLE" => "N",
		"PAGE_ELEMENT_COUNT" => $pageElementCount,
		"LINE_ELEMENT_COUNT" => $lineElementCount,
		"PROPERTY_CODE" => array(
			0 => "TYPE",
			1 => "CAPACITY",
			2 => "POLARITY",
			3 => "START_TOK",
			4 => "TOKEND",
			5 => "TITLE",
			6 => "KEYWORDS",
			7 => "META_DESCRIPTION",
			8 => "SALELEADER",
			9 => "SPECIALOFFER",
			10 => "ARTNUMBER",
			11 => "BLOG_POST_ID",
			12 => "GARANTY",
			13 => "BLOG_COMMENTS_CNT",
			14 => "FORUM_MESSAGE_CNT",
			15 => "vote_count",
			16 => "rating",
			17 => "RECOMMEND",
			18 => "vote_sum",
			19 => "FORUM_TOPIC_ID",
			20 => "PRICE_CHANGE",
			21 => "BRAND",
			22 => "MODEL",
			23 => "MODEL_ROW",
			24 => "",
		),
		"OFFERS_LIMIT" => "5",
		"TEMPLATE_THEME" => "green",
		"PRODUCT_SUBSCRIPTION" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_CLOSE_POPUP" => "N",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"BROWSER_TITLE" => "-",
		"SET_META_KEYWORDS" => "N",
		"META_KEYWORDS" => "-",
		"SET_META_DESCRIPTION" => "Y",
		"META_DESCRIPTION" => "-",
		"ADD_SECTIONS_CHAIN" => "N",
		"SET_STATUS_404" => "N",
		"CACHE_FILTER" => "N",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"BASKET_URL" => "/personal/cart/",
		"USE_PRODUCT_QUANTITY" => "Y",
		"ADD_PROPERTIES_TO_BASKET" => "N",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRODUCT_PROPERTIES" => array(
		),
		"ADD_TO_BASKET_ACTION" => "ADD",
		"DISPLAY_COMPARE" => "Y",
		"COMPARE_PATH" => "/catalog/compare/",
		"PAGER_TEMPLATE" => "arrows",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Товары",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"ADD_PICT_PROP" => "MORE_PHOTO",
		"LABEL_PROP" => "-",
		"MESS_BTN_COMPARE" => "Сравнить",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"SECTION_URL" => "",
		"DETAIL_URL" => "",
		"AJAX_OPTION_ADDITIONAL" => "",
		'SHOW_DEACTIVATED' => 'Y'
	),
	false
);?>
    </article>
</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
