<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Набор инструментов для авто в чемодане в Москве, купить набор инструментов в чемодане для автомобиля");
$APPLICATION->SetPageProperty("description", "Интернет-магазин www.kupit-akkumulyator.ru предлагает купить мото и авто аккумуляторы с доставкой по Москве. Низкие цены. Скидки при сдаче БУ аккумуляторов.");
$APPLICATION->SetTitle("Каталог аккумуляторов, купить мото и авто аккумуляторы в Москве");
$filterView = "VERTICAL";
?>
<?if($_GET['frame'] && !$_GET['set_filter']):?>
<style>
.bx-panel, .filter_row, .f_right, .product_item_similar_link, .catalog_filter, .info_box, header, nav, footer, .grid_38, .grid_37, .section-description{display: none;}
.container {width: 1200px;}
@media only screen 
and (max-device-width : 1024px){

	.container,
	body	{width: 100%;
	    min-width: 100%;}
}
.container .prefix_2 {padding-left: 60px;}
	.container .grid_7 {width: 210px; padding-top: 20px;}
.container .prefix_1 {padding-left: 30px;}
.container .grid_29 {width: 870px;}
.product_item {
    float: left;
    margin: 0 6px 28px;
    position: relative;
    width: 208px;
}
	.products {padding-top: 20px;}
</style>
<script>
$(document).ready(function(){
	$(".d_none, .bx-panel, .filter_row, .f_right, .product_item_similar_link, .catalog_filter, .info_box, header, nav, footer, .grid_38, .grid_37, .section-description").remove();
	$(".product_item_content a").attr('target', '_blank');
	$(".bx_pagination_bottom a").attr('target', '_blank');
	$(".product_item_img a").attr('target', '_blank');

	$('.smartfilter').attr('action', 'http://<?=$_SERVER['SERVER_NAME'];?>' + $('.smartfilter').attr('action'));
 	$(document).on('submit', '.smartfilter', function(){
		$(this).attr('target', '_blank');
		$(this).submit();
		return false;
 	});

	$('#model-filter-form').attr('action', 'http://<?=$_SERVER['SERVER_NAME'];?>' + $('.smartfilter').attr('action'));
 	$(document).on('submit', '#model-filter-form', function(){
		$(this).attr('target', '_blank');
		$(this).submit();
		return false;
 	});

});
</script>
<?endif;?>

<!-- priem-akkumulyatorov.ru.hp.webway.ru/novie_akb/ -->
<? if($_REQUEST['frame']=='y'): ?>

		<link rel="stylesheet" type="text/css" media="screen" href="<?=SITE_TEMPLATE_PATH?>/css/frame.css">
<? endif; ?>
<!-- //priem-akkumulyatorov.ru.hp.webway.ru/novie_akb/ -->
<?
$page_uri = $_SERVER["REQUEST_URI"];
/*
$propsOrder = Array("SORT"=>"ASC", "VALUE"=>"ASC");
$propsFilter = array("IBLOCK_ID" => CATALOG_ID, "CODE" => "TYPE");
$rsAkkumTypes = CIBlockPropertyEnum::GetList($propsOrder, $propsFilter);
$arAkkumTypes = array();
while($arProp = $rsAkkumTypes->Fetch()){
	$arAkkumTypes[] = $arProp;
	$arAkkumTypes_ids[] = $arProp["ID"];
	$arAkkumTypes_assoc[$arProp["VALUE"]] = $arProp;
}

$propsOrder2 = Array("SORT"=>"ASC", "VALUE"=>"ASC");
$propsFilter2 = array("IBLOCK_ID" => CATALOG_ID, "CODE" => "TYPE");
$rsAkkumTypes2 = CIBlockProperty::GetList($propsOrder2, $propsFilter2);
$arAkkumTypes2 = $rsAkkumTypes2->Fetch();
$rsEnum = CIBlockProperty::GetPropertyEnum($arAkkumTypes2["ID"]);
$arEnum = array();
while($Enum = $rsEnum->Fetch()){
	$arEnum[] = $Enum;
}

if(strstr($page_uri, "/catalog/battery/") && !strstr($page_uri, "/li_ion/") && !strstr($page_uri, "/truck-akb/") && !strstr($page_uri, "/akb_dlya_ups/") && !strstr($page_uri, "/moto-akb/") && !strstr($page_uri, "/gelevye/") && !$_GET["set_filter"]){
	$arrFilter["PROPERTY_TYPE"] = isset($arAkkumTypes_assoc["Легковые аккумуляторы"])?$arAkkumTypes_assoc["Легковые аккумуляторы"]["ID"]:$arAkkumTypes[0]["ID"];
}
*/
//CDev::pre($page_uri);
?>

<!-- catalog -->
<?$template = defined('MOBILE') ? 'catalog_mobile' : 'catalog';
$PAGERtemplate = defined('MOBILE') ? 'arrows_mobile' : 'arrows';
$pageElementCount = defined('MOBILE') ? 12 : 24 ;
$lineElementCount = defined('MOBILE') ? 1 : 3 ;

/*$template = 'catalog';
$PAGERtemplate = 'arrows';
$pageElementCount = 24 ;
$lineElementCount = 3 ;*/

/*print_r("<pre>");
print_r($template);
print_r("</pre>");
exit;*/

$APPLICATION->IncludeComponent(
	"irbis:catalog", 
	$template, 
	array(
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "2",
		"TEMPLATE_THEME" => "green",
		"HIDE_NOT_AVAILABLE" => "N",
		"BASKET_URL" => "/personal/cart/",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/catalog/",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "N",
		"SET_TITLE" => "Y",
		"ADD_SECTION_CHAIN" => "Y",
		"ADD_ELEMENT_CHAIN" => "Y",
		"SET_STATUS_404" => "Y",
		"DETAIL_DISPLAY_NAME" => "N",
		"USE_ELEMENT_COUNTER" => "Y",
		"USE_FILTER" => "Y",
		"FILTER_NAME" => "",
		"FILTER_VIEW_MODE" => "VERTICAL",
		"FILTER_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_PROPERTY_CODE" => array(
			0 => "",
			1 => "BRAND",
			2 => "",
		),
		"FILTER_PRICE_CODE" => array(
			0 => "BASE",
		),
		"FILTER_OFFERS_FIELD_CODE" => array(
			0 => "PREVIEW_PICTURE",
			1 => "DETAIL_PICTURE",
			2 => "",
		),
		"FILTER_OFFERS_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"USE_REVIEW" => "Y",
		"MESSAGES_PER_PAGE" => "10",
		"USE_CAPTCHA" => "Y",
		"REVIEW_AJAX_POST" => "Y",
		"PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
		"FORUM_ID" => "1",
		"URL_TEMPLATES_READ" => "",
		"SHOW_LINK_TO_FORUM" => "Y",
		"USE_COMPARE" => "Y",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"PRODUCT_PROPERTIES" => array(
		),
		"USE_PRODUCT_QUANTITY" => "Y",
		"CONVERT_CURRENCY" => "Y",
		"CURRENCY_ID" => "RUB",
		"QUANTITY_FLOAT" => "N",
		"OFFERS_CART_PROPERTIES" => array(
			0 => "SIZES_SHOES",
			1 => "SIZES_CLOTHES",
			2 => "COLOR_REF",
		),
		"SHOW_TOP_ELEMENTS" => "N",
		"SECTION_COUNT_ELEMENTS" => "N",
		"SECTION_TOP_DEPTH" => "1",
		"SECTIONS_VIEW_MODE" => "TEXT",
		"SECTIONS_SHOW_PARENT_NAME" => "N",
		"PAGE_ELEMENT_COUNT" => $pageElementCount,
		"LINE_ELEMENT_COUNT" => $lineElementCount,
		"ELEMENT_SORT_FIELD" => "ACTIVE",
		"ELEMENT_SORT_ORDER" => "desc",
		"ELEMENT_SORT_FIELD2" => "CATALOG_PRICE_1",
		"ELEMENT_SORT_ORDER2" => "asc",
		"LIST_PROPERTY_CODE" => array(
			0 => "SALELEADER",
			1 => "SPECIALOFFER",
			2 => "NEWPRODUCT",
			3 => "",
		),
		"INCLUDE_SUBSECTIONS" => "Y",
"SHOW_ALL_WO_SECTION" => "Y",
		"LIST_META_KEYWORDS" => "UF_KEYWORDS",
		"LIST_META_DESCRIPTION" => "UF_META_DESCRIPTION",
		"LIST_BROWSER_TITLE" => "UF_BROWSER_TITLE",
		"LIST_OFFERS_FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_PICTURE",
			2 => "DETAIL_PICTURE",
			3 => "",
		),
		"LIST_OFFERS_PROPERTY_CODE" => array(
			0 => "SIZES_SHOES",
			1 => "SIZES_CLOTHES",
			2 => "COLOR_REF",
			3 => "MORE_PHOTO",
			4 => "ARTNUMBER",
			5 => "",
		),
		"LIST_OFFERS_LIMIT" => "0",
		"DETAIL_PROPERTY_CODE" => array(
			0 => "TYPE",
			1 => "CAPACITY",
			2 => "POLARITY",
			3 => "START_TOK",
			4 => "PROIZVODITEL_MASLO",
			5 => "OBEM_UPAKOVKI_L_MASLO",
			6 => "ARTNUMBER",
			7 => "GARANTY",
			8 => "MANUFACTURE_2",
			9 => "COUNTRY_MANUFACTURE",
			10 => "TIP_MASLO",
			11 => "KLASS_VYAZKOSTI_SAE_MASLO",
			12 => "GRUZOPODEMNOST_DOMKRATY",
			13 => "VYSOTA_PODKHVATA_DOMKRATY",
			14 => "VYSOTA_PODEMA_DOMKRATY",
			15 => "BRAND",
			16 => "MODEL",
			17 => "MODEL_ROW",
			18 => "",
		),
		"DETAIL_META_KEYWORDS" => "KEYWORDS",
		"DETAIL_META_DESCRIPTION" => "META_DESCRIPTION",
		"DETAIL_BROWSER_TITLE" => "TITLE",
		"DETAIL_OFFERS_FIELD_CODE" => array(
			0 => "NAME",
			1 => "",
		),
		"DETAIL_OFFERS_PROPERTY_CODE" => array(
			0 => "ARTNUMBER",
			1 => "SIZES_SHOES",
			2 => "SIZES_CLOTHES",
			3 => "COLOR_REF",
			4 => "MORE_PHOTO",
			5 => "",
		),
		"LINK_IBLOCK_TYPE" => "",
		"LINK_IBLOCK_ID" => "",
		"LINK_PROPERTY_SID" => "",
		"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
		"USE_ALSO_BUY" => "Y",
		"ALSO_BUY_ELEMENT_COUNT" => "4",
		"ALSO_BUY_MIN_BUYES" => "1",
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER2" => "desc",
		"PAGER_TEMPLATE" => $PAGERtemplate,
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Товары",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",
		"PAGER_SHOW_ALL" => "N",
		"ADD_PICT_PROP" => "MORE_PHOTO",
		"LABEL_PROP" => "-",
		"PRODUCT_DISPLAY_MODE" => "Y",
		"OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
		"OFFER_TREE_PROPS" => array(
			0 => "SIZES_SHOES",
			1 => "SIZES_CLOTHES",
			2 => "COLOR_REF",
			3 => "",
		),
		"SHOW_DISCOUNT_PERCENT" => "Y",
		"SHOW_OLD_PRICE" => "Y",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_COMPARE" => "Сравнение",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"DETAIL_USE_VOTE_RATING" => "Y",
		"DETAIL_VOTE_DISPLAY_AS_RATING" => "rating",
		"DETAIL_USE_COMMENTS" => "Y",
		"DETAIL_BLOG_USE" => "Y",
		"DETAIL_VK_USE" => "N",
		"DETAIL_FB_USE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"USE_STORE" => "Y",
		"USE_STORE_PHONE" => "Y",
		"USE_STORE_SCHEDULE" => "Y",
		"USE_MIN_AMOUNT" => "N",
		"STORE_PATH" => "/store/#store_id#",
		"MAIN_TITLE" => "Наличие на складах",
		"MIN_AMOUNT" => "10",
		"DETAIL_BRAND_USE" => "Y",
		"DETAIL_BRAND_PROP_CODE" => array(
			0 => "",
			1 => "BRAND_REF",
			2 => "",
		),
		"ADD_SECTIONS_CHAIN" => "Y",
		"COMMON_SHOW_CLOSE_POPUP" => "N",
		"DETAIL_SHOW_MAX_QUANTITY" => "N",
		"DETAIL_BLOG_URL" => "catalog_comments",
		"DETAIL_BLOG_EMAIL_NOTIFY" => "N",
		"DETAIL_FB_APP_ID" => "",
		"USE_SALE_BESTSELLERS" => "Y",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"USE_COMMON_SETTINGS_BASKET_POPUP" => "N",
		"TOP_ADD_TO_BASKET_ACTION" => "ADD",
		"SECTION_ADD_TO_BASKET_ACTION" => "ADD",
		"DETAIL_ADD_TO_BASKET_ACTION" => array(
			0 => "BUY",
		),
		"DETAIL_SHOW_BASIS_PRICE" => "Y",
		"DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",
		"DETAIL_DETAIL_PICTURE_MODE" => "IMG",
		"DETAIL_ADD_DETAIL_TO_SLIDER" => "N",
		"DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "E",
		"STORES" => array(
		),
		"USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SHOW_EMPTY_STORE" => "Y",
		"SHOW_GENERAL_STORE_INFORMATION" => "N",
		"USE_BIG_DATA" => "Y",
		"BIG_DATA_RCM_TYPE" => "bestsell",
		"COMMON_ADD_TO_BASKET_ACTION" => "ADD",
		"COMPARE_NAME" => "CATALOG_COMPARE_LIST",
		"COMPARE_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"COMPARE_PROPERTY_CODE" => array(
			0 => "TYPE",
			1 => "CAPACITY",
			2 => "POLARITY",
			3 => "START_TOK",
			4 => "TOKEND",
			5 => "ARTNUMBER",
			6 => "BRAND",
			7 => "MODEL",
			8 => "MODEL_ROW",
			9 => "",
		),
		"COMPARE_ELEMENT_SORT_FIELD" => "sort",
		"COMPARE_ELEMENT_SORT_ORDER" => "asc",
		"DISPLAY_ELEMENT_SELECT_BOX" => "N",
		"COMPARE_POSITION_FIXED" => "Y",
		"COMPARE_POSITION" => "top left",
		"FILTER_SHOW_SECTIONS" => array(
			0 => "battery",
			1 => "accessories",
		),
		"COMPONENT_TEMPLATE" => "catalog",
		"SEF_URL_TEMPLATES" => array(
			"sections" => "",
			"section" => "#SECTION_CODE_PATH#/",
			"element" => "#SECTION_CODE_PATH#/#ELEMENT_CODE#/",
			"compare" => "compare/",
		)
	),
	false
);?>
<?if($_REQUEST['ajax']=='Y')
   die();?>

<?
$mytitle = $APPLICATION->GetTitle();
if ($_GET['PAGEN_1']) {
	$APPLICATION->SetPageProperty("title", $mytitle . " | Страница1 " . $_GET['PAGEN_1']);
	$APPLICATION->SetTitle("Page title");
} ?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>