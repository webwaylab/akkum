<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "");
$APPLICATION->SetPageProperty("keywords", "");
$APPLICATION->SetPageProperty("description", "");
$APPLICATION->SetTitle("");
?>

<?
   global $arrFilter;
   $arrFilter["!PROPERTY_NEDOROGIE_AKKUMULYATORY"] = false;
   $filter_template = (defined('MOBILE') || $_GET["dev_mobile"] == "mobile") ? 'vertical_new_mobile' : 'vertical_new';
	$arParams["ELEMENT_SORT_FIELD2"] = "ACTIVE";
	$arParams["ELEMENT_SORT_ORDER2"] = "DESC";
	$arParams["ELEMENT_SORT_FIELD"] = 'CATALOG_PRICE_1';
	$arParams["ELEMENT_SORT_ORDER"] = 'asc';

?>

<div class="clearfix">
	<? if ( !defined('MOBILE') ): ?>
	<noindex>
    <aside class="grid_8 resp_col7">
        <?$APPLICATION->IncludeComponent(
    		"bitrix:catalog.smart.filter",
    		$filter_template,
    		array(
                "IBLOCK_TYPE" => "catalog",
            	"IBLOCK_ID" => "2",
    			"SECTION_ID" => "18",
    			"FILTER_NAME" => "arrFilter",
    			"PRICE_CODE" => array(),
            	"CACHE_TYPE" => "A",
            	"CACHE_TIME" => "36000000",
            	"CACHE_FILTER" => "Y",
            	"CACHE_GROUPS" => "N",
    			"SAVE_IN_SESSION" => "N",
            	"FILTER_VIEW_MODE" => "VERTICAL",
    			'HIDE_NOT_AVAILABLE' => "Y",
            	"TEMPLATE_THEME" => "green",
    		),
    		$component,
    		array('HIDE_ICONS' => 'Y')
    	);?>
    </aside> <!-- left col END !! -->
    </noindex>
	<? endif; ?>
	<article class="grid_31 prefix_1 resp_col29">
    <?//<article class="812x grid_29 prefix_1 resp_col29">?>
        <div class="hidden-block">
        </div>
        <?
        if($arParams["USE_COMPARE"]=="Y")
        {
        	?><?$APPLICATION->IncludeComponent(
        		"bitrix:catalog.compare.list",
        		"",
        		array(
        			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
        			"NAME" => $arParams["COMPARE_NAME"],
        			"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
        			"COMPARE_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["compare"],
        			"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
        			"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
        			'POSITION_FIXED' => isset($arParams['COMPARE_POSITION_FIXED']) ? $arParams['COMPARE_POSITION_FIXED'] : '',
        			'POSITION' => isset($arParams['COMPARE_POSITION']) ? $arParams['COMPARE_POSITION'] : ''
        		),
        		$component,
        		array("HIDE_ICONS" => "Y")
        	);?><?
        }

        if (isset($arParams['USE_COMMON_SETTINGS_BASKET_POPUP']) && $arParams['USE_COMMON_SETTINGS_BASKET_POPUP'] == 'Y')
        {
        	$basketAction = (isset($arParams['COMMON_ADD_TO_BASKET_ACTION']) ? $arParams['COMMON_ADD_TO_BASKET_ACTION'] : '');
        }
        else
        {
        	$basketAction = (isset($arParams['SECTION_ADD_TO_BASKET_ACTION']) ? $arParams['SECTION_ADD_TO_BASKET_ACTION'] : '');
        }
        $intSectionID = 0;
        ?>



		<? if ( defined('MOBILE') ): ?>
            <noindex>
                <div class="catalog__sort">
                    <div class="catalog__sort__label">Сортировать по</div>
					<?
				        $arSort = array(
							"catalog_PRICE_1" => "Цене",
							"PROPERTY_CAPACITY" => "Емкости",
							"PROPERTY_POLARITY" => "Полярности",
				        );
					?>
                    <fieldset>
                        <ul>
                            <li id="select">
                                <select name="catalog-sort" class="js-select">
		                        <? foreach($arSort as $sortKey => $sortTitle):
		                            if(isset($_GET["sortFields"]) && $_GET["sortFields"] == $sortKey)
		                            {
		                                $sortOrder = "asc";
		                                if($sortKey=="PROPERTY_POLARITY"){
			                                $arrow = "(сначала Обратная)";
		                                }else{
			                                $arrow = "&#x2191";
		                                }
										$active = "";
		                                if($_GET["sortOrder"] == "asc")
		                                {
											$active = "selected";
		                                }
		                            }
		                            else
		                            {
		                                $sortOrder = "asc";
		                                if($sortKey=="PROPERTY_POLARITY"){
			                                $arrow = "(сначала Обратная)";
		                                }else{
			                                $arrow = "&#x2191";
		                                }
										$active = "";
		                                if($sortKey == "catalog_PRICE_1")
		                                {
		                                    $active = "selected";
		                                }
		                            }
		                            $url = $APPLICATION->GetCurPageParam("sortFields=".$sortKey."&sortOrder=".$sortOrder, array("sortFields", "sortOrder")); ?>
		
		                            <option value="<?=$url?>" <?=$active?>><?=$sortTitle?> <?=$arrow?></option>
		                            <?
									if(isset($_GET["sortFields"]) && $_GET["sortFields"] == $sortKey)
		                            {
		                                $sortOrder = "desc";
		                                if($sortKey=="PROPERTY_POLARITY"){
			                                $arrow = "(сначала Прямая)";
		                                }else{
			                                $arrow = "&#x2193"; //Стрелочки
		                                }
										$active = "";
		                                if($_GET["sortOrder"] == "desc")
		                                {
											$active = "selected";
		                                }
		                            }
		                            else
		                            {
		                                $active = "";
		                                $sortOrder = "desc";
		                                if($sortKey=="PROPERTY_POLARITY"){
			                                $arrow = "(сначала Прямая)";
		                                }else{
			                                $arrow = "&#x2193"; //Стрелочки
		                                }
		                            }
		                            $url = $APPLICATION->GetCurPageParam("sortFields=".$sortKey."&sortOrder=".$sortOrder, array("sortFields", "sortOrder")); ?>
		
		                            <option value="<?=$url?>" <?=$active?>><?=$sortTitle?> <?=$arrow?></option>
		
		                        <? endforeach;
		                        if(isset($_GET["sortFields"]))
		                        {
		                            $arParams["ELEMENT_SORT_FIELD"] = $_GET["sortFields"];
		                            $arParams["ELEMENT_SORT_ORDER"] = $_GET["sortOrder"];
		                        }
		                        else
		                        {
		                            $arParams["ELEMENT_SORT_FIELD"] = "catalog_PRICE_1";
		                            $arParams["ELEMENT_SORT_ORDER"] = 'asc';
		                        } ?>
                                </select>
                                <script>
									var select = $('li#select');
									select.on('click', 'ul.chosen-results li.active-result', function () {
										window.location.assign(select.find('select.js-select').val());
									})
                                </script>
                            </li>

							<? if ( isset($_GET["SALELEADER"]) && !empty($_GET["SALELEADER"]) ) {
								$arrFilter["=PROPERTY_SALELEADER"] = 90;
								$urlHit = $APPLICATION->GetCurPageParam("", array("SALELEADER", "SPECIALOFFER"));
							} else {
								$urlHit = $APPLICATION->GetCurPageParam("SALELEADER=Y", array("SALELEADER",
									"SPECIALOFFER",
								));
							}

							if ( isset($_GET["SPECIALOFFER"]) && !empty($_GET["SPECIALOFFER"]) ) {
								$arrFilter["=PROPERTY_SPECIALOFFER"] = 91;
								$urlSale = $APPLICATION->GetCurPageParam("", array("SPECIALOFFER", "SALELEADER"));
							} else {
								$urlSale = $APPLICATION->GetCurPageParam("SPECIALOFFER=Y", array("SPECIALOFFER",
									"SALELEADER",
								));
							} ?>

                            <li>
                                <input type="checkbox" name="catalog-sort"
                                       onchange="window.location.assign('<?=$urlHit?>')"
                                       id="catalog-sort-hits" <?=!empty($_GET["SALELEADER"]) ? 'checked' : ''?>/><label
                                        for="catalog-sort-hits">Хиты продаж</label>
                            </li>
                            <li>
                                <input type="checkbox" name="catalog-sort"
                                       onchange="window.location.assign('<?=$urlSale?>')"
                                       id="catalog-sort-stock" <?=!empty($_GET["SPECIALOFFER"]) ? 'checked' :
									''?>/><label for="catalog-sort-stock">Акции</label>
                            </li>
                        </ul>
                    </fieldset>
                </div>
            </noindex>
		<? else: ?>
		<div class="h2 black fs_24 reg f_left"><?=$sectionName;?></div>
		<noindex>
			<div class="filter_row clearfix grn_bg mb34">
				<div class="f_left">
					<div class="filter_label">Сортировать по:</div>
					<?

					$arSort = array(
						"catalog_PRICE_1" => "цене",
						"NAME" => "названию",
						"PROPERTY_RATING" => "рейтингу",
					);
					foreach($arSort as $sortKey=>$sortTitle)
					{
						if(isset($_GET["sortFields"]) && $_GET["sortFields"]==$sortKey)
						{
							$activeClass = "active";
							if($_GET["sortOrder"]=="desc")
							{
								$sortOrder = "asc";
								$arrowClass = "arrow_down";
							}else{
								$sortOrder = "desc";
								$arrowClass = "arrow_up";
							}
						}else{
							if($sortKey=="catalog_PRICE_1")
							{
								$activeClass = "active";
								$sortOrder = "desc";
								$arrowClass = "arrow_up";
							}else{
								$activeClass = "";
								$sortOrder = "desc";
								$arrowClass = "arrow_down";
							}
						}
						$url = $APPLICATION->GetCurPageParam("sortFields=".$sortKey."&sortOrder=".$sortOrder, array("sortFields", "sortOrder"));
						?>
						<div class="filter_item">
							<span rel="<?=$url?>" class="filter_link <?=$activeClass?> <?=$arrowClass?>"><?=$sortTitle?></span>
						</div>
						<?
					}
					if(isset($_GET["sortFields"]))
					{
						$arParams["ELEMENT_SORT_FIELD"] = $_GET["sortFields"];
						$arParams["ELEMENT_SORT_ORDER"] = $_GET["sortOrder"];
					}
					?>
				</div>
				<div class="f_right">
					<?
					if(isset($_GET["SALELEADER"]) && !empty($_GET["SALELEADER"]))
					{
						$arrFilter["=PROPERTY_SALELEADER"] = 90;
						$urlHit = $APPLICATION->GetCurPageParam("", array("SALELEADER", "SPECIALOFFER"));
					}else{
						$urlHit = $APPLICATION->GetCurPageParam("SALELEADER=да", array("SALELEADER", "SPECIALOFFER"));
					}

					if(isset($_GET["SPECIALOFFER"]) && !empty($_GET["SPECIALOFFER"]))
					{
						$arrFilter["=PROPERTY_SPECIALOFFER"] = 91;
						$urlSale = $APPLICATION->GetCurPageParam("", array("SPECIALOFFER", "SALELEADER"));
					}else{
						$urlSale = $APPLICATION->GetCurPageParam("SPECIALOFFER=да", array("SPECIALOFFER", "SALELEADER"));
					}
					?>
					<div class="filter_item">
						<span rel="<?=$urlHit?>" class="filter_link hits sales <?if(!empty($_GET["SALELEADER"])):?>active<?endif;?>"></span>
					</div>

					<div class="filter_item">
						<span rel="<?=$urlSale?>" class="filter_link hits actions <?if(!empty($_GET["SPECIALOFFER"])):?>active<?endif;?>"></span>
					</div>
				</div>
			</div>
			</noindex>
		<? endif ?>
        <?
        //CDev::pre($arrFilter);
        ?>
		<?
		if(isset($_GET["sortFields"]))
		{
			$arParams["ELEMENT_SORT_FIELD"] = $_GET["sortFields"];
			$arParams["ELEMENT_SORT_ORDER"] = $_GET["sortOrder"];

				if(ToUpper($_GET["sortFields"]) != 'CATALOG_PRICE_1'){
					$arParams["ELEMENT_SORT_FIELD2"] = 'CATALOG_PRICE_1';
					$arParams["ELEMENT_SORT_ORDER2"] = 'asc';
				}

			$arParams["HIDE_NOT_AVAILABLE"] = 'Y';
		}
		?>
        <?
			if ( defined('MOBILE') ){
				
				$intSectionID = $APPLICATION->IncludeComponent(
					"pixelplus:catalog.section",
					"index_mobile",
					array(
						"HEAD_SECTION"                    => "battery",
						"IBLOCK_TYPE"                     => "catalog",
						"IBLOCK_ID"                       => "2",
						"SECTION_ID"                      => "",
						"SECTION_CODE"                    => "",
						"SECTION_USER_FIELDS"             => array(
							0 => "",
							1 => "",
						),
			            "ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
			            "ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
			            "ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
			            "ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
						"FILTER_NAME"                     => "arrFilter",
						"INCLUDE_SUBSECTIONS"             => "Y",
						"SHOW_ALL_WO_SECTION"             => "Y",
						"HIDE_NOT_AVAILABLE"              => "Y",
						"PAGE_ELEMENT_COUNT"              => "12",
						"LINE_ELEMENT_COUNT"              => "3",
						"PROPERTY_CODE"                   => array("BRAND",
							"MODEL",
							"MODEL_ROW",
							"TITLE",
							"KEYWORDS",
							"META_DESCRIPTION",
							"SALELEADER",
							"SPECIALOFFER",
							"ARTNUMBER",
							"BLOG_POST_ID",
							"GARANTY",
							"CAPACITY",
							"BLOG_COMMENTS_CNT",
							"FORUM_MESSAGE_CNT",
							"vote_count",
							"POLARITY",
							"START_TOK",
							"rating",
							"RECOMMEND",
							"vote_sum",
							"FORUM_TOPIC_ID",
							"TYPE",
							"TOKEND",
							"PRICE_CHANGE",
							"LENGTH",
							"WIDTH",
							"HEIGHT",
							"",
						),
						"OFFERS_LIMIT"                    => "5",
						"ADD_PICT_PROP"                   => "-",
						"LABEL_PROP"                      => "-",
						"PRODUCT_SUBSCRIPTION"            => "N",
						"SHOW_DISCOUNT_PERCENT"           => "N",
						"SHOW_OLD_PRICE"                  => "N",
						"SHOW_CLOSE_POPUP"                => "N",
						"MESS_BTN_BUY"                    => "Купить",
						"MESS_BTN_ADD_TO_BASKET"          => "В корзину",
						"MESS_BTN_SUBSCRIBE"              => "Подписаться",
						"MESS_BTN_COMPARE"                => "Сравнить",
						"MESS_BTN_DETAIL"                 => "Подробнее",
						"MESS_NOT_AVAILABLE"              => "Нет в наличии",
						"SECTION_URL"                     => "",
						"DETAIL_URL"                      => "",
						"SECTION_ID_VARIABLE"             => "SECTION_ID",
						"AJAX_MODE"                       => "N",
						"AJAX_OPTION_JUMP"                => "N",
						"AJAX_OPTION_STYLE"               => "N",
						"AJAX_OPTION_HISTORY"             => "N",
						"AJAX_OPTION_ADDITIONAL"          => "",
						"CACHE_TYPE"                      => "A",
						"CACHE_TIME"                      => "36000000",
						"CACHE_GROUPS"                    => "N",
						"SET_TITLE"                       => "N",
						"SET_BROWSER_TITLE"               => "N",
						"BROWSER_TITLE"                   => "-",
						"SET_META_KEYWORDS"               => "N",
						"META_KEYWORDS"                   => "-",
						"SET_META_DESCRIPTION"            => "N",
						"META_DESCRIPTION"                => "-",
						"ADD_SECTIONS_CHAIN"              => "N",
						"SET_STATUS_404"                  => "N",
						"CACHE_FILTER"                    => "N",
						"ACTION_VARIABLE"                 => "action",
						"PRODUCT_ID_VARIABLE"             => "id",
						"PRICE_CODE"                      => array(
							0 => "BASE",
						),
						"USE_PRICE_COUNT"                 => "N",
						"SHOW_PRICE_COUNT"                => "1",
						"PRICE_VAT_INCLUDE"               => "Y",
						"CONVERT_CURRENCY"                => "N",
						"BASKET_URL"                      => "/personal/basket.php",
						"USE_PRODUCT_QUANTITY"            => "N",
						"PRODUCT_QUANTITY_VARIABLE"       => "",
						"ADD_PROPERTIES_TO_BASKET"        => "Y",
						"PRODUCT_PROPS_VARIABLE"          => "prop",
						"PARTIAL_PRODUCT_PROPERTIES"      => "N",
						"PRODUCT_PROPERTIES"              => array(),
						"ADD_TO_BASKET_ACTION"            => "ADD",
						"DISPLAY_COMPARE"                 => "N",
						"PAGER_TEMPLATE"                  => "arrows_mobile",
						"DISPLAY_TOP_PAGER"               => "N",
						"DISPLAY_BOTTOM_PAGER"            => "Y",
						"PAGER_TITLE"                     => "Товары",
						"PAGER_SHOW_ALWAYS"               => "N",
						"PAGER_DESC_NUMBERING"            => "N",
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
						"PAGER_SHOW_ALL"                  => "N",
						"OFFERS_CART_PROPERTIES"          => array(
							0 => "SIZES_SHOES",
							1 => "SIZES_CLOTHES",
							2 => "COLOR_REF",
						),
						"OFFERS_FIELD_CODE"               => array(
							0 => "NAME",
							1 => "PREVIEW_PICTURE",
							2 => "DETAIL_PICTURE",
							3 => "",
						),
						"OFFERS_PROPERTY_CODE"            => array(
							0 => "SIZES_SHOES",
							1 => "SIZES_CLOTHES",
							2 => "COLOR_REF",
							3 => "MORE_PHOTO",
							4 => "ARTNUMBER",
							5 => "",
						),
						"TEMPLATE_THEME"                  => "blue",
					),
					$component
				);
				
			}else{
					$intSectionID = $APPLICATION->IncludeComponent(
					"bitrix:catalog.section",
					"budget",
					array(
						"COMPONENT_TEMPLATE" => "budget",
						"HEAD_SECTION" => "budget",
						"IBLOCK_TYPE" => "catalog",
						"IBLOCK_ID" => "2",
						"SECTION_ID" => "",
						"SECTION_CODE" => "",
						"SECTION_USER_FIELDS" => array(
							0 => "",
							1 => "",
						),
						"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
						"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
						"ELEMENT_SORT_FIELD2" => "id",
						"ELEMENT_SORT_ORDER2" => "desc",
						"FILTER_NAME" => "arrFilter",
						"INCLUDE_SUBSECTIONS" => "Y",
						"SHOW_ALL_WO_SECTION" => "Y",
						"HIDE_NOT_AVAILABLE" => "Y",
						"PAGE_ELEMENT_COUNT" => "12",
						"LINE_ELEMENT_COUNT" => "3",
						"PROPERTY_CODE" => array("BRAND", "MODEL", "MODEL_ROW", "TITLE", "KEYWORDS", "META_DESCRIPTION", "SALELEADER", "SPECIALOFFER", "ARTNUMBER", "BLOG_POST_ID", "GARANTY", "CAPACITY", "BLOG_COMMENTS_CNT", "FORUM_MESSAGE_CNT", "vote_count", "POLARITY", "START_TOK", "rating", "RECOMMEND", "vote_sum", "FORUM_TOPIC_ID", "TYPE", "TOKEND", "PRICE_CHANGE", "LENGTH", "WIDTH", "HEIGHT", ""),
						"OFFERS_LIMIT" => "5",
						"TEMPLATE_THEME" => "",
						"ADD_PICT_PROP" => "-",
						"LABEL_PROP" => "-",
						"PRODUCT_SUBSCRIPTION" => "N",
						"SHOW_DISCOUNT_PERCENT" => "N",
						"SHOW_OLD_PRICE" => "N",
						"SHOW_CLOSE_POPUP" => "N",
						"MESS_BTN_BUY" => "Купить",
						"MESS_BTN_ADD_TO_BASKET" => "В корзину",
						"MESS_BTN_SUBSCRIBE" => "Подписаться",
						"MESS_BTN_COMPARE" => "Сравнить",
						"MESS_BTN_DETAIL" => "Подробнее",
						"MESS_NOT_AVAILABLE" => "Нет в наличии",
						"SECTION_URL" => "",
						"DETAIL_URL" => "",
						"SECTION_ID_VARIABLE" => "SECTION_ID",
						"AJAX_MODE" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "N",
						"AJAX_OPTION_HISTORY" => "N",
						"AJAX_OPTION_ADDITIONAL" => "",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "36000000",
						"CACHE_GROUPS" => "N",
						"SET_TITLE" => "N",
						"SET_BROWSER_TITLE" => "N",
						"BROWSER_TITLE" => "-",
						"SET_META_KEYWORDS" => "N",
						"META_KEYWORDS" => "-",
						"SET_META_DESCRIPTION" => "N",
						"META_DESCRIPTION" => "-",
						"ADD_SECTIONS_CHAIN" => "N",
						"SET_STATUS_404" => "N",
						"CACHE_FILTER" => "N",
						"ACTION_VARIABLE" => "action",
						"PRODUCT_ID_VARIABLE" => "id",
						"PRICE_CODE" => array(
							0 => "BASE",
						),
						"USE_PRICE_COUNT" => "N",
						"SHOW_PRICE_COUNT" => "1",
						"PRICE_VAT_INCLUDE" => "Y",
						"CONVERT_CURRENCY" => "N",
						"BASKET_URL" => "/personal/basket.php",
						"USE_PRODUCT_QUANTITY" => "N",
						"PRODUCT_QUANTITY_VARIABLE" => "",
						"ADD_PROPERTIES_TO_BASKET" => "Y",
						"PRODUCT_PROPS_VARIABLE" => "prop",
						"PARTIAL_PRODUCT_PROPERTIES" => "N",
						"PRODUCT_PROPERTIES" => array(
						),
						"ADD_TO_BASKET_ACTION" => "ADD",
						"DISPLAY_COMPARE" => "N",
						"PAGER_TEMPLATE" => "arrows",
						"DISPLAY_TOP_PAGER" => "N",
						"DISPLAY_BOTTOM_PAGER" => "Y",
						"PAGER_TITLE" => "Товары",
						"PAGER_SHOW_ALWAYS" => "N",
						"PAGER_DESC_NUMBERING" => "N",
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
						"PAGER_SHOW_ALL" => "N"
					),
					$component
				);
			}
?><?
        $GLOBALS['CATALOG_CURRENT_SECTION_ID'] = $intSectionID;
        unset($basketAction);
        //$APPLICATION->SetTitle($sectionName);
        ?>

		<div class="section-description">
			<h2>Для чего в автомобиле АКБ?</h2>
			<p>Если в вашем авто вышла из строя аккумуляторная батарея, вы больше не можете запускать его двигатель, пользоваться навигатором, кондиционером, музыкальным проигрывателем и прочими электронными устройствами, установленными в салоне. Мы поможем вам решить эту проблему. Магазин kupit-akkumulyator.ru предлагает недорого купить аккумулятор для авто в Москве. Наш магазин торгует сертифицированной, оригинальной продукцией. Мы реализуем недорогие автомобильные АКБ, характеризующиеся бесперебойной, надежной работой.</p>
			<p>Цена такого устройства зависит от его марки, качества и мощности. Мы предлагаем АКБ различных ценовых категорий. Мы реализуем как недорогие АКБ для автомобиля, так и достаточно дорогостоящие модели. При выборе подходящей аккумуляторной батареи советуем отдавать предпочтение продукции от авторитетных производителей. Более высокая цена таких моделей полностью компенсируется их отличным качеством. </p>
			<h2>Аккумуляторная продукция в магазине «Купить аккумулятор»</h2>
			<p>Чтобы купить аккумуляторы для автомобиля в Москве, сегодня не обязательно ехать в магазин или на авторынок. При желании покупку можно совершить через интернет. На портале kupit-akkumulyator.ru вы можете приобрести дешевые аккумуляторы для различных марок машин. В нашем каталоге вы найдете модели, предназначенные для большинства современных автомобилей. </p>
			<p>Приобретая у нас недорогие авто АКБ, вы получаете товар отличного качества. Мы работаем с проверенными производителями, поэтому ручаемся за качество нашей продукции. У нас можно приобрести недорогой аккумулятор с обратной полярностью, а также купить другие модели аккумуляторных батарей. Вас приятно удивит богатый ассортимент нашей продукции. Нами осуществляется продажа запчастей для авто от ведущих мировых брендов.</p>
			<h2>Преимущества нашего магазина:</h2>
			<ul>
				<li>Обширный ассортимент оригинальной, сертифицированной продукции высокого качества.</li>
				<li>Доступные цены.</li>
				<li>Своевременная доставка заказанных изделий покупателям.</li>
			</ul>
			<p>Если вы желаете дешево купить автомобильный аккумулятор в Москве, заказывайте его на сайте kupit-akkumulyator.ru. Оформить заказ на нашем портале очень просто. Чтобы купить АКБ на автомобиль в интернет-магазине нашей компании, необходимо выбрать подходящий товар на нашей сайте. Поместите выбранный товар в корзину. Затем оформите свой заказ на аккумуляторную батарею для авто.</p>


		</div>
    </article>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
