<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "");
$APPLICATION->SetPageProperty("keywords", "");
$APPLICATION->SetPageProperty("description", "");
$APPLICATION->SetTitle("");
$filterView = "VERTICAL";
?>

<div class="clearfix">
    <? if($showFilter): ?>
        <aside class="grid_7 prefix_2 resp_col7">
            <?if($sectionName == 'accessories'):?>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:catalog.section.list",
                    "filter",
                    array(
                        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                        "SECTION_ID" => $currentRootSection["ID"],
                        "SECTION_CODE" => $currentRootSection["CODE"],
                        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                        "CACHE_TIME" => $arParams["CACHE_TIME"],
                        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                        "COUNT_ELEMENTS" => "N",
                        "TOP_DEPTH" => "3",
                        "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
                        "VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
                        "SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
                        "HIDE_SECTION_NAME" => "N",
                        "ADD_SECTIONS_CHAIN" => "N"
                    ),
                    $component,
                    array("HIDE_ICONS" => "N")
                );?>
            <?else:?>
			  <?if($sectionName != 'masla'):?>
                <?$APPLICATION->IncludeComponent(
                	"irbis:model.filter",
                	"",
                	array(
                		"BRAND_HL" => 5,
                        "MODEL_HL" => 3,
                        "MODEL_ROW_HL" => 4,
                        "FILTER_NAME" => "",
                        "SELECT_FIELD" => ""
                	),
                	$component,
                	array("HIDE_ICONS" => "Y")
                );
                ?>
			  <?endif;?>
                <?
                if(isset($_GET["set_filter"]))
                {
                    if(!empty($_GET["CATALOG_LENGTH"]))
                    {
                        $arrFilter["<=CATALOG_LENGTH"] = $_GET["CATALOG_LENGTH"];
                    }
                }
                ?>



                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/banner-market.php"), false);?>



                <?

        		if(!empty($arResult['VARIABLES']['SECTION_ID']) && $arResult['VARIABLES']['SECTION_ID'] == 125){
        			$currentRootSection["ID"] = 125;
        		}
        		$APPLICATION->IncludeComponent(
            		"bitrix:catalog.smart.filter",
            		"vertical",
            		array(
                        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                        "SECTION_ID" => $currentRootSection["ID"],
                        "FILTER_NAME" => "arrFilter",
                        "PRICE_CODE" => $arParams["PRICE_CODE"],
                        "CACHE_TYPE" => "N",
                        "CACHE_TIME" => "36000000",
                        "CACHE_NOTES" => "",
                        "CACHE_GROUPS" => "N",
                        "SAVE_IN_SESSION" => "N",
						"DOP_FILTER" => $sectionName,
                        "TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"]
            		),
            		$component,
            		array('HIDE_ICONS' => 'Y')
            	);?>

            <?endif;?>
        </aside> <!-- left col END !! -->
    <?endif;?>

    <article class="812x grid_29 prefix_1 resp_col29">
        <div class="hidden-block">
        </div>
        <?
        if($arParams["USE_COMPARE"]=="Y")
        {
        	?><?$APPLICATION->IncludeComponent(
        		"bitrix:catalog.compare.list",
        		"",
        		array(
        			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
        			"NAME" => $arParams["COMPARE_NAME"],
        			"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
        			"COMPARE_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["compare"],
        			"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
        			"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
        			'POSITION_FIXED' => isset($arParams['COMPARE_POSITION_FIXED']) ? $arParams['COMPARE_POSITION_FIXED'] : '',
        			'POSITION' => isset($arParams['COMPARE_POSITION']) ? $arParams['COMPARE_POSITION'] : ''
        		),
        		$component,
        		array("HIDE_ICONS" => "Y")
        	);?><?
        }

        if (isset($arParams['USE_COMMON_SETTINGS_BASKET_POPUP']) && $arParams['USE_COMMON_SETTINGS_BASKET_POPUP'] == 'Y')
        {
        	$basketAction = (isset($arParams['COMMON_ADD_TO_BASKET_ACTION']) ? $arParams['COMMON_ADD_TO_BASKET_ACTION'] : '');
        }
        else
        {
        	$basketAction = (isset($arParams['SECTION_ADD_TO_BASKET_ACTION']) ? $arParams['SECTION_ADD_TO_BASKET_ACTION'] : '');
        }
        $intSectionID = 0;
        ?>

        <div class="clearfix mb12 catalog_filter">

            <div class="f_right">

        </div>



		<div class="h2 black fs_24 reg f_left"><?=$sectionName;?></div>

<div class = "TOP_TEXT"></div>
		<noindex>
        <div class="filter_row clearfix grn_bg mb34">
            <div class="f_left">
                <div class="filter_label">Сортировать по:</div>
                <?
                global $arrFilter;
                $arrFilter["!PROPERTY_NEDOROGIE_AKKUMULYATORY"] = false;

                $arSort = array(
                    "catalog_PRICE_1" => "цене",
                    "NAME" => "названию",
                    "PROPERTY_RATING" => "рейтингу",
                );
                foreach($arSort as $sortKey=>$sortTitle)
                {
                    if(isset($_GET["sortFields"]) && $_GET["sortFields"]==$sortKey)
                    {
                        $activeClass = "active";
                        if($_GET["sortOrder"]=="desc")
                        {
                            $sortOrder = "asc";
                            $arrowClass = "arrow_down";
                        }else{
                            $sortOrder = "desc";
                            $arrowClass = "arrow_up";
                        }
                    }else{
                        if($sortKey=="catalog_PRICE_1")
                        {
                            $activeClass = "active";
                            $sortOrder = "desc";
                            $arrowClass = "arrow_up";
                        }else{
                            $activeClass = "";
                            $sortOrder = "desc";
                            $arrowClass = "arrow_down";
                        }
                    }
                    $url = $APPLICATION->GetCurPageParam("sortFields=".$sortKey."&sortOrder=".$sortOrder, array("sortFields", "sortOrder"));
                    ?>
                    <div class="filter_item">
                        <a href="<?=$url?>" class="filter_link <?=$activeClass?> <?=$arrowClass?>"><?=$sortTitle?></a>
                    </div>
                    <?
                }
                if(isset($_GET["sortFields"]))
                {
                    $arParams["ELEMENT_SORT_FIELD"] = $_GET["sortFields"];
                    $arParams["ELEMENT_SORT_ORDER"] = $_GET["sortOrder"];
                }
                ?>
            </div>
            <div class="f_right">
                <?
                if(isset($_GET["SALELEADER"]) && !empty($_GET["SALELEADER"]))
                {
                    $arrFilter["PROPERTY_SALELEADER_VALUE"] = $_GET["SALELEADER"];
                    $urlHit = $APPLICATION->GetCurPageParam("", array("SALELEADER", "SPECIALOFFER"));
                }else{
                    $urlHit = $APPLICATION->GetCurPageParam("SALELEADER=да", array("SALELEADER", "SPECIALOFFER"));
                }

                if(isset($_GET["SPECIALOFFER"]) && !empty($_GET["SPECIALOFFER"]))
                {
                    $arrFilter["PROPERTY_SPECIALOFFER_VALUE"] = $_GET["SPECIALOFFER"];
                    $urlSale = $APPLICATION->GetCurPageParam("", array("SPECIALOFFER", "SALELEADER"));
                }else{
                    $urlSale = $APPLICATION->GetCurPageParam("SPECIALOFFER=да", array("SPECIALOFFER", "SALELEADER"));
                }
                ?>
                <div class="filter_item">
                    <a href="<?=$urlHit?>" class="filter_link hits <?if(!empty($_GET["SALELEADER"])):?>active<?endif;?>">хиты продаж</a>
                </div>
                <div class="filter_item">
                    <a href="<?=$urlSale?>" class="filter_link hits <?if(!empty($_GET["SPECIALOFFER"])):?>active<?endif;?>">акции</a>
                </div>
            </div>
        </div>
        </noindex>

        <?
        //CDev::pre($arrFilter);
        ?>
        <?$intSectionID = $APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"index", 
	array(
		"COMPONENT_TEMPLATE" => "index",
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "2",
		"SECTION_ID" => "",
		"SECTION_CODE" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
		"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER2" => "desc",
		"FILTER_NAME" => "arrFilter",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "Y",
		"HIDE_NOT_AVAILABLE" => "Y",
		"PAGE_ELEMENT_COUNT" => "12",
		"LINE_ELEMENT_COUNT" => "3",
   		"PROPERTY_CODE" => array("BRAND", "MODEL", "MODEL_ROW", "TITLE", "KEYWORDS", "META_DESCRIPTION", "SALELEADER", "SPECIALOFFER", "ARTNUMBER", "BLOG_POST_ID", "GARANTY", "CAPACITY", "BLOG_COMMENTS_CNT", "FORUM_MESSAGE_CNT", "vote_count", "POLARITY", "START_TOK", "rating", "RECOMMEND", "vote_sum", "FORUM_TOPIC_ID", "TYPE", "TOKEND", "PRICE_CHANGE", "LENGTH", "WIDTH", "HEIGHT", ""),
		"OFFERS_LIMIT" => "5",
		"TEMPLATE_THEME" => "",
		"ADD_PICT_PROP" => "-",
		"LABEL_PROP" => "-",
		"PRODUCT_SUBSCRIPTION" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_CLOSE_POPUP" => "N",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_BTN_COMPARE" => "Сравнить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"SECTION_URL" => "",
		"DETAIL_URL" => "",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "N",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"BROWSER_TITLE" => "-",
		"SET_META_KEYWORDS" => "N",
		"META_KEYWORDS" => "-",
		"SET_META_DESCRIPTION" => "N",
		"META_DESCRIPTION" => "-",
		"ADD_SECTIONS_CHAIN" => "N",
		"SET_STATUS_404" => "N",
		"CACHE_FILTER" => "N",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRICE_CODE" => array(
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"BASKET_URL" => "/personal/basket.php",
		"USE_PRODUCT_QUANTITY" => "N",
		"PRODUCT_QUANTITY_VARIABLE" => "",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRODUCT_PROPERTIES" => array(
		),
		"ADD_TO_BASKET_ACTION" => "ADD",
		"DISPLAY_COMPARE" => "N",
		"PAGER_TEMPLATE" => "arrows",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Товары",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N"
	),
	$component
);?><?
        $GLOBALS['CATALOG_CURRENT_SECTION_ID'] = $intSectionID;
        unset($basketAction);
        //$APPLICATION->SetTitle($sectionName);
        ?>

		<div class="section-description">
			<h2>Для чего в автомобиле АКБ?</h2>
			<p>Если в вашем авто вышла из строя аккумуляторная батарея, вы больше не можете запускать его двигатель, пользоваться навигатором, кондиционером, музыкальным проигрывателем и прочими электронными устройствами, установленными в салоне. Мы поможем вам решить эту проблему. Магазин kupit-akkumulyator.ru предлагает недорого купить аккумулятор для авто в Москве. Наш магазин торгует сертифицированной, оригинальной продукцией. Мы реализуем недорогие автомобильные АКБ, характеризующиеся бесперебойной, надежной работой.</p>
			<p>Цена такого устройства зависит от его марки, качества и мощности. Мы предлагаем АКБ различных ценовых категорий. Мы реализуем как недорогие АКБ для автомобиля, так и достаточно дорогостоящие модели. При выборе подходящей аккумуляторной батареи советуем отдавать предпочтение продукции от авторитетных производителей. Более высокая цена таких моделей полностью компенсируется их отличным качеством. </p>
			<h2>Аккумуляторная продукция в магазине «Купить аккумулятор»</h2>
			<p>Чтобы купить аккумуляторы для автомобиля в Москве, сегодня не обязательно ехать в магазин или на авторынок. При желании покупку можно совершить через интернет. На портале kupit-akkumulyator.ru вы можете приобрести дешевые аккумуляторы для различных марок машин. В нашем каталоге вы найдете модели, предназначенные для большинства современных автомобилей. </p>
			<p>Приобретая у нас недорогие авто АКБ, вы получаете товар отличного качества. Мы работаем с проверенными производителями, поэтому ручаемся за качество нашей продукции. У нас можно приобрести недорогой аккумулятор с обратной полярностью, а также купить другие модели аккумуляторных батарей. Вас приятно удивит богатый ассортимент нашей продукции. Нами осуществляется продажа запчастей для авто от ведущих мировых брендов.</p>
			<h2>Преимущества нашего магазина:</h2>
			<ul>
				<li>Обширный ассортимент оригинальной, сертифицированной продукции высокого качества.</li>
				<li>Доступные цены.</li>
				<li>Своевременная доставка заказанных изделий покупателям.</li>
			</ul>
			<p>Если вы желаете дешево купить автомобильный аккумулятор в Москве, заказывайте его на сайте kupit-akkumulyator.ru. Оформить заказ на нашем портале очень просто. Чтобы купить АКБ на автомобиль в интернет-магазине нашей компании, необходимо выбрать подходящий товар на нашей сайте. Поместите выбранный товар в корзину. Затем оформите свой заказ на аккумуляторную батарею для авто.</p>
		
		
		</div>
    </article>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>