<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if(!$_REQUEST['code'] || !($imgCode = getBXElement($_REQUEST['code']))){
	status_404();
}
if(($img = getImageByCode($imgCode))){
	putFImage($img);
} else {
	status_404();
}
/////////////////////////////////////

function status_404(){
	global $APPLICATION;
	$APPLICATION->RestartBuffer();
	CHTTP::SetStatus("404 Not Found");
	exit;
}

function getBXElement($code){
	CModule::IncludeModule("iblock");
	$res = CIBlockElement::GetList(
		 array("SORT"=>"ASC"),
		 array("IBLOCK_ID"=>2, "ACTIVE"=>"Y", "=CODE"=>$code),
		 false,
		 false,
		 array("code","ID","DETAIL_PICTURE","PREVIEW_PICTURE")
	);
	while($item = $res->GetNextElement()){
		if($item->fields['CODE'] == $code)
			return ($item->fields["DETAIL_PICTURE"]) ? $item->fields["DETAIL_PICTURE"] : $item->fields["PREVIEW_PICTURE"];	
	}
	return false;
}

function getImageByCode($imgCode){
	return CFile::GetFileArray($imgCode);
}

function putFImage($img){
	$date = date('r', strtotime($img['TIMESTAMP_X']));
#	var_dump(date('r', $date));
	header("Content-Type:".$img['CONTENT_TYPE']);
	header("Content-Length:".$img['FILE_SIZE']);
	header("Accept-Ranges: bytes");
	header("Last-Modified:".$date);
	header("X-size:".$img['WIDTH']."x".$img['HEIGHT']);
	@readfile($_SERVER['DOCUMENT_ROOT'].$img['SRC']);
}