<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Аккумуляторы оптом");
?>
<?if(defined('MOBILE')):?>
	<p>Для оптовых закупок звоните по тел.: <strong class="phone-number">+7 (499) 390 52 82</strong></p>
<?else:?>
	<h1>Аккумуляторы оптом</h1>
	<p>Для оптовых закупок звоните по тел. <span style = "font-weight: bold;">+7 (499) 390 52 82</span></p>
<?endif?>

<?
$formTemplate = defined('MOBILE') ? 'form_mobile' : '.default' ;
$APPLICATION->IncludeComponent(
	"irbis:form",
	"$formTemplate",
	Array()
)?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
