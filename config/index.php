<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Загрузка остатков и цен");

CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");

if(isset($_POST["upload"]))
{
    
    if($_FILES["filename"]["size"] > 1024*3*1024)
    {
        echo ("Размер файла превышает три мегабайта");
        exit;
    }
    if(is_uploaded_file($_FILES["filename"]["tmp_name"]))
    {
        move_uploaded_file($_FILES["filename"]["tmp_name"], $_SERVER["DOCUMENT_ROOT"]."/config/export.csv");
        echo "Файл загружен на сервер. Теперь вы можете выгрузить данные на сайт!<br /><br />";
    } else {
        echo("Ошибка загрузки файла<br />");
    }
}

if(isset($_POST["upload_price"]))
{
    $arItems = array();
    $arSelect = Array("NAME", "ID", "PROPERTY_ARTNUMBER");
    $arFilter = Array("IBLOCK_ID"=>2);
    $res = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arFilter, false, false, $arSelect);
    while($arItem = $res->GetNext())
    { 
        $arItems[$arItem["PROPERTY_ARTNUMBER_VALUE"]] = $arItem;
    }
    
    $file = $_SERVER['DOCUMENT_ROOT']."/config/export.csv";
    
    try {
        $csv = new CSV($file);
        $get_csv = $csv->getCSV();
        foreach ($get_csv as $key=>$arStr) 
        {   
            if($key>0)
            {
                $articul = $arStr[0];
                $price = $arStr[1];
                $changePrice = $arStr[2];
                $quantity = $arStr[3];
                
                if(!empty($articul) && isset($arItems[$articul]))
                {
                    $PRODUCT_ID = $arItems[$articul]["ID"];
                    
                    // Установление цены для товара
                    $arFields = Array(
                        "PRODUCT_ID" => $PRODUCT_ID,
                        "CATALOG_GROUP_ID" => 1,
                        "PRICE" => $price,
                        "CURRENCY" => "RUB",
                    );
                    
                    $res = CPrice::GetList(
                        array(),
                        array(
                                "PRODUCT_ID" => $PRODUCT_ID,
                                "CATALOG_GROUP_ID" => 1
                            )
                    );
                    if ($arr = $res->Fetch())
                    {
                        CPrice::Update($arr["ID"], $arFields);
                    }
                    
                    if(intval($quantity)>0)
                    {
                        $quantity = 1000;
                        $active = "Y";
                    }else{
                        $quantity = 0;
                        $active = "N";
                    }
                    $arFields = array('QUANTITY' => $quantity);
                    CCatalogProduct::Update($PRODUCT_ID, $arFields);
                    
                    $el = new CIBlockElement;
                    $el->Update($PRODUCT_ID, array("ACTIVE"=>$active));
                    
                    if(intval($changePrice)>0)
                    {
                        CIBlockElement::SetPropertyValueCode($PRODUCT_ID, "PRICE_CHANGE", $changePrice);
                    }
                    
                }
            }
        }
        echo "Загрузка цен и остатков завершена успешно!<br />";
    }
    catch (Exception $e) {
        echo "Ошибка: " . $e->getMessage()."<br />";
    }
}
?>
<form action="" method="post" enctype="multipart/form-data">
    <h3 class="fs_20 black reg mb10 black_font">Форма загрузки файла</h3>
    <div class="mb_10">
        <input type="file" name="filename">
    </div> 
    <input type="submit" value="Загрузить файл" name="upload" class="btn2 grn_skin">&nbsp;&nbsp;
    <input type="submit" value="Загрузить остатки и цены" name="upload_price" class="btn2 grn_skin">
</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>