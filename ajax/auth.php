<div>
    <div class="auth_helper">
        <form class="sm_modal_inner al_center login-form">
            <span class="arcticmodal-close"></span>
            <h3 class="heavy fs_36 lts reg mb26">Вход</h3>
            <div class="error"></div>
            <fieldset class="mb15">
                <div class="form_row">
                    <input type="text" name="email" class="txt_field2" value="Ваш логин или почта" onBlur="if(this.value=='') this.value='Ваш логин или почта'" onFocus="if(this.value =='Ваш логин или почта' ) this.value=''">
                </div>
                <div class="form_row">
                    <input type="text" name="password" class="txt_field2" value="Ваш пароль" onBlur="if(this.value=='') this.value='Ваш пароль'" onFocus="if(this.value =='Ваш пароль' ) this.value=''">
                </div>
                <button class="big_btn heavy">войти</button>
            </fieldset>
            <div class="al_center">
                <a href="#" class="link1 modal_btn recover_link" data-modal="#backup_pass">Забыли пароль?</a>
            </div>
        </form>

        <div class="modal_btns al_center val_mid">
            <a href="#" class="link1 d_ib val_mid white modal_btn register_link" data-modal="#registration">Еще не зарегистрированы?</a>
            <a href="#" class="btn5 d_ib val_mid grn_skin modal_btn register_link" data-modal="#registration">регистрация</a>
        </div>
    </div>
</div>

<div>
    <div id="reg_helper">
        <form class="sm_modal_inner al_center register-form">
            <span class="arcticmodal-close"></span>
            <h3 class="heavy fs_36 lts reg mb26">регистрация</h3>
            <div class="error"></div>
            <fieldset class="mb15">
                <div class="form_row">
                    <input type="text" name="name" class="txt_field2" value="Ваше имя" onBlur="if(this.value=='') this.value='Ваше имя'" onFocus="if(this.value =='Ваше имя' ) this.value=''">
                </div>
                <div class="form_row">
                    <input type="text" name="email" class="txt_field2" value="Ваш адрес эл. почты" onBlur="if(this.value=='') this.value='Ваш адрес эл. почты'" onFocus="if(this.value =='Ваш адрес эл. почты' ) this.value=''">
                </div>
                <div class="form_row">
                    <input type="text" name="password" class="txt_field2" value="Ваш пароль" onBlur="if(this.value=='') this.value='Ваш пароль'" onFocus="if(this.value =='Ваш пароль' ) this.value=''">
                </div>
                <div class="form_row">
                    <input type="text" name="phone" class="txt_field2 phone_mask" value="Ваш номер телефона" placeholder="Ваш номер телефона" onBlur="if(this.value=='') this.value='Ваш номер телефона'" onFocus="if(this.value =='Ваш номер телефона' ) this.value=''">
                </div>
                <button class="big_btn heavy">войти</button>
            </fieldset>
        </form>

        <div class="modal_btns al_center val_mid">
            <a href="#" class="link1 d_ib val_mid white modal_btn" data-modal="#authorization">Уже зарегистрированы?</a>
            <a href="#" class="btn5 d_ib val_mid grn_skin modal_btn" data-modal="#authorization">Вход</a>
        </div>

    </div>
</div>

<div>
    <div id="recover_helper">
        <form class="sm_modal_inner al_center recovery-form">
            <span class="arcticmodal-close"></span>
            <h3 class="heavy fs_36 lts reg mb26">Восстановление пароля</h3>
            <div class="error"></div>
            <fieldset class="mb15">
                <div class="form_row">
                    <input type="text" name="email" class="txt_field2" value="Ваш адрес эл. почты" onBlur="if(this.value=='') this.value='Ваш адрес эл. почты'" onFocus="if(this.value =='Ваш адрес эл. почты' ) this.value=''">
                </div>
                <button class="big_btn heavy">сбросить пароль</button>
            </fieldset>
            <div class="form_comment gray_font form_label3">
                На указанный вами адрес электронной почты будет выслана инструкция по восстановлению пароля
            </div>
        </form>

        <div class="modal_btns al_center val_mid">
            <a href="#" class="link1 d_ib val_mid white modal_btn" data-modal="#authorization">Вспомнили пароль?</a>
            <a href="#" class="btn5 d_ib val_mid grn_skin modal_btn" data-modal="#authorization">Вход</a>
        </div>
    </div>
</div>
