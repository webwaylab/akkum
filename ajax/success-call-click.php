<?php
define('STOP_STATISTICS', true);
define('PUBLIC_AJAX_MODE', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
?>

<div class="tooltip_box order_success_message active">
    <span class="close_tooltip"></span>
    <div class="tooltip_box_inner">
        <p>
            <img src="<?= SITE_TEMPLATE_PATH ?>/images/done_form_icon.png" height="33" width="36" alt="">
        </p>
        <h4 class="bold">Ваш заказ принят.</h4>
        Ожидайте звонка. Спасибо!
    </div>
</div>

<?php require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php'); ?>