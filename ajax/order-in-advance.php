<form class="al_center order-in-advance-form">
    <div class="heavy fs_36 lts reg mb26">Заявка на заказ</div>
    <fieldset>
        <div class="form_row">
            <p class="bold fs_18"><?= $_REQUEST['PRODUCT_NAME']; ?> (<?= $_REQUEST['PRODUCT_QUANTITY']; ?> шт.)</p>
            <input type="hidden" name="product_name" value="<?= $_REQUEST['PRODUCT_NAME']; ?>">
            <input type="hidden" name="product_quantity" value="<?= $_REQUEST['PRODUCT_QUANTITY']; ?>">
        </div>
        <div class="error"></div>
        <div class="form_row">
            <input type="text" name="name" class="txt_field2" value="" placeholder="Ваше имя">
        </div>
        <div class="form_row">
            <input type="text" name="phone" class="txt_field2 phone_mask" placeholder="Ваш телефон">
        </div>
        <button class="big_btn heavy">Заказать</button>
    </fieldset>
</form>