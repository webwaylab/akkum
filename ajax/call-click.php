<form class="tooltip_box product_tooltip_box one-click">
    <input type="hidden" name="PRODUCT_ID" value=""/>
    <span class="close_tooltip"></span>
    <div class="error"></div>
    <div class="tooltip_box_inner">
        <h4 class="bold fs_14 mb5">Введите номер телефона:</h4>
            <div class="mb-5">
                <input type="text" name="PERSONAL_PHONE" class="txt_field al_center phone_mask" placeholder="+7(___) ___-__-__">
            </div>
        <button class="btn grn_skin d_block">Купить</button>
    </div>
</form>

<div class="tooltip_box product_tooltip_box order_success_message">
    <span class="close_tooltip"></span>
    <div class="tooltip_box_inner">
        <p><img src="" height="33" width="36" alt=""></p>
        <h4 class="bold">Ваш заказ принят.</h4>
        Ожидайте звонка. Спасибо!
    </div>
</div>
