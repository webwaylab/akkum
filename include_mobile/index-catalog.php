<?
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");
global $APPLICATION, $USER, $arrFilter, $catalogSectionRoot;

$catalogSectionRoot = 18;

$arParams = array();

if(isset($_GET["filter_form"]) && $_GET["filter_form"]=="model-form")
{
    if(!empty($_GET["MODEL_ROW"]))
    {
        $addFilter = getModificationInformation($_GET["MODEL_ROW"]);
        $arrFilter = array_merge($arrFilter, $addFilter);
    }

    $arBrand = getElementByID($_GET["BRAND"]);
    $arModel = getElementByID($_GET["MODEL"]);

    $sectionName = "Купить аккумулятор для ".$arBrand["NAME"]." ".$arModel["NAME"];

}
else
{
    if($APPLICATION->GetCurDir()=="/catalog/battery/" || isset($arrFilter["=PROPERTY_78"]) || isset($_GET["arrFilter_52_MIN"]) || isset($_GET["arrFilter_52_MAX"]))
    {
        $sectionName = "Аккумуляторы автомобильные";

        if(isset($arrFilter["=PROPERTY_78"]))
        {
            $xmlValue = $arrFilter["=PROPERTY_78"][0];
            $sectionName.= " ".getBrandAutoByXmlId($xmlValue);
        }

        if(intval($_GET["arrFilter_52_MIN"])>0 || intval($_GET["arrFilter_52_MAX"])>0)
        {
            $sectionName.=" емкостью ";
            if(intval($_GET["arrFilter_52_MIN"])>0)
            {
                $sectionName.= intval($_GET["arrFilter_52_MIN"]);
            }

            if(intval($_GET["arrFilter_52_MIN"])>0 && intval($_GET["arrFilter_52_MAX"])>0)
            {
                $sectionName.=" - ";
            }

            if(intval($_GET["arrFilter_52_MAX"])>0)
            {
                $sectionName.= intval($_GET["arrFilter_52_MAX"]);
            }
            $sectionName.= " А•ч";
        }
    }
    else
    {
        $sectionName = $currentRootSection["NAME"];
    }
} ?>

<noindex>
    <div class="catalog__sort">
        <div class="catalog__sort__label">Сортировать по:</div>
        <?
        $arSort = array(
			"catalog_PRICE_1" => "Цене",
			"PROPERTY_CAPACITY" => "Емкости",
			"PROPERTY_POLARITY" => "Полярности",
        );
        ?>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
        <fieldset>
            <ul>
                <li id="select">
                    <select name="catalog-sort" class="js" id="select-mobite">
                        <? foreach($arSort as $sortKey => $sortTitle):
                            if(isset($_GET["sortFields"]) && $_GET["sortFields"] == $sortKey)
                            {
                                $sortOrder = "asc";
                                if($sortKey=="PROPERTY_POLARITY"){
	                                $arrow = "(сначала Обратная)";
                                }else{
	                                $arrow = "&#x2191";
                                }
								$active = "";
                                if($_GET["sortOrder"] == "asc")
                                {
									$active = "selected";
                                }
                            }
                            else
                            {
                                $sortOrder = "asc";
								$active = "";
                                if($sortKey=="PROPERTY_POLARITY"){
	                                $arrow = "(сначала Обратная)";
                                }else{
	                                $arrow = "&#x2191";
                                }
                                if($sortKey == "catalog_PRICE_1")
                                {
                                    $active = "selected";
                                }
                            }
                            $url = $APPLICATION->GetCurPageParam("sortFields=".$sortKey."&sortOrder=".$sortOrder, array("sortFields", "sortOrder")); ?>

                            <option value="<?=$url?>" <?=$active?>><?=$sortTitle?> <?=$arrow?></option>
                            <?
							if(isset($_GET["sortFields"]) && $_GET["sortFields"] == $sortKey)
                            {
                                $sortOrder = "desc";
                                if($sortKey=="PROPERTY_POLARITY"){
	                                $arrow = "(сначала Прямая)";
                                }else{
	                                $arrow = "&#x2193"; //Стрелочки
                                }
								$active = "";
                                if($_GET["sortOrder"] == "desc")
                                {
									$active = "selected";
                                }
                            }
                            else
                            {
                                $active = "";
                                $sortOrder = "desc";
                                if($sortKey=="PROPERTY_POLARITY"){
	                                $arrow = "(сначала Прямая)";
                                }else{
	                                $arrow = "&#x2193"; //Стрелочки
                                }
                            }
                            $url = $APPLICATION->GetCurPageParam("sortFields=".$sortKey."&sortOrder=".$sortOrder, array("sortFields", "sortOrder")); ?>

                            <option value="<?=$url?>" <?=$active?>><?=$sortTitle?> <?=$arrow?></option>

                        <? endforeach;
                        if(isset($_GET["sortFields"]))
                        {
                            $arParams["ELEMENT_SORT_FIELD"] = $_GET["sortFields"];
                            $arParams["ELEMENT_SORT_ORDER"] = $_GET["sortOrder"];
                        }
                        else
                        {
                            $arParams["ELEMENT_SORT_FIELD"] = "catalog_PRICE_1";
                            $arParams["ELEMENT_SORT_ORDER"] = 'asc';
                        } ?>
                    </select>
                    <script>
						$(document).ready(function(){
							var select = $('li#select');
							/*select.on('click', 'ul.chosen-results li.active-result', function () {
								window.location.assign(select.find('select.js-select').val());
							});*/
							/*select.on('change', function () {
								window.location.assign(select.find('select.js-select').val());
							});*/
							$("li#select ul.chosen-results li.active-result").on("click", function(){
								alert("q");
							});
							$("select.js-select option").click(function () {
								alert("w");
							});
                            $("#select-mobite").select2({
                                minimumResultsForSearch: -1
                            });
							$("#select-mobite.js").change(function () {
								window.location.assign($(this).val());
							});
						});
                    </script>
                </li>

                <? if(isset($_GET["SALELEADER"]) && !empty($_GET["SALELEADER"]))
                {
                    $arrFilter["=PROPERTY_SALELEADER"] = 90;
                    $urlHit = $APPLICATION->GetCurPageParam("", array("SALELEADER", "SPECIALOFFER"));
                }
                else
                {
                    $urlHit = $APPLICATION->GetCurPageParam("SALELEADER=да", array("SALELEADER", "SPECIALOFFER"));
                }

                if(isset($_GET["SPECIALOFFER"]) && !empty($_GET["SPECIALOFFER"]))
                {
                    $arrFilter["=PROPERTY_SPECIALOFFER"] = 91;
                    $urlSale = $APPLICATION->GetCurPageParam("", array("SPECIALOFFER", "SALELEADER"));
                }
                else
                {
                    $urlSale = $APPLICATION->GetCurPageParam("SPECIALOFFER=да", array("SPECIALOFFER", "SALELEADER"));
                } ?>

                <li>
                    <input type="checkbox" name="catalog-sort" onchange="window.location.assign('<?=$urlHit?>')" id="catalog-sort-hits" <?= !empty($_GET["SALELEADER"]) ? 'checked' : ''?>/><label for="catalog-sort-hits">Хиты продаж</label>
                </li>
                <li>
                    <input type="checkbox" name="catalog-sort" onchange="window.location.assign('<?=$urlSale?>')" id="catalog-sort-stock" <?= !empty($_GET["SPECIALOFFER"]) ? 'checked' : ''?>/><label for="catalog-sort-stock">Акции</label>
                </li>
            </ul>
        </fieldset>
    </div>
</noindex>

<div class="catalog__items catalog__items_slider">
        <?$APPLICATION->IncludeComponent(
            "bitrix:catalog.section",
            "index_mobile",
            array(
                "IBLOCK_TYPE" => "catalog",
                "IBLOCK_ID" => "2",
                "HEAD_SECTION" => "battery",
                "SECTION_ID" => $catalogSectionRoot,
                "SECTION_CODE" => "",
                "SECTION_USER_FIELDS" => array(
                    0 => "",
                    1 => "",
                ),
                "ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
                "ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
                "ELEMENT_SORT_FIELD2" => "id",
                "ELEMENT_SORT_ORDER2" => "desc",
                "FILTER_NAME" => "arrFilter",
                "INCLUDE_SUBSECTIONS" => "Y",
                "SHOW_ALL_WO_SECTION" => "N",
                "HIDE_NOT_AVAILABLE" => "N",
                "PAGE_ELEMENT_COUNT" => "12",
                "LINE_ELEMENT_COUNT" => "1",
                "PROPERTY_CODE" => array(
                    0 => "TYPE",
                    1 => "CAPACITY",
                    2 => "POLARITY",
                    3 => "START_TOK",
                    4 => "TOKEND",
                    5 => "TITLE",
                    6 => "KEYWORDS",
                    7 => "META_DESCRIPTION",
                    8 => "SALELEADER",
                    9 => "SPECIALOFFER",
                    10 => "ARTNUMBER",
                    11 => "BLOG_POST_ID",
                    12 => "GARANTY",
                    13 => "BLOG_COMMENTS_CNT",
                    14 => "FORUM_MESSAGE_CNT",
                    15 => "vote_count",
                    16 => "rating",
                    17 => "RECOMMEND",
                    18 => "vote_sum",
                    19 => "FORUM_TOPIC_ID",
                    20 => "PRICE_CHANGE",
                    21 => "BRAND",
                    22 => "MODEL",
                    23 => "MODEL_ROW",
                    24 => "",
                ),
                "OFFERS_LIMIT" => "5",
                "TEMPLATE_THEME" => "green",
                "PRODUCT_SUBSCRIPTION" => "N",
                "SHOW_DISCOUNT_PERCENT" => "N",
                "SHOW_OLD_PRICE" => "N",
                "SHOW_CLOSE_POPUP" => "N",
                "MESS_BTN_BUY" => "Купить",
                "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                "MESS_BTN_SUBSCRIBE" => "Подписаться",
                "MESS_BTN_DETAIL" => "Подробнее",
                "MESS_NOT_AVAILABLE" => "Нет в наличии",
                "SECTION_ID_VARIABLE" => "SECTION_ID",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_GROUPS" => "N",
                "SET_TITLE" => "N",
                "SET_BROWSER_TITLE" => "N",
                "BROWSER_TITLE" => "-",
                "SET_META_KEYWORDS" => "N",
                "META_KEYWORDS" => "-",
                "SET_META_DESCRIPTION" => "Y",
                "META_DESCRIPTION" => "-",
                "ADD_SECTIONS_CHAIN" => "N",
                "SET_STATUS_404" => "N",
                "CACHE_FILTER" => "N",
                "ACTION_VARIABLE" => "action",
                "PRODUCT_ID_VARIABLE" => "id",
                "PRICE_CODE" => array(
                    0 => "BASE",
                ),
                "USE_PRICE_COUNT" => "N",
                "SHOW_PRICE_COUNT" => "1",
                "PRICE_VAT_INCLUDE" => "Y",
                "CONVERT_CURRENCY" => "N",
                "BASKET_URL" => "/personal/cart/",
                "USE_PRODUCT_QUANTITY" => "Y",
                "ADD_PROPERTIES_TO_BASKET" => "N",
                "PRODUCT_PROPS_VARIABLE" => "prop",
                "PARTIAL_PRODUCT_PROPERTIES" => "N",
                "PRODUCT_PROPERTIES" => array(
                ),
                "ADD_TO_BASKET_ACTION" => "ADD",
                "DISPLAY_COMPARE" => "Y",
                "COMPARE_PATH" => "/catalog/compare/",
                "PAGER_TEMPLATE" => "arrows",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Товары",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "ADD_PICT_PROP" => "MORE_PHOTO",
                "LABEL_PROP" => "-",
                "MESS_BTN_COMPARE" => "Сравнить",
                "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                "COMPONENT_TEMPLATE" => "index",
                "SECTION_URL" => "",
                "DETAIL_URL" => "",
                "AJAX_OPTION_ADDITIONAL" => "undefined"
            ),
            false
        );?>
</div>

<h3 class="ttu">Бюджетные аккумуляторы</h3>

<div class="catalog__items catalog__items_slider">
    <?
    global $similarFilter;
    $similarFilter["!PROPERTY_NEDOROGIE_AKKUMULYATORY"] = false;
    ?>
    <?$APPLICATION->IncludeComponent(
        "bitrix:catalog.top",
        "budget_mobile",
        Array(
            "IBLOCK_TYPE" => "catalog",
            "IBLOCK_ID" => "2",
            'HEAD_SECTION' => "battery",
            "ELEMENT_SORT_FIELD" => "sort",
            "ELEMENT_SORT_ORDER" => "asc",
            "ELEMENT_SORT_FIELD2" => "id",
            "ELEMENT_SORT_ORDER2" => "desc",
            "FILTER_NAME" => "similarFilter",
            "HIDE_NOT_AVAILABLE" => "N",
            "ELEMENT_COUNT" => "12",
            "LINE_ELEMENT_COUNT" => "1",
            "PROPERTY_CODE" => array("BRAND", "MODEL", "MODEL_ROW", "TITLE", "KEYWORDS", "META_DESCRIPTION", "SALELEADER", "SPECIALOFFER", "ARTNUMBER", "BLOG_POST_ID", "GARANTY", "CAPACITY", "BLOG_COMMENTS_CNT", "FORUM_MESSAGE_CNT", "vote_count", "POLARITY", "START_TOK", "rating", "RECOMMEND", "vote_sum", "FORUM_TOPIC_ID", "TYPE", "TOKEND", "PRICE_CHANGE", "LENGTH", "WIDTH", "HEIGHT", ""),
            "OFFERS_LIMIT" => "12",
            "VIEW_MODE" => "SECTION",
            "SHOW_DISCOUNT_PERCENT" => "N",
            "SHOW_OLD_PRICE" => "N",
            "SHOW_CLOSE_POPUP" => "N",
            "MESS_BTN_BUY" => "Купить",
            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
            "MESS_BTN_DETAIL" => "Подробнее",
            "MESS_NOT_AVAILABLE" => "Нет в наличии",
            "SECTION_URL" => "",
            "DETAIL_URL" => "",
            "SECTION_ID_VARIABLE" => "SECTION_ID",
            "CACHE_TYPE" => "N",
            "CACHE_TIME" => "36000000",
            "CACHE_GROUPS" => "N",
            "CACHE_FILTER" => "N",
            "ACTION_VARIABLE" => "action",
            "PRODUCT_ID_VARIABLE" => "id",
            "PRICE_CODE" => array("BASE"),
            "USE_PRICE_COUNT" => "N",
            "SHOW_PRICE_COUNT" => "1",
            "PRICE_VAT_INCLUDE" => "Y",
            "CONVERT_CURRENCY" => "Y",
            "BASKET_URL" => "/personal/cart/",
            "USE_PRODUCT_QUANTITY" => "N",
            "ADD_PROPERTIES_TO_BASKET" => "Y",
            "PRODUCT_PROPS_VARIABLE" => "prop",
            "PARTIAL_PRODUCT_PROPERTIES" => "N",
            "PRODUCT_PROPERTIES" => array(),
            "ADD_TO_BASKET_ACTION" => "ADD",
            "DISPLAY_COMPARE" => "Y",
            "TEMPLATE_THEME" => "site",
            "ADD_PICT_PROP" => "-",
            "LABEL_PROP" => "-",
            "MESS_BTN_COMPARE" => "Сравнить",
            "CURRENCY_ID" => "RUB",
            "COMPARE_PATH" => ""
        )
    );?>
</div>

<div class="section-description js-toggle-wrap">
    <h2>Преимущества сотрудничества с интернет-магазином «Купить аккумулятор»:</h2>
    <ul>
        <li>денежная выгода. Благодаря прямым контактам с дилерами и производителями все АКБ продаются без лишних торгово-закупочных, логистических, посреднических и прочих надбавок. Вы можете не только приобрести дешевый аккумулятор, но и дополнительно сэкономить за счет сдачи старой батареи, стоимость которой будет учтена как часть оплаты; </li>
        <li class="hidden" data-hidden="true">бесплатное консультирование. Специалисты расскажут об ассортименте АКБ, основных электротехнических и эксплуатационных параметрах, которые нужно учесть при выборе. Они же помогут определиться с покупкой аккумулятора для конкретного автомобиля; </li>
        <li class="hidden" data-hidden="true">честность, открытость, прозрачность. Вы приобретаете аккумулятор дешево, причем его стоимость озвучивается сразу и полностью. Платные навязанные услуги, замаскированные надбавки, «мелкий шрифт» и прочие коммерческие уловки исключены; </li>
        <li class="hidden" data-hidden="true">длительная официальная гарантия (срок зависит от конкретного производителя) на каждый экземпляр. </li>
    </ul>
    <p class="hidden" data-hidden="true">Заказать автомобильный аккумулятор можно на сайте интернет-магазина круглосуточно без выходных, бесплатно проконсультироваться — по телефону +7 (499) 650-55-55.</p>
    <span class="lnk-toggle"><span class="lnk-js js-toggle" data-text='{"show": "Показать все преимущества", "hide": "Свернуть"}'>Показать все преимущества</span></span>
</div>
