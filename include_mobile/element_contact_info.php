<div class="info__block">
    <div class="info__block-metro">
        <p>
            <span class="green">В наличии в магазине:</span>  <span><img width="19" height="14" style="margin-left: 5px;" title="" alt="metro" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAAOCAMAAAD6xte7AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABLFBMVEUAAADuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMivuMisAAADIaoX1AAAAYnRSTlMAIXrD5fnmDJP6tmYzIzpisPiVG9PKMQoJJSrA1M6qCIV4dZYEndINBufwEg/tCwK49ylVjIEe8n6xvcelf8lkJ/4uVu85juLklP0r6Udfv1pgN7t7bt5YAY/fbXIibBPxEBma1EcAAAABYktHRACIBR1IAAAACXBIWXMAAAsSAAALEgHS3X78AAAA3ElEQVQY01WNZ0/CABRFb2mFihSUMoqigpNWpopV1CJLsQ5ciAPX/f8/wkeMHzjJS05ObvKACUpA1WaCakDBPyF9NjwXMaKx+YV46C+ZiWTKTItYmcWlrDlJ6eWVVeTyYmvr2Njc2hbTkwUUbGcHxZJTRqWqA0ZtF9gj91EnY8CBa+DwSLYNshZxyWPxk1N4TeCsRdKRYxvoeOj2gHPyQkKfvJSBAz8Fq0T/qk+73WLXguLj+ga35AB38kAl7/Fg4/FpqJHPo5fXyuiN1IbvVYwTH9N4n/hyOc33zy+QOTCxBzL3GQAAAABJRU5ErkJggg==" />
                <b>Текстильщики</b>, 1-й Грайворонский пр-д, д, 4
            </span>
            <div class="info__block-metro-phone">
                <p>Телефон:</p>
                <p class="phone">+7 (499)<span> 650-55-55</span></p>
            </div>


        </p>
    </div>
    <div class="info__block-left">
        <div class="info__block-place">
            <p><span class="delivery__option">Стандартная доставка:</span> <span class="delivery__price"><b>300 руб.</b> в пределах МКАД</span></p>
            <p><span class="delivery__option">Экспресс доставка:</span> <span class="delivery__price"><b>700 руб.</b> в пределах МКАД</span></p>
            <p><span class="delivery__option">За МКАД:</span> <span class="delivery__price"><b>+25 р/км</b></span></p>
        </div>
        <div class="info__block-choose">
            <p><span class="green">Не можете определиться с выбором?</span></p>
            <p>У нас большой ассортимент с гарантией лучшей цены. Наши квалифицированные специалисты Вам помогут! </p>
        </div>
    </div>
    <div class="info__block-right">
        <!-- <a href="#minimap" role="button" data-toggle="modal"> -->
        <a class="info__block-right-open-map" role="button">
            <img src="<?=SITE_TEMPLATE_PATH?>/images/mini-map.png" height="65" width="121" alt="map">
        </a>

        <div style="" id="minimap" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" style="overflow: hidden;">
                    <div class="modal-header">
                        <button type="button" class="minimap__close" data-dismiss="modal" aria-hidden="true"></button>
                    </div>
                    <div class="modal-body">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2247.22833190998!2d37.725333316232344!3d55.71978398054456!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x414ab53db1aba451%3A0xd92ea1997e4ae69c!2zMS3QuSDQk9GA0LDQudCy0L7RgNC-0L3QvtCy0YHQutC40Lkg0L_RgC3QtCwgNCwg0JzQvtGB0LrQstCwLCAxMDk1MTg!5e0!3m2!1suk!2sru!4v1489654153610" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>


        <a data-modal="#callback_form" class="button big_callback_btn d_ib modal_btn" href="#"></a>
    </div>

</div>
