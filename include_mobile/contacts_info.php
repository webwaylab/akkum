<div class="f_left" style="margin-bottom: 2em;">
	<!-- <h3 class="fs_24 black reg mb30 black_font">Контактная информация</h3> -->
	<div class="descript_art" style="margin-bottom: 1.5rem;">
		<table class="transparent" style="width: 100%; height: 50px;" cellspacing="0">
		<tbody>
		<tr>
			<td style = "vertical-align: top;  width: 32%;">
                <span style="font-weight: bold;">Адрес:</span>
			</td>
			<td style = "padding-bottom: 10px;">
				 119590, г. Москва, 1-й Грайвороновский пр., д. 4
			</td>
		</tr>
		<tr>
			<td style = "vertical-align: top;">
                 <span style="font-weight: bold;">Телефоны:</span>
			</td>
			<td style = "padding-bottom: 10px;">
				<a href="tel:+7 (499) 650-52-09" onclick="yaCounter22871155.reachGoal('tel'); ga('send', 'pageview', '/tel/');">+7 (499) 650-52-09</a>,<br> <a href="tel:+7 (499) 650-55-55" onclick="yaCounter22871155.reachGoal('tel'); ga('send', 'pageview', '/tel/');">+7 (499) 650-55-55</a>
			</td>
		</tr>
		<tr>
			<td style = "vertical-align: top;">
                <span style="font-weight: bold;">E-mail:</span>
			</td>
			<td>
                <a href="mailto:info@kupit-akkumulyator.ru">info@kupit-akkumulyator.ru</a>
			</td>
		</tr>
		</tbody>
		</table>
	</div>
	<h2>График работы магазина</h2>
	<div class="descript_art">
		<table class="transparent" style="width: 100%;" cellpadding="0" cellspacing="0">
		<tbody>
		<tr>
			<td style = "vertical-align: top; width: 32%;">
                <span style="font-weight: bold;">Служба заказов:</span>
			</td>
			<td style = "padding-bottom: 10px;">
				 Пн-Вс 10:00 - 21:00, без выходных
			</td>
		</tr>
		<tr>
			<td style = "vertical-align: top;">
                <span style="font-weight: bold;">Доставка:</span>
			</td>
			<td style = "padding-bottom: 10px;">
				 Пн-Вс 13:00 - 21:00, без выходных
			</td>
		</tr>
		<tr>
			<td style = "vertical-align: top;">
                <span style="font-weight: bold;">Самовывоз:</span>
			</td>
			<td style = "padding-bottom: 10px;">
				<p>
					 Пн-Пт 10:00 - 21:00,<br>
					 Сб-Вс 10:00 - 19:00, без выходных
				</p>
			</td>
		</tr>
		</tbody>
		</table>
        <br>
		<p style="font-weight: bold;">
			 Оформление заказа на сайте — <span style="text-transform: uppercase">Круглосуточно</span>
		</p>

		<p>
			<span style="background-color: #ffffff;">ООО "ЛОМ-АКБ"</span><br>
			<span style="background-color: #ffffff;">ИНН/КПП 7722371656/772201001</span><br>
			<span style="background-color: #ffffff;">ОГРН 1167746726070</span><br>
		</p>

	</div>
</div>
<div class="f_right"  style="text-align: center; margin-bottom: 2em;">
    <a class="gallery" href="/upload/medialibrary/7d1/7d1fdcf639eb2ad6ad090de02d8dd244.jpg">
		<img style="width: 100%;" alt="img004" src="/upload/medialibrary/7d1/7d1fdcf639eb2ad6ad090de02d8dd244.jpg" title="LOM_AKB.jpg">
	</a>
</div>
<div class="clearfix">
</div>
<div class="article_video_box">
	 <iframe width="100%" height="348" src="https://www.youtube.com/embed/7qnrZpLPRfg" frameborder="0" allowfullscreen></iframe>
</div>
<br>
<h2>Как проехать</h2>
<div class="article_lig_box_page clearfix" id="wrap" style="text-align: center; margin-bottom: 2em;">
    <a class="gallery" href="/upload/1_610_0_0.jpg"> <img width="283" alt="single_image" src="/bitrix/templates/akk/images/light_box1.jpg" height="188"></a>
	<a class="gallery" href="/upload/2_610_0_0.jpg"> <img width="283" alt="single_image" src="/bitrix/templates/akk/images/light_box2.jpg" height="188"></a>
	<a class="gallery" href="/upload/3_610_0_0.jpg"> <img width="283" alt="single_image" src="/bitrix/templates/akk/images/light_box3.jpg" height="188"></a>
	<a class="gallery" href="/upload/4_610_0_0.jpg"> <img width="283" alt="single_image" src="/bitrix/templates/akk/images/light_box4.jpg" height="188"></a>
	<a class="gallery" href="/upload/5_610_0_0.jpg"> <img width="283" alt="single_image" src="/bitrix/templates/akk/images/light_box5.jpg" height="188"></a>
	<a class="gallery" href="/upload/6_610_0_0.jpg"> <img width="283" alt="single_image" src="/bitrix/templates/akk/images/light_box6.jpg" height="188"></a>
</div>
<h3 class="fs_24 black reg mb30 black_font">Схема проезда</h3>
<script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=aEKwqHJ7PZFFDWX78X5oib9YXMfGkEMg&width=100%&height=450&lang=ru_RU&sourceType=constructor"></script>
<br>
<?$APPLICATION->IncludeComponent(
	"irbis:form-contact",
	"contact_mobile",
	Array(
	)
);?>

<div itemscope="" itemtype="http://schema.org/Organization" style="display: none;">
	<span itemprop="name">Купить аккумулятор</span>
	<div itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
		Main address: <span itemprop="streetAddress">1-й Грайвороновский пр., д. 4</span> <span itemprop="addressLocality">Москва</span>
	,
	</div>
	Tel:<span itemprop="telephone">+7 (499) 650-52-09</span>, Tel:<span itemprop="telephone">+7 (499) 650-55-55</span>,
	E-mail: <span itemprop="email"><a href="mailto:info@kupit-akkumulyator.ru">info@kupit-akkumulyator.ru</a></span>
</div>
<br>
