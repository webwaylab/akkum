<div class="storage__description">
    <div class="block-title">В наличии в магазине:</div>
    <div class="storage-list">
        <div class="storage-item">
            <span class="address-accent"><span class="icon-subway"></span> Текстильщики</span>, 1-й Грайвороновский пр-д, д. 4
        </div>
    </div>
    <ul class="delivery-list">
        <li class="delivery-item">
            <span class="delivery-name">Стандартная доставка:</span>
            <span class="delivery-price">300 руб. в пределах МКАД</span>
        </li>
        <li class="delivery-item">
            <span class="delivery-name">Экспресс-доставка:</span>
            <span class="delivery-price">700 руб. в пределах МКАД</span>
        </li>
        <li class="delivery-item">
            <span class="delivery-name">За МКАД:</span>
            <span class="delivery-price">+25 руб./км</span>
        </li>
    </ul>
    <div class="block-title">Не можете определиться с выбором?</div>
    <p>
        У нас большой ассортимент с гарантией лучшей цены. Наши квалифицированные специалисты Вам помогут!
    </p>
    <div class="block-contacts">
        <div>
            <p class="phone"><a href="tel:+74996505555">+7 (499)<span> 650-55-55</span></a></p>
        </div>
        <div>
            <button href="javascript:" class="button big_callback_btn d_ib" data-flying-hint="#form-callback">Заказать звонок</button>
        </div>
    </div>
</div>

<div class="storage__map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2247.22833190998!2d37.725333316232344!3d55.71978398054456!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x414ab53db1aba451%3A0xd92ea1997e4ae69c!2zMS3QuSDQk9GA0LDQudCy0L7RgNC-0L3QvtCy0YHQutC40Lkg0L_RgC3QtCwgNCwg0JzQvtGB0LrQstCwLCAxMDk1MTg!5e0!3m2!1suk!2sru!4v1489654153610"
            width="700" height="330" frameborder="0" style="border:0"></iframe>
</div>