<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION, $USER;
$aMenuNativeExt = array();

$aMenuNativeExt = Array(
    Array(
		"Корзина",
		SITE_DIR."personal/cart/",
		Array(),
		Array(),
		""
	),
	Array(
		"Ваш профиль",
		SITE_DIR."personal/profile/",
		Array(),
		Array(),
		""
	),
	Array(
		"Мои заказы",
		SITE_DIR."personal/order/",
		Array(),
		Array(),
		""
	),
);

if($USER->IsAuthorized())
{
    $aMenuNativeExt[] = Array(
		"Выйти",
		SITE_DIR."/?logout=yes",
		Array(),
		Array(),
		""
	);
}

$aMenuLinks = array_merge($aMenuLinks, $aMenuNativeExt);
?>