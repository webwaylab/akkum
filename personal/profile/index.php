<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Настройки пользователя");
?>

<div class="clearfix">
    <div class="grid_37 prefix_2">
    
        <h1 class="black fs_24 reg mb16">Личный кабинет</h1>
        
        <?$APPLICATION->IncludeComponent('bitrix:menu', "personal", array(
    		"ROOT_MENU_TYPE" => "personal",
    		"MENU_CACHE_TYPE" => "Y",
    		"MENU_CACHE_TIME" => "36000000",
    		"MENU_CACHE_USE_GROUPS" => "Y",
    		"MENU_CACHE_GET_VARS" => array(),
    		"MAX_LEVEL" => "1",
    		"USE_EXT" => "Y",
    		"ALLOW_MULTI_SELECT" => "N"
    		)
    	);?>
    
        <?$APPLICATION->IncludeComponent("bitrix:main.profile", "eshop_adapt", Array(
        	"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
        	),
        	false
        );?>
    </div>  
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>