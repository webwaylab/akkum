<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Корзина");
$mobile = defined('MOBILE') ? 'basket_mobile' : '' ?>
<? if (empty($mobile)): ?>
    <div class="clearfix">
        <div class="grid_37 prefix_2">

            <h1 class="black fs_24 reg mb16">Личный кабинет</h1>

            <?$APPLICATION->IncludeComponent('bitrix:menu', "personal", array(
                "ROOT_MENU_TYPE" => "personal",
                "MENU_CACHE_TYPE" => "Y",
                "MENU_CACHE_TIME" => "36000000",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "MENU_CACHE_GET_VARS" => array(),
                "MAX_LEVEL" => "1",
                "USE_EXT" => "Y",
                "ALLOW_MULTI_SELECT" => "N"
                )
            );?>
<? endif; ?>

    <?$APPLICATION->IncludeComponent("irbis:sale.basket", $mobile, array())?>

<? if (empty($mobile)): ?>
            <?/*$APPLICATION->IncludeComponent(
                "bitrix:sale.basket.basket",
                "basket",
                array(
                    "COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
                    "COLUMNS_LIST" => array(
                        0 => "NAME",
                        1 => "DELETE",
                        2 => "QUANTITY",
                        3 => "SUM",
                    ),
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "PATH_TO_ORDER" => "/personal/order/make/",
                    "HIDE_COUPON" => "Y",
                    "QUANTITY_FLOAT" => "N",
                    "PRICE_VAT_SHOW_VALUE" => "N",
                    "SET_TITLE" => "Y",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "OFFERS_PROPS" => array(
                        0 => "SIZES_SHOES",
                        1 => "SIZES_CLOTHES",
                        2 => "COLOR_REF",
                    ),
                    "USE_PREPAYMENT" => "N",
                    "ACTION_VARIABLE" => "action"
                ),
                false
            );*/?>
        </div>
    </div>
<? endif; ?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>