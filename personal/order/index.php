<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Заказы");
?>
<div class="clearfix">
    <div class="grid_37 prefix_2">
    
        <h1 class="black fs_24 reg mb16">Личный кабинет</h1>
        
        <?$APPLICATION->IncludeComponent('bitrix:menu', "personal", array(
    		"ROOT_MENU_TYPE" => "personal",
    		"MENU_CACHE_TYPE" => "Y",
    		"MENU_CACHE_TIME" => "36000000",
    		"MENU_CACHE_USE_GROUPS" => "Y",
    		"MENU_CACHE_GET_VARS" => array(),
    		"MAX_LEVEL" => "1",
    		"USE_EXT" => "Y",
    		"ALLOW_MULTI_SELECT" => "N"
    		)
    	);?>
        <?$APPLICATION->IncludeComponent("bitrix:sale.personal.order", "", array(
        	"SEF_MODE" => "Y",
        	"SEF_FOLDER" => "/personal/order/",
        	"ORDERS_PER_PAGE" => "10",
        	"PATH_TO_PAYMENT" => "/personal/order/payment/",
        	"PATH_TO_BASKET" => "/personal/cart/",
        	"SET_TITLE" => "Y",
        	"SAVE_IN_SESSION" => "N",
        	"NAV_TEMPLATE" => "arrows",
        	"SEF_URL_TEMPLATES" => array(
        		"list" => "index.php",
        		"detail" => "detail/#ID#/",
        		"cancel" => "cancel/#ID#/",
        	),
        	"SHOW_ACCOUNT_NUMBER" => "Y"
        	),
        	false
        );?>
        
     </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>